<?php
$hb_title = "Secure Your Site Today";
$hb_phone = "(833) 686-5387";
$hb_phone_link = "8336865387";
$hb_btn = 'GET STARTED';
$hb_target = '';
include 'includes/forms/high-barrier.html';
?>
<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Martech Plans & Pricing</title>
  <link rel="canonical" href="https://www.sitelock.com/ap/affiliate-plans" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="../css/fontawesome-all.min.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/style.css" rel="stylesheet">
  <link href="../css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
</head>

<body>
<!-- HEADER -->
<div id="mainBody"><div id="content">
<br>
<img class="logoNavFringe" src="../img/logos/SiteLock_red.svg" alt="sitelock logo">
<br>
</div></div>

<!--BANNER -->
<div class="text-center sourceBlack" id="mainBlue"><div id="mainBody">
<img class="img90" src="img/ap/martech-logo.png" alt="MarTech Logo" style="max-width: 200px !important;">
<h1 class="font35">Protect Your Website & Visitors<br>Get Automated Website Security From SiteLock</h1>
</div></div>

<!-- PRICE BOXES -->
<div class="whiteSpace25"></div>
<div id="mainBody"><div id="content"><div class="row">

<div class="col-lg-4">
<div class="apPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<h3 class="sourceBlack">Secure<span class="red">Starter</span></h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$30</h1>
<strong>per month</strong><br>
<p><a class="btn btn-red btn-sm" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Contact Us</a></p>
<ul class="text-left font14 ulClean">
<li><i class="far fa-check"></i> SMART Scanner</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Automatic malware detection<br>&nbsp;&nbsp;and removal</li>
<li><i class="far fa-check"></i> Pro WAF </li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Blocks bad bots used<br>&nbsp;&nbsp;to hack websites</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Increases website speed</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Supports SSL</li>
</ul>
</div>
</div>
</div>

<div class="col-lg-4">
<div class="apPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<h3 class="sourceBlack">Secure<span class="red">Speed</span></h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$50</h1>
<strong>per month</strong><br>
<p><a class="btn btn-red btn-sm" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Contact Us</a></p>
<ul class="text-left font14 ulClean">
<li><i class="fas fa-check"></i> SMART Scanner</li>
<li><i class="fas fa-check"></i> Emergency Hack Repair</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Manual malware removal</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Blacklist & suspension resolution</li>
<li><i class="fas fa-check"></i> Premium WAF</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Everything in Pro WAF</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Blocks website database attacks</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Customizable traffic filtering</li>
</ul>
</div>
</div>
</div>

<div class="col-lg-4">
<div class="apPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<h3 class="sourceBlack">Secure<span class="red">Site</span></h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$70</h1>
<strong>per month</strong><br>
<p><a class="btn btn-red btn-sm" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Contact Us</a></p>
<ul class="text-left font14 ulClean"> 
<li><i class="fas fa-check"></i> Infinity Scanner</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> All Smart Scanner features</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Automatic patching and updating<br>&nbsp;&nbsp;of WordPress, Joomla and Drupal</li>
<li>&nbsp;&nbsp;<i class="fas fa-square fa-xs"></i> Detection of website<br>&nbsp;&nbsp;database infections</li>
<li><i class="fas fa-check"></i> Unlimited Hack Repair</li>
<li><i class="fas fa-check"></i> Premium WAF</li>
</ul>
</div>
</div>
</div>




</div>
<p class="text-center hideme"><a class="btn btn-blue" href="#compare">Compare Plans</a></p>
</div></div>

<!-- SOLUTIONS -->
<?php include('includes/solutions-bar.php');?>

<!-- AWARDS -->
<?php include('includes/awards-bar.php');?>


<!-- FEATURES-->
<div class="text-center sourceBlack" id="mainRed"><div id="mainBody">
<h2 class="font25">Features You are Going to Love</h2>
</div></div>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>  
<div class="row sourceBlack text-center">

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/mar1.png" alt="Icon Fix">ACCURATE<br>DETECTION
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/mar2.png" alt="Icon Fix">DETAILED<br>REPORTS
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/mar3.png" alt="Icon Fix">WORKS<br>INSTANTLY
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/mar4.png" alt="Icon Fix">BOOST CUSTOMER<br>TRUST
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/mar5.png" alt="Icon Fix">INCREASE SITE<br>SPEED
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/mar6.png" alt="Icon Fix">24/7<br>SUPPORT
</div>
<div class="whiteSpace50"></div>  
</div></div></div>

<!-- COMPARE PACKAGES -->
<div id="compare" class="hideme"></div>
<div class="text-center sourceBlack hideme" id="mainRed"><div id="mainBody">
<h2 class="font25">Compare Packages</h2>
</div></div>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div> 

<span class="hideme">
<a id="collapsebtn1" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1"><span class="float-left">Reputation Management</span> <span class="float-right"><i id="collapseone" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse1">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Business Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Phone Number Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Search Engine Blacklist Monitoring</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Spam Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">SSL Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Domain Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>

<a id="collapsebtn2" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2"><span class="float-left">Trust Seal</span> <span class="float-right"><i id="collapsetwo" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse2">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Malware-Free Trust Seal</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">SECURE Trust Seal</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">1-Click Install of SiteLock Trust Seal</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>

<a id="collapsebtn3" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3"><span class="float-left">Comprehensive Malware Scanning</span> <span class="float-right"><i id="collapsethree" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse3">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Daily Malware Scan</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Automatic Malware Removal</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Daily FTP Scanning</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">File Change Monitoring</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>

<a id="collapsebtn4" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse4" role="button" aria-expanded="false" aria-controls="collapse4"><span class="float-left">Multi-level Vulnerability Scanning</span> <span class="float-right"><i id="collapsefour" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse4">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Network Scan</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Website Application Scan</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">SQL Injection Scan</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Cross Site Scripting (XSS)</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>

<a id="collapsebtn5" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse5" role="button" aria-expanded="false" aria-controls="collapse5"><span class="float-left">TrueShield Web Application Firewall</span> <span class="float-right"><i id="collapsefive" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse5">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Block Automated Attacks from Bad Bots</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Ensure Search Engine Access</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Comment Spam Elimination</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">CAPTCHA Security Check</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Block Bots Attempting to “Scrape” or Steal Content</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Blacklisting of Clients, Countries and IPs</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">SQL Injection Prevention</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Cross Site Scripting Prevention</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Illegal Resource Access Prevention</div> <div class="col-md-2 text-center"></i></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Remote File Include (RFI) Prevention</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">OWASP Top 10 Threats Protection</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Fine-Grained Settings for Exceptions</div> <div class="col-md-2 text-center"></i></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>

</div></div>
</div>

<a id="collapsebtn6" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse6" role="button" aria-expanded="false" aria-controls="collapse6"><span class="float-left">TrueSpeed Acceleration</span> <span class="float-right"><i id="collapsesix" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse6">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Unlimited Bandwidth</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Global CDN</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Caching of Static Content</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Content Minification</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Website Traffic Stats</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Support for SSL Websites</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Image Optimization</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Advanced Dynamic Content Caching</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Dynamic Content Compression</div> <div class="col-md-2 text-center"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>
<div class="whiteSpace50"></div>  
</div></div>
</span>

<div class="text-center sourceBlack" id="mainBlue"><div id="mainBody">
<h2 class="font25">Not Sure What You Need?</h2>
<h3 class="font18 sourceRegular whiteLink">Contact us today at <a href="tel:<?php echo $hb_phone_link;?>"><?php echo $hb_phone;?></a><br>
or complete our form and we will contact you for a free website security consultation.</h3>
<br>
<a class="btn btn-ghost-white" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">GET A FREE CONSULTATION</a>

</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>