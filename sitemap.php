<?php
//DEFINITIONS
$title = "SiteLock SiteMap";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";

//DYNAMIC SITEMAP GENERATION
$dir = ($_SERVER['DOCUMENT_ROOT']);
$files1 = scandir($dir);
$files1 = json_encode($files1);
$files1 = str_replace('.php', "", $files1);
$files1 = str_replace('".htaccess",', "", $files1);
$files1 = str_replace('"css",', "", $files1);
$files1 = str_replace('"download",', "", $files1);
$files1 = str_replace('"landers",', "", $files1);
$files1 = str_replace('"font",', "", $files1);
$files1 = str_replace('"includes",', "", $files1);
$files1 = str_replace('"js",', "", $files1);
$files1 = str_replace('"favi.ico",', "", $files1);
$files1 = str_replace('"img",', "", $files1);
$files1 = str_replace('"404",', "", $files1);
$files1 = str_replace('"ap",', "", $files1);
$files1 = str_replace('"pr",', "", $files1);
$files1 = str_replace('"news",', "", $files1);
$files1 = str_replace('"template",', "", $files1);
$files1 = str_replace('".",', "", $files1);
$files1 = str_replace('"..",', "", $files1);
$files1 = json_decode($files1);
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div class="text-center" id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<h1 class="sourceBlack">SiteLock Sitemap</h1>
<h3>Looking for something on SiteLock.com? This Sitemap should help guide the way.</h3>
<div class="whiteSpace50"></div>
</div></div></div>

<div class="linkRed" id="mainBody">
<div class="whiteSpace50"></div>


<p class="font-weight-bold">Pages are listed in alphabetical order:</p>
<div class="row">
<?php foreach($files1 as $child) { $child2 = str_replace('-', " ", $child); $child2 = str_replace('index', "home page", $child); echo '<div class="col-md-3"><a class="b" href="'.$child.'">'.$child2.'</a></div>';} ?>
</div>
</div>


<div class="whiteSpace50"></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>