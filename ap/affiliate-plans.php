<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Affliate Plans & Pricing</title>
  <link rel="canonical" href="https://www.sitelock.com/ap/affiliate-plans" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="../css/fontawesome-all.min.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/style.css" rel="stylesheet">
  <link href="../css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
</head>

<body>
<!-- HEADER -->
<div id="mainBody"><div id="content">
<br>
<img class="logoNavFringe" src="../img/logos/SiteLock_red.svg" alt="sitelock logo">
<br>
</div></div>

<!--BANNER -->
<div class="text-center sourceBlack" id="mainBlue"><div id="mainBody">
<h1 class="font55">SiteLock Website Security Solutions</h1>
</div></div>

<!-- PRICE BOXES -->
<div class="whiteSpace25"></div>
<div id="mainBody"><div id="content"><div class="row">

<div class="col-lg-3">
<div class="apPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<img class="ap_icon" src="../img/ap/icon_fix.svg" alt="Icon Fix">
<h3 class="sourceBlack">Fix</h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$109.99</h1>
<strong>per year</strong><br>
<form method="post" action="https://secure.sitelock.com/checkout/new/sl_affiliate">
<input id="marketing_price" name="marketing_price" type="hidden" value="$109.99/year" />
<input id="marketing_description" name="marketing_description" type="hidden" value="fixPlan" />
<input type="hidden" name="selected_plan" id="selectedPlan" value="3826" />
<input type="hidden" name="selected_waf" id="selectedWAF" value="" />
<input type="hidden" name="selected_911" id="selected911" value= "" />
<input type="hidden" name="offer_id" id="offerID" value="1" />
<button class="btn btn-red btn-sm">Choose This Plan</button>
</form>
<ul class="text-left font14">
<li>Automatic Malware Removal</li>
<li>Daily Vulnerability Scans</li>
<li>Daily Malware Scans</li>
<li>Verifiable Trust Seal</li>
<li>Reputation Monitoring</li>
<li>File Change Monitoring</li>
<li>Spam & Blacklist Monitoring</li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="apPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<img class="ap_icon" src="../img/ap/icon_accelerate.svg" alt="Icon Fix">
<h3 class="sourceBlack">Accelerate</h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$299.99</h1>
<strong>per year</strong><br>
<form method="post" action="https://secure.sitelock.com/checkout/new/sl_affiliate">
<input id="marketing_price" name="marketing_price" type="hidden" value="$299.99/year" />
<input id="marketing_description" name="marketing_description" type="hidden" value="acceleratePlan" />
<input type="hidden" name="selected_plan" id="selectedPlan" value="3826" />
<input type="hidden" name="selected_waf" id="selectedWAF" value="10189" />
<input type="hidden" name="selected_911" id="selected911" value= "" />
<input type="hidden" name="offer_id" id="offerID" value="2" />
<button class="btn btn-red btn-sm">Choose This Plan</button>
</form>
<strong>Fix Features, Plus:</strong>
<ul class="text-left font14">
<li>TrueSpeed CDN:
    <br>-WebsiteAcceleration
    <br>-static & Dynamic Content Caching
    <br>-Content Minification</li>
<li>TrueShield WAF:
    <br>-Block Bad Bots
    <br>-SSL Support</li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="apPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<img class="ap_icon" src="../img/ap/icon_prevent.svg" alt="Icon Fix">
<h3 class="sourceBlack">Prevent</h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$499.99</h1>
<strong>per year</strong><br>
<form method="post" action="https://secure.sitelock.com/checkout/new/sl_affiliate">
<input id="marketing_price" name="marketing_price" type="hidden" value="$499.99/year" />
<input id="marketing_description" name="marketing_description" type="hidden" value="preventPlan" />
<input type="hidden" name="selected_plan" id="selectedPlan" value="3826" />
<input type="hidden" name="selected_waf" id="selectedWAF" value="3834" />
<input type="hidden" name="selected_911" id="selected911" value= "" />
<input type="hidden" name="offer_id" id="offerID" value="3" />
<button class="btn btn-red btn-sm">Choose This Plan</button>
</form>
<strong>Accelerate Features, Plus:</strong>
<ul class="text-left font14"> 
<li>TrueSpeed CDN</li>
<li>TrueShield WAF
<br>-Block All OWASP Top 10 Threats, Including SQLi and XSS</li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="apPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<img class="ap_icon" src="../img/ap/icon_911.svg" alt="Icon Fix">
<h3 class="sourceBlack red">911</h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$219.99</h1>
<strong>One Time</strong><br>
<form method="post" action="https://secure.sitelock.com/checkout/new/sl_affiliate">
<input id="marketing_price" name="marketing_price" type="hidden" value="" />
<input id="marketing_description" name="marketing_description" type="hidden" value="plan911" />
<input type="hidden" name="selected_plan" id="selectedPlan" value="" />
<input type="hidden" name="selected_waf" id="selectedWAF" value="" />
<input type="hidden" name="selected_911" id="selected911" value= "10186" />
<input type="hidden" name="offer_id" id="offerID" value="4" />
<button class="btn btn-red btn-sm">Choose This Plan</button>
</form>
<strong class="red">ALREADY COMPROMISED?</strong>
<ul class="text-left font14"> 
<li>One Time Clean</li>
<li>Manual Intervention</li>
<li>Restore Compromised Site</li>
<li>Clean Website Files</li>
</ul>
</div>
</div>
</div>


</div></div></div>

<!-- STANDARD FEATURES -->
<div class="whiteSpace50"></div>
<div class="text-center sourceBlack" id="mainRed"><div id="mainBody">
<h2 class="font25">Standard Features in Every Package</h2>
</div></div>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>  
<div class="row sourceBlack text-center">

<div class="col-md-1"></div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_app.svg" alt="Icon Fix">Daily Malware Scans
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_findMalware.svg" alt="Icon Fix">Automatic Malware Removal
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_ftp.svg" alt="Icon Fix">Daily Vulnerability Scans
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_trustSeal.svg" alt="Icon Fix">SiteLock Trust Seal
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_multilingual.svg" alt="Icon Fix">Multilingual Dashboard
</div>

<div class="col-md-1"></div>

</div></div></div>

<!-- COMPARE PACKAGES -->
<div class="whiteSpace50 hideme"></div>
<div class="text-center sourceBlack hideme" id="mainRed"><div id="mainBody">
<h2 class="font25">Compare Packages</h2>
</div></div>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div> 

<span class="hideme">
<a id="collapsebtn1" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1"><span class="float-left">Reputation Management</span> <span class="float-right"><i id="collapseone" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse1">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Business Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Phone Number Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Search Engine Blacklist Monitoring</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Spam Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">SSL Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Domain Verification</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>

<a id="collapsebtn2" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2"><span class="float-left">Trust Seal</span> <span class="float-right"><i id="collapsetwo" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse2">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Malware-Free Trust Seal</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">SECURE Trust Seal</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">1-Click Install of SiteLock Trust Seal</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>

<a id="collapsebtn3" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3"><span class="float-left">Comprehensive Malware Scanning</span> <span class="float-right"><i id="collapsethree" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse3">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Daily Malware Scan</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Automatic Malware Removal</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Daily FTP Scanning</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">File Change Monitoring</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>

<a id="collapsebtn4" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse4" role="button" aria-expanded="false" aria-controls="collapse4"><span class="float-left">Multi-level Vulnerability Scanning</span> <span class="float-right"><i id="collapsefour" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse4">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Network Scan</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Website Application Scan</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">SQL Injection Scan</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Cross Site Scripting (XSS)</div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>

<a id="collapsebtn5" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse5" role="button" aria-expanded="false" aria-controls="collapse5"><span class="float-left">TrueShield Web Application Firewall</span> <span class="float-right"><i id="collapsefive" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse5">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Block Automated Attacks from Bad Bots</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Ensure Search Engine Access</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Comment Spam Elimination</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">CAPTCHA Security Check</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Block Bots Attempting to “Scrape” or Steal Content</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Blacklisting of Clients, Countries and IPs</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">SQL Injection Prevention</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Cross Site Scripting Prevention</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Illegal Resource Access Prevention</div> <div class="col-md-2 text-center"></i></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Remote File Include (RFI) Prevention</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">OWASP Top 10 Threats Protection</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Fine-Grained Settings for Exceptions</div> <div class="col-md-2 text-center"></i></div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>

</div></div>
</div>

<a id="collapsebtn6" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse6" role="button" aria-expanded="false" aria-controls="collapse6"><span class="float-left">TrueSpeed Acceleration</span> <span class="float-right"><i id="collapsesix" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse6">
<br>
<div id="content"><div class="row">
<div class="col-md-6 ap_drop_header">FEATURE</div> <div class="col-md-2 text-center ap_drop_header">FIX</div> <div class="col-md-2 text-center ap_drop_header">ACCELERATE</div> <div class="col-md-2 text-center ap_drop_header">PREVENT</div>
<div class="col-md-6">Unlimited Bandwidth</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Global CDN</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Caching of Static Content</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Content Minification</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Website Traffic Stats</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Support for SSL Websites</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Image Optimization</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Advanced Dynamic Content Caching</div> <div class="col-md-2 text-center"></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
<div class="col-md-6">Dynamic Content Compression</div> <div class="col-md-2 text-center"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div> <div class="col-md-2 text-center"><i class="far fa-check"></i></div>
</div></div>
</div>

</div></div>
</span>

<!-- WEBSITE SCANNING -->
<div class="whiteSpace50 hideme"></div>
<div class="text-center sourceBlack" id="mainRed"><div id="mainBody">
<h2 class="font25">SiteLock Website Scanning</h2>
</div></div>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div> 
<p class="font18">SiteLock SMART&trade; connects to your files using FTP logins, and monitors when files are added, modified, deleted, or if any malicious scripts are injected within those files. If malware is identified, you are alerted and known malware is removed automatically, giving you the peace of mind you deserve.</p>

<div class="row sourceBlack text-center red">


<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_app.svg" alt="Icon Fix">Website Malware Scan
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_malware.svg" alt="Icon Fix">Website Application Scan
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_spam.svg" alt="Icon Fix">Domain Spam Scan
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_sqli.svg" alt="Icon Fix">SQL Injection (SQLi) Vulnerability Scan
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_xss.svg" alt="Icon Fix">Cross-Site Scripting (XSS) Vulnerability Scan
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/icon_network.svg" alt="Icon Fix">Network Scan
</div>

</div>
</div></div>

<!-- TRUESPEED CDN -->
<div class="whiteSpace50"></div>
<div class="text-center sourceBlack" id="mainRed"><div id="mainBody">
<h2 class="font25">SiteLock&reg; TrueSpeed&trade; CDN</h2>
</div></div>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div> 
<p class="font18">Our SSL-friendly content delivery network (CDN) increases your website speed through its network of data centers across six continents. TrueSpeed employs content caching strategies to further accelerate load time and help boost website performance. No matter where your customers are located, SiteLock TrueSpeed ensures they can access your website in milliseconds.</p>

<img class="img70" src="../img/ddos/ddosMap.png" alt="DDOS Map">

<div class="row">

<div class="col-md-4 text-center">
<h4 class="red sourceBlack">North America</h4>
<ul class="text-left">
<li>Atlanta, Georgia, USA</li>
<li>Ashburn, Virginia, USA</li>
<li>Chicago, Illinois, USA</li>
<li>Dallas, Texas, USA</li>
<li>Los Angeles, California, USA</li>
<li>Miami, Florida, USA</li>
<li>New York, New York, USA</li>
<li>San Jose, California, USA</li>
<li>Seattle, Washington, USA</li>
<li>Toronto, Ontario, Canada</li>
</ul>
</div>

<div class="col-md-4 text-center">
<h4 class="red sourceBlack">Europe</h4>
<ul class="text-left">
<li>Amsterdam, Netherlands</li>
<li>Frankfurt, Germany</li>
<li>London, England, UK</li>
<li>Madrid, Spain</li>
<li>Paris, France</li>
<li>Stockholm, Sweden</li>
<li>Vienna, Austria</li>
<li>Warsaw, Poland</li>
<li>Zurich, Switzerland</li>
</ul>
</div>


<div class="col-md-4 text-center">
<h4 class="red sourceBlack">Asia</h4>
<ul class="text-left">
<li>Hong Kong, China</li>
<li>Singapore, Singapore</li>
<li>Tokyo, Japan</li>
</ul>
</div>

<div class="col-md-4 text-center">
<h4 class="red sourceBlack">Oceania</h4>
<ul class="text-left">
<li>Auckland, New Zealand</li>
<li>Sydney, Australia</li>
</ul>
</div>

<div class="col-md-4 text-center">
<h4 class="red sourceBlack">South America</h4>
<ul class="text-left">
<li>Sao Paulo, Brazil</li>
</ul>
</div>

<div class="col-md-4 text-center">
<h4 class="red sourceBlack">Middle East</h4>
<ul class="text-left">
<li>Tel Aviv, Israel</li>
</ul>
</div>


</div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
