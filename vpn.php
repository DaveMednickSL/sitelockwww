<?php
//DEFINITIONS
$title = "Securely Browse the Internet without Limits";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="vpn-lander";
$keywords = "Setup VPN,VPN Speed,Buy VPN,Remote Access VPN,VPN Remote Desktop,Purchase VPN,VPN Search Engine";
$pagephone = "(833) 263-8630";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
    <!DOCTYPE html5>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>
            <?php echo $title;?>
        </title>
        <link rel="canonical" href="https://www.sitelock.com/<?php echo $canonical;?>" />
        <meta name="description" content="<?php echo $description;?>">
        <meta name="keywords" content="<?php echo $keywords;?>" />
        <link rel="icon" type="image/ico" href="favi.ico">
        <!-- Bootstrap -->
        <link href="landers/css/fontawesome-all.min.css" rel="stylesheet">
        <link href="landers/css/bootstrap.min.css" rel="stylesheet">
        <link href="landers/css/style.css?v=1.1.0" rel="stylesheet">

        <body id="vpn-lander">

            <!-- NAVIGATION -->
            <div id="menu">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 text-left"><a href="/"><img class="logoNav" src="landers/img/logos/SiteLock_red.svg" alt="sitelock logo"></a></div>
                        <div class="col-md-6 text-right">
                            <h3 class="sourceBlack"><?php echo $pagephone;?></h3></div>
                    </div>
                </div>
            </div>
            <!--Banner Section-->
            <section class="p-5 blue-bg-grad banner-border">
                <div class="container white text-left">
                    <div class="row">

                        <div class="col-md-7 col-sm-12 text-left">
                            <h1 class="sourceLight font55">Purchase SiteLock VPN and <span class="sourceBlack">Browse Securely</span></h1>
                            <p class="text-left no-margin sourceRegular font25">Get unlimited access to an Internet free of privacy concerns, content restrictions or data caps.</p>
                            <br><br>
                            <a href="#plans" class="btn btn-red font-weight-bold">Explore Plans</a>
                        </div>

                        <div class="col-md-4 col-sm-12 d-none d-md-block">
                            <img src="landers/img/mobile.png" width="280" class="img-responsive banner-mobile">
                        </div>

                    </div>
                </div>
            </section>

<!--What is a VPN? Section-->
<br><br><br><br>
<section class="p-5">

<div class="container ">
    <div class="row">
        <br>
        <div class="col-md-8 col-xs-12 sp-bg">
            <br>
            <br>
            <br>
            <div class="white-bg padding-around-30">
                <h1 class="sourceLight">What is a <span class="sourceBlack">VPN?</span></h1>
                <p class="grey padding-left25">We conduct more personal and professional business in public places than ever before. Unfortunately, anyone else on the same public network as you can easily see the sites you access and information you transmit. A VPN (virtual private network) allows you to initiate anonymous, encrypted browsing sessions over any network connection. You can conduct business with confidence over a VPN, assured that you are protected from prying eyes on public networks.</p>
                <a href="#HighBarrierForm" class="btn btn-red font-weight-bold" data-toggle="modal" data-target="#HighBarrierForm">Get Started Now</a>
            </div>
            <br>
            <br>
        </div>
        <div class="col-md-3 col-xs-12 sp-bg1">
            <img src="landers/img/tab.png" width="290" class="img-responsive padding10 img-tab">
        </div>

    </div>
</div>
</section>

  <!--SiteLock VPN pricing Section-->
  <section class="p-5" id="plans">

<div class="col-md-12 text-center">
    <h1 class="sourceLight font45">SiteLock VPN <span class="sourceBlack">pricing</span></h1>
    <br/>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div id="priceHatWhite">.</div>
            <div id="priceBox">
                <div id="priceGreyHead">
                    <div id="priceBoxContent">
                        <h3 class="sourceRegular">Monthly</h3>
                    </div>
                </div>
                <div class="text-center" id="priceBoxContent">
                    <br>
                    <h1 class="sourceLight ">$9.99 <span class="font18">/license<span></h1>
                    <br>
                    <a href="checkout?planID=13179" class="btn btn-ghost-blue font-weight-bold width-auto">Buy Now</a>
                    <br>
                    <br>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="sourceRegular" id="priceHatRed">
                <div class="arrow_box"></div>BEST VALUE</div>
            <div id="priceBox">
                <div id="priceGreyHead">
                    <div id="priceBoxContent">
                        <h3 class="sourceRegular">2 Year</h3>
                    </div>
                </div>
                <div class="text-center" id="priceBoxContent" style="padding-bottom:5px;">
                    <br>
                    <h1 class="sourceLight ">$95.76 <span class="font18">/license</span></h1>
                    <span class="font14 sourceBlack"><i>Save 60% compared to monthly</i></span>
                    <br>
                    <a href="checkout?planID=13181" class="btn btn-blue font-weight-bold width-auto">Buy Now</a>
                    <br>
                    <br>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div id="priceHatWhite">.</div>
            <div id="priceBox">
                <div id="priceGreyHead">
                    <div id="priceBoxContent">
                        <h3 class="sourceRegular">1 Year</h3>
                    </div>
                </div>
                <div class="text-center" id="priceBoxContent">
                    <br>
                    <h1 class="sourceLight ">$71.88 <span class="font18">/license<span></h1>
                    <span class="font14 sourceBlack"><i>Save 40% compared to monthly</i></span>
                    <br>
                    <a href="checkout?planID=13180" class="btn btn-ghost-blue font-weight-bold width-auto">Buy Now</a>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>

</section>

<div id="mainGrey"><div id="mainBody">
<div class="row">
<div class="col-sm-3">
<img class="img50" src="landers/img/bbbStars.png" alt="BBB">
</div>
<div class="col-sm-3">
<img class="img50" src="landers/img/consumerAffairsStars.png" alt="Consumer Affairs">
</div>
<div class="col-sm-3">
<img class="img50" src="landers/img/googleStars.png" alt="Google">
</div>
<div class="col-sm-3">
<img class="img50" src="landers/img/trustPilotStars.png" alt="Trust Pilot">
</div>
</div>
</div></div>

            <!--second Section-->

            <section class="p-5">

                <div class="container text-center">
                    <div class="row">

                        <div class="col-md-3 col-xs-12">
                            <img src="landers/img/email.png" width="200" class="img-responsive padding10">
                            <br>
                            <h4 class="sourceBlack">Secure Data</h4>
                            <p class="grey">By encrypting your browser session you prevent cybercriminals from gaining access to sensitive data.</p>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <img src="landers/img/globe.png" width="200" class="img-responsive padding10">
                            <br>
                            <h4 class="sourceBlack">Unrestricted Access</h4>
                            <p class="grey">Safely browse while traveling abroad, without worrying about regional restrictions.</p>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <img src="landers/img/clock.png" width="200" class="img-responsive padding10">
                            <br>
                            <h4 class="sourceBlack">Increased Productivity</h4>
                            <p class="grey">Securely connect to public Wi-Fi networks and remotely access company resources.
                            </p>
                        </div>

                        <div class="col-md-3 col-xs-12">
                            <img src="landers/img/settings.png" width="200" class="img-responsive padding10">
                            <br>
                            <h4 class="sourceBlack">Easy to Manage</h4>
                            <p class="grey">SiteLock VPN provides a simple and intuitive user management center and quick deployment.
                            </p>
                        </div>

                    </div>
                </div>
            </section>

            <!--SiteLock VPN benefits & features Section-->
            <section class="p-5 grey-bg3">

                <div class="col-md-12 text-center">
                    <h1 class="sourceLight font45">SiteLock VPN <span class="sourceBlack">benefits & features</span></h1>
                    <br/>
                </div>

                <div class="container">
                    <div class="row">
                        <!-- Boxes de Acoes -->
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <div class="box">
                                <div class="icon">
                                    <div class="image blue-bg-grad"><i class="fas fa-glasses"></i></div>
                                    <div class="info">
                                        <h3 class="title">No activity logs</h3>
                                        <p>
                                            VPN will not track or store your browsing history or reveal what data you’re sending.
                                        </p>
                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <div class="box">
                                <div class="icon">
                                    <div class="image red-bg-grad"><i class="far fa-lock"></i></div>
                                    <div class="info">
                                        <h3 class="title">IP lock</h3>
                                        <p>
                                            With IPLock, you’re assigned a new IP address at random, every time, and you can appear as if you are coming from anywhere in the world.
                                        </p>

                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <div class="box">
                                <div class="icon">
                                    <div class="image blue-bg-grad"><i class="fal fa-shield"></i></div>
                                    <div class="info">
                                        <h3 class="title">DNS leak protection</h3>
                                        <p>
                                            DNS leak protection provides an extra layer of privacy by securing your initial connection to a website, preventing the leakage of valuable information.
                                        </p>

                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>
                        <!-- /Boxes de Acoes -->
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <!-- Boxes de Acoes -->
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <div class="box">
                                <div class="icon">
                                    <div class="image red-bg-grad"><i class="far fa-rocket"></i></div>
                                    <div class="info">
                                        <h3 class="title">High speed network</h3>
                                        <p>
                                            With 1000+ servers in 40+ locations you get access to one of the fastest VPN’s available.
                                        </p>
                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <div class="box">
                                <div class="icon">
                                    <div class="image blue-bg-grad"><i class="far fa-heart"></i></div>
                                    <div class="info">
                                        <h3 class="title">Unlimited usage</h3>
                                        <p>
                                            Connect to any SiteLock VPN server anytime, without data restrictions or download caps.
                                        </p>

                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <div class="box">
                                <div class="icon">
                                    <div class="image red-bg-grad"><i class="far fa-headset"></i></div>
                                    <div class="info">
                                        <h3 class="title">24/7 US-based support</h3>
                                        <p>
                                            Your VPN comes backed by help and support regardless of time of day: call, email, or live chat.24/7/365.
                                        </p>

                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>
                        <!-- /Boxes de Acoes -->
                    </div>

                </div>

                <div class="row">
                    <div class="col-12 text-center">
                        <a href="#plans" class="btn btn-blue">Let’s See Some Pricing</a>
                        <div>
                            <div>
            </section>

          

 <!--What is website security?-->

            <section class="p-5">

<div class="container">
    <div class="row">
        <br>
        <div class="col-md-6 col-xs-12 sp-bg">
            <br>
            <br>
            <div class="white-bg padding-around-30">
                <h1 class="sourceLight">What is <span class="sourceBlack">website <br>security?</span></h1>
                <p class="grey">You have an SSL certificate on your site, so what else do you need? Most people don’t realize that SSL encryption only protects data being transfered between your site and its visitors - it doesn’t protect your website itself. To make sure your site is protected from malware and cyberattacks, you need website security too. </p>
                <p class="grey">At SiteLock, we provide robust, industry-leading anti-malware technology presented in a simple, easy-to-use dashboard that makes securing your site a snap. Partner with us to get some peace of mind while you focus on building your business.

                    <br>
                    <br><span class="blueLink sourceBlack"><a href="https://www.sitelock.com/pricing">Click here to protect your website</a></span></p>

            </div>
            <br>
            <br>
        </div>
        <div class="col-md-6 col-xs-12 sp-bg1 text-center mx-auto">
            <img src="landers/img/web.png" width="650" class="img-responsive padding10 img-web">
        </div>

    </div>
</div>
</section>

            <!-- FOOTER -->
            <div class="footer">
                <div id="content" style="padding-top: 10px;">

                    <hr class="footerHR">

                    <div class="row">
                        <div class="col-md-4" style="border-right: 1px solid #707070;">
                            <img class="logoFooter" src="landers/img/logos/SiteLock_white.svg" alt="sitelock logo">
                        </div>

                        <div class="col-md-4 font18">
                            <h3 class="sourceRegular">Contact</h3>
                            <p>U.S. <u>855.378.6200</u></p>
                            <p>Int'l <u>415.390.2500</u></p>
                            <!-- <p>Email <u>Support@SiteLock.com</u></p> -->
                        </div>

                        <div class="col-md-4 footerCol footconnect">
                            <h3 class="sourceRegular">Connect</h3>
                            <p>Sign up for SiteLock news & announcements</p>
                            <div class="form-inline footerSpace">
                                <form method="post" action="https://a29565.actonservice.com/acton/eform/29565/01b7e374-ab17-4ff6-a115-3f1742ab84eb/d-ext-0001">
                                    <input type="hidden" name="AcceptsEmail" value="Yes">
                                    <input class="form-control footerInput" type="email" name="Email" placeholder="you@email.com">
                                    <button style=" margin:0;" type="submit" class="btn-f btn-foot"><i class="far fa-arrow-right"></i></button>
                                </form>
                            </div>

                        </div>
                    </div>

                    <hr class="footerHR">

                    <p class="text-center footCopy linkblue">Copyright &copy; SiteLock
                        <script>
                            document.write(new Date().getFullYear())
                        </script> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a>
                        <p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

                </div>
            </div>

            <script src="landers/js/jquery-3.3.1.min.js"></script>
            <script src="landers/js/bootstrap.min.js"></script>
            <script src="landers/js/bootstrap.bundle.min.js"></script>
            <script src="landers/js/custom.js?v=1.0.0"></script>

            <script type="text/javascript">
                //FORM VALIDATION
                $("#inputDomain").blur(function() {
                        var e;
                        document.getElementById("inputDomain").checkValidity() ? ((e = document.getElementById("validateDomain")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validateDomain")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                    }),
                    $("#inputEmail").blur(function() {
                        var e;
                        document.getElementById("inputEmail").checkValidity() ? ((e = document.getElementById("validateEmail")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validateEmail")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                    }),
                    $("#inputName").blur(function() {
                        var e;
                        document.getElementById("inputName").checkValidity() ? ((e = document.getElementById("validateName")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validateName")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                    }),
                    $("#inputlastName").blur(function() {
                        var e;
                        document.getElementById("inputlastName").checkValidity() ? ((e = document.getElementById("validatelastName")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validateName")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                    }),
                    $("#inputPhone").blur(function() {
                        var e;
                        document.getElementById("inputPhone").checkValidity() ? ((e = document.getElementById("validatePhone")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validatePhone")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                    });
            </script>
            <script type="text/javascript">
                $(document).on('click', '.modal', function() {
                    $("#HBinputDomain").blur(function() {
                            var e;
                            document.getElementById("HBinputDomain").checkValidity() ? ((e = document.getElementById("validateDomain")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validateDomain")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                        }),
                        $("#HBinputEmail").blur(function() {
                            var e;
                            document.getElementById("HBinputEmail").checkValidity() ? ((e = document.getElementById("validateEmail")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validateEmail")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                        }),
                        $("#HBinputName").blur(function() {
                            var e;
                            document.getElementById("HBinputName").checkValidity() ? ((e = document.getElementById("validateName")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validateName")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                        }),
                        $("#HBinputlastName").blur(function() {
                            var e;
                            document.getElementById("HBinputlastName").checkValidity() ? ((e = document.getElementById("validatelastName")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validatelastName")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                        }),
                        $("#HBinputPhone").blur(function() {
                            var e;
                            document.getElementById("HBinputPhone").checkValidity() ? ((e = document.getElementById("validatePhone")).classList.remove("fa-circle"), e.classList.remove("fa-exclamation-circle"), e.classList.remove("red1"), e.classList.remove("grey1"), e.classList.add("fa-check-circle"), e.classList.add("blue")) : ((e = document.getElementById("validatePhone")).classList.remove("fa-circle"), e.classList.remove("fa-check-circle"), e.classList.remove("blue"), e.classList.remove("grey1"), e.classList.add("fa-exclamation-circle"), e.classList.add("red1"))
                        });
                });
            </script>

            <?php
$hb_title = "Contact us today";
$hb_phone = "833-686-5384";
$call_phone = "8336865384";
$hb_btn = 'Secure My Website';
?>

                <!-- High Barrier -->
                <div class="modal fade" id="HighBarrierForm" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content modalForm modalFix">
                            <div class="modal-header">
                                <h5 class="modal-title sourceBlack padding-left25 padding-top-10" id="exampleModalLabel"><?php echo $hb_title; ?></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i class="fal fa-times-circle" style="user-select: auto;"></i>
                                </button>

                            </div>
                            <div class="modal-body padding25">
                                <p class="redLink padding-left25">Call us anytime at
                                    <a href="tel:<?php echo $call_phone; ?>">
                                        <?php echo $hb_phone; ?>
                                    </a>
                                    <br/> or fill out the form below:</p>
                                <form class="mt-20" method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">

                                    <span class="form-inline form-group"><i id="validateName" class="far fa-circle grey1 fa-lg" title="First Name should be formatted as firstname"></i>&nbsp;<input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder" for="HBinputName">First Name</label>

        </span>
                                    <span class="form-inline form-group"><i id="validatelastName" class="far fa-circle grey1 fa-lg" title="Last Name should be formatted as lastname"></i>&nbsp;<input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder" for="HBinputlastName">Last Name</label>

        </span>

                                    <span class="form-inline form-group"><i id="validateDomain" class="far fa-circle grey1 fa-lg" title="Domain Name should be formatted as domain.com"></i>&nbsp;<input name="Website" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder" for="HBinputDomain">Domain</label>

        </span>

                                    <span class="form-inline form-group"><i id="validateEmail" class="far fa-circle grey1 fa-lg" title="Email should be formatted as you@domain.com"></i>&nbsp;<input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder" for="HBinputEmail" >Email</label>

        </span>

                                    <span class="form-inline form-group"><i id="validatePhone" class="far fa-circle grey1 fa-lg" title="Phone should be formatted as 0001112222"></i>&nbsp;<input name="Phone" id="HBinputPhone" style="width: 90%;" type="phone" minlength=3 class="form-control" required placeholder="Phone Number" pattern="^(?:\(\d{3}\)|\d{3})[- . ]?\d{3}[- . ]?\d{4}$"><label class="form-control-placeholder" for="HBinputPhone">Phone Number</label>

        </span>

                                    <div class="custom-control custom-checkbox" style="font-size: 14px; padding-left:25px !important; width: 100%;">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Email me the latest website security news</label>
                                    </div>
                                    <input type="hidden" name="Form ID" value="<FORM ID>">
                                    <input type="hidden" name="Lead Source" value="Act-On Form">
                                    <input type="hidden" name="Lead Group" value="Organic Web">
                                    <input type="hidden" name="Accepts Email" value="Accepts Email">
                                    <input type="hidden" name="Division" value="Retail">
                                    <input type="hidden" name="Lead Type" value="General">
                                    <input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
                                    <br>
                                    <p class="text-center">
                                        <button type="submit" class="btn btn-primary" style="width:auto !important;">
                                            <?php echo $hb_btn; ?>
                                        </button>
                                        </center>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <p class="form-notice">By submitting this form you confirm that you have read and accepted our <a href="terms" target="_blank">Terms & Conditions</a> and <a href="privacy-policy" target="_blank">Privacy Policy</a></p>
                            </div>
                        </div>
                    </div>
                </div>

        </body>

    </html>