<?php
//DEFINITIONS
$title = "Awards & Honors | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="sourceBlack">SiteLock Awards & Honors</h1>
<h3 class="font25">We’re grateful to be recognized for our continued growth, innovation, & employee culture. We’re even more grateful to our customers who have helped us get to where we are today. Everything we do is for them. Check out the honors & awards they’ve helped us earn over the years.</h3>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-md-3"><a href="https://cybersecurity-excellence-awards.com/2019-cybersecurity-product-awards-winners-and-finalists" title="SiteLock Named to 2018 Inc. 5000 List for Second Consecutive Year"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/WINNER_gold_2019.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://cybersecurity-excellence-awards.com/2019-cybersecurity-company-awards-winners-and-finalists" title="SiteLock Named to 2018 Inc. 5000 List for Second Consecutive Year"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/WINNER_silver_2019.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/pr/siteLock-named-2018-Inc-5000-list-second-consecutive-year" title="SiteLock Named to 2018 Inc. 5000 List for Second Consecutive Year"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/inc50002018.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/06/top-companies-to-work-for-in-arizona-2018/" title="SiteLock&reg; Named Top Place to Work For By AZCentral"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/azcentralTop2018.png" alt="Award"></div></a></div>

<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/03/info-security-products-guide-2018/" title="INFINITY&trade; 2018 Cloud Security Excellence Award Winner"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/cloudComputing2018.jpg" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://cybersecurity-excellence-awards.com/" title="SiteLock Nonimated 2018 Finalist For the Cyber Security Excellence Awards"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/cyberSecurityFinalist2018.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/pr/sitelock-ranked-arizonas-fastest-growing-software-company" title="SiteLock is One of the Fastest Growing Companies in Deloitte Fast 500"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/deloitte2018.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/04/fortress-cyber-security-awards-2018/" title="INFINITY&trade; 2018 Fortress Cyber Security Award"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/fortressAward2018.png" alt="Award"></div></a></div>

<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/02/2018-cybersecurity-excellence-awards/" title="SMART&reg; Wins Silver Cybersurity Excellence Award 2018"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/WINNER_bronze_2018.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/02/2018-cybersecurity-excellence-awards/" title="INFINITY&trade; Wins Gold Cybersurity Excellence Award 2018"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/WINNER_gold_2018.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/02/2018-cybersecurity-excellence-awards/" title="SiteLock&reg; Wins Most Innovative Cybersecurity Company"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/WINNER_silver_2018.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/03/info-security-products-guide-2018/" title="SiteLock&reg; Wins Bronze in Innovative Company of the Year"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/infosecBronze2018.gif" alt="Award"></div></a></div>

<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/03/info-security-products-guide-2018/" title="INFINITY&trade; Wins Gold in Security Monitoring"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/infosecGold2018.gif" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/03/info-security-products-guide-2018/" title="SMART&reg; Wins Silver in Anti-Malware, Anti-Spam or Anti-Virus"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/infosecSilver2018.gif" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2018/05/healthiest-employer-awards-2018/" title="SiteLock&reg; Name One of Phoenix's Healthiest Places To Work"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/healthiestEmployer2018.jpg" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2017/03/sitelock-best-cool-award-bestcompaniesaz/" title="SiteLock&reg; Wins 2018 Best of Cool Award"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/coolest2018.png" alt="Award"></div></a></div>

<div class="col-md-3"><a href="https://www.bizjournals.com/phoenix/subscriber-only/2017/11/03/ace-private-companies.html" title="SiteLock&reg; Named one of the top 50 places to work for in Phoenix"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/ace2017.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2017/09/infinity-2017-cloud-computing-excellence-award/" title="INFINITY&trade; 2017 Cloud Security Excellence Award Winner"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/cloudComputing2017.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2017/07/sitelock-named-top-companies-in-az/" title="SiteLock is One of the Fastest Growing Companies in Deloitte Fast 500"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/deloitte2017.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://bestcompaniesaz.com/arizona-most-admired-companies-winners/" title="SiteLock Named one of Arizona's Most Admired Companies in 2017"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/mostAdmired2017.png" alt="Award"></div></a></div>

<div class="col-md-3"><a href="https://otalliance.org/system/files/files/initiative/documents/2017honorroll.pdf" title="SiteLock Named on OTA's 2017 Honor Roll"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/ota2017.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www2.deloitte.com/content/dam/Deloitte/us/Documents/technology-media-telecommunications/Technology-Fast-500-Winners-Brochure.PDF" title="SiteLock is One of the Fastest Growing Companies in Deloitte Fast 500"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/deloitte2016.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.sitelock.com/blog/2015/11/sitelock-deloitte-fast-500-2015/" title="SiteLock is One of the Fastest Growing Companies in Deloitte Fast 500"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/deloitte2015.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.bizjournals.com/phoenix/print-edition/2013/12/13/welcome-to-best-places-to-work-2013.html" title="SiteLock&reg; Named one of the top 100 places to work by BizJournal, 2013"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/bizJournal2013.png" alt="Award"></div></a></div>
<div class="col-md-3"><a href="https://www.hosting-review.com/awards/best-website-security-2013.shtml" title="SiteLock is the Winner of the 2013 Best Website Security Award"><div class="d-flex align-items-center" id="awardsBox"><img class="awardIMG" src="img/awards/awardpage/hostingCon2013.png" alt="Award"></div></a></div>


</div></div>
<div class="whiteSpace50"></div>
</div>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<h1 class="text-center font55 sourceBlack">See what all the buzz is about</h1>
<div class="row text-center">
<div class="col-lg-3"></div>
<div class="col-lg-3"><a class="btn btn-ghost-red" href="products">Browse Products</a></div>
<div class="col-lg-3"><a class="btn btn-ghost-red" href="careers">Join the Team</a></div>
<div class="col-lg-3"></div>
</div>
<div class="whiteSpace50"></div>
</div></div>

<div class="whiteSpace50"></div>

<div id="mainBlue"><div id="mainTerms"><div class="text-center" id="content">
<div class="whiteSpace50"></div>
<h2 class="font45 sourceBlack">Connect with SiteLock</h2>
<div class="whiteSpace25"></div>
<div class="row text-center linkWhite">
<div class="col"><a href="https://www.facebook.com/SiteLock/"><i class="fab fa-facebook-square fa-5x"></i></a></div>
<div class="col"><a href="https://twitter.com/SiteLock"><i class="fab fa-twitter-square fa-5x"></i></a></div>
<div class="col"><a href="https://www.linkedin.com/company/sitelock/"><i class="fab fa-linkedin fa-5x"></i></a></div>
<div class="col"><a href="https://www.instagram.com/sitelock/"><i class="fab fa-instagram fa-5x"></i></a></div>
</div>
<div class="whiteSpace50"></div>
</div></div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>