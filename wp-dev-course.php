<?php

?>
<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock | Security For WordPress</title>
  <link rel="canonical" href="https://www.sitelock.com/gg-products" />
  <link rel="icon" type="image/ico" href="favi.ico">
  <!-- Bootstrap -->
  <link href="css/fontawesome-all.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
</head>

<body>
<!-- HEADER -->
<div id="mainBody"><div id="content">
<br>
<img class="logoNavFringe" src="../img/logos/SiteLock_red.svg" alt="sitelock logo">
<br>
</div></div>

<!--BANNER -->
<div class="text-center sourceBlack" id="mainBlueWDC"><div id="mainBody">
<h1 class="font55">Secure Your WordPress<br>Site Today</h1>
<br>
<h2 class="font25">Special WP Site Security packages for students of<br>The Complete WordPress Website Business Course</h2>
<br>
<h2 class="font25"><u>Presented by Gregg Davis on Udemy</u></h2>
<br>
<a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">CONTACT US TODAY</a>
</div></div>

<!-- WHO IS SITELOCK -->
<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>    
<div class="row">
<div class="col-md-6">
<center><iframe style="width: 100%; max-width: 400px; height: 100%; max-height: 300px;" src="https://www.youtube.com/embed/WQcls4B-YRU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></center>
</div>
<div class="col-md-6">
<h3>Who Is SiteLock?</h3>
<p>Founded in 2008 and based in Scottsdale, Arizona, SiteLock is committed to providing comprehensive and affordable security to websites of all sizes.</p>
<p>From automated malware removal and threat prevention to website acceleration and application testing, the SiteLock team provides unmatched security for your site. Add in 24/7 support from our expert teams, and you can rest easy knowing we have your back.</p>
</div>
</div>
<div class="whiteSpace50"></div>
</div></div>

<!-- CMS BANNER -->
<div class="text-center sourceBlack" id="mainBlueWDC"><div id="mainBody">
<img class="img90" style="max-width: 500px !important;" src="img/ap/dev_course_logos.png">
<br>
<p>CMS websites are attractive targets to hackers. That's why SiteLock and WP Dev Course have teamed up to provide you with the latest in website security knowledge and technology.</p>
</div></div>


<!-- WEBSITE SECURITY 101 -->
<div id="mainGrey"><div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<h2 class="text-center red font55">Website Security 101</h2>

<div class="row sourceBlack text-center">

<div class="col-md-1"></div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/wdc1.svg" alt="Icon Fix">Update Your Website
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/wdc2.svg" alt="Icon Fix">Change Passwords
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/wdc3.svg" alt="Icon Fix">Backup Website Files
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/wdc4.svg" alt="Icon Fix">Implement Monitoring
</div>

<div class="col-md-2">
<img class="ap_icon" src="../img/ap/wdc5.svg" alt="Icon Fix">Block Malicious Access
</div>

<div class="col-md-1"></div>

</div>
<div class="whiteSpace50"></div>
</div></div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <script>document.write(new Date().getFullYear())</script> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<?php
$hb_title = "Secure Your Site Today";
$hb_phone = "(833) 263-8629";
$hb_phone_link = "8332638629";
$hb_btn = 'GET STARTED';
$hb_target = '';
include 'includes/forms/high-barrier.html';
?>

<!-- JS -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/custom.js?v=1.0.1"></script>
</body>
</html>