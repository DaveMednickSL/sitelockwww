<?php
//DEFINITIONS
$title = "SiteLock Solutions Hub";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div class="text-center" id="mainBody">
<div class="whiteSpace25"></div>
<h1>Find Your Perfect <span class="sourceBlack">Website Security Solution</span></h1>
<h2 class="font25">Looking to protect your website but not sure where to start? You’ve come to the right place. Whether you run a business or blog, we’re certain you’ll find a website security solution that’s perfect for your needs and budget.</h2>
<a class="btn btn-blue" href="#onpagepricing">Explore Web Security Options</a>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainBody">
<h1 class="text-center font55">Website security <span class="sourceBlack">designed for you</span></h1>

<div class="row sourceBlack">
<div class="col-md-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/home/homeBlog.svg" alt="Blogs">
  <h5>Personal<br>websites & blogs</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey" href="personal-site">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/home/homeMobile.svg" alt="Blogs">
  <h5>Business<br>websites</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey" href="business-solutions">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/siteCart.svg" alt="Blogs">
  <h5>eCommerce<br>websites</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey" href="business-solutions">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/home/homeCode.svg" alt="Blogs">
  <h5>Web<br>developers</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey" href="developer-solutions">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/home/homeWP.svg" alt="Blogs">
  <h5>WordPress<br>websites</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey" href="wordpress-security">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/solutions/hostingProviders.svg" alt="Blogs">
  <h5>Hosting<br>Providers</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey" href="hosting-providers">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/solutions/msp.svg" alt="Blogs">
  <h5>MSP &<br>Telco</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey" href="msp-telco">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/solutions/agencies.svg" alt="Blogs">
  <h5><br>Agencies<br></h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey" href="agencies">Learn More</a><br><br>
</div>
</div>
</div>
</div>

<div class="whiteSpace50"></div>
<div id="mainSplitLeft">
    <div class="row splitSpace">
     <div class="col-lg-5 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2 class="sourceBlack">The SiteLock experience</h2>When a website attack occurs, it can be confusing and overwhelming. Don’t worry—we’ve got your back! Let us walk you through the SiteLock experience.<br><a class="btn btn-ghost-blue" href="#onpagepricing">Get Started</a></div></div>
     <div class="col-lg-7 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/259753291" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
    </div>
  </div>
</div>
<div class="whiteSpace50"></div>
</div>

<?php include 'includes/awards-bar.php';?>

<div id="onpagepricing"></div>
<div class="whiteSpace100"></div>
<div id="mainBody">
<h2 class="font45 text-center">Buy your website security plan today</h2>
<?php include 'includes/price-bar-1.php';?>

<?php include 'includes/includes-bar.php';?>
</div>

<?php include 'includes/solutions-bar.php';?>

<?php include 'includes/news-bar-1.php';?>

<?php 
$benefitsTitle = '<h1 class="text-center font55">Website Security <span class="sourceBlack">benefits</span></h1>'; 
$icon1= 'fa-umbrella'; $title1 = 'Peace of mind'; $content1 = 'Protect your website with automated security and never worry about a compromise damaging your site, reputation, or bottom line.';
$icon2 = 'fa-badge-check'; $title2 = 'Increase Visitor Trust'; $content2 = 'Showcase a trust badge on your site to instill customer confidence. Today, 79 percent of online shoppers expect to see a trust badge. Your website scanner comes with a SiteLock Trust Seal that indicates your site is safe.';
$icon3 = 'fa-rocket'; $title3 = 'Increase website speed'; $content3 = 'Increase your site speed by as much as 50 percent and watch your user experience and conversions improve with your SiteLock plan.';
$icon4 = 'fa-chart-line'; $title4 = 'Protect SEO'; $content4 = 'Save your SEO success. If search engines find malware on your site, it will be blacklisted and potentially removed from results. Protect your SEO by staying malware-free.';
$icon5 = 'fa-location'; $title5 = 'Cost Savings'; $content5 = 'Protect your website with award-winning products proven to find and fix threats with 99.99% accuracy.';
$icon6 = 'fa-user-graduate'; $title6 = 'Always Informed'; $content6 = 'Be the first to know if there’s malicious activity on your site with real-time email alerts. You can also review your security results from your SiteLock Dashboard at any time.';
$benefitsBTN = 'Protect Your Website Now';

include('includes/benefits-bar.php');
?>

<?php include 'includes/rating-bar.php';?>

<div id="mainBody"><div id="content">
<div class="whiteSpace100"></div>
<div id="mainSplitLeft">
    <div class="row splitSpace">
     <div class="col-lg-5 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2 class="sourceBlack">Is your website at risk of cyberthreats?</h2>The reality is, all websites are inherently at risk of cyberthreats. Get your free risk assessment to determine your likelihood of a website compromise on a scale of low, medium, and high.<br><a class="btn btn-red" href="contact">Get Free Assessment</a></div></div>
     <div class="col-lg-7 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/273777919" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
    </div>
  </div>
</div>
<div class="whiteSpace100"></div>

<h2 class="text-center font55">Can we point you in the right direction?</h2>
<div class="row">
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="img70" src="img/channel/hostingComment.svg"><h4>Read SiteLock<br>Reviews</h4><br><a class="btn btn-ghost-blue font-weight-bold" href="reviews">Check Us Out</a></div><br></div></div>
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="img70" src="img/siteResources.svg"><h4>Check out our<br>Resources Center</h4><br><a class="btn btn-ghost-blue font-weight-bold" href="resources">Start Exploring</a></div><br></div></div>
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="img70" src="img/qsrRed.svg"><h4>Download the SiteLock<br>Website Security Insider</h4><br><a class="btn btn-ghost-blue font-weight-bold" href="security-report">Read the Report</a></div><br></div></div>
</div>
<div class="whiteSpace50"></div>

</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>