<?php
//DEFINITIONS
$title = "SiteLock Website Security Pricing | Peace of Mind for Dollars a Day";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div class="headerBottomGrey" id="mainHeaderBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="sourceBlack">SiteLock Pricing</h1>
<div class="whiteSpace50"></div>
</div></div>

<!--<div id="priceSteps"><div id="mainBody">
<div class="row text-center">
<div class="col-md-4 priceStepsBoxSelected">Step 1: Pick a Plan</div>
<div class="col-md-4 priceStepsBox">Step 2: Create an Account</div>
<div class="col-md-4 priceStepsBox">Step 3: Purchase</div>
</div>
</div></div>-->

<div id="mainBody">
<div class="whiteSpace50"></div>
<p class="text-center"><a id="btnYear" class='pricingYearTarget btn btn-ghost-grey btn-sm'>Yearly Pricing</a>&nbsp;&nbsp;&nbsp;<a id="btnMonth" class='pricingMonthTarget btn btn-blue btn-sm'>Monthy Pricing</a></p>
<div class="row">
<div class="col-md-4">
<div id="priceHatWhite">.</div>
<div id="priceBox">
<div id="priceGreyHead">
<div id="priceBoxContent">
<p class="text-center sourceLight font25">Best for <span class="sourceBlack">Personal Sites</span></p>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 id="price1" class="sourceLight">$29.99/mo</h1>
<span class="red" id="monthfree"></span>
<br>
<a id="checkout1" class="btn btn-ghost-blue" href="checkout?planID=1007">Sign Up</a>
<hr>
<p>
<span class="font16">Secure<strong>Starter</strong><br>Automatically guards personal websites from bad bots, malware infections and other cyberthreats</span>
<br><br>
<a class="linkred" href="m1" data-toggle="modal" data-target="#m1"><i class="far fa-plus-circle"></i> Plan Features</a></a></p>

</div>
</div>
</div>

<div class="col-md-4">
  <div class="sourceRegular" id="priceHatRed">Most Popular</div>
  <div id="priceBox">
  <div id="priceGreyHead">
  <div id="priceBoxContent">
  <p class="text-center sourceLight font25">Designed for <span class="sourceBlack">Businesses</span></p>
  </div>
  </div>
  <div class="text-center" id="priceBoxContent">
  <h1 id="price2" class="sourceLight">$49.99/mo</h1>
  <span class="red" id="monthfree2"></span>
  <br>
  <a id="checkout2" class="btn btn-blue" href="checkout?planID=1008">Create My Account</a>
  <hr>
  <p>
  <span class="font16">Secure<strong>Speed</strong><br>Repairs an existing malware infection, prevents future infections and blocks bad bots.</span>
  <br><br>
  <a class="linkred" href="m2" data-toggle="modal" data-target="#m2"><i class="far fa-plus-circle"></i> Plan Features</a></a></p>
  </div>
  </div>
  </div>

<div class="col-md-4">
  <div id="priceHatWhite">.</div>
  <div id="priceBox">
  <div id="priceGreyHead">
  <div id="priceBoxContent">
  <p class="text-center sourceLight font25">Perfect for <span class="sourceBlack">Ecommerce</span></p>
  </div>
  </div>
  <div class="text-center" id="priceBoxContent">
  <h1 id="price3" class="sourceLight">$69.99/mo</h1>
  <span class="red" id="monthfree3"></span>
  <br>
  <a id="checkout3" class="btn btn-ghost-blue" href="checkout?planID=1009">Sign Up</a>
  <hr>
  <p>
  <span class="font16">Secure<strong>Site</strong><br>Protects business websites from online threats with a combination of software and professional services.</span>
  <br><br>
  <a class="linkred" href="m3" data-toggle="modal" data-target="#m3"><i class="far fa-plus-circle"></i> Plan Features</a></a></p>
  </div>
  </div>
</div>
</div>

<div class="text-center linkgrey">
<h4><a href="#compare">Compare Plans</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Request a Custom Quote</a></h4>
<div class="whiteSpace50"></div>
</div>
</div>

<div id="mainBody">

<h2 class="text-center font55">Check Out SiteLock Reviews On TrustPilot</h2>

<!-- TrustBox widget - Carousel -->
<div class="trustpilot-widget" data-locale="en-US" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="5415bf7c00006400057a5178" data-style-height="130px" data-style-width="100%" data-theme="light" data-stars="4,5" data-schema-type="Organization">
<a href="https://www.trustpilot.com/review/sitelock.com" target="_blank">Trustpilot</a>
</div>
<!-- End TrustBox widget -->

<div class="whiteSpace50"></div>

<?php include 'includes/includes-bar.php';?>

<div id="compare"></div>
<h2 class="text-center font55">Compare Plans</h2>
<table class="table">
  <thead>
    <tr>
      <th>FEATURE</th>
      <th class="priceCompareBlue" scope="col">SecureStarter</th>
      <th class="priceCompareBlue" scope="col">SecureSpeed</th>
      <th class="priceCompareBlue" scope="col">SecureSite</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th class="priceCompareGrey"><strong>Scanner</strong></th>
      <td><strong>SMART</strong></td>
      <td><strong>SMART</strong></td>
      <td><strong>INFINITY</strong></td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Pages Scanned</th>
      <td>500</td>
      <td>500</td>
      <td>2500</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Automatic Malware Removal</th>
      <td>Once daily</td>
      <td>Once daily</td>
      <td>Constant</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Automatic Threat Detection Scanning</th>
      <td>Once daily</td>
      <td>Once daily</td>
      <td>Constant</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Automated WordPress, Joomla & Drupal Patching</th>
      <td>No</td>
      <td>No</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Database Scanning</th>
      <td>No</td>
      <td>No</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Automated Database Cleaning</th>
      <td>No</td>
      <td>No</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th class="priceCompareGrey"><strong>Web Application Firewall</strong></th>
      <td><strong>Professional</strong></td>
      <td><strong>Premium</strong></td>
      <td><strong>Premium</strong></td>
    </tr>
    <tr>
      <th class="priceCompareGrey">SSL Support</th>
      <td>Yes</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Website Acceleration</th>
      <td>Yes</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Bad Bot Blocking</th>
      <td>Yes</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Customizable Traffic Filtering</th>
      <td>Yes</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Blocks Database Attacks</th>
      <td>No</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th class="priceCompareGrey"><strong>Expert Services</strong></th>
      <td><strong>Basic</strong></td>
      <td><strong>Better</strong></td>
      <td><strong>Best</strong></td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Emergency Hack Repair</th>
      <td>No</td>
      <td>One Time</td>
      <td>Unlimited</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">Blacklist Removal</th>
      <td>No</td>
      <td>One Time</td>
      <td>Unlimited</td>
    </tr>
    <tr>
      <th class="priceCompareGrey">24/7/365 Support</th>
      <td>Yes</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th class="priceCompareGrey"><strong>Get Started Today</strong></th>
      <td ><a class="btn btn-red btn-sm" href="checkout?planID=959">SecureStarter</a></td>
      <td><a class="btn btn-red btn-sm" href="checkout?planID=960">SecureSpeed</a></td>
      <td><a class="btn btn-red btn-sm" href="checkout?planID=961">SecureSite</a></td>
    </tr>
  </tbody>
</table>

<div class="whiteSpace50"></div>
</div>


<div id="mainGrey">
<div class="whiteSpace50"></div>
<div id="mainWhite">
<div class="whiteSpace25"></div>
<div id="content90">
<h2 class="text-center font55 sourceBlack">Frequently Asked Questions</h2>
<a id="collapsebtn1" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1"><span class="float-left" style="overflow-wrap: break-word;">Isn’t website security my host’s responsibility?</span> <span class="float-right"><i id="collapseone" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse1">
<br>
<p class="font18">It’s a common misconception that hosting providers offer security for each website they host. However, your web host only protects the server your website is hosted on, not the website itself. Think of it like securing an apartment building. Property management takes responsibility for securing the building, but each tenant must lock the door to their own apartment.</p>
</div>

<a id="collapsebtn2" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2"><span class="float-left">What is the difference between Starter, Speed and Site?</span> <span class="float-right"><i id="collapsetwo" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse2">
<br>
<p class="font18">SecureStarter is designed for personal websites or blogs. It can automatically detect and remove malware but doesn’t repair a currently infected website. The SecureSpeed and SecureSite packages will both repair a currently hacked website, however, with the Speed package you only get one manual repair, while Site gives you unlimited access to our team of cybersecurity professionals.</p>
</div>

<a id="collapsebtn3" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3"><span class="float-left">Will these plans remove a current malware infection?</span> <span class="float-right"><i id="collapsethree" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse3">
<br>
<p class="font18">Only SecureSpeed and SecureSite will repair a currently infected website.</p>
</div>

<a id="collapsebtn4" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse4" role="button" aria-expanded="false" aria-controls="collapse4"><span class="float-left">I have an SSL Certificate, why do I need this?</span> <span class="float-right"><i id="collapsefour" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse4">
<br>
<p class="font18">An SSL certificate is a great first step to a secure website. It encrypts information passed between your site and its visitors to help prevent data theft. However, you still need to protect your site from threats like defacement, data theft or resource hijacking.</p>
</div>

<a id="collapsebtn5" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse5" role="button" aria-expanded="false" aria-controls="collapse5"><span class="float-left">Do your services work will all hosting providers?</span> <span class="float-right"><i id="collapsefive" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse5">
<br>
<p class="font18">Yes, our services work with all website hosting providers.</p>
</div>

<a id="collapsebtn6" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse6" role="button" aria-expanded="false" aria-controls="collapse6"><span class="float-left">Do your services work with all website platforms?</span> <span class="float-right"><i id="collapsesix" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse6">
<br>
<p class="font18">SiteLock website security products work with all open source website platforms, such as WordPress, Joomla and Drupal. We also work with custom coded websites and support many ecommerce platforms.</p>
</div>

<a id="collapsebtn7" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse7" role="button" aria-expanded="false" aria-controls="collapse7"><span class="float-left">What type of payments do you accept?</span> <span class="float-right"><i id="collapseseven" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse7">
<br>
<p class="font18">We currently accept Visa, Mastercard, Discover and American Express. We do not accept PayPal or electronic checks currently.</p>
</div>
<div class="whiteSpace25"></div>
</div>
</div>

<div class="whiteSpace50"></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<?php
//FORM DEFINITIONS
$hb_title = "Secure Your Website";
$hb_phone = "(855) 378-6200";
$hb_phone_link = "8553786200";
$hb_btn = 'Get A Free Consultation';
$hb_target = '';
include 'includes/forms/high-barrier.html';
?>
<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>

<!-- MODALS -->
<!-- M1 -->
<div class="modal fade" id="m1" tabindex="-1" role="dialog" aria-labelledby="m1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title sourceLight" id="m1">Secure<span class="sourceBlack">Starter</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="whiteSpace25"></div>
      <div class="row">
      <div class="col-md-6 my-auto">
      <p class="text-center sourceLight font25">Best for <span class="sourceBlack blue">Personal Sites</p>
      <h1 id="price1" class="text-center sourceLight">$29.99/mo</h1>
      <hr>
      <p class="text-center font16">Automatically guards personal websites from bad bots, malware infections and other cyberthreats</p>
      </div>
      <div class="col-md-6">
      <ul>
        <li>SMART Scanner</li>
        <li>Automatic malware detection and removal</li>
        <li>Pro WAF</li>
        <li>Blocks bad bots used to hack websites</li>
        <li>Increases website speed</li>
        <li>Supports SSL</li>
      </ul>
      <p class="text-center"><a id="checkout2" class=" btn btn-blue" href="checkout?planID=1007" style="color: #fff !important; text-decoration: none;">Create Account</a></p>
      </div>
      </div>
      <div class="whiteSpace25"></div>
      </div>
    </div>
  </div></div>

<!-- M2 -->
<div class="modal fade" id="m2" tabindex="-1" role="dialog" aria-labelledby="m2" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title sourceLight" id="m2">Secure<span class="sourceBlack">Speed</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="whiteSpace25"></div>
      <div class="row">
      <div class="col-md-6 my-auto">
      <p class="text-center sourceLight font25">Designed for <span class="sourceBlack blue">Businesses</span></p>
      <h1 id="price1" class="text-center sourceLight">$49.99/mo</h1>
      <hr>
      <p class="text-center font16">Automatically guards business websites from bad bots, malware infections and other cyberthreats</p>
      </div>
      <div class="col-md-6">
      <ul>
      <li>SMART Scanner</li>
      <li>Emergency Hack Repair</li>
      <li>Manual malware removal</li>
      <li>Blacklist & suspension resolution</li>
      <li>Premium WAF</li>
      <li>Everything in Pro WAF</li>
      <li>Blocks website database attacks</li>
      <li>Customizable traffic filtering</li>
      </ul>
      <p class="text-center"><a id="checkout2" class=" btn btn-blue" href="checkout?planID=1008" style="color: #fff !important; text-decoration: none;">Create Account</a></p>
      </div>
      </div>
      <div class="whiteSpace25"></div>
      </div>
    </div>
  </div></div>

<!-- M3 -->
<div class="modal fade" id="m3" tabindex="-1" role="dialog" aria-labelledby="m3" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title sourceLight" id="m3">Secure<span class="sourceBlack">Site</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="whiteSpace25"></div>
      <div class="row">
      <div class="col-md-6 my-auto">
      <p class="text-center sourceLight font25">Perfect for <span class="sourceBlack blue">Ecommerce</span></p>
      <h1 id="price1" class="text-center sourceLight">$69.99/mo</h1>
      <hr>
      <p class="text-center font16">Automatically guards Ecommerce websites from bad bots, malware infections and other cyberthreats</p>
      </div>
      <div class="col-md-6">
      <ul>
      <li>Infinity Scanner</li>
      <li>All Smart Scanner features</li>
      <li>Automatic patching and updating of WordPress, Joomla and Drupal</li>
      <li>Detection of website database infections</li>
      <li>Unlimited Hack Repair</li>
      <li>Premium WAF</li>
      </ul>
      <p class="text-center"><a id="checkout2" class=" btn btn-blue" href="checkout?planID=1009" style="color: #fff !important; text-decoration: none;">Create Account</a></p>
      </div>
      </div>
      <div class="whiteSpace25"></div>
      </div>
    </div>
  </div></div>

</body>
</html>