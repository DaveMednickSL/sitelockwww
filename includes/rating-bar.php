<div id="mainGrey"><div id="mainBody">
<div class="whitespace25"></div>
<div class="row">
<div class="col-md-6">
<div class="row">
<div class="col">
<img class="img50" src="img/reviews/bbbStars.png" alt="BBB">
</div>
<div class="col">
<img class="img50" src="img/reviews/consumerAffairsStars.png" alt="Consumer Affairs">
</div>
</div>
</div>

<div class="col-md-6">
<div class="row">
<div class="col">
<img class="img50" src="img/reviews/googleStars.png" alt="Google">
</div>
<div class="col">
<img class="img50" src="img/reviews/trustPilotStars.png" alt="Trust Pilot">
</div>
</div>
</div>
</div>
<div class="whitespace50"></div>
</div></div>
