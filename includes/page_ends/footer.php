<div class="footer">
<div id="content" style="padding-top: 10px;">
<a href="/"><img class="logoFooter" src="img/logos/SiteLock_white.svg" alt="sitelock logo"></a>
<p class="footerNav">
<span class="footerNavItem"><a href="/currently-infected">MY SITE IS HACKED</a></span>
<span class="footerNavItem"><a href="/products">PRODUCTS</a></span>
<span class="footerNavItem"><a href="/pricing">PRICING</a></span>
<span class="footerNavItem"><a href="/about">OUR COMPANY</a></span>
<span class="footerNavItem"><a href="/blog">BLOG</a></span>
<span class="footerNavItem"><a href="/careers">CAREERS</a></span>
<span class="footerNavItem"><a href="/help-center">HELP CENTER</a></span>
<span class="footerNavItem"><a href="/sitemap">SITEMAP</a></span>
</p>

<hr class="footerHR">

<div class="row">
<div class="col-md-4">
<h3 class="sourceRegular">Search</h3>
<p>Looking for something?</p>
<div class="form-inline footerSpace">
<form method="post" action="search-sitelock">
<input class="form-control footerInput" type="text" name="term" placeholder="Enter Keyword" required>
<button class="btn btn-ghost-blue btn-sm"  style="width: 100%;">Search</button>
</form>
</div>
</div>

<div class="col-md-4">
<hr class=" footerHR footMobileShow">
<h3 class="sourceRegular">Contact</h3>
<p>Reach us 24/7</p>
<p>
<table class="colorWhite font18 linkblue">
<tr><td>U.S&nbsp;&nbsp;</td><td><a href="tel:8553786200">(855) 378-6200</a></td></tr>
<tr><td>Int'l&nbsp;&nbsp;</td><td><a href="tel:4153902500">+1 (415) 390-2500</a></td></tr>
<tr><td>Email&nbsp;&nbsp;</td><td><a href="contact">Contact Us</a></td></tr>
</table>
</p>
</div>

<div class="col-md-4">
<hr class=" footerHR footMobileShow">
<h3 class="sourceRegular">Stay in touch</h3>
<p>Sign up for SiteLock news & announcements</p>
<div class="form-inline footerSpace">
<form method="post" action="https://a29565.actonservice.com/acton/eform/29565/f370b272-2264-4afa-bfea-f962cfb91e3d/d-ext-0002">
<input class="form-control footerInput" type="text" placeholder="you@email.com" required>
<button class="btn btn-ghost-blue btn-sm" style="width: 100%;">Sign Up</button>
</div>
</form>
</div>



</div>
</div>

<hr class="footerHR">

<h3 class="text-center sourceRegular">Connect</h3>

<div id="mainBody"><div id="content90" style="max-width: 350px;"><div class="row">
<div class="col text-center">
<a href="https://www.facebook.com/SiteLock/" target="_blank"><span class="fa-stack fa-2x">
  <i class="fal fa-circle fa-stack-2x"></i>
  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
</span></a>
</div>

<div class="col text-center">
<a href="https://twitter.com/SiteLock" target="_blank"><span class="fa-stack fa-2x">
  <i class="fal fa-circle fa-stack-2x"></i>
  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
</span></a>
</div>

<div class="col text-center">
<a href="https://www.linkedin.com/company/sitelock/" target="_blank"><span class="fa-stack fa-2x">
  <i class="fal fa-circle fa-stack-2x"></i>
  <i class="fab fa-linkedin-in fa-stack-1x fa-inverse"></i>
</span></a>
</div>

<div class="col text-center">
<a href="https://www.instagram.com/sitelock/" target="_blank"><span class="fa-stack fa-2x">
  <i class="fal fa-circle fa-stack-2x"></i>
  <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
</span></a>
</div>
</div></div></div>

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock 2008-<span id="COPYRIGHT"></span> | <a href="privacy-policy">Privacy Policy</a> | <a href="terms">Terms of Use</a> | <a href="affiliate-tos">Affiliates Terms of Service</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

</div>
</div>
