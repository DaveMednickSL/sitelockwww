<div id="mainBody"><div id="content">

<div class="row justify-content-center footMobileHide">
<!-- LEFT -->
<div class="col-lg-6">
<div class="row">
<div class="col my-auto">
<img class="img90" src="img/media_outlet_icons/threatpost.png" alt="Threat Post">
</div>
<div class="col my-auto">
<img class="img90" src="img/media_outlet_icons/darkreading.png" alt="Dark Reading">
</div>
<div class="col my-auto">
<img class="img90" src="img/media_outlet_icons/inc.png" alt="Inc">
</div>
<div class="col my-auto">
<img class="img90" src="img/media_outlet_icons/wsj.png" alt="Wall Street Journal">
</div>
</div>
</div>

<!-- RIGHT -->
<div class="col-lg-6">
<div class="row">
<div class="col my-auto">
<img class="img90" src="img/media_outlet_icons/washingtonpost.png" alt="The Washington Post">
</div>
<div class="col my-auto">
<img class="img90" src="img/media_outlet_icons/cnbc.png" alt="CNBC">
</div>
<div class="col my-auto">
<img class="img90" src="img/media_outlet_icons/forbes.png" alt="Forbes">
</div>
<div class="col my-auto">
<img class="img90" src="img/media_outlet_icons/usatoday.png" alt="USA Today">
</div>
</div>
</div>
</div>

<img class="img90 footMobileShow" src="img/media_outlet_icons/newsMobile.png">

</div><br></div>
