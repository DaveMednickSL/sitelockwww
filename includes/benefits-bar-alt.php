<div id="mainBody">
<div class="whiteSpace100"></div>
<?php  echo $benefitsTitle;?>

<div class="row font18">
<div class="col-lg-4 text-center">
<div id="benefitBoxAlt"><div id="benefitBoxContent">
<div id="benefitCircleBlueAlt"><i class="fal <?php echo $icon1; ?> fa-3x"></i></div>
<h3 class="sourceBlack font25"><?php echo $title1; ?></h3>
<p><?php echo $content1; ?></p>
</div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitBoxAlt"><div id="benefitBoxContent">
  <div id="benefitCircleRedAlt"><i class="fal <?php echo $icon2; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title2; ?></h3>
  <p><?php echo $content2; ?></p>
  </div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitBoxAlt"><div id="benefitBoxContent">
  <div id="benefitCircleBlueAlt"><i class="fal <?php echo $icon3; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title3; ?></h3>
  <p><?php echo $content3; ?></p>
  </div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitBoxAlt"><div id="benefitBoxContent">
  <div id="benefitCircleRedAlt"><i class="fal <?php echo $icon4; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title4; ?></h3>
  <p><?php echo $content4; ?></p>
  </div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitBoxAlt"><div id="benefitBoxContent">
  <div id="benefitCircleBlueAlt"><i class="fal <?php echo $icon5; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title5; ?></h3>
  <p><?php echo $content5; ?></p>
  </div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitBoxAlt"><div class="" id="benefitBoxContent">
  <div id="benefitCircleRedAlt"><i class="fal <?php echo $icon6; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title6; ?></h3>
  <p><?php echo $content6; ?></p>
  </div></div>
</div>

</div>
</div>
