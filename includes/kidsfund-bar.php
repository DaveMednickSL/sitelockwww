<div id="mainRed"><div id="mainBody">
<div class="row">
<div class="col-sm-6 my-auto">
<h2 class="sourceLight font45">Supporting the <span class="sourceBlack">SiteLock Digital Kids Fund</span> is easy!</h2>
<p class="">Did you know SiteLock donates $1 to our Digital Kids Fund for every product purchased by WordPress customers? These donations support STEM (Science, Technology, Engineering, and Math) programs for schools in need. Thanks to your help, SiteLock funded 234 school projects, benefiting nearly 31,000 students at 184 schools in 2017!</p>
<a class="btn btn-ghost-white" href="giving-back">Support the Cause</a><br><br>
</div>

<div class="col-sm-6">
<img class="img80" src="/img/about/kids.jpg" alt="SiteLock Digital Kids Fund">
</div>
</div>
</div>
<div class="whiteSpace25"></div>
</div>