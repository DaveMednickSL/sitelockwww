<div id="mainRed"><div id="mainBody"><div id="content90">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-6 my-auto">
<img class="img90 qsrMobile" src="img/qsrCover.png" alt="">
</div>

<div class="col-lg-6">
<h2 class="sourceLight font35"><span class="sourceBlack">SiteLock Website Security Insider:</span> Insights into today’s top cybersecurity threats</h2>
<p class="font18">The average website is attacked 50 times per day. Read the SiteLock Website Security Insider to learn about common cyberthreats and website security best practices.</p>
<a class="btn btn-ghost-white" href="security-report">Download Your Report</a>
</div>
</div>
<div class="whiteSpace50"></div>
</div></div></div>