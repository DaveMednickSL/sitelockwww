<div id="mainGrey">
<div class="whiteSpace50"></div>
<h2 class="sourceLight text-center font45">Every SiteLock Plan <span class="sourceBlack">Includes</span></h2>
<div class="row text-center">
<div class="col-md-3">
<img class="img80" src="img/scanningAlert.svg" alt="Scanning Alert">
<strong>Industry-leading<br>anti-malware<br>technology</strong>
</div>

<div class="col-md-3">
<img class="img80" src="img/trustSeal.svg" alt="Trust Seal">
<strong>SiteLock Trust Seal<br>to boost visitor<br>confidence</strong>
</div>

<div class="col-md-3">
<img class="img80" src="img/headset.svg" alt="Support">
<strong>24/7 U.S.-based<br>customer service &<br>support</strong>
</div>

<div class="col-md-3">
<img class="img80" src="img/rocket.svg" alt="Site Speed">
<strong>Faster loading and<br>saved bandwidth<br>with our CDN</strong>
</div>
</div>
<div class="whiteSpace100"></div>
</div>
<div class="whiteSpace50"></div>
