<div id="mainBody">
<div class="whiteSpace100"></div>
<h2 class="text-center sourceBlack font45">Award-Winning Security</h2>
<div class="row font18 justify-content-center">
<div class="col-md-3">
<div id="greyAwardBox"><div id="content"><p class="text-center"><img class="awardBump" src="img/awards/awardCyberSec2019.png" alt="Award"></p><i>Best Threat Detection:<br></i><br><a class="btn btn-ghost-grey" href="malware-removal#infinity">SiteLock INFINITY</a><br><br></div></div>
</div>

<div class="col-md-3">
<div id="greyAwardBox"><div id="content"><p class="text-center"><img class="awardBump" src="img/awards/awardInfoSec2018.svg" alt="Award"></p><i>Best Security Monitoring and<br>Website Security Solution:</i><br><a class="btn btn-ghost-grey" href="malware-removal#infinity">SiteLock INFINITY</a><br><br></div></div>
</div>

<div class="col-md-3">
<div id="greyAwardBox"><div id="content"><p class="text-center"><img class="awardBump" src="img/awards/awardFortress2018.svg" alt="Award"></p><i>Best Anti-Malware, Anti-Spam,<br>and Anti-Virus Scanner:</i><br><a class="btn btn-ghost-grey" href="malware-removal#smart">SiteLock SMART</a><br><br></div></div>
</div>
</div>
<div class="whiteSpace100"></div>
</div>