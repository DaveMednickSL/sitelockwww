<br>
<div class="row">
<div class="col-md-4">
<div id="priceHatWhite">.</div>
<div id="priceBox">
<div id="priceGreyHead">
<div id="priceBoxContent">
<h3 class="sourceLight text-center font25">Best for <span class="sourceBlack">Personal Sites</span></h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<br>
<h2 class="sourceLight font45">$29.99</h2>
Per Month/Domain<br><br>
<a class="btn btn-ghost-blue width95" href="checkout?planID=1007">Sign Up</a>
<p class="text-left">
<br>
<span id="pricehide">Daily website scanning, automatic malware removal, and basic threat blocking for small sites.<br><br></span>
</p>
</div>
</div>
</div>

<div class="col-md-4">
  <div class="sourceRegular" id="priceHatRed">Most Popular</div>
  <div id="priceBox">
  <div id="priceGreyHead">
  <div id="priceBoxContent">
  <h3 class="sourceLight text-center font25">Designed for <span class="sourceBlack">Businesses</span></h3>
  </div>
  </div>
  <div class="text-center" id="priceBoxContent">
  <br>
  <h2 class="sourceLight font45">$49.99</h2>
  Per Month/Domain<br><br>
  <a class="btn btn-blue width95" href="checkout?planID=1008">Create My Account</a><br>
  <p class="text-left">
  <br>
  <span id="pricehide">Emergency hacked site repair, backed by a year of comprehensive website security tools and our expert support.</span>
  </p>
  </div>
  </div>
  </div>

<div class="col-md-4">
  <div id="priceHatWhite">.</div>
  <div id="priceBox">
  <div id="priceGreyHead">
  <div id="priceBoxContent">
  <h3 class="sourceLight text-center font25">Perfect for <span class="sourceBlack">Ecommerce</span></h3>
  </div>
  </div>
  <div class="text-center" id="priceBoxContent">
  <br>
  <h2 class="sourceLight font45">$69.99</h2>
  Per Month/Domain<br><br>
  <a class="btn btn-ghost-blue width95" href="checkout?planID=1009">Sign Up</a>
  <p class="text-left">
  <br>
  <span id="pricehide">Full service website protection plus WordPress database protection and automated patching for major CMS & ecommerce platforms</span>
  </p>
  </div>
  </div>
</div>
</div>


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-4 text-center linkgrey"><h4><a class="priceHover" href="pricing#compare">Compare Plans</a></h4></div>
<div class="col-md-2"></div>
<div class="col-md-4 text-center linkgrey"><h4><a class="priceHover" href="PricingForm" data-toggle="modal" data-target="#PricingForm">Request a Custom Quote</a></h4></div>
<div class="col-md-1"></div>
</div>

<?php
//FORM DEFINITIONS
$hb_title = "Get a custom Quote";
$hb_phone = "(855) 378-6200";
$hb_phone_link = "8553786200";
$hb_btn = 'Get A Free Consultation';
$hb_target = '';
include 'includes/forms/pricing.html';
?>

<div class="whiteSpace50"></div>

