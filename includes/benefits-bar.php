<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace100"></div>
<?php  echo $benefitsTitle;?>

<div class="row">
<div class="col-md-4">
<div id="benefitBox"><div id="benefitBoxContent">
<div id="benefitCircleBlue"><i class="fal <?php echo $icon1; ?> fa-3x"></i></div>
<h3 class="sourceBlack font25"><?php echo $title1; ?></h3>
<p><?php echo $content1; ?></p>
</div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleRed"><i class="fal <?php echo $icon2; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title2; ?></h3>
  <p><?php echo $content2; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleBlue"><i class="fal <?php echo $icon3; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title3; ?></h3>
  <p><?php echo $content3; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleRed"><i class="fal <?php echo $icon4; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title4; ?></h3>
  <p><?php echo $content4; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleBlue"><i class="fal <?php echo $icon5; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title5; ?></h3>
  <p><?php echo $content5; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleRed"><i class="fal <?php echo $icon6; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title6; ?></h3>
  <p><?php echo $content6; ?></p>
  </div></div>
</div>


</div>
<br><br>
<p class="text-center"><a class="btn btn-blue" href="#onpagepricing"><?php echo $benefitsBTN;?></a></p>
<div class="whiteSpace50"></div>
</div></div>
