<div id="mainBlue"><div class="text-center" id="mainBody">
<div class="row">
<div class="col-md-4 my-auto">
<div class="row">
<div class="col"><img class="img50" src="img/cms_icons/magentoWhite.svg"></div>
<div class="col"><img class="img50" src="img/cms_icons/joomlaWhite.svg"></div>
</div>
</div>

<div class="col-md-4 my-auto">
  <h3 class="text-center sourceBlack font35">Compatible with all types of websites</h3>
</div>

<div class="col-md-4 my-auto">
  <div class="row">
  <div class="col"><img class="img50" src="img/cms_icons/wordpressWhite.svg"></div>
  <div class="col"><img class="img50" src="img/cms_icons/drupalWhite.svg"></div>
  </div>
</div>
</div>
</div></div>
