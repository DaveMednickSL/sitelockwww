<?php
$_SESSION['HTTP_REFERER'] = "http://www.greengeeks.com/";
$_SESSION['ref'] = 'fc95a7444a92875d25be94c221b30300b989c4b0';
?>
<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock | Website Security</title>
  <link rel="canonical" href="https://www.sitelock.com/gg-products" />
  <link rel="icon" type="image/ico" href="favi.ico">
  <!-- Bootstrap -->
  <link href="css/fontawesome-all.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
</head>

<body>
<!-- HEADER -->
<div id="mainBody"><div id="content">
<br>
<img class="logoNavFringe" src="../img/logos/SiteLock_red.svg" alt="sitelock logo">
<br>
</div></div>

<!--BANNER -->
<div class="text-center sourceBlack" id="mainBlue"><div id="mainBody">
<h1 class="font55">SiteLock&reg; protects your business.</h1>
<h2 class="font25">Choose the package that's right for you.</h2>
</div></div>

<!-- PRICE BOXES -->
<div id="mainBody"><div id="content">

<div class="whiteSpace25"></div>
<img class="logoNavFringe" src="img/ap/greengeeks.png" alt="Green Geeks Logo">
<div class="whiteSpace25"></div>

<div class="row">
<div class="col-lg-3">
<div class="ggPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<h3 class="sourceBlack">Basic</h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$1.25</h1>
<strong>per month</strong><br>
<form method="post" action="https://secure.sitelock.com/signup.php?wtu=<?php echo $_SESSION['ref']; ?>">
<input type="hidden" name="selected_plan" id="selected_plan" value="181"  />
<input type="hidden" name="marketing_price" id="marketing_price" value="$1.25/mo" />
<button class="btn btn-red btn-sm">Choose This Plan</button>
</form>
<ul class="text-left font14">
<li>Deep 360-Degree Site Scan*</li>
<li>Reputation Management</li>
<li>Business Validation</li>
<li>Verifiable Trust Seal</li>
<li>Unlimited Expert Support</li>
<li>Up to 25 Pages</li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="ggPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<h3 class="sourceBlack">Professional</h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$3.99</h1>
<strong>per month</strong><br>
<form method="post" action="https://secure.sitelock.com/signup.php?wtu=<?php echo $_SESSION['ref']; ?>">
<input type="hidden" name="selected_plan" id="selected_plan" value="407"  />
<input type="hidden" name="marketing_price" id="marketing_price" value="$3.99/mo" />
<button class="btn btn-red btn-sm">Choose This Plan</button>
</form>
<ul class="text-left font14">
<li>Automatic Malware Removal</li>
<li>Daily Malware Scan</li>
<li>Daily FTP Scanning</li>
<li>File Change Monitoring</li>
<li>Reputation Management</li>
<li>Business Validation</li>
<li>1-Time Vulnerability Scan</li>
<li>Verifiable Trust Seal</li>
<li>Unlimited Expert Support</li>
<li>Up to 100 Pages</li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="ggPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<h3 class="sourceBlack">Premium</h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$4.95</h1>
<strong>per month</strong><br>
<form method="post" action="https://secure.sitelock.com/signup.php?wtu=<?php echo $_SESSION['ref']; ?>">
<input type="hidden" name="selected_plan" id="selected_plan" value="2988"  />
<input type="hidden" name="marketing_price" id="marketing_price" value="$4.95/mo" />
<button class="btn btn-red btn-sm">Choose This Plan</button>
</form>
<ul class="text-left font14"> 
<li>Automatic Malware Removal</li>
<li>Deep 360-Degree Site Scan</li>
<li>Reputation Management</li>
<li>Business Validation</li>
<li>Verifiable Trust Seal</li>
<li>Unlimited Expert Support</li>
<li>Network Security Scanning</li>
<li>Up to 500 Pages</li>
</ul>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="ggPriceBox" id="priceBox">
<div id="priceGreyHead">
<div class="text-center" id="priceBoxContent">
<h3 class="sourceBlack">Enterprise</h3>
</div>
</div>
<div class="text-center" id="priceBoxContent">
<h1 class="sourceLight ">$14.99</h1>
<strong>per month</strong><br>
<form method="post" action="https://secure.sitelock.com/signup.php?wtu=<?php echo $_SESSION['ref']; ?>">
<input type="hidden" name="selected_plan" id="selected_plan" value="2989"  />
<input type="hidden" name="marketing_price" id="marketing_price" value="$14.99/mo" />
<button class="btn btn-red btn-sm">Choose This Plan</button>
</form>
<ul class="text-left font14">
<li>Automatic Malware Removal</li>
<li>Deep 360-Degree Site Scan</li>
<li>Reputation Management</li>
<li>Business Validation</li>
<li>Verifiable Trust Seal</li>
<li>Unlimited Expert Support</li>
<li>Network Security Scanning</li>
<li>Advanced Security Scanning</li>
<li>Up to 2500 Pages</li>
</ul>
</div>
</div>
</div>


</div></div></div>

<!-- QA SECTION -->
<div id="mainGrey"><div id="mainBody"><div id="content">
<h3 class="sourceBlack">How do I pay for SiteLock service?</h3>
<p>We accept all major credit cards including Visa, Mastercard, Discover and American Express. Once your account is active, we only charge you at the end of your free trial and then annually on your renewal date.</p>

<h3 class="sourceBlack">Can I speak with a representative directly?</h3>
<p>Of course. SiteLock has U.S.-based representatives at our office in Scottsdale, AZ. Reach us any time toll-free at <a href="tel:8772579263">(877) 257-9263</a> or from outside the U.S. at <a href="tel:4153902500">(415) 390-2500</a>. Or, e-mail us 24-hours at <a href="mailto:support@sitelock.com">support@sitelock.com</a></p>

<h3 class="sourceBlack">Is my website data safe?</h3>
<p>Absolutely. SiteLock does not modify any of your website data or applications. We simply scan your site, much like your PC anti-virus scanning software. We identify potential problems and notify you of them. If you need help fixing any issues, our expert services team is available to provide technical support.</p>

<h3 class="sourceBlack">What if I need to change or cancel my account?</h3>
<p>You can upgrade, downgrade or cancel your account at any time. Just give us a call at <a href="tel:8442170577">(844) 217-0577</a>.</p>

<hr>

<p class="font14">* The basic and Professonal Plans include continuous Malware scanning. All other elements of these plans are one-time only.</p>

</div></div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/custom.js?v=1.0.1"></script>
</body>
</html>