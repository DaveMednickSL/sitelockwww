<?php
//DEFINITIONS
$title = "Giving Back | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div class="givingBackHeader" id="mainHeader"><div id="mainBody">
<div class="whiteSpace50"></div>

<div class="row">
<div class="col-lg-8 my-auto">
<h1 class="sourceBlack text-center">SiteLock Loves Giving Back</h1>
<h2 class="font18">At SiteLock, we are extremely passionate about giving back to the communities where we live and work. Not only are we actively involved in a variety of important causes, but we also created the <strong>SiteLock Digital Kids Fund</strong> to support STEM programs (Science, Technology, Engineering, and Math) for schools in need. Through this initiative, we hope to inspire the next generation of technology innovators and pioneers.</h2>
</div>
<div class="col-lg-4"><img class="img90 givingIcon" src="img/giving-back/community.png" alt="Community"></div>
</div>
</div>
<div class="whiteSpace50"></div>
</div>

<div id="mainRedAlt"><div id="mainBody"><div id="content90">
<div class="row">
<div class="col-md-6 my-auto">
<h3 class="font55">About the <span class="sourceBlack">SiteLock Digital Kids Fund</span></h3>
<p>
In 2015, the SiteLock Digital Kids Fund was established to help local schools fund technology-based projects. We chose to partner with DonorsChoose.org, an organization that makes it easy to help a classroom in need. SiteLock donates $1 for every product purchased by WordPress customers to DonorsChoose.org.
<br><br>
Thanks to you, we’ve helped fund hundreds of school projects that positively impact tens of thousands of children.
</p>
</div> 
<div class="col-md-6">
<img class="img70" src="img/giving-back/givingBack1.png" alt="SiteLock Digital kids">
<br><br>
<img class="img70" src="img/giving-back/givingBack2.png" alt="SiteLock Gives Back">
</div> 
</div>
</div></div></div>

<div id="mainBody">
<div class="whiteSpace50"></div>
<h2 class="text-center font55">Making a difference <span class="sourceBlack">together</span></h2>
<div class="row">

<div class="col-md-6">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-4"><br><img class="givingBackImg" src="img/giving-back/phoenixRescueMission.png" alt="SiteLock Digital kids"><br></div>
<div class="col-md-8 my-auto"><strong>Phoenix Rescue Mission</strong><br><br>SiteLock donated over 70,000 bottles of water to those without shelter in the hot summer months</div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-4"><br><img class="givingBackImg" src="img/giving-back/phoenixChildrens.png" alt="SiteLock Digital kids"><br></div>
<div class="col-md-8 my-auto"><strong>Phoenix Children's Hospital</strong><br><br>SiteLock helps raise funds for the hospital and participates in ongoing activities to support the children</div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-4"><br><img class="givingBackImg" src="img/giving-back/followYourHeart.png" alt="SiteLock Digital kids"><br></div>
<div class="col-md-8 my-auto"><strong>Follow Your Heart Animal Rescue</strong><br><br>We donated over 400lbs of pet food and 20 plus additional items</div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-4"><br><img class="givingBackImg" src="img/giving-back/arizonaHelpingHands.png" alt="SiteLock Digital kids"><br></div>
<div class="col-md-8 my-auto"><strong>AZ Helping Hands</strong><br><br>SiteLock donated new school supplies to kids in foster care</div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-4"><br><img class="givingBackImg" src="img/giving-back/stMarysFoodBank.png" alt="SiteLock Digital kids"><br></div>
<div class="col-md-8 my-auto"><strong>St. Mary's Food Bank</strong><br><br>SiteLock donated more than 1,200 pounds of food during the holiday season</div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-4"><br><img class="givingBackImg" src="img/giving-back/animalCareandControl.png" alt="SiteLock Digital kids"><br></div>
<div class="col-md-8 my-auto"><strong>Maricopa Animal Care and Control Center</strong><br><br>SiteLock employees donated over 100 items to animals in need</div>
</div>
</div></div>
</div>

</div>
<div class="whiteSpace50"></div>
</div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>