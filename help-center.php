<?php
//DEFINITIONS
$title = "Help Center | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="text-center sourceBlack">Welcome to the<br><span class="sourcBlack">SiteLock Help Center</span></h1>
<h3 class="text-center">Whether you need help setting up your new products, resolving an alert, or you<br>just want to know what website security is all about, you’re in the right place.</h3>
<div class="whiteSpace50"></div>
</div>
</div>



<div id="mainGrey">
<div id="mainBody">
<div class="whiteSpace50"></div>
<form method="post" action="">
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8">
<span class="sourceBlack font18">Search The Database</span>
<input class="form-control-alt" type="text" name="term">
<span class="float-right"><button class="btn btn-ghost-grey btn-sm" type="submit"><i class="far fa-search"></i> Submit</button></span>
</div>
<div class="col-md-2"></div>
</div>
</form>

<div class="row">
<div class="col-md-3">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-lg-5 text-center"><br><i class="far fa-lock fa-4x"></i><br><br></div>
<div class="col-lg-6 my-auto font18 sourceBlack tabCenter">About website security</div>
</div>
</div></div>
</div>

<div class="col-md-3">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-lg-5 text-center"><br><i class="far fa-cog fa-4x"></i><br><br></div>
<div class="col-lg-7 my-auto font18 sourceBlack tabCenter">New customer set-up</div>
</div>
</div></div>
</div>


<div class="col-md-3">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-lg-5 text-center"><br><i class="far fa-map-marked-alt fa-4x"></i><br><br></div>
<div class="col-lg-7 my-auto font18 sourceBlack tabCenter">Navigating your dashboard</div>
</div>
</div></div>
</div>


<div class="col-md-3">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-lg-5 text-center"><br><i class="far fa-user fa-4x"></i><br><br></div>
<div class="col-lg-7 my-auto font18 sourceBlack tabCenter">Navigating your account</div>
</div>
</div></div>
</div>

<div class="col-md-3">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-lg-5 text-center"><br><i class="far fa-cloud fa-4x"></i><br><br></div>
<div class="col-lg-7 my-auto font18 sourceBlack tabCenter">SiteLock and web hosts</div>
</div>
</div></div>
</div>

<div class="col-md-3">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-lg-5 text-center"><br><i class="far fa-credit-card fa-4x"></i><br><br></div>
<div class="col-lg-7 my-auto font18 sourceBlack tabCenter">Billing and payments</div>
</div>
</div></div>
</div>

<div class="col-md-3">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-lg-5 text-center"><br><i class="far fa-thumbs-up fa-4x"></i><br><br></div>
<div class="col-lg-7 my-auto font18 sourceBlack tabCenter">Picking the right plan</div>
</div>
</div></div>
</div>

<div class="col-md-3">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-lg-5 text-center"><br><i class="far fa-glasses fa-4x"></i><br><br></div>
<div class="col-lg-7 my-auto font18 sourceBlack tabCenter">Explore More topics <i class="fas fa-chevron-down"></i></div>
</div>
</div></div>
</div>
</div>


<div class="whiteSpace50"></div>
</div>

<div id="mainWhite">
<div class="whiteSpace25"></div>
<div id="content90">
<a id="collapsebtn1" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1"><span class="float-left">Who is SiteLock?</span> <span class="float-right"><i id="collapseone" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse1">
<br>
<p class="linkblue font18">SiteLock is the <a href="about">global leader in website security</a>. We founded the company in 2008 with a passion to protect every website on the internet. Fast forward to the present, and we now protect over 12 million websites of all sizes around the world.</p>
</div>

<a id="collapsebtn2" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2"><span class="float-left">What is website security</span> <span class="float-right"><i id="collapsetwo" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse2">
<br>
<p class="linkblue font18">Comprehensive website security protects your website from malicious cyber threats. This includes protection of your site code and web applications. Depending on your website security package, you’ll receive daily website scans, automated malware removal and vulnerability patching, and a web application firewall to block harmful traffic from entering your site.</p>
</div>

<a id="collapsebtn3" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3"><span class="float-left">Doesn’t my hosting provider protect my website?</span> <span class="float-right"><i id="collapsethree" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse3">
<br>
<p class="linkblue font18">It’s a common <a href="https://www.sitelock.com/blog/2017/03/do-i-need-sitelock/">misconception that hosting providers protect each website they host</a>. The reality is, your web host only protects the server your website is hosted on, not the website itself. Think of it like securing an apartment building. Property management takes responsibility for securing the building, but each tenant must lock the door to their own apartment.</p>
</div>

<a id="collapsebtn4" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse4" role="button" aria-expanded="false" aria-controls="collapse4"><span class="float-left">Why do I need website security?</span> <span class="float-right"><i id="collapsefour" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse4">
<br>
<p class="linkblue font18">Today, websites are targeted a whopping 50 times per day, per website on average. A website compromise can be a heartbreaking and costly experience—potentially resulting in damage to your site visitors, revenue, and reputation. To protect your site and visitors, website security is essential. And, it’s affordable and easy to get started! <a href="pricing">Click here to protect your site today</a></p>
</div>
<br>
</div>
<div class="whiteSpace25"></div>
</div>

<div class="whiteSpace50"></div>
</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>