<?php
//DEFINITIONS
$title = "Affiliate Agreement";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="sourceRegular"><span class="sourceBlack">SiteLock</span> Affiliate Agreement</h1>
<div class="whiteSpace50"></div>
<br>
</div></div>

<div class="linkred" id="mainTerms">
<div class="whiteSpace50"></div>

<p>
<strong>SIGNING UP FOR SITELOCK'S AFFILIATE PROGRAM CREATES A CONTRACT BETWEEN YOU AND SITELOCK. ANY ONE OF THE FOLLOWING ACTIONS CONSTITUTES YOUR ACCEPTANCE AND AGREEMENT TO BE BOUND BY THESE TERMS AND CONDITIONS: (1) ACCEPTING THE TERMS AND CONDITIONS ELECTRONICALLY DURING THE AFFILIATE SIGN UP PROCESS, (2) YOUR USE OF THE PROGRAM DESCRIBED HEREIN. THROUGH THESE ACTIONS YOU ARE AGREEING TO BE BOUND BY THE TERMS OF THIS AGREEMENT AND ALL TERMS AND CONDITIONS INCORPORATED BY REFERENCE IN THIS AGREEMENT.</strong>
</p>
<div class="whiteSpace25"></div>

<p>
This SITELOCK AFFILIATE AGREEMENT together with any operating rules, policies, price schedules, or other supplemental documents expressly incorporated herein by reference and published from time to time (collectively, the "Agreement"), constitutes the entire agreement between SITELOCK, LLC, an Arizona limited liability company, with its principle place of business located at 8701 E. Hartford Drive, Ste. 200 Scottsdale, AZ 85255 ("SiteLock") and the party set forth in the related registration order form (herein after referred to as "Company") regarding SiteLock's Affiliate Program (as defined in Exhibit A), and supersedes all prior agreements, discussions and writings between the parties regarding the subject matter of this Agreement. For purposes of this Agreement, the term "SiteLock" includes our respective subsidiaries, affiliates, agents, employees, predecessors in interest, successors, attorneys and any other service provider that furnished services to you in connection with this agreement.
</p>
<div class="whiteSpace25"></div>

<h3>Recitals.</h3>
<p>
Sitelock owns and operates the website located at http://www.Sitelock.com (the "Site") and provides a comprehensive suite of business verification and security products to businesses, as more particularly described in Exhibit A, attached hereto and incorporated herein by this reference (the "Services"). Sitelock wishes to have Company promote the Services to Company's customers, subject to the terms and conditions of this Agreement and subject to the standard terms and conditions posted on the Site (the "Sitelock Terms of Use"). In consideration of the promises and mutual covenants contained herein, the parties hereby agree as follows:
</p>
<div class="whiteSpace25"></div>

<h3>Section 1.</h3>
<p>
<strong>Appointment.</strong> Sitelock hereby appoints Company as its nonexclusive, worldwide, authorized representative subject to all of the terms and conditions of this Agreement. Company is hereby authorized to promote the Services.
</p>
<div class="whiteSpace25"></div>

<h3>Section 2. Intellectual Property.</h3>
<p>
<strong>2.1 License Grant.</strong> Sitelock hereby grants a nonexclusive, nontransferable, nonassignable and royalty-free worldwide license to Company for the Term (as defined herein) of this Agreement, to use the name, logotype, trademarks, slogans, brochures, sales literature, and other works of authorship of Sitelock that have been or that may hereinafter be provided by Sitelock; provided, that any such use shall be subject to the prior approval of Sitelock and shall be in accordance with any reasonable guidelines for and restrictions on such use that may be provided to Company by Sitelock.
</p>

<p>
<strong>2.2 Trade Secrets.</strong> Company, during the Term of this Agreement, may have access to and become acquainted with various trade secrets of Sitelock, including but not limited to, Sitelock customer lists, sales and technical information and other confidential information as described in Section 2.3. All such Sitelock trade secrets shall remain the exclusive property of Sitelock and any use thereof by Company shall be only for the purposes of and in connection with the transactions contemplated by this Agreement. Sitelock, during the Term of this Agreement, may have access to and become acquainted with various trade secrets of Company, including but not limited to, Company customer lists, sales and technical information and other confidential information. All such Company trade secrets shall remain the exclusive property of Company and any use thereof by Sitelock shall be only for the purposes of and in connection with the transactions contemplated by this Agreement. 
</p>

<p>
<strong>2.3 Confidentiality.</strong> "Confidential Information" includes, without limitation, any and all pricing information, business plans, sales opportunities, customer lists, technical information, information regarding the marketing or promoting of any product, business policies or practices, personnel, research, development or know-how and information received from others that a party is obligated to treat as confidential, and any other information that by the nature of the circumstances surrounding the disclosure ought to be treated as proprietary and confidential. Company agrees that it will protect Sitelock's Confidential Information from unauthorized dissemination with the same degree of care that Company uses to protect its own like information, in no event using less than a reasonable degree of care. Company agrees not to use Sitelock's Confidential Information for purposes other than those necessary to directly further the purposes of this Agreement. Sitelock agrees that it will protect Company's Confidential Information from unauthorized dissemination with the same degree of care that Sitelock uses to protect its own like information, in no event using less than a reasonable degree of care. Sitelock agrees not to use Company's Confidential Information for purposes other than those necessary to directly further the purposes of this Agreement.
</p>

<p>
<strong>2.4 Non-Solicitation and Non-Competition.</strong> As noted above, information about SiteLock's customers is Confidential Information and constitutes SiteLock's valuable trade secrets. Accordingly, Company agrees that during its service and for a period of one year thereafter Company will not, either directly or indirectly, separately or in association with others, interfere with, impair, disrupt or damage SiteLock's relationship with any of its customers by soliciting or encouraging others to solicit any of them for the purpose of diverting or taking away business from SiteLock. Additionally, Company acknowledges and recognizes that SiteLock's employees are a valuable resource and the loss of employees generally tends to interfere with, impair, disrupt or damage such party's business. Accordingly, Company agrees that during the term of this Agreement and for a period of one year thereafter Company will not, separately or in association with others, solicit, encourage or attempt to hire any of SiteLock's employees, or cause others to solicit or encourage any of SiteLock's employees to discontinue their service with SiteLock.
</p>

<p>
<strong>2.5 Termination of License.</strong> In the event this Agreement is terminated for any reason whatsoever, the license and right to use trade secrets granted in this Section 2 shall immediately terminate and (x) Company shall discontinue its use (if any) of the name, logotype, trademarks, slogans and other intellectual property and Confidential Information of Sitelock and the Services, and (y) Sitelock shall discontinue its use (if any) of the name, logotype, trademarks, slogans and other intellectual property and Confidential Information of Company and any services of Company. In addition, (a) Company shall return to Sitelock or destroy all Sitelock catalogs, literature and other materials in connection with Sitelock or the Services then in the possession of Company, and (b) Sitelock shall return to Company or destroy all catalogs, literature and other materials of Company in connection herewith then in the possession of Sitelock.
</p>
<div class="whiteSpace25"></div>

<h3>Section 3. Payment/Revenue Share.</h3>
<p>
<strong>Commission.</strong> Company agrees to pricing and payment to SiteLock as outlined in Exhibit A.
</p>

<p>
<strong>3.1 Reporting.</strong> Each party collecting the fees hereunder shall provide tracking and reporting to the other party of all sign-ups and other information related to this Agreement. The non-reporting party shall have the right at that party's expense to audit any and all records of the reporting party related to this Agreement not more than twice annually and in the event there is a material difference between the amount paid to the non-reporting party under this section and the amount due as reflected in such Audit, the reporting party shall promptly pay such difference to the non-reporting party, provided that in the event such difference is >5%, the reporting party shall also pay to the non-reporting party the cost of such audit.
</p>
<div class="whiteSpace25"></div>

<h3>Section 4. Responsibilities of Sitelock.</h3>
<p>
Sitelock shall have the following responsibilities: (a) Provide Company with sales and technical information regarding the Services as reasonably necessary, including customer support for any Sitelock products that Company sells for Sitelock and (b) Inform Company of any changes in the Services, prices, terms of payment and/or listing options, which Sitelock may make in its sole discretion at any time. 
</p>
<div class="whiteSpace25"></div>

<h3>Section 5. Responsibilities of Company.</h3>
<p>
Company's duties hereunder are as follows: (a) Company shall endeavor to promote the Services as described in Exhibit A; (b) Company may advise Sitelock from time to time as to problems encountered with the Services and as to any resolutions arrived at for those problems; (c) the parties shall collaborate in connection with promotional activities of the Services; (d) Company is not permitted or authorized to offer customers any terms for the Services different from those provided to Company by Sitelock in accordance with this Agreement; and (e) Company may not make any representations or warranties concerning the Services to customers unless so authorized in writing in advance by Sitelock.
</p>
<div class="whiteSpace25"></div>

<h3>Section 6. Relationship of Company.</h3>
<p>
<strong>6.1 Independent Contractors.</strong> The parties hereto expressly understand and agree that Company is an independent contractor in the performance of each and every part of this Agreement and is solely responsible for all of its employees and agents, if any, and its labor costs and expenses arising in connection therewith and for any and all claims, liabilities, damages and debts of any type whatsoever that may arise on account of Company's activities, or those of its employees or agents, in the performance of this Agreement. Sitelock is in no manner associated or otherwise connected with the actual performance of this Agreement on the part of Company, nor with Company's employment of other persons or incurring of other expenses. Company shall have no right, power, or authority to bind or obligate Sitelock in any manner whatsoever or to affix its name or signature on behalf of Sitelock unless expressly authorized to do so by Sitelock in writing.
</p>

<p>
<strong>6.2 Taxes.</strong> Company shall assume full responsibility for the proper reporting and payment of all federal, state and local taxes, contributions and/or special levies imposed or required under unemployment insurance, social security, income tax (state and federal), and/or other laws or regulations, with respect to the performance by Company and its employees and agents of this Agreement. Sitelock hereby represents and warrants that, to the best of its knowledge, no taxes apply to the sale of the Services to customers. Sitelock shall notify Company in writing in the event any such Taxes become due, in which event Company shall have the right to terminate this Agreement by notice in writing to Sitelock. For the avoidance of doubt, Sitelock shall be fully responsible for payment of any and all taxes related to the revenues or net income of Sitelock.
</p>

<p>
<strong>6.3 Approvals.</strong> Each party shall obtain such authorizations, licenses, and other governmental or regulatory agency approvals as are required for its performance of this Agreement.
</p>
<div class="whiteSpace25"></div>

<h3>Section 7. Disclaimer; Limitation on Liability.</h3>
<p>
<strong>7.1 Disclaimer.</strong> The Services ARE provided on an "As Is" basis and as available, without any warranty or representation of any kind, whether express or implied. Sitelock expressly disclaims any and all warranties, EXPRESS, or implied, including without limitation the implied warranties of merchantability, fitness for a particular purpose and noninfringement with respect to the Site or the Services. UNDER NO CIRCUMSTANCES WILL THE COMPANY OR ANY OTHERS INVOLVED IN CREATING THE SERVICES BE LIABLE FOR ANY DAMAGES OR INJURY, INCLUDING ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, PUNITIVE DAMAGES, OR FOR ANY DAMAGES FOR LOSS OF REVENUE OR PROFITS, OR OTHER DAMAGES RESULTING FROM ANY CIRCUMSTANCE INVOLVING THE SERVICES (INCLUDING BUT NOT LIMITED TO DAMAGES OR INJURY CAUSED BY ERROR, OMISSION, INTERRUPTION, DEFECT, FAILURE OF PERFORMANCE, MISDIRECTED OR REDIRECTED TRANSMISSIONS, FAILED INTERNET CONNECTIONS, UNAUTHORIZED USE OF THE WEBSITE, LOST DATA, DELAY IN OPERATION OR TRANSMISSION, BREACH OF SECURITY, LINE FAILURE, DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF ANY USER OF THE WEBSITE, OR COMPUTER VIRUS, WORM, TROJAN HORSE OR OTHER HARMFUL COMPONENT), EVEN IF CUSTOMER HAS ADVISED THE COMPANY IN ADVANCE OF THE POSSIBILITY OF SUCH DAMAGE.
</p>

<p>
<strong>7.2 Limitation of Liability.</strong> By entering into this Agreement, Company expressly acknowledges that Sitelock's liability is specifically limited to amounts paid by Sitelock to Company, OR THE AMOUNTS RETAINED BY COMPANY, FOR THE PREVIOUS TWELVE MONTH PERIOD. Company hereby waives all rights of recovery of any damages, whether actual or special, punitive or consequential, that Company may incur over and above said amounts, including, without limitation, damages for Negligence, lost data, use, profits, income, savings, loss of or damage to property, PERSONAL injury, goodwill or any claims of third parties whatsoever with respect to the Site or the Services. Some states do not allow the exclusion or limitation of liability for consequential or incidental damages, so the above exclusions may not apply to all parties; in such states liability is limited to the fullest extent permitted by law.
</p>
<div class="whiteSpace25"></div>

<h3>Section 8. Term; Termination.</h3>
<p>
<strong>8.1 Term.</strong> This Agreement shall be in full force and effect for an initial term of twelve (12) months from the Effective Date ("Term"), and such Term and each Term thereafter shall automatically extend for additional twelve (12) month periods.
</p>

<p>
<strong>8.2 Termination; Material Breach.</strong> Either party will have the right to terminate this Agreement for any material breach that is not cured within thirty (30) days after written notice of such breach; provided, however, that if Company breaches this agreement by marketing or promoting products and services (that are not the Services) to any current customer or prospect of SiteLock, or commits fraud, theft or intentional misconduct against SiteLock or any SiteLock customer, then SiteLock may immediately terminate this agreement. In the event Sitelock terminates this Agreement for material breach or Company's insolvency, Company shall perform all acts necessary to allow Sitelock to continue providing to Company's customers who have purchased the Services ("Company's Customers"), including assigning to Sitelock its rights to receive any amounts owing by Company's Customers for such. 
</p>
<div class="whiteSpace25"></div>

<h3>Section 9. Survival.</h3>
<p>
The following sections shall survive the termination of this Agreement: Sections 2 (Intellectual Property), 3 (Payment/Revenue Share), 7 (Limitation on Liability), 8 (Term; Termination), 10 (Indemnity), 12 (Amendment and Assignment), 13 (Severability), 14 (Notice), 15 (Governing Law), and 18 (Dispute Resolution; Arbitration).
</p>
<div class="whiteSpace25"></div>

<h3>Section 10. Indemnity.</h3>
<p>
The Parties agree that each shall at all times at its expense defend any action, litigation or claim against the other Party, its employees and agents: (a) arising out of or related to the negligence of such Party, its employees and agents, or (b) arising out of or related to acts of Such Party that are beyond the scope of or in breach of this Agreement, or (c) arising out of or related to any warranties or representations made by a Party not authorized by the other, and the Parties agree to indemnify and hold the other Party, its employees and agents harmless against all costs and damages adjudged against them or any of them in any such action, litigation or claim as aforesaid; provided, however, that such Party shall have notified the other Party of any such action, litigation or claims against it, its employees or agents. The parties agree that if it fails to defend any such action, litigation or claim against the other Party, its employees and agents, after such notice, such Party and its employees and agents may defend such action, litigation or claim and such Party agrees to compensate the defending Party its employees and agents for any resulting fees, expense, damages or settlement.
</p>
<div class="whiteSpace25"></div>

<h3>Section 11. Amendment and Assignment.</h3>
<p>
This Agreement constitutes the entire agreement between the parties hereto and cancels all previous arrangements and agreements between the parties, and there are no understandings or agreements relative to the subject matters of this Agreement that are not fully expressed herein. This Agreement may be amended only in writing, and, except as expressly provided in this Agreement with regard to amendments to the Appendices hereof, only upon mutual consent of the parties. Neither party shall have the right to assign, sublicense or otherwise transfer (collectively, "assign") its rights or obligations under this Agreement without the other party's prior written consent, which consent shall not be unreasonably withheld, provided, however, that an assignment (in whole, but not in part) to an affiliated entity or to a third party acquiring substantially all of the assignor's stock or assets will be permitted without the other party's consent so long as (a) the assignee agrees to be bound by the terms herein, and (b) the assignee is not a direct competitor of the non-assigning party.
</p>
<div class="whiteSpace25"></div>

<h3>Section 12. Severability.</h3>
<p>
If any term, provision, covenant, or condition of this Agreement is held by a court of competent jurisdiction to be invalid or unenforceable, the remainder of the provisions hereof shall remain in full force and effect and shall not be affected, impaired or invalidated in any way.
</p>
<div class="whiteSpace25"></div>

<h3>Section 13. Notice.</h3>
<p>
Except as otherwise provided in this Agreement, all communications, notices and demands of any kind which either party may be required or may desire to give the other party shall be in writing and delivered by personal service to the other party, or by overnight courier, facsimile or email, each with confirmation of delivery. Notice shall be considered given when delivered in person, on the seventh day after being deposited in the United States mail or upon confirmation of the delivery of the facsimile or email. Notices shall be sent to the addresses provided at the beginning of this agreement. 
</p>
<div class="whiteSpace25"></div>

<h3>Section 14. Governing Law.</h3>
<p>
This Agreement will be governed and construed in accordance with the laws of the State of Arizona without giving effect to principles of conflict of laws.
</p>
<div class="whiteSpace25"></div>

<h3>Section 15. Duly Authorized Representative.</h3>
<p>
Anyone signing this Agreement on behalf of an entity represents and warrants that he or she has the express authority to do so.
</p>

<h3>Section 16. Further Assurances.</h3>
<p>
Each party hereto agrees to execute, acknowledge, deliver, file, and record such further certificates, amendments, instruments and documents, and to do all such other acts and things, as may be required by law or as may be necessary, advisable, or convenient to carry out the intent and purpose of this Agreement. 
</p>
<div class="whiteSpace25"></div>

<h3>Section 17. Counterparts.</h3>
<p>
This Agreement may be executed in any number of counterparts and by the parties hereto in separate counterparts, each of which when so executed shall be deemed to be an original and all of which taken together shall constitute one and the same agreement. 
</p>
<div class="whiteSpace25"></div>

<h3>Section 18. Dispute Resolution; Arbitration.</h3>
<p>
If any dispute arises under this Agreement, the parties shall make a good faith effort to resolve the dispute before taking any action. The parties shall meet to discuss the dispute no later than thirty (30) days after either party gives written notice to the other party that such a dispute exists. Such meeting may be held telephonically if travel is impractical for either party. At such meeting, an officer of each of the parties who has authority to resolve the dispute shall be in attendance. No action, suit, arbitration or other proceeding may be commenced (other than the collection of money due on unpaid undisputed invoices and other than any injunctive relief expressly provided for in this Agreement) before the parties have met pursuant to this provision unless immediate injunctive relief is being sought, in which case the noted meeting shall take place at the earliest opportunity after such immediate injunctive relief is sought. All disputes, controversies, or differences which may arise between the parties hereto, out of or in relation to or in connection with this Agreement shall be finally settled by arbitration in Phoenix, Arizona in accordance with the commercial arbitration rules of the American Arbitration Association. The award rendered by the arbitrator(s) shall be final and binding upon the parties hereto. In the event of any arbitration proceedings hereunder, each party agrees to bear its own reasonable fees, costs and expenses in connection with such proceedings, provided that upon the conclusion of any such arbitration proceeding, in addition to any award granted by the arbitrator(s), the prevailing party shall have their reasonable fees, costs and expenses reimbursed by the other party. 
</p>

<p>
<strong>EXHIBIT A TO<br>
AFFILIATE AND PROMOTION AGREEMENT AMONG<br>
SITELOCK, LLC AND COMPANY</strong>
</p>

<p>
<i>Affiliate Program Description</i><br>
The SiteLock Affiliate program will allow Companies to generate commissions for each referred new customer. Banner ads and other creatives within the affiliate dashboard, will allow each sale to be tracked according to each affiliate. Companies will be paid a set commission for each conversion. SiteLock will host the main affiliate ecommerce page and handle billing for each package purchased. 
</p>

<p>
<i>Pricing and Payment:</i><br>
<strong>1.) Pricing</strong><br>
<table class="table text-center">
<thead class="thead-dark"><tr><th scope="col">SiteLock Package(s)</th><th scope="col">Commissionable Payouts ($US)- Annual</th><th scope="col">Customer Annual Prices($US)</th></tr></thead>
<tbody>
<tr><th scope="col">SiteLock Fix<br>(Premium Scan) - Annual Subscription </th><th scope="col">$27.50 per unit</th><th scope="col">$109.99 Annually</th></tr>
<tr><th scope="col">SiteLock Accelerate<br>(Premium Scan + Professional WAF/CDN)</th><th scope="col">$75.00 per unit</th><th scope="col">$299.99 Annually</th></tr>
<tr><th scope="col">SiteLock Prevent<br>(Premium Scan + Premium WAF/CDN) </th><th scope="col">$125.00 per unit</th><th scope="col">$499.99 Annually</th></tr>
<tr><th scope="col">SiteLock 911<br>One-time clean</th><th scope="col">$55.00 per unit</th><th scope="col">$219.99 One-time payment</th></tr>
</tbody>
</table>
</p>

<p>
Company acknowledges that pricing to end users of SiteLock products influences SiteLock's brand reputation and therefore Company agrees to the "Customer Annual Prices" described in the table above. Any pricing set below that listed in the above table must be approved by SiteLock, as it may negatively influence SiteLock's brand in the marketplace.
</p>

<p>
<strong>2.) Payment</strong><br>
a. SiteLock will pay Company the Commissionable Payouts described above for sales generated from customers who purchase one or more of the above SiteLock packages from the SiteLock Affiliate Page, located at <a href="https://www.sitelock.com/ap/affiliate-plans">https://www.sitelock.com/ap/affiliate-plans</a> but only after a Company has generated $250 minimum requirement in commissionable sales in a month. If the $250 minimum requirement is not met in a given month, then the balance in such month will carry forward to the following month.
</p>

<p>
b. Commissionable Payouts will only be paid on sales generated from the SiteLock Affiliate Page, located at <a href="https://www.sitelock.com/ap/affiliate-plans">https://www.sitelock.com/ap/affiliate-plans</a>
</p>

<p>
c. Company acknowledges that subsequent annual billing cycles will not be credited to the Company for SiteLock customers who sign up directly through the dedicated affiliate page (only the initial sign-up year), and billing for these accounts will be solely handled by SiteLock. 
</p>

<p>
d. Company acknowledges any product upgrades by SiteLock Expert services will not be credited to the Company. 
</p>

<p>
e. Any cancels requested by SiteLock customers will be processed by SiteLock at the sole discretion of SiteLock if such cancel request is received by SiteLock within the initial 48 hours of purchase. Company acknowledges that any cancellations will reduce commissionable sales.
</p>

<p>
f. Commission payments will be made monthly on the 20th day of the month following the month commissionable sales are recorded.
</p>
<div class="whiteSpace25"></div>
</div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>