<?PHP
$term = $_POST['term'];
$term = htmlspecialchars($term);
//SEARCH
if (preg_match('/product/',$term)) {
    $result = '<a href="">website-scanning</a><br><a href="">DDOS</a><br><a href="">Web Application Firewall</a><br><a href="">Malware Removal</a><br><a href="">CMS Patching</a>';
} else if (preg_match('/scanning/',$term)) {
    $result = "website-scanning";
} else {
    $result = "hmm, we can't find a matching page. Try contacting us";
}

//DEFINITIONS
$title = "Search SiteLock | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>
<br><br>
<div class="headerspace" id="mainGrey"><div class="linkred" id="mainBody">
<br>
<h1 class="sourceBlack">Search SiteLock.com</h1>
<form method="post" action="search-sitelock">
<input class="form-control" type="text" name="term" placeholder="Enter Keyword" autocomplete="off"><button type="submit" class="btn btn-blue float-right" style="font-size: 14px !important;">Search <i class="far fa-search"></i></button>
</form>
</div>
<div class="linkred" id="mainBody">
<hr>
<p class="font18"><strong class="font25">You searched for:</strong><br><?PHP echo $term;?></p>
<hr>
<p class="font18"><strong class="font25">These results may help you:</strong><br> <?PHP echo $result;?></p>
<hr>
</div>
<div id="mainGrey"><div id="mainBody">
<strong class="font25">Still need help?</strong>

<div class="row">
<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/chatBubbleBlue.svg" alt="Scanning Cloud"><h4 class="sourceLight">See the latest at the<br>SiteLock Blog</h4><br><a class="btn btn-ghost-blue" href="https://sitelock.com/blog">Get in the Know</a></div><br></div>
</div>

<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/lightbulbRed.svg" alt="Scanning Cloud"><h4 class="sourceLight">Check out our<br>Resources Center</h4><br><a class="btn btn-ghost-blue" href="learn-more">Start Exploring</a></div><br></div>
</div>

<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/qsrRed.svg" alt="Scanning Cloud"><h4 class="sourceLight">Download the SiteLock<br>Website Security Insider</h4><br><a class="btn btn-ghost-blue" href="learn-more">Read the Report</a></div><br></div>
</div>
</div>

<div class="whiteSpace50"></div>

</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>

