<?php
//DEFINITIONS
$title = "Business Solutions | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div id="mainBody">
<div class="whiteSpace25"></div>
<div class="row">
<div class="col-lg-6 my-auto solutionHead">
<h1 class="sourceBlack">Enhance your business with guaranteed security.</h1>
<h3>Select the only solution that automatically protects and enhances your business website.</h3>
<a class="btn btn-blue" href="#onpagepricing">Explore Plans</a>
</div>
<div class="col-lg-6">
<img class="headerimgmain" src="/img/business-solutions/businessHeader.png" alt="Business Solutions">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<?php include 'includes/rating-bar.php';?>

<div id="mainBody">
<div class="whiteSpace50"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding sourceLight"><div id="content"><h2><span class="sourceBlack">Website security</span> at a glance</h2>Be in control of your website with website security. SiteLock solutions are designed to monitor your website each day for malicious or suspicious activity. When threats are found, they’re automatically blocked or fixed, and you are notified immediately—allowing you to remain in charge.<br><a class="btn btn-red font-weight-bold" href="#onpagepricing">Gain Control of Your Site Now</a></div></div>
   <div class="col-lg-5 splitPadding footMobileHide"><img class="splitImg" src="/img/business-solutions/businessCloud.svg" alt="Scanning Calendar"></div>
  </div>
</div>

<div class="whiteSpace50"></div>
</div>

<?php include 'includes/cms-bar.php';?>

<div id="mainBody">
<div class="whiteSpace50"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-md-5 splitPadding"><img class="splitImg" src="/img/siteCart.svg" alt="Scanning Alert"></div>
  <div class="col-md-7 whitebk splitPadding"><div id="content"><h2><span class="sourceBlack">Protect</span> <span class="sourceRegular">your business<br>and customers</span></h2>Never let a website compromise disrupt your business or interfere with your success. Today, two-thirds of all cyberattacks are directed at smaller businesses. Cybercriminals are after money, customer information, traffic, and your site resources, like bandwidth. The good news is, there are fast and affordable ways to protect your site. SiteLock will automatically find, fix, and prevent cyberattacks—so your business, customers, and success are always safe.<br><a class="btn btn-red font-weight-bold" href="#onpagepricing">Find and Fix Threats Now</a></div></div>
 </div>
 </div>

<div class="whiteSpace50"></div>
</div>

<div id="mainGrey">
<?php include 'includes/news-bar-1.php';?>
</div>

<div id="onpagepricing"></div>
<div id="mainBody">
<div class="whiteSpace100"></div>
<h1 class="text-center font55 sourceBlack">Protect your website today!</span></h1>
<?php include 'includes/price-bar-1.php';?>

<?php include 'includes/includes-bar.php';?>
</div>

<?php include 'includes/solutions-bar.php';?>

<?php 
$benefitsTitle = '<h1 class="text-center font55"><span class="sourceBlack">Website security</span> benefits for your business</h1>'; 
$icon1= 'fa-umbrella'; $title1 = 'Peace of mind'; $content1 = 'Your business won’t suffer from a website compromise. SiteLock is the only website security provider to offer automated malware removal and vulnerability patching.';
$icon2 = 'fa-chart-line'; $title2 = 'Boost SEO'; $content2 = 'Protect your SEO success with malware removal. If search engines find malware on your site, it could be blacklisted and temporarily removed from results.';
$icon3 = 'fa-lock'; $title3 = 'Keep customer data safe'; $content3 = 'Protect your most important asset—your customers. Your web application firewall will block bad traffic from entering your site and stealing customer information.';
$icon4 = 'fa-rocket'; $title4 = 'Increase website speed'; $content4 = 'Increase your site speed by as much as 50 percent. Your web application firewall comes with a content delivery network (CDN) proven to decrease load time.';
$icon5 = 'fa-shopping-cart'; $title5 = 'Increase conversions'; $content5 = 'Increase conversions by showcasing a SiteLock Trust Seal. Today, 79 percent of online shoppers expect to see a trust badge. Your website scanner comes with a SiteLock Trust Seal to instill trust.';
$icon6 = 'fa-user-graduate'; $title6 = 'Always informed'; $content6 = 'Stay updated on the security of your website with automated alert emails and by viewing your security results at any time from your SiteLock Dashboard.';
$benefitsBTN = 'Protect Your Website Now';

include('includes/benefits-bar.php');
?>
<div id="mainBody">
<div class="whiteSpace50"></div>
<div id="mainSplitLeft">
    <div class="row splitSpace">
     <div class="col-lg-5 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2 class="sourceBlack">How SiteLock works with your hosting provider</h2>Your site is hosted in a secure server environment—however, you are responsible for protecting your website within that server. Learn the difference between a secure server and a secure website, and why both are essential.</div></div>
     <div class="col-lg-7 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/209592176" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
    </div>
  </div>
<div class="whiteSpace50"></div>
</div>

<?php include 'includes/awards-bar.php';?>

<div id="mainBody">

<h1 class="text-center sourceBlack font55">Website security products for your business</h1>
<h3 class="text-center text25">Learn about the products that protect your site</h3>

<div class="row">
<div class="col-lg-3">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningCloud.svg" alt="Scanning Cloud">
  <h5 class="sourceBlack text-center">SiteLock SMART&trade;</h5>
  <p class="text-center">Automated malware removal</p>
  <ul>
  <li>Daily website scanning</li>
  <li>Find malware and vulnerabilities</li>
  <li>Automatically remove malware in site files</li>
  <li>Receive immediate threat alerts</li>
  </ul>
  <br><br><p class="text-center"><a class="btn btn-ghost-grey" href="website-scanning">Learn More</a></p></div></div>
</div>

<div class="col-lg-3">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningThreat.svg" alt="Scanning Threat">
  <h5 class="sourceBlack text-center">SiteLock INFINITY&trade;</h5>
  <p class="text-center">Automated vulnerability patching</p>
  
  <ul>
  <li>Automatically remove malware</li>
  <li>Remove malware in WordPress databases</li>
  <li>Automatically patch CMS vulnerabilities</li>
  <li>24/7 access to SiteLock engineers</li>
  </ul>
<br><p class="text-center"><a class="btn btn-ghost-grey" href="vulnerability-patching">Learn More</a></p></div></div>
</div>

<div class="col-lg-3">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningWAF.svg" alt="Scanning WAF">
  <h5 class="sourceBlack text-center">SiteLock TrueShield&trade;</h5>
  <p class="text-center">Web application firewall</p>
  <ul>
  <li>Block bad bots</li>
  <li>Block comment spam</li>
  <li>Block direct hack attempts</li>
  <li>Increase website speed</li>
  <li>Receive traffic statistics<br>and reports</li>
  <li>Supports SSL enabled sites</li>
  </ul>
  <br><br><p class="text-center"><a class="btn btn-ghost-grey" href="web-application-firewall">Learn More</a></p></div></div>
</div>

<div class="col-lg-3">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningWebsite.svg" alt="Scanning Website">
  <h5 class="sourceBlack text-center">PCI Compliance</h5>
  <p class="text-center">Ecommerce protection</p>
  <ul>
  <li>Protects payment card data</li>
  <li>Simple self-assessment questionnaire</li>
  <li>Quarterly vulnerability<br>scans included</li>
  <li>Custom fix-it plans</li>
  </ul>
  <br><br><br><p class="text-center"><a class="btn btn-ghost-grey" href="pci-compliance">Learn More</a></div></p></div>
</div>
</div>

<div class="whiteSpace100"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-lg-5 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2><span class="sourceBlack">Get your complimentary website risk assessment</h2>The factors that make your website unique and engaging could also be putting it at risk of compromise. That’s why SiteLock created a proprietary Risk Assessment to offer education on how likely your website is to be compromised.<br><a class="btn btn-blue" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Get My Free Risk Assessment</a></div></div>
  <div class="col-lg-7 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/273777919" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
 </div>
</div>

<div class="whiteSpace100"></div>

  <div id="mainSplitRight">
    <div class="row splitSpace">
     <div class="col-md-5 whitebk splitPadding"><img class="splitImgAlt" src="/img/wordpress/amandNaor.png" alt="Bill Kasal"></div>
     <div class="col-md-7 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2 class="sourceBlack">Meet this security superhero: </h2>Amanda of amandanaorphotography.com is known for her documentary-style photos of children, families, and “the beauty of the everyday.” Her website ran smoothly for years until it was attacked by cybercriminals—twice in a row. Amanda called SiteLock and saved her website within a few hours.<p><br><i>"To see my site so deformed was heartbreaking. I never thought this could happen to me. If you have a website, you need to have website security because anyone is susceptible!"</i></p><a class="btn btn-ghost-blue font-weight-bold" href="https://www.sitelock.com/blog/2018/05/case-study-amanda-naor-photography/">Read More</a></div></div>
    </div>
  </div>

<div class="whiteSpace50"></div>
</div>

<?php include 'includes/qsr-bar.php';?>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<?php
//FORM DEFINITIONS
$hb_title = "Secure Your Website";
$hb_phone = "(855) 378-6200";
$hb_phone_link = "8553786200";
$hb_btn = 'Get Your Assessment';
$hb_target = '';
include 'includes/forms/high-barrier.html';
?>

</body>
</html>