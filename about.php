<?php
//DEFINITIONS
$title = "About SiteLock | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1><span class="sourceBlack">The SiteLock Story</span></h1>
<h3>SiteLock is the global leader in website security. We founded the company in 2008 with a passion to make website security affordable and accessible to small businesses, a previously underserved market. Fast forward to the present, we protect over 12 million sites around the world-and honestly, we're just getting started!</h3>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-md-6 my-auto">
<br><img class="img90" src="/img/logos/SiteLock_logo_cloud.svg" alt="SiteLock Logo Cloud">
</div>

<div class="col-md-6">
<h1 class="font55">Our <span class="sourceBlack">mission</span></h1>
<p class="font25">It is our mission to protect every website on the internet and create a world where each of us, our communities, and our customers can flourish.</p>
</div>
</div>
<div class="whiteSpace50"></div>
</div>

<?php include 'includes/awards-bar.php';?>

<div id="mainBody">
<div class="whiteSpace50"></div>
<div id="mainSplitLeft">
    <div class="row splitSpace">
     <div class="col-md-5 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2 class="sourceBlack">The SiteLock experience</h2>We’re committed to helping website owners & delivering exceptional service along the way. Let us walk you through the SiteLock experience.</div></div>
     <div class="col-md-7 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/259753291" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
    </div>
  </div>
<div class="whiteSpace50"></div>
</div>

<div id="mainBody">
<?php include 'includes/news-bar-1.php';?>
</div>

<?php include 'includes/kidsfund-bar.php';?>

<div class="whiteSpace50"></div>
<div id="mainTerms">
<h2 class="text-center sourceBlack font55">Meet the Team</h2>

<!--<div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/259753291" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>-->

<div class="row linkabout text-center">
<div class="col-md-3"><a href="m1" data-toggle="modal" data-target="#m1"><img class="aboutImg img80" src="img/about/icon-TomH.svg" alt=""></a><span class="fontReset">Tom Heiser</span><br><br></div>

<div class="col-md-3"><a href="m2" data-toggle="modal" data-target="#m2"><img class="aboutImg img80" src="img/about/icon-Neill.svg" alt=""></a><span class="fontReset">Neill Feather</span><br><br></div>

<div class="col-md-3"><a href="m3" data-toggle="modal" data-target="#m3"><img class="aboutImg img80" src="img/about/icon-Scott.svg" alt=""></a><span class="fontReset">Scott Lovell</span><br><br></div>

<div class="col-md-3"><a href="m4" data-toggle="modal" data-target="#m4"><img class="aboutImg img80" src="img/about/icon-Brian.svg" alt=""></a><span class="fontReset">Brian Sargent</span><br><br></div>

<div class="col-md-3"><a href="m5" data-toggle="modal" data-target="#m5"><img class="aboutImg img80" src="img/about/icon-Blake.svg" alt=""></a><span class="fontReset">Blake Rodgers</span><br><br></div>

<div class="col-md-3"><a href="m6" data-toggle="modal" data-target="#m6"><img class="aboutImg img80" src="img/about/icon-James.svg" alt=""></a><span class="fontReset">James Murphy</span><br><br></div>

<div class="col-md-3"><a href="m7" data-toggle="modal" data-target="#m7"><img class="aboutImg img80" src="img/about/icon-TomS.svg" alt=""></a><span class="fontReset">Tom Serani</span><br><br></div>

<!--<div class="col-3"><a href="m8" data-toggle="modal" data-target="#m8"><img class="aboutImg img80" src="img/about/icon-Norm.svg" alt=""></a></div>-->
</div>

</div>

<div class="whiteSpace50"></div>

<?php include 'includes/rating-bar.php';?>

<div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/chatBubbleBlue.svg" alt="Scanning Cloud"><h4 class="sourceLight">See the latest at the<br>SiteLock Blog</h4><br><a class="btn btn-ghost-blue" href="https://sitelock.com/blog">Get in the Know</a></div><br></div>
</div>

<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/lightbulbRed.svg" alt="Scanning Cloud"><h4 class="sourceLight">Check out our<br>Resources Center</h4><br><a class="btn btn-ghost-blue" href="resources">Start Exploring</a></div><br></div>
</div>

<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/qsrRed.svg" alt="Scanning Cloud"><h4 class="sourceLight">Download the SiteLock<br>Website Security Insider</h4><br><a class="btn btn-ghost-blue" href="security-report">Read the Report</a></div><br></div>
</div>
</div>

<div class="whiteSpace50"></div>
</div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

<!-- MODALS -->
<!-- M1 -->
<div class="modal fade" id="m1" tabindex="-1" role="dialog" aria-labelledby="m1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title" id="m1"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="col-md-5 my-auto text-center"><img src="img/about/icon-TomH.svg" alt=""></a></div>
      <div class="col-md-7">
        <h5>Tom Heiser</h5>
        <h6 class="sourceBlack">Chairman & CEO</h6>
        Tom Heiser is Chairman and CEO of SiteLock, the leading provider of website security solutions for business. Heiser is a seasoned technology leader with more than 30 years of experience accelerating innovation and scaling high-growth companies, ranging from startups to complex multinational businesses. He has held notable positions throughout his career including EVP of EMC Corporation, president of RSA Security, and CEO of ClickSoftware. In addition to SiteLock, Heiser also sits on the board of directors for multiple technology companies, including Arxan, AtScale, and NuSpire, and formerly sat on the board of directors for CyberArk and ZSclaer.
      </div>
      </div>
      
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div></div>

  <!-- M2 -->
<div class="modal fade" id="m2" tabindex="-1" role="dialog" aria-labelledby="m2" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title" id="m2"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="col-md-5 my-auto text-center"><img src="img/about/icon-Neill.svg" alt=""></a></div>
      <div class="col-md-7">
        <h5>Neill Feather</h5>
        <h6 class="sourceBlack">CIO</h6>
        As chief innovation officer for SiteLock, Neill will fuel the company’s growth strategy through aggressive organic and inorganic expansion of product capabilities. Neill has over 20 years of experience in the technology and systems industry, notably providing technology solutions and industry insights for Johnson & Johnson prior to joining SiteLock. Neill holds B.S. degrees in Statistics & Information Systems and International Business from the Pennsylvania State University, and an MBA from the University of Pennsylvania's Wharton School of Business.
      </div>
      </div>
      
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div></div>

    <!-- M3 -->
<div class="modal fade" id="m3" tabindex="-1" role="dialog" aria-labelledby="m3" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title" id="m3"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="col-md-5 my-auto text-center"><img src="img/about/icon-Scott.svg" alt=""></a></div>
      <div class="col-md-7">
        <h5>Scott Lovell</h5>
        <h6 class="sourceBlack">CTO</h6>
        Scott Lovell is the director of engineering at SiteLock. One of the founding members of the SiteLock team, Scott runs the technology infrastructure and product enhancement branch of SiteLock. With more than 14 years in the shared hosting environment as a programmer, software architect and manager, Scott&#39;s expertise lies in performance optimization, database management, code security and the internet-based environment. Scott holds degrees in Computer Science and Psychology from Clark University.
      </div>
      </div>
      
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div></div>

    <!-- M4 -->
    <div class="modal fade" id="m4" tabindex="-1" role="dialog" aria-labelledby="m4" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title" id="m4"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="col-md-5 my-auto text-center"><img src="img/about/icon-Brian.svg" alt=""></a></div>
      <div class="col-md-7">
        <h5>Brian Sargent</h5>
        <h6 class="sourceBlack">VP of Product</h6>
        Brian Sargent is the vice president of product at SiteLock. With more than 15 years of experience leading product teams, Brian is responsible for establishing strategic product direction to drive the business forward. Prior to SiteLock, Brian was vice president of product management at Docutech.
      </div>
      </div>
      
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div></div>

    <!-- M5 -->
    <div class="modal fade" id="m5" tabindex="-1" role="dialog" aria-labelledby="m5" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title" id="m5"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="col-md-5 my-auto text-center"><img src="img/about/icon-Blake.svg" alt=""></a></div>
      <div class="col-md-7">
        <h5>Blake Rodgers</h5>
        <h6 class="sourceBlack">VP of Marketing</h6>
        Blake Rodgers is the senior vice president of marketing at SiteLock. At SiteLock, Blake leads the go-to-market strategy, and enhances the product roadmap to increase the company&#39;s market footprint. With 20 years in the product management, brand development, and operations leadership space, Blake holds expertise in product development and integrated marketing management. Prior to joining SiteLock, he was a global marketing director at Johnson &amp; Johnson, where he spent almost 10 years bringing new products to market and driving growth. He holds a degree in Public Affairs from Indiana University, and an MBA from Franklin University.
      </div>
      </div>
      
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div></div>

    <!-- M6 -->
    <div class="modal fade" id="m6" tabindex="-1" role="dialog" aria-labelledby="m6" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title" id="m6"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="col-md-5 my-auto text-center"><img src="img/about/icon-James.svg" alt=""></a></div>
      <div class="col-md-7">
        <h5>James Murphy</h5>
        <h6 class="sourceBlack">VP of Sales</h6>
        James is the vice president of sales for SiteLock. As a seasoned sales leader with over 12 years of leadership experience, James has worked with some of Arizona’s most recognizable entrepreneurial organizations. He has a strong history of building and managing profitable sales organizations that drive exponential growth. James has been influential in creating a winning culture inside each sales organization he has had the honor to be a part of. Early in his career, James was an integral member of a sales organization that experienced one of the largest mergers in the web hosting industry, which eventually led to an IPO.
      </div>
      </div>
      
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div></div>

    <!-- M7 -->
    <div class="modal fade" id="m7" tabindex="-1" role="dialog" aria-labelledby="m7" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title" id="m7"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="col-md-5 my-auto text-center"><img src="img/about/icon-TomS.svg" alt=""></a></div>
      <div class="col-md-7">
        <h5>Tom Serani</h5>
        <h6 class="sourceBlack">Chief Channel Officer</h6>
        Tom Serani is the chief channel officer at SiteLock, where he is responsible for overall channel strategy, execution, and profitability. Tom has more than 20 years of experience in the internet and security technology field, and leverages this in his role at SiteLock, where he develops and maintains channel partnerships with leaders in key growth markets. His track record is built on driving multi-million-dollar growth and global expansion at established and startup technology companies, including GeoTrust, VeriSign, RatePoint, EMC, and CoreSecurity. Tom has also served on multiple advisory boards for cloud-based technology and online security related companies. He holds a degree in Communications from the University of Massachusetts.
      </div>
      </div>
      
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div></div>

    <!-- M8 -->
    <div class="modal fade" id="m8" tabindex="-1" role="dialog" aria-labelledby="m8" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title" id="m8"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fal fa-times-circle"></i>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="col-md-5 my-auto text-center"><img src="https://via.placeholder.com/160x170" alt=""></a></div>
      <div class="col-md-7">
        <h5>Norm Bunton</h5>
        <h6 class="sourceBlack">Support Services</h6>
        Neill Feather is the president of SiteLock. Neill leads the company’s approach to 360-degree domain security by providing industry analysis and using rapidly evolving data sets related to security and hacking trends. Neill has over 20 years of experience in the technology and systems industry, notably providing technology solutions and industry insights for Johnson & Johnson prior to joining SiteLock. Neill holds B.S. degrees in Statistics & Information Systems and International Business from the Pennsylvania State University, and an MBA from the University of Pennsylvania’s Wharton School of Business.
      </div>
      </div>
      
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div></div>

</body>
</html>
