<!DOCTYPE html>
<html lang="en">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<head>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5DMG67');</script>
  <!-- End Google Tag Manager -->
<?php include($root.'/includes/gt.php'); ?>
<title>Purchasing Error</title>
<link rel="icon" type="image/ico" href="favi.ico">
<link rel="canonical" href="https://www.sitelock.com/checkout" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/fontawesome-all.min.css" rel="stylesheet">
<link href="css/checkout.css" rel="stylesheet">

<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=321148,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
/* DO NOT EDIT BELOW THIS LINE */
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');
if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body>

<div id="content">
<div class="row" style="margin-top: 15px; margin-bottom: 15px;">
<div class="col-sm-2"></div>

<div class="col-sm-8">
  <img class="img-fluid imgCenter" src="img/logos/SiteLock_red.svg" alt="SiteLock Logo" style="max-width: 274px;">
  <p class="text-center" style="margin-top: 10px; font-size:14px;">International: (415) 390-2500 | Toll-Free: (877) 257-9263</p>
</div>

<div class="col-sm-2">
<div class="float-right" id="7d494ce9-2e39-4a4f-a1ee-be6081ee3bd1">
<script type="text/javascript" src="//privacy-policy.truste.com/privacy-seal/SiteLock,-LLC/asc?rid=7d494ce9-2e39-4a4f-a1ee-be6081ee3bd1"></script><a href="//privacy.truste.com/privacy-seal/SiteLock,-LLC/validation?rid=3fc6f9d5-9959-423e-a8a7-b516ce9bdef4" title="TRUSTe European Safe Harbor certification" target="_blank"><img style="border: none" src="//privacy-policy.truste.com/privacy-seal/SiteLock,-LLC/seal?rid=3fc6f9d5-9959-423e-a8a7-b516ce9bdef4" alt="TRUSTe European Safe Harbor certification"></a>
</div>
</div>
</div>


</div>

<div class="linkblue" id="main">
<div class="checkoutPadding text-center" id="content">
<br>
<h1>Uh-Oh - it's a System Error</h1>
<h2>Something didn’t work right with your online order.</h2>
<br>
<h3>Please call us at <a href="tel:8447737558">844-773-7558</a> so</h3><h3>we can help you secure your website.</h3>
<br><br><br><br>
<br><br><br><br>
</div>
</div>

<div id="footer"></div>

</body>
</html>