<?php
//DEFINITIONS
$title = "Developer Solutions| SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "Website Developers";

$hb_title = "Get Started With SiteLock";
$hb_phone = "833.715.1304";
$hb_btn = 'Contact Us';
include 'includes/forms/channel.html';
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div id="mainBody">
<div class="whiteSpace25"></div>
<div class="row">
<div class="col-lg-6 my-auto solutionHead">
<h1 class="headTxtDrop">Security that <span class="sourceBlack">works for you</span></h1>
<h3>Build New Revenue Streams and Increase Satisfaction</h3>
<a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Grow Your Business Today</a>
</div>
<div class="col-lg-6">
<img class="headerimgmain" src="/img/developer-solutions/developerHeader.png" alt="Web Developer Security">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<?php include 'includes/rating-bar.php';?>

<div id="mainBody">

<div class="whiteSpace50"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2><span class="sourceRegular">Keep your clients</span><br><span class="sourceBlack">happy and secure</span></h2>Security is an integral part of any website. If and when your clients experience a website compromise, they’ll look to you for help. Add website security to your portfolio that automatically finds, fixes, and prevents cyberthreats on the sites you manage. Your job just got easier, and your clients just got happier.<br><a class="btn btn-red font-weight-bold" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Secure Your Clients’ Websites</a></div></div>
   <div class="col-lg-5 splitPadding footMobileHide"><img class="splitImg" src="/img/channel/agencyBenefit.svg" alt="Developer Growth"></div>
  </div>
</div>

<div class="whiteSpace100"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-md-5 splitPadding"><img class="splitImg" src="/img/developer-solutions/devDashboard.png" alt="Client Control Panel"></div>
  <div class="col-md-7 whitebk splitPadding"><div id="content"><h2><span class="sourceRegular">Easy </span> <span class="sourceBlack">management</span></h2>Manage all of your clients’ websites in one place through the SiteLock Dashboard. With the click of a button, you’ll have visibility to the status of their security, like pages scanned, malware found, threats blocked, and more. If clients are using a content management system (CMS), you’ll have access to any outdated applications or existing vulnerabilities.<br><a class="btn btn-red font-weight-bold" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Get Started</a></div></div>
 </div>
</div>
<div class="whiteSpace50"></div>
</div>
</div>

<?php include 'includes/awards-bar-alt.php';?>

<?php 
$benefitsTitle = '<h1 class="text-center font55">Website security <span class="sourceBlack">benefits for web developers</span></h1>'; 
$icon1= 'fa-heart'; $title1 = 'Happy clients'; $content1 = 'Keep your clients happy by keeping their websites safe. When a website attack hits, they’ll be protected—all thanks to you.';
$icon2 = 'fa-watch'; $title2 = 'Save time'; $content2 = 'Let automated website security work for you. Threats to your clients’ sites are blocked or fixed automatically.';
$icon3 = 'fa-location'; $title3 = 'Accuracy'; $content3 = 'Protect your website with award-winning products proven to find and fix threats with 99.99% accuracy.';
$icon4 = 'fa-piggy-bank'; $title4 = 'Cost savings'; $content4 = 'Time is money. Save money by automating the manual intervention needed to remove malicious code or fix vulnerabilities (which will also help keep your clients’ costs low).';
$icon5 = 'fa-user-graduate'; $title5 = 'Always informed'; $content5 = 'Stay updated about your clients’ sites at all times. Your SiteLock Dashboard provides visibility into the number of threats blocked, pages scanned, malware removed, and more.';
$icon6 = 'fa-rocket'; $title6 = 'Boost Site Performance'; $content6 = 'Increase your clients’ websites speed by as much as 50%. A content delivery network is included with each web application firewall to enhance site performance.';
$benefitsBTN = 'Protect Your Website Now';

include('includes/benefits-bar.php');
?>

<?php include 'includes/cms-bar.php';?>

<div id="onpagepricing"></div>
<div id="mainBody">
<div class="whiteSpace100"></div>

<?php include 'includes/includes-bar.php';?>
</div>

<?php include 'includes/solutions-bar.php';?>

<div id="mainBody">

<h1 class="text-center sourceRegular font55">Get to know <span class="sourceBlack">the security behind the website</span></h1>

<div class="row">
<div class="col text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningWebsite.svg" alt="Scanning Website"><h5 class="sourceBlack">WEBSITE<br>SCANNING</h5><p>Monitor websites daily for malicious or suspicious activity. When threats are found, you are notified immediately.</p><br><a class="btn btn-ghost-grey" href="website-scanning">Learn More</a></div><br></div>
</div>

<div class="col text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningCloud.svg" alt="Scanning Cloud"><h5 class="sourceBlack">MALWARE<br>REMOVAL</h5><p>Find and automatically remove website malware before any damage is caused to the sites you manage.</p><br><a class="btn btn-ghost-grey" href="malware-removal">Learn More</a></div><br></div>
</div>

<div class="col text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningThreat.svg" alt="Scanning Threat"><h5 class="sourceBlack">VULNERABILITY<br>PATCHING</h5><p>Automatically patch vulnerabilities in website code before cybercriminals exploit them and gain access to your site.</p><br><a class="btn btn-ghost-grey" href="vulnerability-patching">Learn More</a></div><br></div>
</div>

<div class="col text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningWAF.svg" alt="Scanning WAF"><h5 class="sourceBlack">WEB APPLICATION<br>FIREWALL</h5><p>Prevent cybercriminals, bad bots, and DDoS attacks from impacting your clients’ websites, while increasing your website speed.</p><br><a class="btn btn-ghost-grey" href="web-application-firewall">Learn More</a></div><br></div>
</div>
</div>

<div class="whiteSpace100"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-lg-5 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2><span class="sourceBlack">Are your clients’ websites at risk of a cyberattack?</h2>Did you know all websites are inherently at risk of cyberthreats? Get a free risk assessment to determine your clients’ websites risk and likelihood of compromise.<br><a class="btn btn-blue font-weight-bold" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Get Your Free Risk Assessment</a></div></div>
  <div class="col-lg-7 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/273777919" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
 </div>
</div>

<div class="whiteSpace100"></div>
</div>

<?php include 'includes/qsr-bar.php';?>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>