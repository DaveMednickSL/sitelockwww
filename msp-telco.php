<?php
//DEFINITIONS
$title = "MSP & Telcos | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";

$hb_title = "Get Started With SiteLock";
$hb_phone = "888.878.2417";
$hb_btn = 'Contact Us';
include 'includes/forms/channel.html';
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div id="mainHeaderBody">
<div class="whiteSpace50 tabshow"></div>
<div class="row">
<div class="col-lg-6 my-auto solutionHead">
<h1><span class="sourceBlack">Increase revenue and reduce churn</span> with all-in-one security solutions</h1>
<h3 class="font25">As their trusted advisor, help customers secure their websites from ever-evolving cyberthreats.</h3>
<a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Protect Your Customers Today</a>
</div>
<div class="col-lg-6">
<img class="headerimgmain" src="/img/channel/mspHeader.png" alt="MSP/TELCO Benefits">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<div id="mainRed"><div id="mainBody"><div id="content">
<div class="row">
<div class="col-lg-3 text-center"><img class="circleHeadGrey" src="/img/channel/newtek.png" alt="MSP/TELCO Benefits"></div>
<div class="col-lg-9 font25">
<i class="fas fa-quote-left fa-2x"></i><br>
Website security both protects my customers and further establishes us as a trusted advisor.
<br><span class="float-right"><i>- Newtek Technology Solutions</i></span></div>
</div>
</div></div></div>

<div id="mainTerms"><div id="center">
<div class="whiteSpace100"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2><span class="sourceBlack">Why website security?</span><br></h2><p>Your customers trust you as their IT and security experts—and website security is critical in maintaining their trust. Endpoint and network security are not enough to ensure your customers and their users’ data are secure. By partnering with SiteLock, you can empower your customers to take proactive measures to protect their websites from cyberthreats. Offering a holistic package to automatically protect and increase website speed ensures your customers’ websites are safe at all times.</p><a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Partner with SiteLock</a></div></div>
   <div class="col-lg-5 footMobileHide"><img class="splitImg" src="/img/channel/mspDatabase.svg" alt="MSP Telco Benefits"></div>
  </div>
</div>

<div class="whiteSpace100"></div>

<h2 class="font55 text-center sourceBlack">Solutions optimized for MSPs and Telcos</h2>

<div class="whiteSpace50"></div>

<h2 class="font35 text-center">User Experience</h2>

<div class="row">
<!-- ROW 1 -->
<div class="col-md-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Gears.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Automatic malware detection & removal</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed" src="img/fa_icons/icon-Magnifying-Glass.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>External malware scanning (HTTP/S)</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Download.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Internal file-based malware scanning (FTP/SSH)</p></div>
  </div>
  </div></div>
</div>

<!-- ROW 2 -->
<div class="col-md-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed" src="img/fa_icons/icon-Clip-check.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Plans for ongoing services or one-time emergencies</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Database.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Largest proprietary database of known malware</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed" src="img/fa_icons/icon-Network.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Network effect of 12 million customers</p></div>
  </div>
  </div></div>
</div>

<!-- ROW 3 -->
<div class="col-md-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Lock.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>SMART technology leveraged by >750k sites</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed" src="img/fa_icons/icon-Headset.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Dedicated Tier 3 Team executing manual cleans</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Grad-Cap.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Dedicated Research Team performing malware analysis</p></div>
  </div>
  </div></div>
</div>
<div>

<div class="whiteSpace100"></div>
<div id="mainBody"><div id="content"><h2 class="font35 text-center">Vulnerability detection and remediation</h2></div></div>

<div class="row">
<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed" src="img/fa_icons/icon-Warn.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Daily scans for common<br>vulnerabilities</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Gear.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Automatic patching for most platforms</p></div>
  </div>
  </div></div>
</div>
<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed" src="img/fa_icons/icon-Page-check.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Website analyzed & risk score provided</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Code.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Static analysis for in-depth code auditing</p></div>
  </div>
  </div></div>
</div>
</div>
<div>

<div class="whiteSpace100"></div>
<div id="mainBody"><div id="content"><h2 class="font35 text-center">Reputation management for websites</h2></div></div>

<div class="row">
<div class="col-lg-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Stop.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Daily search engine blacklist monitoring</p></div>
  </div>
  </div></div>
</div>

<div class="col-lg-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed" src="img/fa_icons/icon-Envelope.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>Daily spam blacklist monitoring</p></div>
  </div>
  </div></div>
</div>

<div class="col-lg-4">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Badge-check.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto chThreeTier"><p>TrustSeal to establish the reputation as a safe website</p></div>
  </div>
  </div></div>
</div>

<div class="col-lg-3"></div>
<div class="col-lg-3 text-center"><br><a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Partner with SiteLock</a></div>
<div class="col-lg-3 text-center"><br><a class="btn btn-ghost-grey" href="tel:8447768614">Call 844.776.8614</a></div>
<div class="col-lg-3"></div>
</div>

</div></div></div>

</div></div>

<div class="whiteSpace100"></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>