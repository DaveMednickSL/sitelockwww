<?php
//DEFINITIONS
$title = "SiteLock TrueReview";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$pagephone = "(877) 563 2832";
?>
<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?php echo $title;?></title>
  <meta name="description" content="<?php echo $description;?>">
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="../css/fontawesome-all.min.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/style.css?v=1.0.0" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<body>
<!-- NAVIGATION -->
<div id="menu"><div id="menu_inner">
<div class="row text-center">
<div class="col-md-6"><img class="logoNav" src="../img/logos/SiteLock_red.svg" alt="sitelock logo"></div>
<div class="col-md-6"><h3 class="sourceBlack"><?php echo $pagephone;?></h3></div>
</div>
</div></div>

<!-- HEADER -->
<div class="text-center" id="mainblue"><div id="mainBody"><div id="content">
<h1 class="sourceBlack font55">About the SiteLock&reg;<br>TrueReview Report</h1>
</div></div></div>

<!-- FAQ -->
<div class="whiteSpace50"></div>

<div id="mainBody"><div id="content">
<p class="font25">SiteLock TrueReview provides a high-level overview of your web application firewall statistics. Each week we gather and email your results – ensuring you always have visibility to the security of your site.</p>
<hr>

<h2 class="red font25">THREATS BLOCKED</h2>
<p>SiteLock&reg; TrueShield&trade; web application firewall protects against the top ten online threats, such as SQL injection, XSS, CSRF and unvalidated redirects and forwards. This report reflects the total number of threats blocked from your site.</p>
<hr>

<h2 class="red font25">BOT TRAFFIC</h2>
<p>There are essentially two types of bots — good bots (search engine bots indexing your site) and bad bots (cybercriminals trying to break in). TrueShield allows good bots in, while blocking bad bots from entering your site to protect your customers and business. This report reflects your total bot traffic.</p>
<hr>

<h2 class="red font25">REQUESTS SAVED</h2>
<p>When your website receives requests from site visitors, it needs resources from your server to process these requests. This can cause your site to slow down. With TrueShield, the end-user receives your website's content without passing the request to your server, which gets it to your visitors faster. This report reflects the number of requests saved.</p>
<hr>

<h2 class="red font25">BANDWIDTH SAVED</h2>
<p>Reducing bandwidth is important to improving your website's load time and saves you money on bandwidth charges or overages. Saving bandwidth means a faster page load for your customers and site visitors. This report reflects the amount of bandwidth you saved with TrueShield.</p>
</div></div>

<div class="whiteSpace50"></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">


<hr class="footerHR">

<div class="row">
<div class="col-md-4" style="border-right: 1px solid #707070;">
<img class="logoFooter" src="../img/logos/SiteLock_white.svg" alt="sitelock logo">
</div>

<div class="col-md-4 font18">
<h3 class="sourceRegular">Contact</h3>
<p>U.S. <u>855.378.6200</u></p>
<p>Int'l <u>415.390.2500</u></p>
<p>Email <u>Support@SiteLock.com</u></p>
</div>

<div class="col-md-4 footerCol footconnect">
<h3 class="sourceRegular">Connect</h3>
<p>Sign up for SiteLock news & announcements</p>
<div class="form-inline footerSpace">
<form method="post" action="https://a29565.actonservice.com/acton/eform/29565/01b7e374-ab17-4ff6-a115-3f1742ab84eb/d-ext-0001">
<input type="hidden" name="AcceptsEmail" value="Yes">
<input class="form-control footerInput" type="email" name="Email" placeholder="you@email.com"><button style=" margin:0;" type="submit" class="btn-f btn-foot"><i class="far fa-arrow-right"></i></button>
</form>
</div>

</div>
</div>

<hr class="footerHR">

<p class="text-center footCopy linkblue">Copyright &copy; SiteLock <script>document.write(new Date().getFullYear())</script> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.0"></script>
</body>
</html>