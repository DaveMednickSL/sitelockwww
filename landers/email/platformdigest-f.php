<?php
//DEFINITIONS
$title = "SiteLock Platform Digest";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$pagephone = "(844) 303-1508";
?>
<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?php echo $title;?></title>
  <meta name="description" content="<?php echo $description;?>">
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="../css/fontawesome-all.min.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/style.css?v=1.0.0" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<body>
<!-- NAVIGATION -->
<div id="menu"><div id="menu_inner">
<div class="row text-center">
<div class="col-md-6"><img class="logoNav" src="../img/logos/SiteLock_red.svg" alt="sitelock logo"></div>
<div class="col-md-6"><h3 class="sourceBlack"><?php echo $pagephone;?></h3></div>
</div>
</div></div>

<!-- HEADER -->
<div class="text-center" id="mainblue"><div id="mainBody"><div id="content">
<h1 class="sourceBlack font55">About the SiteLock&reg;<br>Platform Digest</h1>
</div></div></div>

<!-- FAQ -->
<div class="whiteSpace50"></div>

<div id="mainBody"><div id="content">
<p class="font25">The SiteLock Platform Digest provides a high-level security analysis of the health and risks associated with your website. We deliver your individual scanning results on a weekly basis – ensuring you always have visibility to the security of your site.</p>
<img class="img90" src="../img/email/platform-digest-header.png" alt="Platform Digest">
<hr>

<h2 class="red font25">Platform Scan</h2>
<p>During the SiteLock WordPress Platform Scan, we review your website plugins, themes and core installation for vulnerabilities. The Platform Scan will notify you if vulnerabilities are found. Your results are defined on a scale of low, medium or high. The color of results indicates the level of severity of the vulnerabilities found. For example, if your platform results are highlighted in red, you have a critical number of vulnerabilities on your website and your immediate attention is recommended.</p>
<p class="linkblue">Please <a href="https://secure.sitelock.com/login">login</a> to your SiteLock Dashboard for more information and next steps.</p>
<hr>

<h2 class="red font25">Malware Scan</h2>
<p>During the malware scan, we review your website source code for malicious content and links to malicious sites. The scan is also designed to alert you when your site has been identified on anti-virus or popular search engine blacklists. Your malware results are defined on a scale of low, medium or high. The color of results indicates the level of severity of the malware infection. For example, if your malware results are highlighted in red, then your website has been infected with malware and your immediate attention is recommended.</p>
<p class="linkblue">Please <a href="https://secure.sitelock.com/login">login</a> to your SiteLock Dashboard for more information and next steps.</p>
<hr>

<h2 class="red font25">Risk Assessment</h2>
<p>To help you better understand the likelihood of your website being infected, we’ve developed a predictive model we call the Risk Assessment. Based on the complexity, composition and popularity or your site, this assessment assigns it a score that offers insight into the risk of it being compromised by malware and vulnerabilities.</p>
<p>If you are interested in learning your website’s risk score or have questions about your scanning results, please give us a call at <?php echo $pagephone;?>. We’re here for you 24/7/365.</p>
<p class="text-center"><a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Contact Me</a></p>
</div></div>

<div class="whiteSpace50"></div>

<!-- FORM -->
<?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "(844) 303-1508";
$hb_phone_link = "8443031508";
$hb_btn = 'Call Me Now';
include '../includes/forms/high-barrier.html';
?>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">


<hr class="footerHR">

<div class="row">
<div class="col-md-4" style="border-right: 1px solid #707070;">
<img class="logoFooter" src="../img/logos/SiteLock_white.svg" alt="sitelock logo">
</div>

<div class="col-md-4 font18">
<h3 class="sourceRegular">Contact</h3>
<p>U.S. <u>855.378.6200</u></p>
<p>Int'l <u>415.390.2500</u></p>
<p>Email <u>Support@SiteLock.com</u></p>
</div>

<div class="col-md-4 footerCol footconnect">
<h3 class="sourceRegular">Connect</h3>
<p>Sign up for SiteLock news & announcements</p>
<div class="form-inline footerSpace">
<form method="post" action="https://a29565.actonservice.com/acton/eform/29565/01b7e374-ab17-4ff6-a115-3f1742ab84eb/d-ext-0001">
<input type="hidden" name="AcceptsEmail" value="Yes">
<input class="form-control footerInput" type="email" name="Email" placeholder="you@email.com"><button style=" margin:0;" type="submit" class="btn-f btn-foot"><i class="far fa-arrow-right"></i></button>
</form>
</div>

</div>
</div>

<hr class="footerHR">

<p class="text-center footCopy linkblue">Copyright &copy; SiteLock <script>document.write(new Date().getFullYear())</script> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.0"></script>
</body>
</html>