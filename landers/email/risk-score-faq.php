<?php
//DEFINITIONS
$title = "Risk Score FAQ";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$pagephone = "(877) 563 2832";
?>
<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?php echo $title;?></title>
  <meta name="description" content="<?php echo $description;?>">
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="../css/fontawesome-all.min.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/style.css?v=1.0.0" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<body>
<!-- NAVIGATION -->
<div id="menu"><div id="menu_inner">
<div class="row text-center">
<div class="col-md-6"><img class="logoNav" src="../img/logos/SiteLock_red.svg" alt="sitelock logo"></div>
<div class="col-md-6"><h3 class="sourceBlack"><?php echo $pagephone;?></h3></div>
</div>
</div></div>

<!-- HEADER -->
<div class="text-center" id="mainblue"><div id="mainBody"><div id="content">
<h1 class="sourceBlack font55">SiteLock&reg; Risk Score<br>Frequently Asked Questions</h1>
</div></div></div>

<!-- FAQ -->
<div class="whiteSpace50"></div>

<div id="mainBody"><div id="content">
<p class="font25">The Risk Score was created to help educate website owners about their website health and proactively identify potential vulnerabilities or threats before they can be exploited.</p>

<p class="font25">The SiteLock Risk Score is a tool used to determine your website's likelihood of compromise. We compare the makeup of your website to other websites similar to yours, in terms of features and complexity, to determine its risk of compromise.</p>

<hr>

<h2 class="red font25">What does a low risk score mean?</h2>
<p>We perform a risk assessment on your website to determine your site’s likelihood of attack. During this assessment, we leverage over 500 variables to calculate a risk score on a scale of low, medium and high. A low risk score indicates that vulnerabilities exist; however, they are not very likely to be exploited at this time.</p>
<hr>

<h2 class="red font25">What does a medium risk score mean?</h2>
<p>We perform a risk assessment on your website to determine your site’s likelihood of attack. During this assessment, we leverage over 500 variables to calculate a risk score on a scale of low, medium and high. A medium risk score indicates that vulnerabilities exist, but may be difficult for an attacker to exploit.</p>
<hr>

<h2 class="red font25">What does a high-risk score mean?</h2>
<p>We perform a risk assessment on your website to determine your site’s likelihood of attack. During this assessment, we leverage over 500 variables to calculate a risk score on a scale of low, medium and high. When a website indicates a high-risk score, it is 12+ times more likely to be exploited than the average website.</p>
<hr>

<h2 class="red font25">Why are you telling me I have a high risk if you're protecting me?</h2>
<p>We want to provide all of our customers with visibility to the health and risks associated with their websites – even if they are fully protected with SiteLock services. Even though you are fully protected by SiteLock, your website may still be at a high risk due to a number of different factors (open source, feature-rich, eCommerce, etc.).</p>
<hr>

<h2 class="red font25">Once I fix this issue, can I check this manually myself?</h2>
<p>The Platform Digest will only be emailed to you every Tuesday, but you can always check your SiteLock Dashboard to review your security results and status.</p>

</div></div>

<div class="whiteSpace50"></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">


<hr class="footerHR">

<div class="row">
<div class="col-md-4" style="border-right: 1px solid #707070;">
<img class="logoFooter" src="../img/logos/SiteLock_white.svg" alt="sitelock logo">
</div>

<div class="col-md-4 font18">
<h3 class="sourceRegular">Contact</h3>
<p>U.S. <u>855.378.6200</u></p>
<p>Int'l <u>415.390.2500</u></p>
<p>Email <u>Support@SiteLock.com</u></p>
</div>

<div class="col-md-4 footerCol footconnect">
<h3 class="sourceRegular">Connect</h3>
<p>Sign up for SiteLock news & announcements</p>
<div class="form-inline footerSpace">
<form method="post" action="https://a29565.actonservice.com/acton/eform/29565/01b7e374-ab17-4ff6-a115-3f1742ab84eb/d-ext-0001">
<input type="hidden" name="AcceptsEmail" value="Yes">
<input class="form-control footerInput" type="email" name="Email" placeholder="you@email.com"><button style=" margin:0;" type="submit" class="btn-f btn-foot"><i class="far fa-arrow-right"></i></button>
</form>
</div>

</div>
</div>

<hr class="footerHR">

<p class="text-center footCopy linkblue">Copyright &copy; SiteLock <script>document.write(new Date().getFullYear())</script> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.0"></script>
</body>
</html>