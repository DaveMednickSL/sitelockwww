<?php
//DEFINITIONS
$title = "Website Scraping Protection";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="website-scraping-protection";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 263-8630";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="website-scraping">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                   <!--Banner Section-->
           <section class="black-bg p-5">
                <div class="container white text-left">
                    <div class="row">

                        <div class="col-md-8 col-sm-12 text-left">
                            <h1 class="lg-text5 sourceBlack">WEB SCRAPPING PROTECTION</h1>
                            <h3 class="text-left no-margin sourceBlack">Automatically Prevent Website Scraping and Enhance Site Performance</h3>
                            <p class="sourceLight">Protector of 12M+ Websites Worldwide</p>

                    </div>

                    </div>
                </div>
            </section>




 <!--Worry-Free Website Protection-->
 <section class="p-5 grey-bg-scraping">
                <div class="container">
                    <div class="row">
                       
                        <div class="col-md-7 col-xs-12">
                        <div class="whiteSpace50"></div>
                        <div class="media">
                           <div class="media-left"><span><i class="fas fa-check"></i></span></div>
                           <div class="media-body"><strong>Protect Business Data:</strong> Stop bots from scraping your websites to gain insight into your pricing, SEO strategies and other sensitive business data.</div>
                        </div>
                        <br>
                         <div class="media">
                           <div class="media-left"><span><i class="fas fa-check"></i></span></div>
                           <div class="media-body"><strong>Stay off Lead Lists:</strong> Block botted programs from scrapping contact information to sell as part of a lead list.</div>
                        </div>
                        <br>
                         <div class="media">
                           <div class="media-left"><span><i class="fas fa-check"></i></span></div>
                           <div class="media-body"><strong>Safeguard Reputation:</strong> Protect against website suspension, blacklisting, traffic redirects, malware infections and more.</div>
                        </div>
                        <br>
                         <div class="media">
                           <div class="media-left"><span><i class="fas fa-check"></i></span></div>
                           <div class="media-body"><strong>Increase Site Speed:</strong> Boost your site speed by more than 50% with the power of our Content Delivery Network (CDN).</div>
                        </div>
<div class="whiteSpace100"></div>

                        </div>

                         <div class="col-md-5 col-xs-12 col-sm-12 float-sm-left">
                            <div class="fom-style-scraping">
                        
                            <div class="form-container"> 
                                        <h3 class="sourceBlack text-center">PROTECT YOUR WEBSITE TODAY</h3>
                                        <p class="text-center">Enter your information and we will contact you to provide more information on protecting your site.</p>
                                        
                                        <form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
			<span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
            <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<span style="padding:0 20px 0px 25px;" ><input class="btn btn-red upper-font bold" type="submit" value="Start Now" style="width: 90%;"></span>
		</form>

</div>                  



                        </div>
                        </div>
                    </div>
                </div>
            </section>




                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "(833) 263-8630";
$hb_btn = 'Start Today';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>