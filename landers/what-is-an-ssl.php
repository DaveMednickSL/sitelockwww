<?php
//DEFINITIONS
$title = "Is SSL Protecting Your Site?";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="what-is-an-ssl";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(844) 776-8612";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="ssl">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <!--Banner Section-->
                    <section class="image-banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-8 mx-auto">
                                    <iframe id="video" src="https://www.youtube.com/embed/9vrr-AEG4q0?ecver=2" width="640" height="360" frameborder="0" style="position:absolute;width:100%; left:0;" allowfullscreen></iframe>

                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Is an SSL Protecting Your Site?-->
                    <section class="p-5">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 ">
                                    <h1 class="red">Is an SSL Protecting Your Site?</h1>
                                    <h4 class="red sourceBlack">Learn how an SSL Certificate works and what it protects.</h4>
                                </div>

                                <div class="col-12 text-center">
                                    <a id="play-video" href="#" class="btn btn-red bold">Watch the Video</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--The Trusted Security-->
                    <section id="mainDarkGrey">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <br/>
                                    <h1 class="text-center sourceLight">12M+ Websites Protected</h1>
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/Tennis-Channel.png" width="70">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/new-orleans.png" width="200">
                                    </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/frontier.png" width="150">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/nextiva.png" width="150">
                                </div>

                            </div>
                            <div class="whiteSpace25"></div>
                        </div>
                        </div>
                    </section>

                    <!--The Complete Website Security Picture Section start-->
                    <section class="p-5">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="grey sourceLight">The Complete Website Security Picture</h1>
                                    <h3 class="grey sourceLight">Why you need more than SSL to secure your site</h3>
                                    <br>
                                    <img src="img/ssl/DiagramArtboard.png" class="d-none d-md-block img-responsive">
                                    <img src="img/ssl/DiagramArtboard-m.png" class="d-block d-md-none img-responsive">
                                </div>

                                <div class="col-12 text-center">
                                    <a href="#HighBarrierForm" class="btn btn-red bold" data-toggle="modal" data-target="#HighBarrierForm">COMPLETE YOUR WEBSITE SECURITY</a>
                                </div>

                            </div>
                        </div>
                    </section>

                    <!--Reap the Benefits of Website Security-->

                    <section class="p-5">

                        <div class="container text-center grey">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1 class="blue sourceLight font45">Reap the Benefits of Website Security</h1>
                                    <p class="font25 sourceLight">Website security not only protects your website from infection but provides direct bottom line benefits.</p>
                                    <br/>
                                </div>

                                <div class="col-md-4 col-sm-12">
                                    <img src="img/ssl/pci.png" width="177">
                                    <br>
                                    <h4 class="sourceBlack blue">BUILD TRUST</h4>
                                    <p>Trust Seals, displayed on secure and malware-free websites, are proven to boost trust and conversions.</p>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <img src="img/ssl/seo.png" width="145">
                                    <br>
                                    <h4 class="sourceBlack blue">IMPROVE SEO</h4>
                                    <p>Detect, remove and block website security issues that negatively impact your search engine rankings.</p>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <img src="img/ssl/performance.png" width="105">
                                    <br>
                                    <h4 class="sourceBlack blue">INCREASE PERFORMANCE</h4>
                                    <p>Automatically repair malware and site issues that slow performance and degrade visitor experience.
                                    </p>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#HighBarrierForm" class="btn btn-ghost-red bold" data-toggle="modal" data-target="#HighBarrierForm">PROTECT & BOOST YOUR WEBSITE</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
                        $hb_title = "Secure and Accelerate Your Website";
                        $hb_phone = "(844) 776-8612";
                        $hb_btn = 'Get Started';
                        include 'includes/forms/high-barrier.html';
                    ?>


                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>