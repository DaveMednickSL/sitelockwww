//FORM VALIDATION
$("#inputDomain").blur(function(){var e;document.getElementById("inputDomain").checkValidity()?((e=document.getElementById("validateDomain")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validateDomain")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
$("#inputEmail").blur(function(){var e;document.getElementById("inputEmail").checkValidity()?((e=document.getElementById("validateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
$("#inputName").blur(function(){var e;document.getElementById("inputName").checkValidity()?((e=document.getElementById("validateName")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validateName")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
$("#inputlastName").blur(function(){var e;document.getElementById("inputlastName").checkValidity()?((e=document.getElementById("validatelastName")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validateName")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
$("#inputPhone").blur(function(){var e;document.getElementById("inputPhone").checkValidity()?((e=document.getElementById("validatePhone")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validatePhone")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))});

//HIGH BARRIER MODAL
$(document).on('click', '.modal', function(){
  $("#HBinputDomain").blur(function(){var e;document.getElementById("HBinputDomain").checkValidity()?((e=document.getElementById("validateDomain")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validateDomain")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#HBinputEmail").blur(function(){var e;document.getElementById("HBinputEmail").checkValidity()?((e=document.getElementById("validateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#HBinputName").blur(function(){var e;document.getElementById("HBinputName").checkValidity()?((e=document.getElementById("validateName")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validateName")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#HBinputlastName").blur(function(){var e;document.getElementById("HBinputlastName").checkValidity()?((e=document.getElementById("validatelastName")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validatelastName")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#HBinputPhone").blur(function(){var e;document.getElementById("HBinputPhone").checkValidity()?((e=document.getElementById("validatePhone")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("validatePhone")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))});
});

//LOW BARRIER MODAL
$(document).on('click', '.modal', function(){
  $("#LBinputEmail").blur(function(){var e;document.getElementById("LBinputEmail").checkValidity()?((e=document.getElementById("LBvalidateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("LBvalidateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#LBinputName").blur(function(){var e;document.getElementById("LBinputName").checkValidity()?((e=document.getElementById("LBvalidateName")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("LBvalidateName")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))});
});

//CONTACT MODAL
$(document).on('click', '.modal', function(){
  $("#CTinputEmail").blur(function(){var e;document.getElementById("CTinputEmail").checkValidity()?((e=document.getElementById("CTvalidateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("CTvalidateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#CTinputName").blur(function(){var e;document.getElementById("CTinputName").checkValidity()?((e=document.getElementById("CTvalidateName")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("CTvalidateName")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#CTinputPhone").blur(function(){var e;document.getElementById("CTinputPhone").checkValidity()?((e=document.getElementById("CTvalidatePhone")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("CTvalidatePhone")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))});
});

//REVIEW MODAL
$(document).on('click', '.modal', function(){
  $("#RVinputEmail").blur(function(){var e;document.getElementById("RVinputEmail").checkValidity()?((e=document.getElementById("RVvalidateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("RVvalidateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#RVinputName").blur(function(){var e;document.getElementById("RVinputName").checkValidity()?((e=document.getElementById("RVvalidateName")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("RVvalidateName")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#RVinputDomain").blur(function(){var e;document.getElementById("RVinputDomain").checkValidity()?((e=document.getElementById("RVvalidateDomain")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("RVvalidateDomain")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))})
});

//VPN BETA FORM
  $("#VPNinputEmail").blur(function(){var e;document.getElementById("VPNinputEmail").checkValidity()?((e=document.getElementById("VPNvalidateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("VPNvalidateEmail")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))}),
  $("#VPNinputName").blur(function(){var e;document.getElementById("VPNinputName").checkValidity()?((e=document.getElementById("VPNvalidateName")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("VPNvalidateName")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))});
  $("#VPNinputLast").blur(function(){var e;document.getElementById("VPNinputLast").checkValidity()?((e=document.getElementById("VPNvalidateLast")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("VPNvalidateLast")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))});
  $("#VPNinputPhone").blur(function(){var e;document.getElementById("VPNinputPhone").checkValidity()?((e=document.getElementById("VPNvalidatePhone")).classList.remove("fa-circle"),e.classList.remove("fa-exclamation-circle"),e.classList.remove("red"),e.classList.remove("grey"),e.classList.add("fa-check-circle"),e.classList.add("blue")):((e=document.getElementById("VPNvalidatePhone")).classList.remove("fa-circle"),e.classList.remove("fa-check-circle"),e.classList.remove("blue"),e.classList.remove("grey"),e.classList.add("fa-exclamation-circle"),e.classList.add("red"))});

//STAR RATING
$(document).ready(function(){$("#one").click(function(){$("#one").removeClass("far").addClass("fas"),$("#two").removeClass("fas").addClass("far"),$("#three").removeClass("fas").addClass("far"),$("#four").removeClass("fas").addClass("far"),$("#five").removeClass("fas").addClass("far"),$("input[name=stars]").val("1")})}),$(document).ready(function(){$("#two").click(function(){$("#one").removeClass("far").addClass("fas"),$("#two").removeClass("far").addClass("fas"),$("#three").removeClass("fas").addClass("far"),$("#four").removeClass("fas").addClass("far"),$("#five").removeClass("fas").addClass("far"),$("input[name=stars]").val("2")})}),$(document).ready(function(){$("#three").click(function(){$("#one").removeClass("far").addClass("fas"),$("#two").removeClass("far").addClass("fas"),$("#three").removeClass("far").addClass("fas"),$("#four").removeClass("fas").addClass("far"),$("#five").removeClass("fas").addClass("far"),$("input[name=stars]").val("3")})}),$(document).ready(function(){$("#four").click(function(){$("#one").removeClass("far").addClass("fas"),$("#two").removeClass("far").addClass("fas"),$("#three").removeClass("far").addClass("fas"),$("#four").removeClass("far").addClass("fas"),$("#five").removeClass("fas").addClass("far"),$("input[name=stars]").val("4")})}),$(document).ready(function(){$("#five").click(function(){$("#one").removeClass("far").addClass("fas"),$("#two").removeClass("far").addClass("fas"),$("#three").removeClass("far").addClass("fas"),$("#four").removeClass("far").addClass("fas"),$("#five").removeClass("far").addClass("fas"),$("input[name=stars]").val("5")})});

//DIV REPLACE
$(function () {
  count = 0;
  imagesArray = [
  '<div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/ipage.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/fatcow.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/1and1.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/nextiva2.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/zannaland_logo.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/blackfriday_logo.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/inkspired_magazine.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/self_logo.png" alt="ipage"></div>', 
  '<div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/hostpapa.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/thegumtree_logo.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/animal_rescue_corps_logo.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/tennis_channel.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/bluehost.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/pearl_logo.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/dog_for_dog.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/battery_junction.png" alt="ipage"></div>', 
  '<div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/frontier.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/entrust.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/online_shoes.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/godaddy_logo.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/love_my_credit_union.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/helene_fuld_edu.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/new_orleans_logo.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/live_pc_expert.png" alt="ipage"></div>', 
  '<div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/warehouse_fabrics.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/pawn_shops_dot_net.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/harvard.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/virtuix_omni.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/copa_di_vino.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/hostgator.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/autumn_publishing.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/build_ca.png" alt="ipage"></div>', 
  '<div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/tdot_performance_ca.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/camp_bow_wow.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/us_lacrosse.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/dog_food_advisor.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/european_endowment.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/farrah_gray.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/web_dot_com_logo.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/pimsleur.png" alt="ipage"></div>',
  '<div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/disc_omnidex.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/partners/netsol_logo.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/magic_pay.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/rings_and_things.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/shoelander.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/bank_of_coushatta.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/dvdempire.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/axis_communications.png" alt="ipage"></div>',
  '<div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/national_sleep_foundation.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/luxury_bazaar.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/adam_mesh.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/moms_magazine.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/paleo_diet.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/az_science.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/websitealive.png" alt="ipage"></div><div class="col-md-3 logoBox"><img class="img-responsive logoImg mx-auto" src="img/logos/customers/devita_skin_care.png" alt="ipage"></div>'
  ];
  setInterval(function () {
    count++;
    $(".logoSlide").fadeOut(400, function () {
      $(this).html(imagesArray[count % imagesArray.length]).fadeIn(400);
    });
  }, 4000);
});

//PLAY VIDEO
$(document).ready(function() { $('#play-video').on('click', function(ev) { $("#video")[0].src += "&autoplay=1"; ev.preventDefault(); }); });

//GET STARTED
$(".leadwebsite").click(function(){ var Website = $('#Website').val(); $('#HBinputDomain').val(Website); });

//scanVidTranscript
$(window).resize(function(){
  var winWidth = $(window).width();
  if(winWidth < 1000) {
      var videoWidth = Math.round( $(window).width() - 30 );
      var changeVar = +(( 360/640 ).toFixed(2));
      var videoHeight = Math.round( videoWidth * changeVar);
      $('iframe#scanningVideoSMART').width(videoWidth);
      $('iframe#scanningVideoSMART').height(videoHeight);
  }

});

$(document).ready(function() {

  var winWidth = $(window).width();
  if(winWidth < 1000) {
      var videoWidth = Math.round( $(window).width() - 30 );
      var changeVar = +(( 360/640 ).toFixed(2));
      var videoHeight = Math.round( videoWidth * changeVar);
      $('iframe#scanningVideoSMART').attr('width',videoWidth);
      $('iframe#scanningVideoSMART').attr('height',videoHeight);
  }

$('#scanVidTranscript').click(function() {
var transDisplay = $('.vidTranscript').css('display');
if(transDisplay != 'block') {
$('.vidTranscript').slideDown();
$('p#scanVidTranscript').html('Hide Transcript');
} else {
$('.vidTranscript').slideUp();
$('p#scanVidTranscript').html('View Transcript');
} // End if-else

});


$('#partnershipTranscript').click(function() {
var transDisplay = $('.vidTranscript').css('display');
if(transDisplay != 'block') {
$('.vidTranscript').slideDown();
$('p#partnershipTranscript').html('Hide Transcript');
} else {
$('.vidTranscript').slideUp();
$('p#partnershipTranscript').html('View Transcript');
} // End if-else

});
});



//scanVidTranscript
$(document).ready(function() {
	$(".btn-red").click(function() {
   
    var getName = $('input#inputName').val();
    if(getName != '' && getName != null) {
      $('#HBinputName').val(getName);
      $('#HBinputlastName').focus();
          $('html, body').animate({
            scrollTop: $("#forms").offset().top
         }, 1000);
              $('#inputName').val('');
    } else {
      $('#inputName').css({ 'border':'solid 1px #ff0000'});
      $('#inputName').addClass('alert');
      $('input#inputName').attr("placeholder","Please enter first name");
    } // End if-else
  });   });

  
  //Get started
$("#ra_step1_submit").click(function()
{ 
  var Website = $('#website1').val(); 
  if(Website == '')
  {
    $('#error-message').text("Website URL is required.");
  }
  else {

    $('#websiteName').text(Website);
    $('#website-url').val(Website);
    $('#test').show();
    $('#hide-form').hide();
  } 
});

 //PLACEHOLDER
(function(e){e.fn.placeholderLabel=function(t){var n=e.extend({placeholderColor:"#898989",labelColor:"#4AA2CC",labelSize:this.css("font-size"),useBorderColor:true,inInput:true,timeMove:200},t);var r=function(e,t,r){var i=e.height();var s=Number(r.replace("px",""))+i/2;if(!n.inInput){s+=i/2;e.css("background-color","")}e.animate({marginTop:"-="+s,fontSize:n.labelSize},n.timeMove);t.keyup()};e(this).each(function(t,i){var s=e(i);if(s.attr("bind-placeholder-label")!=undefined){var o=s.css("padding-top");r(s.prev(),s,o)}var u=s.css("border-color");var a=s.css("font-size");if(s.attr("placeholder")){var f=e("<label></label>");f.css("position","absolute");f.css("cursor","initial");f.css("color",n.placeholderColor);f.css("font-size",a);var l=s.attr("placeholder");s.removeAttr("placeholder");f.text(l);var c=s.position().left;var o=s.css("padding-top");var h=s.css("padding-left");var p=s.css("margin-top");var d=s.css("margin-left");f.css("margin-top",Number(o.replace("px",""))+Number(p.replace("px","")));f.css("margin-left",Number(h.replace("px",""))-5+Number(d.replace("px","")));f.css("padding-left","5px");f.css("padding-right","5px");f.css("background-color",s.css("background-color"));var s=s;f.click(function(){s.focus()});s.focus(function(){if(n.useBorderColor){s.css("border-color",n.labelColor)}f.css("color",n.labelColor);if(!s.val().length){var e=f.height();var t=Number(o.replace("px",""))+e/2;if(!n.inInput){t+=e/2;f.css("background-color","")}f.animate({marginTop:"-="+t,fontSize:n.labelSize},n.timeMove)}});s.blur(function(){if(n.useBorderColor){s.css("border-color",u)}f.css("color",n.placeholderColor);if(!s.val().length){var e=f.height();var t=Number(o.replace("px",""))+e/2;if(!n.inInput){t+=e/2;f.css("background-color","")}f.animate({marginTop:"+="+t,fontSize:a},n.timeMove)}});if(s.attr("alt")){var v=s.attr("alt");var m=f.text();s.removeAttr("alt");s.keyup(function(){if(s.val().length){f.text(v)}else{f.text(m)}})}s.before(f);if(s.val().length){r(f,s,o)}return s.attr("bind-placeholder-label","true")}else{return null}})}})(jQuery)

