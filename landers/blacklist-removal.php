<?php
//DEFINITIONS
$title = "Website Security built for small businesses";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="blacklist-removal";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(844) 303-1594";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="blacklist-removal">
            <?php include 'includes/assets/ALPHA.php';?>
                
                    <!-- blacklist-removal -->
                    <section class="p-5 bg-main">
                        <div class="container">
                            <div class="row">
                           

                                <div class="col-md-6 col-sm-12">
                                <img src="img/logo.svg" alt="SiteLock" width="200" class="img-responsive mb-30" />
<h2 class="sourceBlack blue upper-font">Fast, Affordable Blacklist Removal and Prevention</h2>
<p>Fast, affordable blacklist removal and prevention for websites. Protect your website and enhance SEO rankings. Automatically find and remove malware.</p>
                            
                        <div class="media">
                           <div class="media-left iconLg"><img src="img/auto.svg" alt="industry leading" /></div>
                           <div class="media-body"><strong>Proven:</strong> Protecting more than 12 million websites and proactively keeping them off blacklists</div>
                        </div>
                        <br>
                        <div class="media">
                           <div class="media-left iconLg"><img src="img/website-performance.svg" alt="industry leading" /></div>
                           <div class="media-body"><strong>Fast:</strong> The fastest blacklist removal in the industry</div>
                        </div>
                        <br>

                        <div class="media">
                           <div class="media-left iconLg"><img src="img/fast.svg" alt="industry leading" /></div>
                           <div class="media-body"><strong>Affordable:</strong> Blacklist removal and protection for every budget</div>
                        </div>
                        <br>

                        <div class="media">
                           <div class="media-left iconLg"><img src="img/comprehensive.svg" alt="industry leading" /></div>
                           <div class="media-body"><strong>Comprehensive:</strong> Largest threat database means the most effective blacklist protection</div>
                        </div>
                        <br>

                        <div class="media">
                           <div class="media-left iconLg"><img src="img/superior-support.svg" alt="industry leading" /></div>
                           <div class="media-body"><strong>24/7 U.S.-Based Support:</strong> Available via phone or live chat, 24/7 for blacklist removal and protection</div>
                        </div>
<div class="whiteSpace50"></div>
                                </div>

                                 <div class="col-md-6 col-sm-12 blue-bg">

 <div class="form-container">
 <div class="whitelink text-center col-md-12"><h2 class="sourceBlack"><a href="tel:8443031594 "><img src="img/phone-icon-white.svg" alt="phone" width="36" height="36" /> (844) 303-1594 </a></h2></div><br>
      <h3 class="sourceBlack text-center white upper-font">Website Blacklist Protection</h3>

     <form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
			<span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
         <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
         <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 whiteLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<span style="padding:0 20px 0px 25px;" ><input class="btn btn-red upper-font bold" type="submit" value="Blacklist Removal &amp; Prevention" style="width: 90%;"></span>
		</form>
                                </div>

                            
                                    </div>


                            </div>
                        </div>
                    </section>


            


                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "(844) 303-1594";
$hb_btn = 'Start Today';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>