<?php
//DEFINITIONS
$title = "Fast Website Suspension Repair";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="suspended-website";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 263-8630";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>>

        <body id="websitewarning">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <section class="p-5">
                        <div class="container">
                            <div class="row container1">
                                <div class="col-md-6 col-xs-12 white black3">
                                    <h1 class="sourceBlack">Fast Website<br> Suspension Repair</h1>
                                    <p class="sourceLight banner-paragraph" style="max-width: 470px;">SiteLock, the industry leader in website security, quickly identifies and repairs issues that cause website suspensions. Contact us today to restore your website.</p>
                                    <ul class="list-head">
                                        <li><b>Fastest Full-Service Suspension Removal:</b> We immediately identify and resolve site malware and resubmit your site to search engine providers. No one does it faster.</li>
                                        <li><b>24/7/365 Availability:</b> Our U.S.-based team works around the clock to ensure your satisfaction.</li>
                                        <li><b>Prevent Future Suspensions:</b> After repairing your site we put in place automated security solutions to protect you against future suspension.</li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-container formContainer">
                                        <h3 class="sourceBlack text-center red">Fix My Website Now</h3>

  <form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
			<span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
            <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<input class="btn btn-blue upper-font bold" type="submit" value="Fix My Site">
		</form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--The Trusted Security Provider for 12M+ Websites Section start-->
                    <section class="grey-bg-lite p-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="text-center sourceLight">The Trusted Security Provider for 12M+ Websites</h1>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Why Does My Website Have a Warning Message? Section-->
                    <section class="p-5">
                        <div class="container text-center">
                            <div class="row">

                                <div class="col-md-6 col-xs-12 text-left">
                                    <h2 class="red sourceBlack">Why Is My Website Suspended?</h2>
                                    <p>Your website is suspended, which means the hosting provider has temporarily taken it offline. Website hosts suspend websites for a myriad of reasons ranging from malware to spam.  They suspend websites to protect their servers and the other websites on them from infection.</p>

                                    <a href="#HighBarrierForm" class="btn btn-blue bold" data-toggle="modal" data-target="#HighBarrierForm">FIX MY SITE</a>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <img src="img/website-warning-messages/blacklisted.jpg" width="500" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--How does TrueShield™ work Section-->
                    <section class="p-5">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <img src="img/website-warning-messages/warningArtboard.png" width="500" class="img-responsive">
                                </div>
                                <div class="col-md-6 col-sm-12 text-left">
                                    <h2 class="red sourceBlack">What are the Consequences of Suspended Website?</h2>
                                    <p>When your website is suspended, it is inaccessible to your visitors. This puts you at risk of losing new customers, upsetting existing customers, and potentially damaging your reputation.  As your trusted security partner, SiteLock can detect and repair the malware causing your site issue and provide automated protection to prevent future infections.</p>
                                    <a href="#HighBarrierForm" class="btn btn-blue bold" data-toggle="modal" data-target="#HighBarrierForm">GET STARTED</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Protect Your Bacon Section-->
                    <section class="blue-bg p-5">
                        <div class="container white">
                            <div class="row">
                                <div class="col-md-8 col-sm-12 offset-md-2 text-left">
                                    <h2 class="white sourceBlack">Protect Visitor Trust and Confidence</h2>
                                    <p class="font18">"Trust and confidence for our customers is paramount for us. SiteLock prevents the malware, spam and virus attacks. We strongly recommend SiteLock for every website.”</p>
                                    <p class="float-right"><strong>- S. DUDAKA</strong></p>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Free Risk Assessment Section-->
                    <section class="red-bg p-5">
                        <div class="container white">
                            <div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 text-center">
                                    <img src="img/website-warning-messages/Assessment.png" width="240" class="img-responsive">
                                </div>
                                <div class="col-md-7 col-sm-12 col-xs-12 text-left">
                                    <h2 class="white sourceRegular">Free Risk Assessment</h2>
                                    <p>A fantastic way to learn more about website security is to get a free risk assessment for your site. <br>The Website Risk Assessment is SiteLock’s proprietary predictive model that gives you information on unique features or qualities that may affect your site’s risk of being attacked.</p>
                                    <a href="#HighBarrierForm" class="btn btn-ghost-white bold" data-toggle="modal" data-target="#HighBarrierForm">GET MY FREE RISK ASSESSMENT</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Fix My Website Now";
$hb_phone = "(833) 263-8630";
$hb_btn = 'FIX MY SITE';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>