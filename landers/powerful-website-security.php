<?php
//DEFINITIONS
$title = "The Most Powerful Website Security";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="powerful-website-security";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(877) 563-2791";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="powerful-website">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <!--Banner Section-->
                    <section class="image-banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <h1 class="sourceBlack font55">The Most Powerful <br>Website Security</h1>
                                    <h3 class="sourceRegular">PLUS A PRICE MATCH GUARANTEE </h3>
                                    <ul class="sourceLight font25">
                                        <li>Only fully automated website security solution</li>
                                        <li> Largest website threat database</li>
                                        <li>U.S.-based 24/7 customer service</li>
                                    </ul>
                                </div>
                                <div class="col-md-5 col-xs-12">
                                <div class="form-container"> 
                                        <h2 class="sourceBlack text-center">SECURE MY WEBSITE</h2>
                                        <form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
			<span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
            <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<span style="padding:0 20px 0px 25px;" ><input class="btn btn-blue upper-font bold" type="submit" value="CHECK MY WEBSITE" style="width: 90%;"></span>
		</form>

</div>                  
                                </div>
                            </div>

                        </div>

                    </section>

                    <!--The Trusted Security-->
                    <section id="mainDarkGrey">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <br/>
                                    <h1 class="text-center sourceLight">Trusted by 12M+ Companies</h1>
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/Tennis-Channel.png" width="70">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/new-orleans.png" width="200">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/frontier.png" width="150">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/nextiva.png" width="150">
                                </div>

                            </div>
                            <div class="whiteSpace25"></div>
                        </div>
                        </div>
                    </section>

                    <!--Advance Your Website Security Section-->
                    <section class="grey-bg p-3">
                        <div class="container white">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12 text-left">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <h1>Advanced Website Security</h1>
                                    <p>SiteLock® SMART® (Secure Malware Automatic Removal Tool) provides you worry-free website security. SMART continuously checks your site from all angles to detect malware. If malware is found it is automatically removed, so your website stays safe and secure.</p>
                                    <a href="#HighBarrierForm" class="btn btn-red" data-toggle="modal" data-target="#HighBarrierForm">GET SMART</a>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 pull-left">
                                    <iframe src="https://player.vimeo.com/video/174228615?title=0&byline=0&portrait=0" width="90%" height="350" class="ifr" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Protect Your Bacon Section-->
                    <section class="blue-bg p-5">
                        <div class="container color-white">
                            <div class="row text-center">
                                <div class="col-12">
                                    <img src="img/powerful-website-security/heart.png" width="147">
                                    <h2 class="sourceBlack white">Protect Your Bacon</h2>
                                    <div class="row justify-content-center">
                                        <div class="col-8">
                                            <p class="white font18">“I was faced with the possibility of re-developing a whole site. In the end the malicious software was removed and the password for my database was restored. Good work!”</p>
                                        </div>
                                    </div>
                                    <p class="white sourceRegular"><a href="https://www.trustpilot.com/users/59d809b10000ff000ad1f60f" class="linkwhite">R. Gaustad</a><br>
                                        <span><a href="https://voifone.com/" target="_blank" class="linkwhite">voifone.com</a></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Reap the Benefits of Website Security-->

                    <section class="p-5">

                        <div class="container text-center grey">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1 class="blue sourceLight font45">Reap the Benefits of Website Security</h1>
                                    <p class="font25 sourceLight">Website security not only protects your website from infection but provides direct bottom line benefits.</p>
                                    <br/>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                <img src="img/ssl/pci.png" width="177" class="img-responsive padding10">
                                    <br>
                                    <h4 class="sourceBlack blue">BUILD TRUST</h4>
                                    <p>Trust Seals, displayed on secure and malware-free websites, are proven to boost trust and conversions.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                <img src="img/ssl/seo.png" width="145" class="img-responsive padding10">
                                    <br>
                                    <h4 class="sourceBlack blue">IMPROVE SEO</h4>
                                    <p>Detect, remove and block website security issues that negatively impact your search engine rankings.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                <img src="img/ssl/performance.png" width="105" class="img-responsive padding10">
                                    <br>
                                    <h4 class="sourceBlack blue">INCREASE PERFORMANCE</h4>
                                    <p>Automatically repair malware and site issues that slow performance and degrade visitor experience.
                                    </p>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#HighBarrierForm" class="btn btn-ghost-red" data-toggle="modal" data-target="#HighBarrierForm">PROTECT & BOOST YOUR WEBISTE
</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Shantel B. Ellenfield SBECPA Section-->
                    <section class="blue-bg p-5 security">
                        <div class="container text-center white">
                            <div class="row text-left">
                                <div class="col-md-5 col-sm-8 col-xs-12 pull-left">
                                    <p class="sourceRegular font25"><em>“SiteLock gives me peace of mind. It’s one less thing to worry about and frees up more of my time to spend on meeting my client’s needs.”</em></p>
                                    <br>
                                    <h4>S. Ellenfield<br><span class="sourceRegular">SBECPA</span></h4>
                                </div>
                                <div class="col-md-7 col-sm-8 col-xs-12"></div>
                            </div>
                        </div>
                    </section>

                    <!--Talk with a Security Specialist-->
                    <section>
                        <div class="whiteSpace50"></div>
                        <div class="container text-center">
                            <div class="row">

                                <div class="col-md-12">
                                    <h2 class="blue">Talk with a Security Specialist</h2>
                                    <h4 class="sourceLight linkblue">
                                    Talk with a website security specialist to learn about the security needs of your website.
</h4>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#HighBarrierForm" class="btn btn-red" data-toggle="modal" data-target="#HighBarrierForm">CONTACT US</a>
                                </div>
                            </div>
                            <div class="whiteSpace50"></div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "SECURE MY WEBSITE";
$hb_phone = "(877) 563-2791";
$hb_btn = 'Start Now';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>