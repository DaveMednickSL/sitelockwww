<?php
//DEFINITIONS
$title = "WEBSITE SECURITY STATISTICS";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="security-trend-lander";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 263-8630";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="security-trend">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                   <!--Banner Section-->
                   <section>
                <div class="container text-left grey-bg1">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 text-left"><br>
                        <h2 class="lg-text5 sourceLight">WEBSITE SECURITY STATISTICS</h2>
                            <h1 class="text-left no-margin sourceBlack">IMPACTING HOSTING PROVIDERS</h1>
                            <div class="whiteSpace25"></div>
 <div class="row no-margin">
<div class="col-md-2 red-bg padding-around-10"></div>
<div class="col-md-2 blue-bg padding-around-10 mx-5 d-none d-md-block"></div>
<div class="col-md-2 green-bg padding-around-10 mx-5 d-none d-md-block"></div>
<div class="col-md-2 orange-bg padding-around-10 mx-5 d-none d-md-block"></div>
</div>
<div class="whiteSpace25"></div>
                            <p class="sourceLight font18">Plus, how to monetize those trends</p>
                    </div>

                    </div>
                </div>
            </section>

           <section>
                <div class="container white text-left image-banner">
                    <div class="row">

                        <div class="col-md-3 col-sm-12 text-left shadow">
                            <h3 class="text-center no-margin sourceBlack padding-around-10 orange-bg linkwhite"><a href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Get the Free Guide</a></h3>
                    </div>

                    </div>
                </div>
            </section>




 <!--Worry-Free Website Protection-->
 <section class="p-5">
                <div class="container">
                    <div class="row">
                       
                        <div class="col-md-7 col-xs-12">
                        <h3 class="sourceBlack">Protect and Grow Your Business</h3>
                        <p>It’s predicted that cybercrime will cost the world $6 trillion annually by 2021. As a hosting provider, you are not only indirectly responsible for the websites in the crosshairs of these cybercriminals, but your business directly feels the pain associated with these villainous acts.</p>
                        <p>In this white paper you will learn about:</p>
                        <ul>
                        <li>Trends in cybersecurity stats that are impacting your hosting business.</li>
                        <li>This includes statistics relating to attacks on websites, updates to CMS platforms, and bot traffic.</li>
                        <li>The impact these threats can have on your hosting business.</li>
                        <li>How you can both mitigate these negative experiences and turn them into an opportunity to grow your business.</li>
                        </ul>
                        <p>Download the guide today.</p>
                        

                        </div>

                         <div class="col-md-5 col-xs-12 col-sm-12 float-sm-left">
                            
                        
                            <div class="form-container"> 
                                        <h3 class="sourceBlack text-center">Download the Free Guide</h3>
                                        
                                        
                                        <form method="post" class="mt-20" action="https://a29565.actonservice.com/acton/eform/29565/f370b272-2264-4afa-bfea-f962cfb91e3d/d-ext-0002">
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
			<span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
            <input type="hidden" name="Form ID" value="<FORM ID>">
           <input type="hidden" name="Lead Source" value="Act-On Form">
           <input type="hidden" name="Lead Group" value="Marketing">
           <input type="hidden" name="Accepts Email" value="Accepts Email">
           <input type="hidden" name="Division" value="Channel">
           <input type="hidden" name="Lead Type" value="Display">
           <input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<span style="padding:0 20px 0px 25px;" ><button class="btn btn-blue upper-font bold" type="submit" style="width: 90%;">Email Me The Guide</button></span>
		</form>

</div>                  



                       
                        </div>
                    </div>
                </div>
            </section>




                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "(833) 263-8630";
$hb_btn = 'Start Today';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>