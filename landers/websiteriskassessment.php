<?php
//DEFINITIONS
$title = "Website Risk Assessment | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="websiteriskassessment";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 715-1302";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="risk-assessment" class="black-bg">
            <?php include 'includes/assets/ALPHA.php';?>
               
               
<!-- What We Do section -->
<div id="hide-form">
<section class="image-banner">

<div class="container">
    <div class="row">
        
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 offset-lg-7">
        <img src="img/sitelock_logo_black_bg.svg" alt="website security" width="250" class="mt-80" /> 
<br>
        <h3 class="sourceLight">Is Your Website&nbsp;a Security&nbsp;Risk?</h3>
                <form id="risk-assess01" name="risk-assess01" action="websiteriskassessment" method="post">
                <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">

                    <input type="text" placeholder="Enter Domain Name" id="website1" name="acton_domain_c" data-validation="url" class="input-style"/>
                    <div class="red padding5" id="error-message"></div>
                    <input type="button" class="btn btn-red bold" name="ra_step1_submit" id="ra_step1_submit" value="Get My Free Website Risk Assessment" />
                </form>  
                
                <br />
                
<div class="row justify-content-center ">
<div class="col-12"><p>SiteLock is recognized as the global leader in website security, with more than 12 million websites protected.</p></div>
                     <div class="col-3">
                     <img src="img/legal/cnbc-logo-white.svg" width="50" class="img-responsive" /> 
                     </div>

                     <div class="col-3">
                     <img src="img/legal/forbes-logo-white.svg" width="70" class="img-responsive" /> 
                     </div>

                      <div class="col-3">
                      <img src="img/legal/wsj-logo-white.svg" width="80" class="img-responsive img70" />              
                     </div>

                     <div class="col-3">
                     <img src="img/legal/bloomberg-logo-white.svg" width="120" class="img-responsive" /> 
                     </div>
                </div>
<!--End 'press-logos'-->
        </div>

    </div>
</div>
</section>
</div>  <!--hide form'-->         

<div id="test" style="display:none">

 <section class="black-bg p-4 banner-small">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-12">
                            <img src="img/sitelock_logo_black_bg.svg" width="250" class="text-center" /> 
                            <h2 class="white sourceLight">Is Your Website A Security Risk?</h2>
                        </div>
                            </div>
                        </div>
                    </section>

<section class="white-bg p-4">
                        <div class="container text-center">
                            <div class="row">
                            <div class="col-sm-6 offset-sm-3">
<div class="form-container">
 <p class="sourceLight text-center">Enter the information below to get your website risk assessment. Your assessment will examine your risk level, provide a quick scan for current infection, and examine your site for malicious links.</p>
<form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
<h3 class="sourceBlack font18">Company Information:</h3> 
<div id="websiteName" class="red"></div>
        <span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
        <span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
        <span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
        <h3 class="sourceBlack font18">Tell Us Where To Send The Report:</h3> 
        <span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>   
        <input type="hidden" name="Form ID" value="<FORM ID>">

              <input type="hidden" id="website-url"  name="HBinputDomain" value="">
              <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
              <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
              <br>
              <p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

              <input class="btn btn-blue upper-font bold" type="submit" value="Get My Report">
              </form>

             </div>

            </div>

             </div>
            </div>
            </section>

    </div>



                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>