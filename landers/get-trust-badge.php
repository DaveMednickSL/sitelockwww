<?php
//DEFINITIONS
$title = "Boost Visitor Trust, Site Speed and Conversions";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="get-trust-badge";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "833-715-1302";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="get-trust-badge">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <!--Banner Section-->
                    <section class="image-banner p-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 mt-80">
                                    <h1 class="sourceBlack font45">Boost Visitor Trust, Site Speed and Conversions</h1>
                                    <p class="white">Join the 12M websites using a SiteLock trust badge to protect and enhance their website.</p>

                                    <a href="#HighBarrierForm" class="btn btn-red bold" data-toggle="modal" data-target="#HighBarrierForm">Get My Trust Badge</a>
                                </div>
                            </div>
                        </div>

                    </section>

                    <!--news-bar-->
                    <section class="grey-bg1">
                        <div class="container">
                            <div class="row">
                                <?php include('includes/news-bar-1.php');?>
                            </div>
                        </div>
                    </section>

                    <!--Select the Industry Leader in Malware Removal Section start-->
                    <section class="p-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <h1 class="blue sourceBlack">Super Power Your Website</h1>
                                    <p>
                                        When infusing your website with SiteLock security products you automatically protect your site, and its visitors from the danger of cyber villains. Plus, by displaying your new superpowers you can increase visitor trust, conversions and website performance.
                                    </p>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="card grey-bg2">
                                        <!--<span class="glyphicon glyphicon-plus"></span>-->
                                        <br>
                                        <p class="cardp"><span class="blue sourceBlack">Simple:</span> No need for a complex origin story here, you can sign up and activate your new site security and enjoy your new powers in minutes.</p>
                                    </div>
                                    <div class="card grey-bg1">
                                        <!--<span class="glyphicon glyphicon-plus"></span>-->
                                        <br>
                                        <p class="cardp"><span class="blue sourceBlack">Fast:</span> Increase website speed by up to 50% by using our Content Delivery Network, minimizing code and keeping your site free of infection.</p>
                                    </div>
                                    <div class="card grey-bg2">
                                        <!--<span class="glyphicon glyphicon-plus"></span>-->
                                        <br>
                                        <p class="cardp"><span class="blue sourceBlack">Converting:</span> Your new website powers not only protect your visitors but convey to them the trust needed to convert.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Trust and Performance Matter-->
                    <section class="grey-bg1 p-5">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2 class="color-red text-center sourceBlack red">Trust and Performance Matter</h2>
                                    <br/>
                                </div>

                                <div class="col-sm-4 col-xs-12">
                                    <span class="sourceBlack font45">25% </span>
                                    <p>
                                        of shopping carts are abandoned because customers don't know if they can trust a business. 1</p>
                                </div>

                                <div class="col-sm-4 col-xs-12">
                                    <span class="sourceBlack font45">61%</span>
                                    <p>
                                        of consumers have not purchased something online because trustmarks were missing.2</p>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <span class="sourceBlack font45">46%</span>
                                    <p>
                                        Of users will abandoned a site that takes more than 3 seconds to load. 3</p>
                                </div>

                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <p><sup>1</sup> According to Baymard Institute on January 9, 2017 , <sup>2</sup> According to Actual Insights in August 2011, <sup>3</sup> According to Dynatrace Gomez study</p>

                            </div>
                        </div>
                    </section>

                    <!--Security Superheroes-->
                    <section id="photo" class="p-5">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2 class="sourceBlack">Why Website Owners Love SiteLock</h2>
                                    <br>
                                </div>
                                <br>
                                <div class="col-lg-4 col-xs-12">
                                    <div class="card">
                                        <img src="img/photographer.png" class="img-responsive header-bg" width="300" height="250">
                                        <div class="avatar red-bg">
                                            <i class="fa fa-quote-left white"></i>
                                        </div>
                                        <div class="content">
                                            <p>I receive weekly reports that show me how much bandwidth I am saving and how many bad bots are being blocked. It feels great to know that my website is being protected.</p>
                                            <!-- <p><a href="https://www.sitelock.com/blog/2018/05/case-study-amanda-naor-photography/" target="_blank">Read More</a></p> -->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-xs-12">
                                    <div class="card">
                                        <img src="img/restaurant.jpg" class="img-responsive header-bg" width="300" height="250">
                                        <div class="avatar blue-bg">
                                            <i class="fa fa-quote-left white"></i>
                                        </div>
                                        <div class="content">
                                            <p>The customer support team was helpful and knowledgeable. They worked with me to find the right security products for my specific needs. I would definitely recommend SiteLock. </p>
                                            <!-- <p><a href="https://www.sitelock.com/blog/2018/05/sitelock-reviews-marlowesmemphis/" target="_blank">Read More</a></p> -->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-xs-12">
                                    <div class="card">
                                        <img src="img/buckingstocktalk.jpg" class="img-responsive header-bg" width="300" height="250">
                                        <div class="avatar red-bg">
                                            <i class="fa fa-quote-left white"></i>
                                        </div>
                                        <div class="content">
                                            <p>With SiteLock protecting my website, my traffic and page views have increased by more than 50%. My visitors are leaving their email information to the subscription list with confidence.</p>
                                            <!-- <p><a href="https://www.sitelock.com/blog/2018/05/sitelock-reviews-buckingstocktalk/" target="_blank">Read More</a></p> -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </section>

                    <!--Call me today-->
                    <section class="p-5">
                        <div class="container text-center">
                            <div class="row red-border">
                                
                                    <div class="col-sm-6">
                                        <p class="text-right mt-20 ">
                                            Learn how to protect and enhance your website
                                        </p>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                    <a href="#HighBarrierForm" class="btn btn-red bold" data-toggle="modal" data-target="#HighBarrierForm">Contact Me Today</a>
                                    </div>
                                

                            </div>
                        </div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Boost Trust, Boost Your Website";
$hb_phone = "833-715-1302";
$hb_btn = 'Get My Trust Badge';
?>

	
  <!-- High Barrier -->
  <div class="modal fade" id="HighBarrierForm" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content modalForm modalFix">
      <div class="modal-header">
        <h5 class="modal-title sourceBlack padding-left25 padding10" id="exampleModalLabel"><?php echo $hb_title; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="fal fa-times-circle" style="user-select: auto;"></i>
        </button>

      </div>
      <div class="modal-body padding25">
        <p class="redLink padding-left25">Fill out the form or contact us at <a href=""><?php echo $hb_phone; ?></a> to get started on boosting website trust.</p>
      <form class="mt-20" method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing"">

        <span class="form-inline form-group"><i id="validateName" class="far fa-circle grey fa-lg" title="First Name should be formatted as firstname"></i>&nbsp;<input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder" for="HBinputName">First Name</label>

        </span>
        <span class="form-inline form-group"><i id="validatelastName" class="far fa-circle grey fa-lg" title="Last Name should be formatted as lastname"></i>&nbsp;<input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder" for="HBinputlastName">Last Name</label>

        </span>

        <span class="form-inline form-group"><i id="validateDomain" class="far fa-circle grey fa-lg" title="Domain Name should be formatted as domain.com"></i>&nbsp;<input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder" for="HBinputDomain">Domain</label>
 
        </span>

        <span class="form-inline form-group"><i id="validateEmail" class="far fa-circle grey fa-lg" title="Email should be formatted as you@domain.com"></i>&nbsp;<input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder" for="HBinputEmail" >Email</label>

        </span>

        <span class="form-inline form-group"><i id="validatePhone" class="far fa-circle grey fa-lg" title="Phone should be formatted as 0001112222"></i>&nbsp;<input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder" for="HBinputPhone">Phone Number</label>

        </span>

      <!-- <div class="custom-control custom-checkbox" style="font-size: 14px; padding-left:48px !important;">
        <input type="checkbox" class="custom-control-input" id="customCheck1">
        <label class="custom-control-label" for="customCheck1">Email me the latest website security news</label>
      </div> -->
            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
            <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
      <br>
      <p class="text-center"><button type="submit" class="btn btn-primary"><?php echo $hb_btn; ?></button></center>
      </form>
      </div>
      <div class="modal-footer">
        <p class="form-notice">By submitting this form you confirm that you have read and accepted our <a href="terms" target="_blank">Terms & Conditions</a> and <a href="privacy-policy" target="_blank">Privacy Policy</a></p>
      </div>
    </div>
  </div>
</div>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>