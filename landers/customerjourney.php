<?php
//DEFINITIONS
$title = "Customer Journey";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="customerjourney";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 686-5388";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="customerjourney">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <!--Banner Section-->
                    <section class="banner-bg">
                        <div class="container white">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12 txt-s-center">
                                    <h2 class="sourceLight font45">Worry Free. No Sweat. Website Protection.</h2>
                                    <h4 class="sourceBlack">LEARN HOW SITELOCK PROVIDES A SUPERIOR<br> WEBSITE SECURITY EXPERIENCE. </h4>
                                    <br>
                                    <a href="#HighBarrierForm" class="btn btn-blue bold" data-toggle="modal" data-target="#HighBarrierForm">START YOUR SECURITY JOURNEY</a>
                                </div>
                                <div class="col-md-6 col-sm-6 white-bg banner-right col-sm-12">
                                    <div class="video">
                                        <iframe src="https://www.youtube.com/embed/N4TgirARTn4?rel=0" class="ifr" width="540" height="300" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Experience Section-->
                    <section class="p-5">
                        <div class="container quote padding-bottom-20">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                                    <br>
                                    <img src="img/customerjourny/David-Ryan1.jpg" width="550" class="img-responsive move-top">
                                    <br>
                                </div>
                                <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                                    <h2 class="red sourceLight font45 t-font text-left">Experience the <br>SiteLock Difference</h2>
                                    <p class="para-w text-left">In order to become the global leader in website security, you have to do things differently. Powered by a mission to protect every website on the internet, our customers benefit from industry-leading technology, 24/7 U.S.-based support, reliable performance, and a world-class customer experience to back it up.</p>
                                    <a href="#HighBarrierForm" class="btn btn-blue b-align bold" data-toggle="modal" data-target="#HighBarrierForm">GET STARTED</a>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Your Journey to a Secure Site Section-->
                    <section>
                        <div class="container">
                            <div class="row">
                                <div class="security-col">
                                    <h1 class="sourceBlack blue-bg padding10 text-center white">Your Journey to a Secure Site</h1>
                                    <p class="text-peek">Here’s a peek at your journey to a secure site
                                        <br /> and what to expect along the way!
                                    </p>
                                    <div class="main-container">
                                        <img src="img/customerjourny/right-arrow1.png" width="245" class="arrow-icon">
                                        <div class="number-bg number-bg2">
                                            <div class="triangle-top">
                                                <div class="triangle-bottom"></div>
                                            </div>
                                            <h2>1</h2>
                                        </div>
                                        <h3 class="section-heading sourceLight">Security Consultation</h3>
                                        <div class="gray-bg">
                                            <p>As a SiteLock customer, your first interaction will be with one of our Website Security Consultants. They’ll help you choose the best security solution for your website.</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br>
                                        <div class="col-md-12 mt-80">
                                            <img src="img/customerjourny/left-arrow.png" width="230" class="arrow-icon-2">
                                            <div class="left-space">
                                                <div class="number-bg">
                                                    <div class="triangle-top">
                                                        <div class="triangle-bottom"></div>
                                                    </div>
                                                    <h2>2</h2>
                                                </div>
                                                <h3 class="section-heading-1 sourceLight">Onboarding</h3>
                                                <div class="gray-bg bg-height">
                                                    <p>Next, you will be partnered with a SiteLock onboarding specialist to help get you up and running quickly, and ensure you know the important details about your services, contract and customer dashboard.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="container mt-80">
                            <div class="row">
                                <div class="security-col">
                                    <div class="main-container">
                                        <img src="img/customerjourny/right-arrow.png" width="200" class="arrow-icon3">
                                        <div class="number-bg number-bg2">
                                            <div class="triangle-top">
                                                <div class="triangle-bottom"></div>
                                            </div>
                                            <h2>3</h2>
                                        </div>
                                        <h3 class="section-heading sourceLight">Set-Up</h3>
                                        <div class="gray-bg">
                                            <p>Finally, a SiteLock Technical Support team member will make sure all your services are set up correctly and running efficiently.</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br>
                                        <div class="col-md-12 mt-80">
                                            <img src="img/customerjourny/left-arrow1.png" width="230" class="arrow-icon-4">
                                            <div class="left-space">
                                                <div class="number-bg">
                                                    <div class="triangle-top">
                                                        <div class="triangle-bottom"></div>
                                                    </div>
                                                    <h2>4</h2>
                                                </div>
                                                <h3 class="section-heading-2 sourceLight">Proactive Communications</h3>
                                                <div class="gray-bg bg-height gray-bgs">
                                                    <p>But the conversation doesn’t stop here. As part of your subscription, we will send you weekly reports on the security of your site, so you can stay informed and proactive about any important updates.</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>

                    <section>
                        <div class="container padding25">
                            <div class="row text-center">
                                <div class="col-sm-12 ">
                                    <a href="#HighBarrierForm" class="btn btn-ghost-red bold" data-toggle="modal" data-target="#HighBarrierForm">GET STARTED</a>
                                </div>
                            </div>
                        </div>
<br/>
                    </section>

                    <!--Great Website Deserves Great Protection Section-->
                    <section class="grey-bg-lite p-5">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <h2 class="blue-bg heading-top padding10 sourceBlack h-font white">A Great Website Deserves Great Protection.</h2>
                                </div>
                                <div class="col-md-4 col-sm-12 col-lg-4">
                                    <img src="img/customerjourny/automatic.png" width="190" class="padding10" >
                                    <h4 class="sourceBlack">AUTOMATIC</h4>
                                    <p>The only solution that automatically detects, removes and blocks threats.</p>
                                </div>
                                <div class="col-md-4 col-sm-12 col-lg-4">
                                    <img src="img/customerjourny/clear.png" width="190" class="padding10">
                                    <h4 class="sourceBlack">CLEAR</h4>
                                    <p>Easy to understand weekly reports make digesting security simple.</p>
                                </div>
                                <div class="col-md-4 col-sm-12 col-lg-4">
                                    <img src="img/customerjourny/quick.png" width="190" class="padding10" >
                                    <h4 class="sourceBlack">QUICK</h4>
                                    <p>Cloud-based technology deploys and protects your site in minutes.</p>
                                </div>
                                <div class="col-md-12 col-sm-12 text-center">
                                    <a href="#HighBarrierForm" class="btn btn-blue bold" data-toggle="modal" data-target="#HighBarrierForm">PROTECT MY SITE</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "(833) 686-5388";
$hb_btn = 'Start Today';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>