<?php
//DEFINITIONS
$title = "Website Security built for small businesses";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="ddos-lander";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 686-5384";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="ddos-lander">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <!-- Top header section -->
                    <section class="red-bg p-5">
                        <div class="container white">
                            <div class="row">
                                <div class="col-sm-8 text-center offset-md-2">
                                    <h1 class="sourceLight">Automatic Mitigation for<br>Sophisticated DDoS Attacks</h1>
                                    <p>SiteLock offers website owners powerful, always-on DDoS protection that quickly defeats attacks without impacting site performance or visitors.</p>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- banner section -->
                    <section class="p-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <img src="img/desktop_scanning.png" class="text-center zindex-top img-responsive" width="500">
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <div class="form-container">
                                        <h3 class="sourceBlack text-center red">Mitigate DDoS Attacks Now</h3>

                          <form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
			<span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
            <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<span style="padding:0 20px 0px 20px;" ><input class="btn btn-blue upper-font bold" type="submit" value="Protect My Website" style="width: 90%;"></span>
		</form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--The Trusted Security-->
                    <section id="mainDarkGrey">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <br/>
                                    <h1 class="text-center sourceLight">SiteLock Makes Website Scanning <br>Simple for 12M+ Companies</h1>
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/Tennis-Channel.png" width="70">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/new-orleans.png" width="200">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/frontier.png" width="150">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/nextiva.png" width="150">
                                </div>

                            </div>
                            <div class="whiteSpace25"></div>
                        </div>
                        </div>
                    </section>

                    <!--Why More Website Owners Choose SiteLock-->

                    <section id="mainBlue">

                        <div class="container white text-center">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1 class="white sourceLight font45">The Clear Choice in DDos Protection</h1>
                                    
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <img src="img/icon_quick.svg" class="img50 img-circle">
                                    <br>
                                    <h4 class="sourceBlack">QUICKEST RESPONSE</h4>
                                    <p>With nearly 30 data centers around the world, SiteLock quickly detects harmful websites visitor requests to counter DDoS attacks.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <img src="img/icon_unrivaled.svg" class="img50 img-circle ">
                                    <br>
                                    <h4 class="sourceBlack">UNRIVALED ACCURACY</h4>
                                    <p>Our DDoS mitigation has a less than 1% false positive rate, which means more than 99% of your authentic website traffic will be unaffected.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <img src="img/icon_transparent.svg" class="img50 img-circle ">
                                    <br>
                                    <h4 class="sourceBlack">TRANSPARENT PROTECTION</h4>
                                    <p>Legitimate visitors will not encounter latency, CAPTCHAs or annoying wait screens if your website comes under attack.</p>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#HighBarrierForm" class="btn btn-ghost-white bold" data-toggle="modal" data-target="#HighBarrierForm">PROTECT MY SITE</a>

                                </div>
                            </div>
                        </div>
                    </section>

                    <!--What is a Website Virus?-->

                    <section class="p-5 black-bg">

                        <div class="container white">
                            <div class="row">
                                <div class="col-md-5 col-sm-12">

                                    <img src="img/icon_stop_ddos.svg" width="250" class="img-responsive float-right">
                                </div>
                                <div class="col-md-6 col-sm-12 text-left">
                                    <h1 class="white sourceLight">Stop DDoS Attacks</h1>
                                    <p>Distributed denial of service (DDoS) assaults threaten websites with downtime and financial losses. SiteLock is guaranteed to stop DDoS attacks, regardless of size and without getting in the way of legitimate traffic.</p>
                                </div>

                                <div class="col-sm-12 text-center ">
                                    <a href="#HighBarrierForm" class="btn btn-ghost-white bold" data-toggle="modal" data-target="#HighBarrierForm">GET STARTED NOW</a>

                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Get the SiteLock Website Security Report-->

                    <section class="p-5">

                        <div class="container">
                            <div class="row">

                                <div class="col-md-5 col-sm-12 text-left offset-md-2">
                                    <h1 class="sourceLight">Powerful Automated DDoS Protection</h1>
                                    <p>SiteLock's powerful network automatically detects and blocks all DDoS attacks, freeling your time for more strategic IT and business management tasks.</p>
                                </div>
                                <div class="col-md-5 col-sm-12">

                                    <img src="img/icon_powerful_automated.svg" width="250" class="img-responsive float-left">
                                </div>

                                <div class="col-sm-12 text-center ">
                                <a href="#HighBarrierForm" class="btn btn-ghost-blue bold" data-toggle="modal" data-target="#HighBarrierForm">PROTECT MY SITE</a>

                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Mitigate DDoS Attacks Now";
$hb_phone = "(833) 686-5384";
$hb_btn = 'Request Information';
include 'includes/forms/high-barrier.html';
?>


                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>