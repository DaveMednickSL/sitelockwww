<?php
//DEFINITIONS
$title = "Website Security built for small businesses";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="competitor-b";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 686-5384";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="ddos-lander">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <!-- Top header section -->
                    <section class="red-bg p-5">
                        <div class="container white">
                            <div class="row">
                                <div class="col-sm-8 text-center offset-md-2">
                                    <h1 class="sourceLight">Protect and Enhance Your Website With Industry Leading Security&nbsp;Solutions</h1>
                                    <p>12M+ website owners choose SiteLock to secure their website, boost performance, protect reputation, and improve customer trust.</p>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- banner section -->
                    <section class="p-5">
                        <div class="container">
                            <div class="row">

                                <div class="col-md-6 col-sm-12">
                                    <div class="form-container">
                                        <h5 class="sourceLight text-center red">Contact us today to learn how to put our<br> automated solutions to work for you</h5>

                                        <form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
                                            <span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
                                            <span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
                                            <span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
                                            <span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
                                            <span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
                                            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
                                            <input type="hidden" name="Form ID" value="<FORM ID>">
                                            <input type="hidden" name="Lead Source" value="Act-On Form">
                                            <input type="hidden" name="Lead Group" value="Paid Marketing">
                                            <input type="hidden" name="Accepts Email" value="Accepts Email">
                                            <input type="hidden" name="Division" value="Retail">
                                            <input type="hidden" name="Lead Type" value="PPC">
                                            <input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
                                            <span style="padding:0 20px 0px 20px;" ><input class="btn btn-blue upper-font bold" type="submit" value="Secure My Website" style="width: 90%;"></span>
                                            <br><br>
                                            <p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>
                                        </form>

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <img src="img/desktop_scanning.png" class="text-center zindex-top img-responsive" width="500">
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--The Trusted Security-->
                    <section id="mainDarkGrey">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <br/>
                                    <h1 class="text-center sourceLight">Featured in:</h1>
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/Tennis-Channel.png" width="70">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/new-orleans.png" width="200">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/frontier.png" width="150">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/nextiva.png" width="150">
                                </div>

                            </div>
                            <div class="whiteSpace25"></div>
                        </div>
                        </div>
                    </section>

                    <!--Why More Website Owners Choose SiteLock-->

                    <section class="black-bg p-5">

                        <div class="container white text-center">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1 class="white sourceLight font45">Why More Website Owners Choose SiteLock</h1>
                                    <br>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <img src="img/blue-shield.svg" class="img50 img-circle">
                                    <br>
                                    <h4 class="sourceBlack">Proven Results</h4>
                                    <p>With the largest threat database and 24/7 support, we provide the most comprehensive protection in the&nbsp;industry.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <img src="img/rocket-blue.svg" class="img50 img-circle ">
                                    <br>
                                    <h4 class="sourceBlack">Fast &amp; Easy</h4>
                                    <p>Easy setup, and the only solution that automatically detects vulnerabilities and removes malware.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <img src="img/stars.svg" class="img50 img-circle ">
                                    <br>
                                    <h4 class="sourceBlack">Customer Friendly</h4>
                                    <p>Our U.S.-based customer service team is available 24/7/365 to assist with security issues and&nbsp;questions.</p>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#HighBarrierForm" class="btn btn-ghost-white bold" data-toggle="modal" data-target="#HighBarrierForm">Request Information</a>

                                </div>
                            </div>
                        </div>
                    </section>

                    <!--SiteLock Garners Industry&nbsp;Attention-->
                    <section class="blue-bg p-5 industry">
                        <div class="container text-center white">
                            <div class="row text-left">
                                <div class="col-md-7 col-sm-12 col-xs-12"></div>
                                <div class="col-md-4 col-sm-12 col-xs-12 float-right text-right">
                                    <h2 class="sourceBlack upper-font font25">SiteLock Garners Industry&nbsp;Attention</h2>
                                    <p class="sourceRegular">For the second year in a row, SiteLock has earned recognition in the Gartner Magic Quadrant for Application Security&nbsp;Testing&nbsp;(AST).</p>

                                    <a href="#" class="btn btn-red bold">Download Report</a>
                                </div>

                            </div>
                        </div>
                    </section>

                    <!--Get the SiteLock Website Security Report-->

                    <section class="p-5">

                        <div class="container">
                            <div class="row">

                                <div class="col-md-5 col-sm-12 text-left offset-md-2">
                                    <h2 class="sourceBlack upper-font font25">Endpoint Security Isn't Enough</h2>
                                    <p>Over 90% of web-based malware is missed by traditional endpoint security solutions.</p>
                                    <a href="#" class="btn btn-red bold">Download The Report</a>
                                </div>
                                <div class="col-md-5 col-sm-12">

                                    <img src="img/white-paper.png" width="350" class="img-responsive float-left">
                                </div>

                            </div>
                        </div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Contact us today";
$hb_phone = "(833) 686-5384";
$hb_btn = 'Secure My Website';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>