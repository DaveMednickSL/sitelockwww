<?php
//DEFINITIONS
$title = "Securely Browse the Internet without Limits";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="waitinglist";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 263-8630";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="waitinglist">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                   <!--Banner Section-->
            <section class="p-5 grey-bg-lite verflow">
                <div class="container white text-left white-bg">
                    <div class="row">

                        <div class="col-md-6 col-sm-12 text-left offset-md-1">
                            <h1 class="sourceLight font55 blue padding-around-30">The time for an <span class="sourceBlack">internet without restrictions</span> or fear of privacy is here.</h1>
                            <div class="col-md-10 col-sm-12 text-center unblock">
                            <a href="#HighBarrierForm" class="btn btn-red orange-bg-grad font-weight-bold padding-lr50" data-toggle="modal" data-target="#HighBarrierForm">Join the Wait List</a>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 d-none d-md-block">
                            <img src="img/mobile.png" width="300" class="img-responsive banner-mobile">
                        </div>

                    </div>
                </div>
            </section>
          



 <!--Worry-Free Website Protection-->
 <section class="p-5">
                <div class="container">
                    <div class="row">
                       
                        <div class="col-md-11 col-xs-12 offset-md-1">

                        <div class="kolonat-hom">
            <div class="ikonat-home text-center align-self-center bg-grad1 shadow"> <i class="far fa-eye-slash white font30"></i> </div> 
                 <div class="teksti-home grey"><h1 class="sourceBlack font35">Secure Data</h1>
                  <p ckass="grey">By encrypting your browser session and data, you protect yourself from prying eyes.</p>
                </div> </div>
                <div class="whiteSpace50"></div>
                <div class="kolonat-hom">
            <div class="ikonat-home text-center align-self-center blue-bg-grad shadow"> <i class="far fa-globe white font30"></i> </div> 
                 <div class="teksti-home grey"><h1 class="sourceBlack font35">Unrestricted Access</h1>
                  <p ckass="grey">Eliminate blocks, data throttling and censorship based on your location or sites you are accessing.</p>
                </div> </div>
                <div class="whiteSpace50"></div>
                <div class="kolonat-hom">
            <div class="ikonat-home text-center align-self-center bg-grad2 shadow"> <i class="fas fa-wifi white font20"></i> </div> 
                 <div class="teksti-home grey"><h1 class="sourceBlack font35">Secure Wi-Fi</h1>
                  <p ckass="grey">Safely and securely access public Wi-Fi networks with out concern.</p>  
                </div> </div>



                        </div>
                    </div>
                </div>
            </section>


 <!-- FOOTER -->
 <div class="footer">
                <div id="content" style="padding-top: 10px;">

                    <hr class="footerHR">

                    <div class="row">
                        <div class="col-md-4" style="border-right: 1px solid #707070;">
                            <img class="logoFooter" src="img/logos/SiteLock_white.svg" alt="sitelock logo">
                        </div>

                        <div class="col-md-4 font18">
                            <h3 class="sourceRegular">Contact</h3>
                            <p>U.S. <u>855.378.6200</u></p>
                            <p>Int'l <u>415.390.2500</u></p>
                            <!-- <p>Email <u>Support@SiteLock.com</u></p> -->
                        </div>

                        <div class="col-md-4 footerCol footconnect">
                            <h3 class="sourceRegular">Connect</h3>
                            <p>Sign up for SiteLock news & announcements</p>
                            <div class="form-inline footerSpace">
                                <form method="post" action="https://a29565.actonservice.com/acton/eform/29565/01b7e374-ab17-4ff6-a115-3f1742ab84eb/d-ext-0001">
                                    <input type="hidden" name="AcceptsEmail" value="Yes">
                                    <input class="form-control footerInput" type="email" name="Email" placeholder="you@email.com">
                                    <button style=" margin:0;" type="submit" class="btn-f btn-foot"><i class="far fa-arrow-right"></i></button>
                                </form>
                            </div>

                        </div>
                    </div>

                    <hr class="footerHR">

                    <p class="text-center footCopy linkblue">Copyright &copy; SiteLock
                        <script>
                            document.write(new Date().getFullYear())
                        </script> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a>
                        <p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

                </div>
            </div>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "(833) 263-8630";
$hb_btn = 'Start Today';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>