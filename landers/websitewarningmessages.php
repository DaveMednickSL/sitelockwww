<?php
//DEFINITIONS
$title = "Quickly Remove Website Warning Messages";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="websitewarningmessages";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 263-8630";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="websitewarning">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <section class="p-5">
                        <div class="container">
                            <div class="row container1">
                                <div class="col-md-6 col-xs-12 white black3">
                                    <h1 class="sourceBlack">Quickly Remove Website Warning Messages</h1>
                                    <p class="sourceLight banner-paragraph">SiteLock, the industry leader in website security, quickly identifies and repairs issues causing your website to display warning messages such as “deceptive site ahead,” “the site ahead contains malware,” and others. Contact us today to get started.</p>
                                    <ul class="list-head">
                                        <li><b>Fastest Full-Service Site Repair:</b> In as little as three hours we identify and resolve site malware and resubmit your site to search engine providers for warning message removal. No one does it faster. </li>
                                        <li><b>Massive Coverage:</b> Every day we scan more than 200M+ webpages, analyze 300M+ source code files, and block 400M+ threats. No one resolves more issues that cause site warning messages.</li>
                                        <li><b>24/7/365 Availability:</b> Our U.S.-based team works around the clock to ensure your satisfaction.</li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-container formContainer">
                                        <h3 class="sourceBlack text-center red">Fix My Website Now</h3>

<form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
			<span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
            <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<input class="btn btn-blue upper-font bold" type="submit" value="Fix My Site">
		</form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--The Trusted Security Provider for 12M+ Websites Section start-->
                    <section class="grey-bg-lite p-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="text-center sourceLight">The Trusted Security Provider for 12M+ Websites</h1>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Why Does My Website Have a Warning Message? Section-->
                    <section class="p-5">
                        <div class="container text-center">
                            <div class="row">

                                <div class="col-md-6 col-xs-12 text-left">
                                    <h2 class="red sourceBlack">Why Does My Website Have a Warning Message?</h2>
                                    <p>Popular search engines place warning messages on websites as a way of alerting site owners and visitors that the site is compromised. The most common issue triggering these warning messages is a malware infection. A website can also be flagged by search engines if the site has experienced multiple attempts at phishing attacks, or it contains harmful links from a low-quality ad network. Contact us today for a free site scan to determine what is causing your issue.</p>

                                    <a href="#HighBarrierForm" class="btn btn-blue bold" data-toggle="modal" data-target="#HighBarrierForm">SCAN MY SITE</a>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <img src="img/website-warning-messages/blacklisted.jpg" width="500" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--How does TrueShield™ work Section-->
                    <section class="p-5">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <img src="img/website-warning-messages/warningArtboard.png" width="500" class="img-responsive">
                                </div>
                                <div class="col-md-6 col-sm-12 text-left">
                                    <h2 class="red sourceBlack">What are the Consequences of Website Warning Messages?</h2>
                                    <p>When a warning message is placed on a website, visitors are not able to access the site. Furthermore, an infected website can pass malware to site visitors or steal their information. As your trusted security partner, SiteLock can detect and repair the malware causing your site issue and provide automated protection to prevent future infections.</p>
                                    <a href="#HighBarrierForm" class="btn btn-blue bold" data-toggle="modal" data-target="#HighBarrierForm">GET STARTED</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Protect Your Bacon Section-->
                    <section class="blue-bg p-5">
                        <div class="container white">
                            <div class="row">
                                <div class="col-md-8 col-sm-12 offset-md-2 text-left">
                                    <h2 class="white sourceBlack">Protect Visitor Trust and Confidence</h2>
                                    <p class="font18">"Trust and confidence for our customers is paramount for us. SiteLock prevents the malware, spam and virus attacks. We strongly recommend SiteLock for every website.”</p>
                                    <p class="float-right"><strong>- S. DUDAKA</strong></p>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Free Risk Assessment Section-->
                    <section class="red-bg p-5">
                        <div class="container white">
                            <div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 text-center">
                                    <img src="img/website-warning-messages/Assessment.png" width="240" class="img-responsive">
                                </div>
                                <div class="col-md-7 col-sm-12 col-xs-12 text-left">
                                    <h2 class="white sourceRegular">Free Risk Assessment</h2>
                                    <p>A fantastic way to learn more about website security is to get a free risk assessment for your site. <br>The Website Risk Assessment is SiteLock’s proprietary predictive model that gives you information on unique features or qualities that may affect your site’s risk of being attacked.</p>
                                    <a href="#HighBarrierForm" class="btn btn-ghost-white bold" data-toggle="modal" data-target="#HighBarrierForm">GET MY FREE RISK ASSESSMENT</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Fix My Website Now";
$hb_phone = "(833) 263-8630";
$hb_btn = 'Fix My Site';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>