<?php
//DEFINITIONS
$title = "Free Scan";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="free-scan";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 263-8626";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="free-scan">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <!--Banner Section-->
                    <section class="image-banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <h1 class="sourceLight">Your website is valuable.<br> We can help protect it.</h1>
                                    <h3 class="red sourceBlack">GET AUTOMATED WEBSITE SECURITY FROM SITELOCK </h3>

                                    <h4 class="sourceLight"><span><img src="img/free-scan/tick1.png" width="25"></span>&nbsp; Automatic threat detection and removal </h4>
                                    <h4 class="sourceLight"><span><img src="img/free-scan/tick1.png" width="25"></span>&nbsp; Block sophisticated attacks</h4>
                                    <h4 class="sourceLight"><span><img src="img/free-scan/tick1.png" width="25"></span>&nbsp; Protect reputation and SEO</h4>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-2 col-sm-12"> <a href="/checkout?planID=973" class="btn btn-blue bold">START FOR FREE*</a></div>

                                <div class="col-lg-3 col-sm-12"> <a href="#compare" class="btn btn-ghost-white bold">COMPARE PRODUCTS</a></div>

                                <div class="col-md-12 col-sm-12 padding10">
                                    <p><em>*Secure Alert plan, only provides basic monitoring and threat blocking.</em></p>
                                </div>

                            </div>
                        </div>

                    </section>

                    <!--The Trusted Security-->
                    <section class="padding25">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <br/>
                                    <h1 class="blue text-center sourceLight">The Trusted Security Provider for Over 12 Million Websites.</h1>

                                    <img src="img/free-scan/websites.png" class="img-responsive">

                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Compare Plans Section-->
                    <section class="padding25">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="text-center red p-4">Compare Plans</h2>
                                </div>
                                <a name="compare"></a>
                                <div class="col col-md-8 offset-md-4">
                                    <div class="row white">
                                        <div class="col-3 my_planHeader blue-bg">
                                            <div class="my_planPrice">SecureAlert<br>Free</div>
                                            Per New Domain
                                        </div>
                                        <div class="col-3 my_planHeader no-left-border blue-bg">
                                            <div class="my_planPrice">SecureStarter<br>$30 a month</div>
                                            Per New Domain
                                        </div>
                                        <div class="col-3 my_planHeader no-left-border blue-bg">
                                            <div class="my_planPrice">SecureSpeed<br>$50 a month</div>
                                            Per New Domain
                                        </div>
                                        <div class="col-3 my_planHeader no-left-border blue-bg">
                                            <div class="my_planPrice">SecureSite<br>$70 a month</div>
                                            Per New Domain
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span><strong>Scanner</strong></span>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> <strong>Basic</strong></p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> <strong>SMART</strong></p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> <strong>SMART</strong></p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> <strong> INFINITY</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Pages Scanned</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> 25</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> 500</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> 500</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> 2500</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Automated Malware Removal</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Once daily</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Once daily</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Constant</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Automatic Threat Detection Scanning</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Once daily</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Once daily</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Constant</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Automated WP, Joomla & Drupal Patching</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Yes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Database Scanning </span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Yes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Automated Database Cleaning </span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Yes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span><strong>WAF</strong></span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p><strong>Basic</strong></p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> <strong>Professional</strong></p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> <strong>Premium</strong></p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> <strong>Premium</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>SSL Support</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Yes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Website Acceleration</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Yes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Bad Bot Blocking</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Yes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Customizable Traffic Filtering</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p>No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p>No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p>Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Yes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Blocks Database Attacks</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Yes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span><strong>Expert Services</strong> </span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p><strong>Basic</strong></p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p><strong>Basic</strong></p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p><strong>Better</strong></p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> <strong>Best</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Emergency Hack Repair </span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p>No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> One Time</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Unlimited</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow no-bottom-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>Blacklist Removal</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p> No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p>No</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> One Time</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Unlimited</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my_featureRow ">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                    <span>24/7/365 Support </span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-3 my_planFeature right-border">
                                            <p>Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p>Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature right-border">
                                            <p> Yes</p>
                                        </div>
                                        <div class="col-3 my_planFeature">
                                            <p> Yes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-md-8 offset-md-4">
                                <div class="row">
                                    <div class="col-3">
                                        <a href="/checkout?planID=973" class="btn btn-blue">ADD TO CART</a>
                                    </div>
                                    <div class="col-3">
                                        <a href="/checkout?planID=1007" class="btn btn-blue">ADD TO CART</a>
                                    </div>
                                    <div class="col-3">
                                        <a href="/checkout?planID=1008" class="btn btn-blue">ADD TO CART</a>
                                    </div>
                                    <div class="col-3">
                                        <a href="/checkout?planID=1009" class="btn btn-blue">ADD TO CART</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Why More Website Owners Choose SiteLock-->

                    <section id="mainBlue">

                        <div class="container white text-center">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1 class="white sourceLight font45">Why More Website Owners Choose SiteLock</h1>
                                    <br>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <img src="img/free-scan/simple-setup.png" class="img50 img-circle white-bg">
                                    <br>
                                    <h4 class="sourceBlack">SIMPLE SET-UP</h4>
                                    <p>Cloud-based technology
                                        <br> protects your website
                                        <br> in minutes.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <img src="img/free-scan/automated.png" class="img50 img-circle white-bg">
                                    <br>
                                    <h4 class="sourceBlack">AUTOMATED</h4>
                                    <p>Automated protection
                                        <br> removes malware
                                        <br> immediately.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <img src="img/free-scan/support.png" class="img50 img-circle white-bg">
                                    <br>
                                    <h4 class="sourceBlack">24/7 SUPPORT</h4>
                                    <p>Our U.S.-based
                                        <br> customer service team
                                        <br> is available 24/7/365.</p>
                                </div>
                                <div class="col-sm-12">
                                    <a href="/checkout?planID=973" class="btn btn-ghost-white bold">GET MY FREE SCANNER</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Not Sure What You Need?-->
                    <section>
                        <div class="whiteSpace50"></div>
                        <div class="container text-center">
                            <div class="row">

                                <div class="col-md-12">
                                    <h2 class="red">Not Sure What You Need?</h2>
                                    <h4 class="sourceLight linkblue">
                  Contact us today at<a href="tel:8332638626"><span class="sourceBlack"> 833-263-8626 </span></a> <br>or complete our form and we will contact you for a free website security consultation.
</h4>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#HighBarrierForm" class="btn btn-red bold" data-toggle="modal" data-target="#HighBarrierForm">GET A FREE CONSULTATION</a>
                                </div>
                            </div>
                            <div class="whiteSpace50"></div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "(833) 263-8626";
$hb_btn = 'Start Today';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>