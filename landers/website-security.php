<?php
//DEFINITIONS
$title = "Website Security | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="website-security";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(877) 798-5144";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="website-security">
            <?php include 'includes/assets/ALPHA.php';?>
            <?php include 'includes/page_ends/menu.php';?>
<!-- top-banner-part -->
                    <section class="p-5" style="padding-bottom:0 !important;">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
                                <h1 class="text-right sourceLight font50">Comprehensive,<br /> Cloud-Based <br /><span class="red">Website Security</span></h1>
                <p class="text-right">Your website is unique&mdash;your website security<br /> should be too.  Contact SiteLock to begin a free<br /> consultation with our security&nbsp;experts.
<br><a href="#form" class="btn btn-red">Get Started Today</a>
            </p>
                
                <!-- <form name="" id="" action="" method="post" class="text-right">
                    <input type="text" id="inputName" placeholder="First Name" name="inputName" required />
                    <div class="btn btn-red">Get Started Today</div>
                </form> -->
                
                
                                </div>
                                <div class="col-md-8 col-sm-12">
                                <img src="img/lander_a.jpg" width="820" class="img-responsive" />

                                </div>
                            </div>
                        </div>
                    </section>

<!-- What We Do section -->
<section class="p-5 black-bg">

<div class="container text-center">
    <div class="row">
        <div class="col-md-12 col-sm-12">
                    <h1 class="sourceLight white">What We Do</h1>
        </div>
    </div>
</div>
</section>


<!-- Find fix section -->
<section class="p-5">
                    <div class="container text-center">
                        <div class="row">

                            <div class="col-md-4 col-sm-12 my-auto">
                                <img src="img/icon_find.svg" width="100" class="padding10 img-responsive">
                                <p><span class="sourceBlack red">Find </span></p>
                                <ul class="padding-left110">
                        <li><span>Vulnerability Scanning</span></li>
                        <li><span>Malware Detection</span></li>
                        <li><span>Automated Pen Testing</span></li>
                        <li><span>Source Code Testing</span></li>
                    </ul>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <img src="img/icon_fix.svg" width="100" class="padding10 img-responsive">
                                <p><span class="sourceBlack red">Fix</span></p>
                                <ul class="padding-left110">
                        <li><span>Eliminate Backdoors</span></li>
                        <li><span>Automatic Malware Removal</span></li>
                        <li><span>Vulnerability Remediation</span></li>
                    </ul>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <img src="img/icon_prevent.svg" width="100" class="padding10 img-responsive">
                                <p><span class="sourceBlack red">Prevent</span></p>
                                <ul class="padding-left110">
                        <li><span>Managed Web App Firewall</span></li>
                        <li><span>DDoS Prevention</span></li>
                        <li><span>Backdoor Mitigation</span></li>
                        <li><span>SQLi &amp; XSS Prevention</span></li>
                    </ul>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4 col-sm-12 offset-md-2">
                                <img src="img/icon_accelerate.svg" width="100" class="padding10 img-responsive">
                                <p><span class="sourceBlack red">Accelerate</span></p>
                                <ul class="padding-left110">
                        <li><span>Vulnerability Scanning</span></li>
                        <li><span>Malware Detection</span></li>
                        <li><span>Automated Pen Testing</span></li>
                        <li><span>Source Code Testing</span></li>
                    </ul>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <img src="img/icon_comply.svg" width="100" class="padding10 img-responsive">
                                <p><span class="sourceBlack red">Comply</span></p>
                                <ul class="padding-left110">
                        <li><span>Eliminate Backdoors</span></li>
                        <li><span>Automatic Malware Removal</span></li>
                        <li><span>Vulnerability Remediation</span></li>
                    </ul>
                            </div>

                          

                        </div>
                    </div>
                </section>



<!-- And So Much More section -->
<section class="p-5 black-bg">

<div class="container text-center">
    <div class="row">
        <div class="col-md-12 col-sm-12">
                    <h1 class="sourceLight white">And So Much More</h1>
        </div>
    </div>
</div>
</section>

                    <!-- Form -->

                    <section class="p-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-12 offset-md-2">
                                <div class="form-container" id="forms">
      <p class="text-center font25 sourceLight">Call 877.798.5144 or fill out the form below to start your free consultation with our website security experts.</p>

       <form method="post" id="form" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
			<span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
            <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<span style="padding:0 20px 0px 25px;" ><input class="btn btn-red upper-font bold" type="submit" value="Get Your Free Consultation" style="width: 90%;"></span>
		</form>
                                </div>
                                </div>

                            </div>
                        </div>
                    </section>


            


                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Get Your Free Malware Check";
$hb_phone = "(877) 798-5144";
$hb_btn = 'Check&nbsp;Now';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>