<?php
//DEFINITIONS
$title = "Emergency Hacked Website Repair";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="repair-now";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(833) 686-5384";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="repair-now">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                       <!--Banner Section-->
                       <section class="p-5" id="mainGrey">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-xs-12 white-bg shadow ">
                                  
                                <h2 class="red sourceBlack font50">EMERGENCY HACKED WEBSITE REPAIR</h2>

<h5 class="grey sourceBlack"><em>Fix your website now to prevent loss of traffic, damage to reputation and data theft.</em></h5>

<div class="list-head grey">
<div class="media">
<div class="media-left iconLg red"><i class="fa fa-rocket" aria-hidden="true"></i></i></div>
<div class="media-body"><strong>Fastest repair:</strong> With 24/7/365 availability, automated malware removal and a world class team of security experts we can instantly repair your hacked site.</div>
</div>
<div class="media">
                    <div class="media-left iconLg red"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
                    <div class="media-body"><strong>Comprehensive:</strong> With a database of 10M+ threats we find and detect more website security issues.</div>
                    </div>
                    <div class="media">
                      <div class="media-left iconLg red"><i class="fa fa-certificate" aria-hidden="true"></i></i></div>
                      <div class="media-body"><strong>Satisfaction guaranteed:</strong> All website malware cleans are backed by a 100% money back satisfaction guarantee.</div>
                      </div>
                      <div class="media">
                            <div class="media-left iconLg red"><i class="fa fa-suitcase" aria-hidden="true"></i></div>
                            <div class="media-body"><strong>Vulnerability repair:</strong> We won’t just fix the infection but eliminate the vulnerability hackers exploited to infect your website.</div>
                            </div>
                </div>
                <div class="arrow-right"></div>
                                </div>
                                
                                <div class="col-md-5 col-xs-12 white-bg shadow offset-md-1 text-center">
                                <!-- <div class="form-container">
                                        <h3 class="sourceBlack text-center blue">Repair Your Hacked Site Now</h3>

                                        <form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
                                            <span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
                                            <span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
                                            <span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
                                            <span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
                                            <span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
                                            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
                                            <input type="hidden" name="Form ID" value="<FORM ID>">
                                            <input type="hidden" name="Lead Source" value="Act-On Form">
                                            <input type="hidden" name="Lead Group" value="Paid Marketing">
                                            <input type="hidden" name="Accepts Email" value="Accepts Email">
                                            <input type="hidden" name="Division" value="Retail">
                                            <input type="hidden" name="Lead Type" value="PPC">
                                            <input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
                                            <span style="padding:0 20px 0px 20px;" ><input class="btn btn-blue upper-font bold" type="submit" value="Secure My Website" style="width: 90%;"></span>
                                            <br><br>
                                            <p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>
                                        </form>

                                    </div> -->


<h3 class="sourceBlack text-center blue">Repair & Secure Your Site</h3>

                                                    <div id="priceBoxContent">

                                                        

                                                        <p class="font16">
                                                            <br>Quickest repair of existing infections, removal from search engine blacklists and the only automated protection that prevents and repairs future website attacks.

                                                        </p>
                                                        <h1 class="sourceBlack">$49.99<span class="sourceRegular font18">/month</span></h1>
                                                        <a href="https://www.sitelock.com/checkout?planID=1008" class="btn btn-blue font-weight-bold width-auto">FIX MY WEBSITE NOW</a>
                                                       
                                                        <br>
                                                        <a href="#HighBarrierForm" class="btn btn-ghost-grey font-weight-bold width-auto mt-10" data-toggle="modal" data-target="#HighBarrierForm">REQUEST MORE INFO</a>
                                                        <br>
                                                        <a href="tel:8336865384" class="btn btn-ghost-grey width-auto font-weight-bold mt-10">Call 833-686-5384</a>
                                                        <br>
                                                    </div>
                                               


                             </div>

                                </div>
                                
                        </div>

                    </section>


                    <!--Trusted by 12M+ Websites Section start-->
<section class="p-5" id="mainGrey">
<div class="container">
    <div class="row">
<div class="col-md-12">
<h1 class="text-center sourceLight">The Recognized Leader in Website Security</h1>
<div id="mainGrey"><div id="mainBody">
<div class="row font18">
<div class="col-md-4">
<div id="greyAwardBox"><div id="content"><p class="text-center"><img class="awardBump" src="img/awardCyberSec2018.svg" alt="Award"></p><i><strong>Best Threat Detection:<br></strong></i><br><br><a class="btn btn-ghost-grey font-weight-bold" href="">SiteLock INFINITY</a><br><br></div></div>
</div>

<div class="col-md-4">
<div id="greyAwardBox"><div id="content"><p class="text-center"><img class="awardBump" src="img/awardInfoSec2018.svg" alt="Award"></p><strong><i>Best Security Monitoring and<br>Website Security Solution:</i></strong><br><br><a class="btn btn-ghost-grey font-weight-bold" href="">SiteLock INFINITY</a><br><br></div></div>
</div>

<div class="col-md-4">
<div id="greyAwardBox"><div id="content"><p class="text-center"><img class="awardBump" src="img/awardFortress2018.svg" alt="Award"></p><strong><i>Best Anti-Malware, Anti-Spam,<br>and Anti-Virus Scanner:</i></strong><br><br><a class="btn btn-ghost-grey font-weight-bold" href="">SiteLock SMART</a><br><br></div></div>
</div>
</div>
<div class="whiteSpace50"></div>
</div></div>
</div>
</div>
</div>
</section>



      <!--Trusted by 12M+ Websites Section start-->
      <section class="p-5">
      <div class="container">
         <div class="row text-center">
            <div class="col-md-12">
               <h1 class="text-center sourceLight">Featured in</h1>
            </div>
            <div class="row lg-img">
            <div class="col-md-4 col-xs-12">
               <img src="/repair-now-includes/img/forbes.png" alt="forbes logo" width="260" />
               </div>
               <div class="col-md-4 col-xs-12">
               <img src="/repair-now-includes/img/wsj.png" alt="wsj logo" width="208" />
               </div>
               <div class="col-md-4 col-xs-12">
               <img src="/repair-now-includes/img/usa-today.png" alt="usa-today logo" width="292" />
               </div>
            </div>

            <div class="row lg-img">
            <div class="col-md-4 col-xs-12">
               <img src="/repair-now-includes/img/inc.png" alt="Inc logo" width="220" />
               </div>
               <div class="col-md-4 col-xs-12">
               <img src="/repair-now-includes/img/politico.png" alt="politico logo" width="290" />
               </div>
               <div class="col-md-4 col-xs-12">
               <img src="/repair-now-includes/img/the-huffington-post.png" alt="the-huffington-post logo" width="256" />
               </div>
            </div>
         </div>
      </div>
   </section>

<!-- testimonials strat -->
<section class="testimonial p-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                    <img src="/repair-now-includes/img/air-speed.png" class="img-circle img-responsive" width="250">
                    </div>
                    <div class="col-md-7 col-sm-7 col-lg-7 col-xs-12">
                    <p>Security protects my customers and it helps protect me from liability if their information is compromised.</p>
                    <div class="media-attribution"> <em>- Tony Spiridigliozzi</em><br>Owner Airspeed Wireless Networks</div>
                    
                    </div>
                </div>
            </div>
        </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Repair Your Hacked Site Now";
$hb_phone = "833-686-5384";
$hb_btn = 'Start my repair';

?>
	
  <!-- High Barrier -->
  <div class="modal fade" id="HighBarrierForm" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content modalForm modalFix">
      <div class="modal-header">
        <h5 class="modal-title sourceBlack padding-left25 padding10" id="exampleModalLabel"><?php echo $hb_title; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="fal fa-times-circle" style="user-select: auto;"></i>
        </button>

      </div>
      <div class="modal-body padding25">
        <p class="redLink padding-left25">Enter your information or call us today at  <a href=""><?php echo $hb_phone; ?></a> to get started.</p>
        <form class="mt-20" method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">

<span class="form-inline form-group"><i id="validateName" class="far fa-circle grey1 fa-lg" title="First Name should be formatted as firstname"></i>&nbsp;<input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder" for="HBinputName">First Name</label>

</span>
<span class="form-inline form-group"><i id="validatelastName" class="far fa-circle grey1 fa-lg" title="Last Name should be formatted as lastname"></i>&nbsp;<input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder" for="HBinputlastName">Last Name</label>

</span>

<span class="form-inline form-group"><i id="validateDomain" class="far fa-circle grey1 fa-lg" title="Domain Name should be formatted as domain.com"></i>&nbsp;<input name="Website" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder" for="HBinputDomain">Domain</label>

</span>

<span class="form-inline form-group"><i id="validateEmail" class="far fa-circle grey1 fa-lg" title="Email should be formatted as you@domain.com"></i>&nbsp;<input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder" for="HBinputEmail" >Email</label>

</span>

<span class="form-inline form-group"><i id="validatePhone" class="far fa-circle grey1 fa-lg" title="Phone should be formatted as 0001112222"></i>&nbsp;<input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder" for="HBinputPhone">Phone Number</label>

</span>

<div class="custom-control custom-checkbox" style="font-size: 14px; padding-left:25px !important; width: 100%;">
<input type="checkbox" class="custom-control-input" id="customCheck1">
<label class="custom-control-label" for="customCheck1">Email me the latest website security news</label>
</div>
<input type="hidden" name="Form ID" value="<FORM ID>">
     <input type="hidden" name="Lead Source" value="Act-On Form">
     <input type="hidden" name="Lead Group" value="Marketing">
     <input type="hidden" name="Accepts Email" value="Accepts Email">
     <input type="hidden" name="Division" value="Retail">
     <input type="hidden" name="Lead Type" value="General">
     <input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
<br>
      <p class="text-center"><button type="submit" class="btn btn-primary"><?php echo $hb_btn; ?></button></center>
      </form>
      </div>
      <div class="modal-footer">
        <p class="form-notice">By submitting this form you confirm that you have read and accepted our <a href="terms" target="_blank">Terms & Conditions</a> and <a href="privacy-policy" target="_blank">Privacy Policy</a></p>
      </div>
    </div>
  </div>
</div>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>