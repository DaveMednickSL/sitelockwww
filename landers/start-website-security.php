<?php
//DEFINITIONS
$title = "Website Security built for small businesses";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="start-website-security";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(855) 378-6200";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="start-website">
            <?php include 'includes/assets/ALPHA.php';?>

                <!-- Top header section -->
                <section class="red-bg p-5">
                    <div class="container white">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <a href="#"><img src="img/logo-white.png" width="300"></a>
                            </div>
                        </div>
                    </div>
                </section>

                <!--Banner Section-->
                <section class="red-bg no-padding bottom-wave p5">
                    <div class="container white text-center">
                        <div class="row">

                            <div class="col-12">
                                <h1 class="white text-center sourceBlack">Website Security built for <span class="blue">small businesses</span></h1>
                                <p>SiteLock products empower you to focus on your business, <br>while we concentrate on protecting your website from the threat of cyberattacks.</p>

                                <form id="newwslander-page">

                                    <div class="input-group justify-content-center align-items-center">
                                        <div class="col-sm-4 pt-3">
                                            <input name="acton_domain_c" class="required placeholder form-control error" placeholder="Your Domain Name" id="Website" required value="" aria-required="true" aria-invalid="true" type="text">
                                        </div>
                                        <div class="col-sm-2 align-items-center">
                                            <a href="#HighBarrierForm" class="btn btn-blue leadwebsite bold" data-toggle="modal" data-target="#HighBarrierForm">GET STARTED</a>
                                        </div>

                                    </div>

                                </form>
                                <br>
                                <p>Trusted Security Provider of 12M Websites</p>
                            </div>

                            <div class="col-sm-12">
                                <img src="img/desktop_scanning.png" class="text-center zindex-top img-responsive" width="500">
                            </div>

                        </div>
                    </div>
                </section>
                <div class="clearfix"></div>
                <div class="whiteSpace100 d-none d-md-block"></div>

                <!--Every SiteLock plans includes start-->
                <div class="whiteSpace100 d-none d-md-block"></div>
                <div class="whiteSpace50"></div>
                <section class="p-5">
                    <div class="container text-center">
                        <div class="row">

                            <div class="col-md-4 col-sm-12">
                                <img src="img/effortless.png" width="170">
                                <p><span class="sourceBlack">Effortless Protection </span>
                                    <br> Automatically monitor for, block and repair threats to your website.</p>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <img src="img/technology.png" width="150">
                                <p><span class="sourceBlack">Instant Deployment</span>
                                    <br>Cloud-based technology deploys and protects your site in minutes.</p>
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <img src="img/faster.png" width="150">
                                <p><span class="sourceBlack">Boost Performance</span>
                                    <br> Instantly boost site speed, customer trust and conversions.</p>
                            </div>

                        </div>
                    </div>
                </section>

                <!-- rating-bar -->
                <section id="mainGrey">
                    <div class="container text-center">
                        <div class="row">
                            <?php include('includes/rating-bar.php');?>
                        </div>
                    </div>
                </section>

                <!--Worry-Free Website Protection-->
                <section class="p-5">
                    <div class="container">
                        <div class="row">

                            <div class="col-md-6 col-xs-12 text-left ">
                                <br>
                                <h2 class="sourceBlack">Worry-Free Website Protection</h2>
                                <p>Partner with SiteLock to automatically protect your website and visitors from infection, data theft, redirection of site traffic, removal from search results, suspension and more.</p>

                                <a href="#HighBarrierForm" class="btn btn-blue bold" data-toggle="modal" data-target="#HighBarrierForm">GET STARTED TODAY</a>
                            </div>

                            <div class="col-md-6 col-xs-12 text-center float-xs-left">
                                <img src="img/worry.png" width="300" class="img-responsive">
                            </div>

                        </div>
                    </div>
                </section>

                <!-- awards-bar -->
                <section id="mainGrey" >
                    <div class="container text-center">
                        <div class="row">
                        <div class="col-12 text-center mt-30">
                        <h2 class="sourceLight font45">The Recognized Leader in Website Security</h2>
                        </div>
                            <?php include('includes/awards-bar.php');?>
                        </div>
                    </div>
                </section>

                <!--Agency, Enterprise or Multisite Solutions Section start-->
                <section class="p-5">
                    <div class="container text-center">
                        <div class="row">
                            <div class="col-md-8 col-xs-12 offset-md-2">

                                <h4 class="sourceBlack">"Security protects my customers and it helps protect me from liability if their information is compromised."</h4>
                                <p class="sourceRegular font25">Tony Spiridigliozzi- <a href="https://airspeed-wireless.com/" target="_blank">Airspeed-Wireless.com</a></p>
                                </p>
                            </div>
                            <div class="col-sm-12 text-center">
                                <a href="#HighBarrierForm" class="btn btn-blue bold" data-toggle="modal" data-target="#HighBarrierForm">GET STARTED</a>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- DYNAMIC FORM INFO -->
                <?php
$hb_title = "Protect Your Website";
$hb_phone = "(855) 378-6200";
$hb_btn = 'Get Started';
include 'includes/forms/high-barrier.html';
?>

                    <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>