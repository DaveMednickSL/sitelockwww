<?php
//DEFINITIONS
$title = "69 Tips to Protect Your Business from Cyberattacks";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="69-cyber-security-tips";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(877) 798-5144";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="cyber-69">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <!--Banner Section-->
                    <section class="p-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                   <div class="offset-color"> 
                                       <span class="tips-img"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 text-center form-container">
                                
                                <form id="form" method="post" action="https://a29565.actonservice.com/acton/eform/29565/f370b272-2264-4afa-bfea-f962cfb91e3d/d-ext-0002">
                                    <h2 class="sourceBlack red">Get Your Free Tip Sheet Now</h2>
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
		    <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<span style="padding:0 20px 0px 25px;" ><input class="btn btn-blue upper-font bold" type="submit" value="Email me the Tips" style="width: 90%;"></span>
		</form>
                                           
                                </div>
                            </div>
                        </div>
                    </section>

                 <!--second Section-->
                    <section class="p-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                <p>
                        We’ve rounded up 69 easy and effective cybersecurity tips to help protect your small business from disruptive cyberattacks.  We’ve organized these tips into the following categories to make them more easily digestible.
                        </p>
                        <ul>
                            <li>Website Security</li>
                            <li>Policies and Procedures</li>
                            <li>Behavior Based</li>
                            <li>Device Security</li>
                            <li>Physical Device</li>
                            <li>Passwords</li>
                            <li>Email</li>
                            <li>Data</li>
                        </ul>
                        <p>Download the tip sheet today to see how many of these security best practices you are using in your business.</p>

                                </div>
                                <div class="col-md-6 col-sm-12 text-center">
                                <img src="img/lock01.png" class="img50" width="300">
                                            <br>
                                 <a href="#form" class="btn btn-ghost-red bold">Email me the Tips</a>
                                </div>
                            </div>
                        </div>
                    </section>
                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>