<?php
//DEFINITIONS
$title = "Website Security built for small businesses";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="website-virus";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(855) 378-6200";

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$UTM = strstr($actual_link, 'utm_campaign');
$UTM = str_replace("utm_campaign=", "", "$UTM");
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>

        <body id="website-virus">
            <?php include 'includes/assets/ALPHA.php';?>
                <?php include 'includes/page_ends/menu.php';?>

                    <!-- Top header section -->
                    <section class="red-bg p-5">
                        <div class="container white">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <h1 class="sourceLight">Powerful Website Virus Protection and Removal</h1>
                                    <p>Learn why 12M+ website owners choose SiteLock to protect their site against viruses, malware and other threats.</p>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- banner section -->
                    <section class="p-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <img src="img/desktop_scanning.png" class="text-center zindex-top img-responsive" width="500">
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <div class="form-container">
                                        <h3 class="sourceBlack text-center red">Free Website Virus Check</h3>

<form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
			<span class="form-inline form-group"><input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder slide-p" for="HBinputName">First Name</label></span>
			<span class="form-inline form-group"><input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder slide-p" for="HBinputlastName">Last Name</label></span>
			<span class="form-inline form-group"><input name="acton_domain_c" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder slide-p" for="HBinputDomain">Domain</label></span>
			<span class="form-inline form-group"><input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder slide-p" for="HBinputEmail" >Email</label></span>
			<span class="form-inline form-group"><input name="Phone" id="HBinputPhone" style="width: 90%;" type="number" minlength=3 class="form-control" required placeholder="Phone Number"><label class="form-control-placeholder slide-p" for="HBinputPhone">Phone Number</label></span>
            <input type="hidden" name="Lead_Source_Description" value="<?php echo $UTM;?>">
            <input type="hidden" name="Form ID" value="<FORM ID>">
			<input type="hidden" name="Lead Source" value="Act-On Form">
			<input type="hidden" name="Lead Group" value="Paid Marketing">
			<input type="hidden" name="Accepts Email" value="Accepts Email">
			<input type="hidden" name="Division" value="Retail">
			<input type="hidden" name="Lead Type" value="PPC">
			<input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
			<br>
			<p class="font12 blueLink text-center">By submitting this form you confirm that you have read and accepted our <br><a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>

			<span style="padding:0 20px 0px 25px;" ><input class="btn btn-blue upper-font bold" type="submit" value="CHECK MY WEBSITE" style="width: 90%;"></span>
		</form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--The Trusted Security-->
                    <section id="mainDarkGrey">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <br/>
                                    <h1 class="text-center sourceLight">SiteLock Makes Website Scanning <br>Simple for 12M+ Companies</h1>
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/Tennis-Channel.png" width="70">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/new-orleans.png" width="200">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/frontier.png" width="150">
                                </div>
                                <div class="col-3 list-img">
                                    <img src="img/powerful-website-security/nextiva.png" width="150">
                                </div>

                            </div>
                            <div class="whiteSpace25"></div>
                        </div>
                        </div>
                    </section>

                    <!--Why More Website Owners Choose SiteLock-->

                    <section id="mainBlue">

                        <div class="container white text-center">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1 class="white sourceLight font45">Why More Website Owners Choose SiteLock</h1>
                                    <p>The average website is attacked more than 59 times per day. If just one of these attempts is successful it can result in dire consequences, including data theft, loss of site traffic and damaged reputation. Protect your site with SiteLock, the only provider offering automated protection, quick and easy set-up, and simple weekly reports on the security of your website. Protecting your site is now easier than ever.</p>
                                    <br>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <img src="img/free-scan/simple-setup.png" class="img50 img-circle white-bg">
                                    <br>
                                    <h4 class="sourceBlack">AUTOMATIC</h4>
                                    <p>The only website security that automatically detects and removes viruses.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <img src="img/free-scan/automated.png" class="img50 img-circle white-bg">
                                    <br>
                                    <h4 class="sourceBlack">CLEAR</h4>
                                    <p>Easy to understand weekly reports make digesting security simple.</p>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <img src="img/free-scan/support.png" class="img50 img-circle white-bg">
                                    <br>
                                    <h4 class="sourceBlack">QUICK</h4>
                                    <p>Our cloud-based technology deploys and protects your site in minutes.</p>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#HighBarrierForm" class="btn btn-ghost-white bold" data-toggle="modal" data-target="#HighBarrierForm">PROTECT MY SITE</a>

                                </div>
                            </div>
                        </div>
                    </section>

                    <!--What is a Website Virus?-->

                    <section class="p-5 black-bg">

                        <div class="container white">
                            <div class="row">
                                <div class="col-md-5 col-sm-12">

                                    <img src="img/website.png" width="250" class="img-responsive float-right">
                                </div>
                                <div class="col-md-6 col-sm-12 text-left">
                                    <h1 class="white sourceLight">What is a Website Virus?</h1>
                                    <p>You might hear someone say they have a website “virus,” but did you know that a virus is just a specific type of malware? Malware, short for malicious software, is used to gather sensitive data, gain unauthorized access to websites and even hijack computers. Nearly one million new malware threats are released every day.</p>
                                </div>

                                <div class="col-sm-12 text-center ">
                                    <a href="#HighBarrierForm" class="btn btn-ghost-white bold" data-toggle="modal" data-target="#HighBarrierForm">GET PROTECTED</a>

                                </div>
                            </div>
                        </div>
                    </section>

                    <!--Get the SiteLock Website Security Report-->

                    <section class="p-5">

                        <div class="container">
                            <div class="row">

                                <div class="col-md-5 col-sm-12 text-left offset-md-2">
                                    <h1 class="sourceLight">Get the SiteLock Website Security Report</h1>
                                    <p>The average website is attacked 59 times every day. Learn more in the SiteLock Website Security Insider.</p>
                                </div>
                                <div class="col-md-5 col-sm-12">

                                    <img src="img/qsr.jpg" width="250" class="img-responsive float-left">
                                </div>

                                <div class="col-sm-12 text-center ">
                                    <a href="https://www.sitelock.com/website-security-report" class="btn btn-ghost-blue bold" data-toggle="modal" data-target="#HighBarrierForm">DOWNLOAD FREE REPORT</a>

                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Free Website Virus Check";
$hb_phone = "(855) 378-6200";
$hb_btn = 'Check My Website';
include 'includes/forms/high-barrier.html';
?>

                        <!-- END DYNAMIC FORM INFO -->

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>