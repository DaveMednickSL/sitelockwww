<div class="footer">
<div id="content" style="padding-top: 10px;">


<hr class="footerHR">

<div class="row">
<div class="col-md-4" style="border-right: 1px solid #707070;">
<img class="logoFooter" src="img/logos/SiteLock_white.svg" alt="sitelock logo">
</div>

<div class="col-md-4 font18">
<h3 class="sourceRegular">Contact</h3>
<p>U.S. <u>855.378.6200</u></p>
<p>Int'l <u>415.390.2500</u></p>
</div>

<div class="col-md-4 footerCol footconnect">
<h3 class="sourceRegular">Connect</h3>
<p>Sign up for SiteLock news & announcements</p>
<div class="form-inline footerSpace">
<form method="post" action="https://a29565.actonservice.com/acton/eform/29565/01b7e374-ab17-4ff6-a115-3f1742ab84eb/d-ext-0001">
<input type="hidden" name="AcceptsEmail" value="Yes">
<input class="form-control footerInput" type="email" name="Email" placeholder="you@email.com"><button style=" margin:0;" type="submit" class="btn-f btn-foot"><i class="far fa-arrow-right"></i></button>
</form>
</div>

</div>
</div>

<hr class="footerHR">

<p class="text-center footCopy linkblue">Copyright &copy; SiteLock <script>document.write(new Date().getFullYear())</script> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

</div>
</div>
