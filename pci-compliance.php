<?php
//DEFINITIONS
$title = "PCI Compliance | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "PCI Compliance";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div class="headerBottomGrey" id="mainHeader"><div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-6 my-auto">
<h1 class="headTxtDrop">Simplified, <span class="sourceBlack">Streamlined PCI Compliance</span></h1>
<h3>Meet your PCI requirements today</h3>
<a class="btn btn-blue" href="#ContactForm" data-toggle="modal" data-target="#ContactForm">Get Started</a>
</div>
<div class="col-lg-6">
<img class="headerimg headImgDrop" src="/img/pci-compliance/pciCheck.svg" alt="PCI Compliance" style="max-width: 70%;">
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div></div>

<div id="mainBody">
<div class="whiteSpace50"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2><span class="sourceRegular">What is </span> <span class="sourceBlack">PCI compliance?</span></h2>PCI compliance is a set of security standards used to protect consumers’ credit card data online. The term is short for Payment Card Industry (PCI) Data Security Standard (DSS), also known as PCI DSS. It was established to help control where cardholder data is stored, processed, or transmitted. The SiteLock PCI compliance product is a fast and easy way to meet PCI requirements. Our simplified questionnaire reduces the number of questions you have to answer by up to 80 percent.<br><a class="btn btn-red" href="#ContactForm" data-toggle="modal" data-target="#ContactForm">Protect Your Customers Today</a></div></div>
   <div class="col-lg-5 footMobileHide"><img class="splitImg" src="/img/pci-compliance/pciCard.svg" alt="Scanning Calendar"></div>
  </div>
</div>

<div class="whiteSpace100"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-md-5 splitPadding"><img class="splitImg" src="/img/siteCart.svg" alt="Scanning Alert"></div>
  <div class="col-md-7 whitebk splitPadding"><div id="content"><h2><span class="sourceBlack">Who</span> needs to be PCI compliant?</h2>Any individual or business that stores, processes, or transmits payment card information needs to be PCI compliant. This includes companies that only take payments over the phone and companies that use a third-party payment processing system, like PayPal. If you don’t comply with PCI DSS, you may be subject to penalties and fines between $5,000 and $100,000 per month until PCI compliance standards are met.<br><a class="btn btn-red" href="#ContactForm" data-toggle="modal" data-target="#ContactForm">Get PCI Compliant Today</a></div></div>
 </div>
</div>
<div class="whiteSpace50"></div>
</div>

<?php 
$benefitsTitle = '<h1 class="text-center font55">SiteLock PCI compliance <span class="sourceBlack">benefits</span></h1>'; 
$icon1= 'fa-umbrella'; $title1 = 'Simplified questionnaire'; $content1 = 'Save time and only answer 20 percent of the lengthy 280 PCI compliance questionnaire. Becoming PCI compliant can be a time-consuming process, but we’ll help you every step of the way.';
$icon2 = 'fa-gem'; $title2 = 'Keep customer<br>data safe'; $content2 = 'Protect your visitors’ financial data with every transaction. Today, 65 percent of consumers who have had data compromised online won’t return to the site where their info was stolen.';
$icon3 = 'fa-chart-line'; $title3 = 'Maintain compliance<br>with ease'; $content3 = 'Scan your site for vulnerabilities and stay compliant. An initial and quarterly vulnerability scan may be required to maintain compliance. Scanning is included with your SiteLock PCI compliance package.';
$icon4 = 'fa-rocket'; $title4 = 'Custom fix-it plan'; $content4 = 'This plan comes with policy and procedure templates, so you can reduce your legal and financial risks.';
$icon5 = 'fa-user-graduate'; $title5 = 'Avoid fees and fines'; $content5 = 'Avoid hefty fines by becoming PCI compliant. Those who fail to meet PCI DSS requirements could be fined $5,000 and $100,000 per month until compliance standards are met.';
$icon6 = 'fa-magic'; $title6 = 'Guides and tips'; $content6 = 'Throughout your application process our software will use a series of questions to guide you to the right policy for your busniess needs.';
$benefitsBTN = 'I’m Ready to Get PCI Compliant';
?>

<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace100"></div>
<?php  echo $benefitsTitle;?>

<div class="row">
<div class="col-md-4">
<div id="benefitBox"><div id="benefitBoxContent">
<div id="benefitCircleBlue"><i class="fal <?php echo $icon1; ?> fa-3x"></i></div>
<h3 class="sourceBlack font25"><?php echo $title1; ?></h3>
<p><?php echo $content1; ?></p>
</div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleRed"><i class="fal <?php echo $icon2; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title2; ?></h3>
  <p><?php echo $content2; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleBlue"><i class="fal <?php echo $icon3; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title3; ?></h3>
  <p><?php echo $content3; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleRed"><i class="fal <?php echo $icon4; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title4; ?></h3>
  <p><?php echo $content4; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleBlue"><i class="fal <?php echo $icon5; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title5; ?></h3>
  <p><?php echo $content5; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleRed"><i class="fal <?php echo $icon6; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title6; ?></h3>
  <p><?php echo $content6; ?></p>
  </div></div>
</div>


</div>
<br><br>
<p class="text-center"><a class="btn btn-blue" href="#ContactForm" data-toggle="modal" data-target="#ContactForm"><?php echo $benefitsBTN;?></a></p>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainBlue"><div id="mainBody">
<div class="row">
<div class="col-lg-6 my-auto">
<img class="img70" src="img/waf/wafSheildEarth.svg" alt="WAF Shield">
</div>

<div class="col-lg-6">
<h1 class="sourceBlack font45">Take your PCI compliance to the next level</h1>
<h3 class="font25">Add a SiteLock PCI compliant web application firewall (WAF) to your website to block harmful traffic and bad bots from entering your site. Plus, with your WAF in place, you are sure to meet PCI requirement 6.6.</h3>
</div>
</div>
</div></div>

<?php include 'includes/trusted-bar.php';?>

<div id="mainBody">
<h1 class="sourceBlack text-center font55">Steps to becoming PCI compliant</h1>
<h3 class="font25 text-center">Being PCI compliant is necessary when accepting credit card payments online—but it doesn’t have to be intimidating or complicated. We’ll guide you through each step of the process, ensuring your customers’ credit card data is protected.</h3>

<div class="row text-center">
<div class="col-lg-2"><center><p class="largeNumbersBlue">1</p></center>Take your pre-SAQ to determine your correct PCI SAQ type.</div>

<div class="col-lg-2"><center><p class="largeNumbersBlue">2</p></center>SiteLock will provide a simplified SAQ based on your SAQ type.</div>

<div class="col-lg-2"><center><p class="largeNumbersBlue">3</p></center><strong>Complete your SAQ.</strong></div>

<div class="col-lg-2"><center><p class="largeNumbersBlue">4</p></center>SiteLock will run an initial PCI website scan for vulnerabilities.</div>

<div class="col-lg-2"><center><p class="largeNumbersBlue">5</p></center>Sign and attest to being PCI compliant.</div>

<div class="col-lg-2"><center><p class="largeNumbersBlue">6</p></center>Retake your PCI SAQ to maintain compliance. SiteLock can help!</div>
</div>

<div class="whiteSpace50"></div>

</div>

<div id="mainTerms">
<div class="whiteSpace50"></div>
<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2><span class="sourceBlack">What is</span><br>a website vulnerability?</h2>
   <p>A website vulnerability is a weakness in website code that cybercriminals can exploit to gain unauthorized access to a site. An outdated plugin or unprotected contact form are just a few examples that can cause a website vulnerability.</p>
   <ul>
    <li>One vulnerability can impact over 1,000 pages on a single website.</li>
    <li class="red">Website vulnerabilities can exist for weeks, months, and even years without being detected.</li>
   </ul>
   </div></div>
   <div class="col-lg-5 splitPadding iconFirst"><img class="splitImg" src="/img/vulnerability-patching/vulnGlobe.svg" alt="Scanning Threat"></div>
  </div>
</div>
<div class="whiteSpace50"></div>
</div>


<div class="text-center" id="mainBlue"><div id="mainBody">
<h1 class="sourceBlack font55">Secure your website today.</h1>
<h3 class="font25">Get in touch with our website secruity specialists today to start building your comprehensive security solution.</h3>
<p class="text-center"><a class="btn btn-red" href="#ContactForm" data-toggle="modal" data-target="#ContactForm">Get Started Today</a></p>
</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<?php 
$ct_title = "Contact SiteLock 24/7";
$ct_phone = "855.378.6200";
$ct_btn = 'Send Message';
include 'includes/forms/contact.html';
?>

</body>
</html>
