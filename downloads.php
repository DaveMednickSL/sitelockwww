<?php 
//DEFINITIONS
$title = "Downloads | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="sourceBlack">Download Your Complimentary<br>Website Security Resources</h1>
<h3>Get free access to SiteLock security reports, whitepapers, infographics, help guides, and more.</h3>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainGrey"><div id="mainBody">

<div class="whiteSpace50"></div>

<div class="row">
<div class="col-md-2"><button class="btn-d btn-downloads dnavy filter-button" data-filter="all" href="all">View All</button></div>
<div class="col-md-2"><button class="btn-d btn-downloads dred filter-button" data-filter="reports">Reports</button></div>
<div class="col-md-2"><button class="btn-d btn-downloads dgreen filter-button" data-filter="partner">Partner</button></div>
<div class="col-md-2"><button class="btn-d btn-downloads dyellow filter-button" data-filter="tips">Tips</button></div>
<div class="col-md-2"><button class="btn-d btn-downloads dpurple filter-button" data-filter="podcast">Podcasts</button></div> 
<!--<div class="col-md-2"><button class="btn-d btn-downloads dblue filter-button" data-filter="whitepapers">White Papers</button></div>-->
<div class="col-md-2">
<button onclick="downloadFunc()" class="btn-d btn-downloads dropbtn" style="background: #fff; color:#000 !important;">More</button>
<div id="showMoreDiv" class="dl-dropdown-content">
    <center><button class="btn-d btn-downloads dblue filter-button" data-filter="whitepapers" style="max-width: 80%;">White<br>Papers</button><br>
    <button class="btn-d btn-downloads dorange filter-button" data-filter="other" style="max-width: 80%;">Other</button><br><br></center>
  </div>
</div> 
</div>

<br><br>

<div id="download_cards_section" class="row">


</div>
<div class="whiteSpace50"></div>
</div>


<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<script>
////////////////////////////////////////////

//SCROLL OBJECT
    $(document).ready(function(){

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');

        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');

        }
    });

    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});

//WHITE RABBIT OBJECT
fetch('includes/data/downloads.json')
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }

      // Examine the text in the response
      response.json().then(function(downloadData) {
        console.log(downloadData);
        var download_cards_section = "";
        for(index in downloadData) {
          for(download in downloadData[index]) {
            download_cards_section += 
              '<div class="gallery_product col-md-4 filter ' + downloadData[index][download]['filter'] + '">'
            + '<div class="' + downloadData[index][download]['color'] + '" id="downloadBoxHeader">' + downloadData[index][download]['type'] + '</div>'
            + '<div id="downloadBox">'
            + '<h3>' + downloadData[index][download]['title'] + '</h3>'
            + '<p>' + downloadData[index][download]['snippet'] + '</p>'
            + '<a class="btn btn-ghost-grey" href="' + downloadData[index][download]['dl_link'] + '" target="_blank">' + downloadData[index][download]['btn_text'] + '</a>'
            + '</div></div>';
          }
        }
        document.getElementById("download_cards_section").innerHTML = download_cards_section;
      });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });

//BUTTON DROP DOWN
function downloadFunc() {document.getElementById("showMoreDiv").classList.toggle("dl-show");}

window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dl-dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('dl-show')) {
        openDropdown.classList.remove('dl-show');
      }}}}
</script>
</body>
</html>