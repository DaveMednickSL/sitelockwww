<?php
//DEFINITIONS
$title = "SiteLock User Experience | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";

//FORM DEFINITIONS
$hb_title = "Secure Your Website";
$hb_phone = "(855) 378-6200";
$hb_phone_link = "8553786200";
$hb_btn = 'Get A Free Consultation';
$hb_target = '';
include 'includes/forms/high-barrier.html';
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="sourceBlack">Welcome to the SiteLock Experience</h1>
<h3>Where website security is simple.</h3>
<a class="btn btn-ghost-white" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Contact us & we'll be in touch</a>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<h1 class="sourceBlack font45">Welcome To SiteLock</h1>
<p class="font18">Hello from SiteLock, your website security experts. We keep websites like yours safe and secure from cybercriminals. We understand that website security can be confusing and overwhelming, especially if you don’t know where to start. Don’t worry, we’re here to help! Let us walk you through the SiteLock experience and answer any questions you may have along the way.</p>
<div class="whiteSpace25"></div>

<div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/259753291" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>

<div class="whiteSpace50"></div>
<div class="row">
<div class="col-md-6"><h2 class="text-center sourceBlack font35">The SiteLock experience</h2>If you’re not receiving exceptional customer service, then we’re not doing our job. As our customer, we vow to make your SiteLock experience as positive as possible. Take a look at your journey once you join the SiteLock family.<div class="whiteSpace25"></div><p class="text-center"><a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Get Started</a></p></div>
<div class="col-md-6"><h2 class="text-center sourceBlack font35">Get help 24/7/365</h2>Did you know the SiteLock team of U.S.-based support is available to help you 24/7/365 via phone, email, or live chat? That’s right—even nights, holidays, and weekends!<div class="whiteSpace25"></div><p class="text-center"><a class="btn btn-red" href="#FAQ">Read Our FAQ</a></p></div>
</div>

</div>
<div class="whiteSpace50"></div>
</div>

<div class="text-center" id="mainBlue"><div id="mainBody">
<h2 class="sourceBlack font25">SiteLock has been providing me with a wonderful service for a number of years and have been absolutely top notch - the personnel are friendly and courteous and have always provided answers to all my questions. I would highly recommend SiteLock to everyone I know. </h2>
<p class="font18">-Donald</p>
</div></div>

<div id="mainBody"><div id="content">

<div id="FAQ" class="whiteSpace50"></div>

<h2 class="text-center font55">You have questions, we have answers</h2>
<a id="collapsebtn1" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1"><span class="float-left">Who is SiteLock?</span> <span class="float-right"><i id="collapseone" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse1">
<br>
<p class="linkblue font18">SiteLock is the <a href="about">global leader in website security</a>. We founded the company in 2008 with a passion to protect every website on the internet. Fast forward to the present, and we now protect over 12 million websites of all sizes around the world.</p>
</div>

<a id="collapsebtn2" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2"><span class="float-left">What is website security?</span> <span class="float-right"><i id="collapsetwo" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse2">
<br>
<p class="linkblue font18">Comprehensive website security protects your website from malicious cyber threats. This includes protection of your site code and web applications. Depending on your website security package, you’ll receive daily website scans, automated malware removal and vulnerability patching, and a web application firewall to block harmful traffic from entering your site.</p>
</div>

<a id="collapsebtn3" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3"><span class="float-left">Doesn’t my hosting provider protect my website?</span> <span class="float-right"><i id="collapsethree" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse3">
<br>
<p class="linkblue font18">It’s a common <a href="https://www.sitelock.com/blog/2017/03/do-i-need-sitelock/">misconception that hosting providers protect each website they host</a>. The reality is, your web host only protects the server your website is hosted on, not the website itself. Think of it like securing an apartment building. Property management takes responsibility for securing the building, but each tenant must lock the door to their own apartment.</p>
</div>

<a id="collapsebtn4" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse4" role="button" aria-expanded="false" aria-controls="collapse4"><span class="float-left">Why do I need website security?</span> <span class="float-right"><i id="collapsefour" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse4">
<br>
<p class="linkblue font18">Today, websites are targeted a whopping 50 times per day, per website on average. A website compromise can be a heartbreaking and costly experience—potentially resulting in damage to your site visitors, revenue, and reputation. To protect your site and visitors, website security is essential. And, it’s affordable and easy to get started! <a href="pricing">Click here to protect your site today</a>.</p>
</div>

<a id="collapsebtn5" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse5" role="button" aria-expanded="false" aria-controls="collapse5"><span class="float-left">Doesn’t my SSL Certificate protect my website?</span> <span class="float-right"><i id="collapsefive" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse5">
<br>
<p class="linkblue font18">Your SSL certificate protects data <i>in transit</i>, not data <i>stored</i> on your website. To fully protect your website, additional security is highly recommended.</p>
</div>

<a id="collapsebtn6" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse6" role="button" aria-expanded="false" aria-controls="collapse6"><span class="float-left">Do SiteLock services work will all hosting providers?</span> <span class="float-right"><i id="collapsesix" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse6">
<br>
<p class="linkblue font18">Yep! SiteLock works with all website hosting providers.</p>
</div>

<div class="whiteSpace50"></div>

</div></div>

<?php include 'includes/rating-bar.php';?>


<div class="whiteSpace50"></div>

<div id="mainBody"><div id="content">
<h2 class="font45 text-center">SiteLock <span class="sourceBlack">customer success stories</span></h2>

<div id="caseBox">
<div class="row">
<div class="col-md-6">
<div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/288594809" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack">A Photographer Bounces Back<br>After Website Hit With Malware</h3>
<p class="font18">The cyberattack to Melissa’s WordPress site damaged her company’s bottom line and reputation. Watch as she shares her SiteLock experience and path to success after her site was restored.</p>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/09/sitelock-video-reviews-website-malware/">Read More</a>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
<div id="caseBox">
<div class="row">
<div class="col-md-6">
<img class="img90" src="img/community/vic.png" alt="Vic's Tree Service">
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack">Vic’s Tree Service Springs Back to Life With Website Security</h3>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/09/sitelock-reviews-tree-trimming-business-case-study/">Read More</a>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div id="caseBox">
<div class="row">
<div class="col-md-6">
<img class="img90" src="img/community/marlowes.png" alt="Marlowe's BBQ">
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack">Memphis Restaurant’s Joomla! Site Stays Safe</h3>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/05/sitelock-reviews-marlowesmemphis/">Read More</a>
</div>
</div>
</div>
</div>
</div>

</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>