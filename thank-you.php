<?php
//DEFINITIONS
$title = "Thank You | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainTerms" style="margin-top: 120px;">
<div class="row">
<div class="col-md-6">
<h1 class="sourceBlack blue">Thanks!</h1>
<p style="font-size: 18px;">You’ve taken an important first step in ensuring the security of your business and your reputation. One of our website security consultants will reach out shortly to help build the perfect security bundle for your site.</p>
</div>

<div class="col-md-6 text-center">
<br><br><img class="img90" src="/img/logos/SiteLock_logo_cloud.svg" alt="SiteLock Logo Cloud"><br><br>
</div>
</div>

<h1 class="sourceBlack blue">In the meantime...</h1>
<div class="row">
<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/chatBubbleBlue.svg" alt="Scanning Cloud"><h4 class="sourceLight">See the latest at the<br>SiteLock Blog</h4><br><a class="btn btn-ghost-blue" href="https://sitelock.com/blog">Get in the Know</a></div><br></div>
</div>

<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/lightbulbRed.svg" alt="Scanning Cloud"><h4 class="sourceLight">Check out our<br>Resources Center</h4><br><a class="btn btn-ghost-blue" href="resources">Start Exploring</a></div><br></div>
</div>

<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/qsrRed.svg" alt="Scanning Cloud"><h4 class="sourceLight">Download the SiteLock<br>Website Security Insider</h4><br><a class="btn btn-ghost-blue" href="security-report">Read the Report</a></div><br></div>
</div>
</div>

<div class="whiteSpace50"></div>

</div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>
