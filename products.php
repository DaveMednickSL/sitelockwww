<?php
//DEFINITIONS
$title = "Products | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1><span class="sourceBlack">Fast, Affordable Website Security Products</span></h1>
<h3>Find, fix, and prevent website threats today. Take a look around and get to<br>know the products that can protect your site.</h3>
<a class="btn btn-blue" href="#onpagepricing">Choose Your Website Security</a>
<div class="whiteSpace50"></div>
</div></div>

<?php include 'includes/rating-bar.php';?>

<div id="mainBody">
<div class="row">
<div class="col text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><br><img class="imgHeight130" src="/img/scanningWebsite.svg" alt="Scanning Website"><h5 class="sourceBlack">WEBSITE<br>SCANNING</h5><p>Monitor your website daily for malicious or suspicious activity. When threats are found, you are notified immediately.</p><br><a class="btn btn-ghost-grey" href="website-scanning">Learn More</a></div><br></div>
</div>

<div class="col text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><br><img class="imgHeight130" src="/img/scanningCloud.svg" alt="Scanning Cloud"><h5 class="sourceBlack">MALWARE<br>REMOVAL</h5><p>Find and automatically remove website malware before any damage is caused to your site or visitors.</p><br><a class="btn btn-ghost-grey" href="malware-removal">Learn More</a></div><br></div>
</div>

<div class="col text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><br><img class="imgHeight130" src="/img/scanningWAF.svg" alt="Scanning WAF"><h5 class="sourceBlack">WEB APPLICATION<br>FIREWALL</h5><p>Block cybercriminals and bad bots, plus cut your load time by up to 50 percent.</p><br><br><a class="btn btn-ghost-grey" href="web-application-firewall">Learn More</a></div><br></div>
</div>

<div class="col text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><br><img class="imgHeight130" src="/img/scanningThreat.svg" alt="Scanning Threat"><h5 class="sourceBlack">VULNERABILITY<br>PATCHING</h5><p>Automatically patch vulnerabilities in your CMS before cybercriminals can exploit them.</p><br><br><a class="btn btn-ghost-grey" href="vulnerability-patching">Learn More</a></div><br></div>
</div>

<div class="col text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><br><img class="imgHeight130" src="/img/scanningPCI.svg" alt="Scanning Threat"><h5 class="sourceBlack">PCI<br>COMPLIANCE</h5><p>The SiteLock PCI compliance program is a fast and easy way to meet PCI requirements.</p><br><br><a class="btn btn-ghost-grey" href="pci-compliance">Learn More</a></div><br></div>
</div>
</div>
<div class="whiteSpace100"></div>
</div>

<?php include 'includes/awards-bar.php';?>

<?php include 'includes/cms-bar.php';?>

<div id="onpagepricing"></div>
<div id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="text-center font55">Don’t wait, protect your website now.<br><span class="sourceBlack">Buy your website security plan today!</span></h1>


<?php include 'includes/price-bar-1.php';?>

<?php include 'includes/includes-bar.php';?>

</div>

<?php include 'includes/solutions-bar.php';?>

<div id="mainGrey"><div id="mainBody">
  <h1 class="text-center font55">Want to do more research before<br>making a decision?</h1>
  <div class="row">
  <div class="col-md-4 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/chatBubbleBlue.svg" alt="Scanning Cloud"><h3 class="sourceLight">See the latest at the<br>SiteLock Blog</h3><br><a class="btn btn-ghost-blue" href="/blog">Get in the Know</a></div><br></div>
  </div>

  <div class="col-md-4 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/lightbulbRed.svg" alt="Scanning Cloud"><h3 class="sourceLight">Check out our<br>Resources Center</h3><br><a class="btn btn-ghost-blue" href="help-center">Start Exploring</a></div><br></div>
  </div>

  <div class="col-md-4 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/qsrRed.svg" alt="Scanning Cloud"><h3 class="sourceLight">Download the SiteLock<br>Website Security Insider</h3><br><a class="btn btn-ghost-blue" href="security-report">Read the Report</a></div><br></div>
  </div>
  </div>

  <div class="whiteSpace100"></div>

</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>
