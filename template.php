<?php
//DEFINITIONS
$title = "Project Noble";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div id="mainBody">
<h1 class="sourceBlack">Website Security</h1>
<p>Protect your website today</p>
<style>
  .col-md-3 {
    min-height: 150px;
  }
</style>
</div></div>

<div id="main"><div id="content">

<h1>Buttons</h1>
<hr>
<p><button class="btn btn-red">Learn More</button></p>
<p><button class="btn btn-blue">Learn More</button></p>
<p><button class="btn btn-ghost-grey">Learn More</button></p>
<p style="background: #707070; padding: 5px;"><button class="btn btn-ghost-white">Learn More</button></p>
<p><button class="btn btn-ghost-red">Learn More</button></p>
<p><button class="btn btn-ghost-blue">Learn More</button></p>

<h1>Forms</h1>
<hr>
<div class="row text-center">
<div class="col-sm-3">
    <a class="btn btn-ghost-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">High Barrier</a>
</div>

<div class="col-sm-3">
    <a href="#LowBarrierForm" class="btn btn-blue" data-toggle="modal" data-target="#LowBarrierForm">Low Barrier</a>
</div>

<div class="col-sm-3">
    <a href="#ContactForm" class="btn btn-red" data-toggle="modal" data-target="#ContactForm">Contact</a>
</div>

<div class="col-sm-3">
    <a href="#ReviewForm" class="btn btn-ghost-white" data-toggle="modal" data-target="#ReviewForm">Review</a>
</div>
</div>
</div><br></div>
<br>
<?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "xxx.xxx.xxxx";
$hb_btn = 'Start Today';
include 'includes/forms/high-barrier.html';
?>

<?php
$lb_title = "Request A Custom Quote";
$lb_btn = 'Get My custom Quote';
include 'includes/forms/low-barrier.html';
?>

<?php
$ct_title = "Contact SiteLock 24/7";
$ct_phone = "xxx.xxx.xxxx";
$ct_btn = 'Send Message';
include 'includes/forms/contact.html';
?>

<?php
include 'includes/forms/review.html';
?>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>
