<?php
//DEFINITIONS
$title = "Agencies | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";

$hb_title = "Get Started With SiteLock";
$hb_phone = "833.715.1304";
$hb_btn = 'Contact Us';
include 'includes/forms/channel.html';
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div id="mainHeaderBody">
<div class="whiteSpace50 tabshow"></div>
<div class="row">
<div class="col-lg-6 my-auto solutionHead">
<h1><span class="sourceBlack">Increase customer satisfaction</span> and website uptime</h1>
<h3 class="font25">Ensure your clients’ websites are protected from cyberthreats.</h3>
<a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Partner with SiteLock</a>
</div>
<div class="col-lg-6">
<img class="headerimgmain" src="/img/channel/agencyHeader.png" alt="Agency Benefits">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<?php include 'includes/awards-bar.php';?>

<div id="mainTerms"><div id="center">
<div class="whiteSpace50"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2><span class="sourceBlack">The ongoing benefit</span><br>of website security</h2><p>Keep your clients safe and set your business apart from the competition by partnering with SiteLock. Include website security in your maintenance packages and protect your customers from detrimental cyberattacks.</p><a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Protect Your Clients Today</a></div></div>
   <div class="col-lg-5 splitPadding footMobileHide"><img class="splitImg" src="/img/channel/agencyBenefit.svg" alt="Agency Benefits"></div>
  </div>
</div>

<div class="whiteSpace100"></div>

<h2 class="font55 text-center sourceBlack">Agency Benefits</h2>

<div class="row">
<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed setFA" src="img/fa_icons/icon-Smile-comment.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto"><p>Protect your<br>business reputation</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Gears.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto"><p>Manage multiple domains<br>simultaneously (0 - 1000+)</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Cart.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto"><p>Benefit from<br>tiered-volume discounts</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed" src="img/fa_icons/icon-Magic.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto"><p>Easily modify, add,<br>or remove websites</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed setFA" src="img/fa_icons/icon-Shield.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto"><p>Protect your customer’s<br>websites and projects</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Star.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto"><p>Differentiate your agency<br>from the competition</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleBlue" src="img/fa_icons/icon-Gear.svg" alt="Cogs"></div>
  <div class="col-sm-8 my-auto"><p>Automatically remove<br>malware and patch CMS<br>vulnerabilities</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6">
  <div id="channelBox"><div id="channelBoxContent">
  <div class="row">
  <div class="col-sm-4"><img class="fal fa-comment-smile channelCircleRed" src="img/fa_icons/icon-Watch.svg" alt="Smile Comment"></div>
  <div class="col-sm-8 my-auto"><p>Save time dealing<br>by automating website<br>cleans</p></div>
  </div>
  </div></div>
</div>

<div class="col-md-6 text-center"><br><a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Get Started Today</a></div>
<div class="col-md-6 text-center"><br><a class="btn btn-ghost-grey" href="tel:8447768614">Call 844.776.8614</a></div>
</div>

<div class="whiteSpace100"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-lg-5 splitPadding"><div id="content" style="width: 90%; margin-left: 10%; margin-right: auto;"><h2 class="sourceBlack">Learn how one web developer protects 125 websites</h2></div></div>
  <div class="col-lg-7 iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/288599817" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
 </div>
</div>
<div class="whiteSpace50"></div>

</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>