<?php
//DEFINITIONS
$title = "Currently Infected | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderDark"><div id="mainBody">
<div class="row">
<div class="col-lg-6 my-auto">
<h1>Is your website<br><span class="sourceBlack">hacked, blacklisted, or suspended?</span></h1>
<h2 class="font25">Don’t worry—we’ll fix your website fast.</h2>
<a class="btn btn-red" href="#onpagepricing">Get Help Now</a>
<br>
</div>
<div class="col-lg-6">
<img class="headerimg" src="/img/currently-infected/infectedSuspended.png" alt="Laptop SiteLock Dashboard">
</div>
</div>
</div></div>

<?php include 'includes/rating-bar.php';?>

<div id="onpagepricing"></div>
<div id="mainBody">
<div class="whiteSpace100"></div>
<div class="row">
<div class="col-md-6">
<div id="highlightedPriceRed">
  <div id="priceRedHead">
  <div id="priceBoxContent">
  <h3 class="sourceRegular colorWhite"><span class="sourceBlack">Business and Personal</span> websites</h3>
  </div>
  </div>
  <div class="text-center" id="priceBoxContent">
  <br>
  <h2 class="sourceLight font35">$49.99</h2>
  Per Month/Domain<br>
  <strong>Billed Annually</strong><br><br>
  <p class="text-left">
  Quickest repair of existing infections, removal from blacklists, and resolution suspension. Plus, protect against future attacks, increase site speed, and protect website forms.
  </p>
  <a class="btn btn-red" href="checkout?planID=1008">GET STARTED TODAY</a>
  <div class="whiteSpace25"></div>
  </div>
  </div>
</div>

<div class="col-md-6">
<div id="highlightedPriceBox">
  <div id="priceGreyHead">
  <div id="priceBoxContent">
  <h3 class="sourceRegular">Perfect for <span class="sourceBlack">Ecommerce</span></h3>
  </div>
  </div>
  <div class="text-center" id="priceBoxContent">
  <br>
  <h2 class="sourceLight font35">$69.99</h2>
  Per Month/Domain<br>
  <strong>Billed Annually</strong><br><br>
  <p class="text-left">
  All the features of the business and personal website plan plus safeguard credit card data, automatically patch ecommerce plugins and protect CMS database files.
  </p>
  <a class="btn btn-blue" href="checkout?planID=1009">CLEAN MY SITE</a>
  <div class="whiteSpace25"></div>
  </div>
  </div>
</div>

<div class="whiteSpace50"></div>

<?php include 'includes/includes-bar.php';?>
</div>
<div class="whitespace50"></div>
</div>

<?php include 'includes/solutions-bar.php';?>

<?php include 'includes/awards-bar.php';?>

<?php include 'includes/cms-bar.php';?>

<?php 
$benefitsTitle = '<h2 class="text-center font55">Website Security <span class="sourceBlack">benefits</span></h2>'; 
$icon1= 'fa-rocket'; $title1 = 'Fast website repair'; $content1 = 'No matter how severe the website attack, we work around the clock to get you back online as quickly as possible—no exceptions.';
$icon2 = 'fa-badge-check'; $title2 = 'Satisfaction guaranteed'; $content2 = 'Protect your site with affordable and comprehensive security. Delivering exceptional service is our number one priority. Our team is available 24/7/365 to help.';
$icon3 = 'fa-shield'; $title3 = 'Prevent future attacks'; $content3 = 'Stay off web host suspensions and blacklists with a security plan that prevents malware from infecting your website.';
$icon4 = 'fa-heart'; $title4 = 'Ongoing protection'; $content4 = 'Never worry about a website compromise again. With ongoing, proactive protection, we’ll find and fix threats before you even know they exist.';
$icon5 = 'fa-headset'; $title5 = '24/7/365<br>U.S.-based support'; $content5 = 'Day or night, our friendly, U.S.-based team of security experts is available to help 24/7/365 via phone or live chat.';
$icon6 = 'fa-magic'; $title6 = 'Immediate alerts and updates'; $content6 = 'Always be informed about the security of your site with automated alert emails and live results from your SiteLock Dashboard.';
$benefitsBTN = 'Protect Your Website Today';

include('includes/benefits-bar.php');
?>

<div id="mainBody">
<div class="whiteSpace100"></div>
<h2 class="text-center sourceBlack font55">Learn more about the products<br>protecting your website</h2>

<div class="row">
<div class="col-md-4 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningCloud.svg" alt="Scanning Cloud">
  <h5 class="sourceBlack">SiteLock SMART&trade;</h5>
  <p>Automated malware removal</p>
  <p class="text-left">
  Website file monitoring<br>
  <i class="far fa-check"></i> Daily website scanning<br>
  <i class="far fa-check"></i> Find malware and vulnerabilities<br>
  <i class="far fa-check"></i> Automatically remove malware in site files<br>
  <i class="far fa-check"></i> Receive immediate threat alerts
  </p>
  <br><br><br><a class="btn btn-ghost-grey" href="malware-removal">Learn More</a></div><br></div>
</div>

<div class="col-md-4 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningThreat.svg" alt="Scanning Threat">
  <h5 class="sourceBlack">SiteLock INFINITY&trade;</h5>
  <p>Continuous, non-stop scanning</p>
  <p class="text-left">
  <i class="far fa-check"></i> Find malware and vulnerabilities<br>
  <i class="far fa-check"></i> Automatically remove malware<br>
  <i class="far fa-check"></i> Automatically remove malware in WordPress databases<br>
  <i class="far fa-check"></i> Automatically patch CMS vulnerabilities<br>
  <i class="far fa-check"></i> Access to SiteLock engineers for sophisticated threats<br>
  <i class="far fa-check"></i> Receive immediate threat alerts
  </p>
  <a class="btn btn-ghost-grey" href="malware-removal">Learn More</a></div><br></div>
</div>

<div class="col-md-4 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningWAF.svg" alt="Scanning WAF">
  <h5 class="sourceBlack">SiteLock TrueShield&trade;</h5>
  <p>Web application firewall</p>
  <p class="text-left">
  <i class="far fa-check"></i> Block bad bots<br>
  <i class="far fa-check"></i> Block comment spam<br>
  <i class="far fa-check"></i> Block direct hack attempts<br>
  <i class="far fa-check"></i> Increase website speed<br>
  <i class="far fa-check"></i> Receive traffic statistics and reports<br>
  <i class="far fa-check"></i> Supports SSL enabled sites
  </p>
  <br><br><a class="btn btn-ghost-grey" href="malware-removal">Learn More</a></div><br></div>
</div>
</div>

<div class="whiteSpace100"></div>
<div id="mainSplitLeft">
    <div class="row splitSpace">
     <div class="col-md-5"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;">
     <h2 class="sourceBlack">What to do if your website is suspended by your web host</h2>
     Your hosting provider will temporarily suspend your website when it’s infected with malware. This helps prevent your site from cross-infecting other sites on its shared server. If your website is suspended, you will need to clean the malicious files to remove the suspension. SiteLock will work with you and your web host to clean your site and get you back online fast.
     <br><a class="btn btn-red" href="#onpagepricing">Clean My Website Now</a></div></div>
     <div class="col-md-7 splitPadding"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/209592176" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
    </div>
  </div>
<div class="whiteSpace100"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-md-7 whitebk splitPadding"><div id="content"><h2><span class="sourceRegular">What to do if your website</span><br><span class="sourceBlack">is blacklisted by search engines</span></h2>If search engines find malware on your website, it will be blacklisted. This means it could be temporarily removed from search engine listings and your visitors will receive a warning messaging stating your site is hacked. SiteLock will remove the malware from your website and get you off search engine blacklists as quickly as possible.</strong><br><a class="btn btn-red" href="#onpagepricing">Remove Malware Now</a></div></div>
   <div class="col-md-5 splitPadding"><img class="splitImg" src="/img/currently-infected/infectedRedScreen.png" alt="Scanning Calendar"></div>
  </div>
</div>
<div class="whiteSpace100"></div>
</div>
</div>

<div id="mainRed"><div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-6">
<h2 class="font55">What is <span class="sourceBlack">website malware?</span></h2>
<p class="font25">Malware, short for malicious software, is designed to harm your website and visitors. Malware is used to vandalize websites, steal traffic, or even take control of your site.</p>
</div>
<div class="col-lg-6 my-auto"><img class="img90" src="/img/currently-infected/infectedAlert.svg"></div>
</div>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainBody">
<div class="whiteSpace100"></div>
<div id="mainSplitLeft">
    <div class="row splitSpace">
     <div class="col-md-5"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2 class="sourceBlack">Don’t sweat a website attack</h2>When it comes to fast website protection, SiteLock has you covered. Let us walk you through your journey as a SiteLock customer, so you can save your website from a cyberattack.<br><a class="btn btn-red">Get Started</a></div></div>
     <div class="col-md-7 splitPadding"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/259753291" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
    </div>
  </div>
<div class="whiteSpace100"></div>

<div class="row">
<div class="col-md-6">
<div id="highlightedPriceRed">
  <div id="priceRedHead">
  <div id="priceBoxContent">
  <h3 class="sourceRegular colorWhite"><span class="sourceBlack">Business and Personal</span> websites</h3>
  </div>
  </div>
  <div class="text-center" id="priceBoxContent">
  <br>
  <h2 class="sourceLight font35">$49.99</h2>
  Per Month/Domain<br>
  <strong>Billed Annually</strong><br><br>
  <p class="text-left">
  Quickest repair of existing infections, removal from blacklists, and resolution suspension. Plus, protect against future attacks, increase site speed, and protect website forms.
  </p>
  <a class="btn btn-red" href="checkout?planID=1008">GET STARTED TODAY</a>
  <div class="whiteSpace25"></div>
  </div>
  </div>
</div>

<div class="col-md-6">
<div id="highlightedPriceBox">
  <div id="priceGreyHead">
  <div id="priceBoxContent">
  <h3 class="sourceRegular">Perfect for <span class="sourceBlack">Ecommerce</span></h3>
  </div>
  </div>
  <div class="text-center" id="priceBoxContent">
  <br>
  <h2 class="sourceLight font35">$69.99</h2>
  Per Month/Domain<br>
  <strong>Billed Annually</strong><br><br>
  <p class="text-left">
  All the features of the business and personal website plan plus safeguard credit card data, automatically patch ecommerce plugins and protect CMS database files.
  </p>
  <a class="btn btn-blue" href="checkout?planID=1009">CLEAN MY SITE</a>
  <div class="whiteSpace25"></div>
  </div>
  </div>
</div>
</div>
<div class="whitespace50"></div>
</div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>
