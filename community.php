<?php
//DEFINITIONS
$title = "Community | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div id="mainBody">

<div class="row">
<div class="col-md-6">
<h1><span class="sourceBlack">Developing</span> a safer internet <span class="sourceBlack">together.</span></h1>
<h3 class="font16">From WordPress, to Joomla!, to Drupal, and more—we're passionate about protecting websites. Explore our community hub and find out what we're up to. Want to team up with SiteLock on a project or event? Drop us a line!</h3>
<a class="btn btn-red" href="contact">Let's Collaborate</a>
</div>
<div class="col-md-6 my-auto"><img class="communityHeader" src="img/community/communityHeader.png" alt="Community Hub"></div>
</div>

</div></div>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<h2 class="font45 sourceBlack">Let’s meet!</h2>
<h3 class="font25">Check out the events we’re attending</h3>

<div class="row">

<div class="col-md-3"><div class="bkgrey text-center" id="darkBorderBox">
<div class="gradone"><br></div>
<div id="darkBorderBoxContent">
<p class="sourceBlack font25">WordCamp Miami</p>
<br>
<p><span class="font18">March</span>
<br><span class="sourceBlack font35">15-17</span></p>
<hr>
<a class="btn btn-ghost-blue btn-sm" href="https://2019.miami.wordcamp.org">Learn More</a><br><br>
</div></div></div>

<div class="col-md-3"><div class="bkgrey text-center" id="darkBorderBox">
<div class="gradone"><br></div>
<div id="darkBorderBoxContent">
<p class="sourceBlack font25">CloudFest</p>
<br>
<p><span class="font18">March</span>
<br><span class="sourceBlack font35">23-29</span></p>
<hr>
<a class="btn btn-ghost-blue btn-sm" href="https://www.cloudfest.com/">Learn More</a><br><br>
</div></div></div>

<div class="col-md-3"><div class="bkgrey text-center" id="darkBorderBox">
<div class="gradone"><br></div>
<div id="darkBorderBoxContent">
<p class="sourceBlack font25">WITS N. America</p>
<br>
<p><span class="font18">May</span>
<br><span class="sourceBlack font35">03-05</span></p>
<hr>
<a class="btn btn-ghost-blue btn-sm" href="https://witsummit.com/northamerica">Learn More</a><br><br>
</div></div></div>

<div class="col-md-3"><div class="bkgrey text-center" id="darkBorderBox">
<div class="gradone"><br></div>
<div id="darkBorderBoxContent">
<p class="sourceBlack font25">WordCamp Europe</p>
<br>
<p><span class="font18">June</span>
<br><span class="sourceBlack font35">20-22</span></p>
<hr>
<a class="btn btn-ghost-blue btn-sm" href="https://2019.europe.wordcamp.org/">Learn More</a><br><br>
</div></div></div>
</div>

<div class="whiteSpace100"></div>

<h2 class="font45 sourceBlack">Couldn’t make an event?</h2>
<h3 class="font25">No worries! Get caught up with our event recaps</h3>
<div class="row">
<div class="col-md-6">
<div class="bkgrey" id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack text-center my-auto"><br><h3 class="font35">WordCamp US 2018 – Blockbuster Security</h3><br></div>
<div class="col-md-3 my-auto text-center linkred" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/12/wordcamp-us-2018-recap/"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div class="bkgrey" id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack text-center my-auto"><br><h3 class="font35">WordCamp Portland – Keep Portland WORD!</h3><br></div>
<div class="col-md-3 my-auto text-center linkred" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/11/wordcamp-portland-keeping-portland-word/"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div class="bkgrey" id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack text-center my-auto"><br><h3 class="font35">WC Sacramento 2018 – In Review</h3><br></div>
<div class="col-md-3 my-auto text-center linkred" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/09/wordcamp-sacramento-2018-in-review//"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div class="bkgrey" id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack text-center my-auto"><br><h3 class="font35">WordCamp Denver – WordPress is People!</h3><br></div>
<div class="col-md-3 my-auto text-center linkred" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/08/wordcamp-denver-2018-wordpress-is-people/"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>
</div>

<br><center><a class="btn btn-red" href="https://www.sitelock.com/blog/tag/wordcamp/">View All</a></center>

<div class="whiteSpace50"></div>
</div></div>

<div id="mainGrey"><div id="mainBody"><div id="content">

<div class="whiteSpace50"></div>

<h2 class="font45"><span class="sourceBlack">Get helpful tips</span> and<br>information on our blog</h2>

<div class="row">

<div class="col-md-6">
<div id="darkBorderBoxAlt">
<img class="img100" src="img/community/blog1.png" alt="blog 1">
<div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack"><br><h3 class="font35">WordCamp US 2018 – Blockbuster Security</h3><br></div>
<div class="col-md-3 my-auto text-center linkblue" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/12/wordcamp-us-2018-recap/"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt">
<img class="img100" src="img/community/blog2.jpg" alt="blog 2">
<div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack"><br><h3 class="font35">WC Baltimore 2018 – A Star Spangled Event</h3><br></div>
<div class="col-md-3 my-auto text-center linkblue" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/10/wordcamp-baltimore-2018-recap/"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt">
<img class="img100" src="img/community/blog3.jpg" alt="blog 3">
<div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack"><br><h3 class="font35">WC Los Angeles 2018 in Review</h3><br></div>
<div class="col-md-3 my-auto text-center linkblue" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/10/wordcamp-los-angeles-2018-in-review/"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt">
<img class="img100" src="img/community/blog4.jpg" alt="blog 4">
<div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack"><br><h3 class="font35">WC Sacramento 2018 in Review</h3><br></div>
<div class="col-md-3 my-auto text-center linkblue" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/09/wordcamp-sacramento-2018-in-review/"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt">
<img class="img100" src="img/community/blog5.jpg" alt="blog 5">
<div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack"><br><h3 class="font35">WC Montreal 2018: 10 Years of WordPress</h3><br></div>
<div class="col-md-3 my-auto text-center linkblue" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/08/wordcamp-montreal-2018-recap/"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>

<div class="col-md-6">
<div id="darkBorderBoxAlt">
<img class="img100" src="img/community/blog6.png" alt="blog 6">
<div id="darkBorderBoxContent">
<div class="row">
<div class="col-md-9 my-auto sourceBlack"><br><h3 class="font35">Events We Love<br>in 2018</h3><br></div>
<div class="col-md-3 my-auto text-center linkblue" style="padding: 10px !important;"><a href="https://www.sitelock.com/blog/2018/05/sitelock-reviews-events-2018/"><i class="far fa-chevron-circle-right fa-4x"></i></a></div>
</div>
</div></div>
</div>
</div>

<div class="whiteSpace50"></div>

</div></div></div>

<div class="whiteSpace50"></div>

<div id="mainBody"><div id="content">
<h2 class="font45">A word from our <span class="sourceBlack">security superheroes!</span></h2>

<div id="caseBox">
<div class="row">
<div class="col-md-6">
<div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/288594809" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 10px !important;">A Photographer Bounces Back After a Website Compromise</h3>
<p class="font18" style="padding: 10px !important;">Amanda was locked out of her website and turned to SiteLock for help. Now, Amanda’s website is back in action and there isn’t anything standing in between her, her clients, and her photography.</p>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/09/sitelock-video-reviews-website-malware/">Read More</a>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
<div id="caseBox">
<div class="row">
<div class="col-md-6">
<img class="img90" src="img/community/vic.png" alt="Vic's Tree Service">
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 10px !important;">Vic’s Tree Service Springs Back to Life With Website Security</h3>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/09/sitelock-reviews-tree-trimming-business-case-study/">Read More</a>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div id="caseBox">
<div class="row">
<div class="col-md-6">
<img class="img90" src="img/community/marlowes.png" alt="Marlowe's BBQ">
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 10px !important;">Memphis Restaurant’s Joomla! Site Stays Safe</h3>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/05/sitelock-reviews-marlowesmemphis/">Read More</a>
</div>
</div>
</div>
</div>
</div>

</div></div>

<div class="whiteSpace50"></div>

<div id="mainBlue"><div id="mainBody"><div class="text-center" id="content">
<div class="whiteSpace50"></div>
<h2 class="font45 sourceBlack">Connect with SiteLock</h2>
<div class="whiteSpace25"></div>
<div class="row text-center linkWhite">
<div class="col-sm-3"><a href="https://www.facebook.com/SiteLock/" target="_blank"><i class="fab fa-facebook-square fa-6x"></i></a></div>
<div class="col-sm-3"><a href="https://twitter.com/SiteLock" target="_blank"><i class="fab fa-twitter-square fa-6x"></i></a></div>
<div class="col-sm-3"><a href="https://www.linkedin.com/company/sitelock/" target="_blank"><i class="fab fa-linkedin fa-6x"></i></a></div>
<div class="col-sm-3"><a href="https://www.instagram.com/sitelock/" target="_blank"><i class="fab fa-instagram fa-6x"></i></a></div>
</div>
<div class="whiteSpace50"></div>
</div></div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>