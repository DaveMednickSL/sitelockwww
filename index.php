<?php
//DEFINITIONS
$title = "Website Security | Web Security | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "Website Security, Website Protection, Website Security Software, Web Page Security";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div id="mainBody">
<div class="whiteSpace50 tabshow"></div>
<div class="row">
<div class="col-lg-6 my-auto solutionHead">
<h1 class="sourceBlack">Website Security & Protection</h1>
<h3>Automatically protect your website, reputation, and visitors against cyber threats.</h3>
<div class="whiteSpace25"></div>
<a class="btn btn-red" href="#onpagepricing">Explore Plans</a>
</div>
<div class="col-lg-6">
<img class="headerimgmain" src="/img/home/indexHeader.png" alt="Website Security Solutions">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<div id="mainBody">
<div class="whiteSpace100"></div>
<h2 class="text-center font45 sourceLight">Website security <span class="sourceBlack">designed for you</span></h2>

<div class="row sourceBlack">
<div class="col-lg-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/home/homeBlog.svg" alt="Blogs">
  <h5>Personal<br>websites & blogs</h5>
</div>
<div id="darkBorderBoxContent">
<a class="btn btn-ghost-grey" href="personal-site">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-lg-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/home/homeMobile.svg" alt="Blogs">
  <h5>Business<br>websites</h5>
</div>
<div id="darkBorderBoxContent">
<a class="btn btn-ghost-grey" href="business-solutions">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-lg-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/home/homeCode.svg" alt="Blogs">
  <h5>Web<br>developers</h5>
</div>
<div id="darkBorderBoxContent">
<a class="btn btn-ghost-grey" href="developer-solutions">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-lg-3 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/home/homeWP.svg" alt="Blogs">
  <h5>WordPress<br>websites</h5>
</div>
<div id="darkBorderBoxContent">
<a class="btn btn-ghost-grey" href="wordpress-security">Learn More</a><br><br>
</div>
</div>
</div>
</div>

<div class="whiteSpace100"></div>

</div>

<?php include 'includes/rating-bar.php';?>

<div id="mainBody">
<div class="whiteSpace50"></div>
<div id="mainSplitLeft">
    <div class="row splitSpace">
     <div class="col-lg-5 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2 class="sourceBlack">Welcome to the SiteLock experience</h2>We’re dedicated to helping website owners succeed while providing exceptional customer service along the way. Let us walk you through the SiteLock experience.<br><a class="btn btn-ghost-blue" href="#onpagepricing">Get Started</a></div></div>
     <div class="col-lg-7 splitPadding iconFirst"> <div class="vimMob" style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/259753291" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div></div>
  </div>
</div>

<div class="whiteSpace50"></div>
<div id="mainGrey">
<?php include 'includes/news-bar-1.php';?>
</div>

<div id="onpagepricing"></div>
<div id="mainBody">
<div class="whiteSpace100"></div>
<h2 class="text-center sourceBlack font45">Don’t Wait, Protect Your Website Now!</h2>
<?php include 'includes/price-bar-1.php';?>
<div class="whiteSpace100"></div>

<?php include 'includes/includes-bar.php';?>
<div class="whiteSpace100"></div>
</div>

<?php include 'includes/solutions-bar.php';?>

<?php include 'includes/awards-bar.php';?>

<?php 
$benefitsTitle = '<h2 class="text-center font45">Website Security <span class="sourceBlack">benefits</span></h2>'; 
$icon1= 'fa-umbrella'; $title1 = 'Peace of mind'; $content1 = 'Protect your website with automated security and never worry about a compromise damaging your site, reputation, or bottom line.';
$icon2 = 'fa-badge-check'; $title2 = 'Build visitor trust'; $content2 = 'Showcase a trust badge on your site to instill customer confidence. Today, 79 percent of online shoppers expect to see a trust badge. Your website scanner comes with a SiteLock Trust Seal that indicates your site is safe.';
$icon3 = 'fa-rocket'; $title3 = 'Increase website speed'; $content3 = 'Increase your site\'s speed by as much as 50 percent and watch your user experience and conversions improve with your SiteLock plan.';
$icon4 = 'fa-chart-line'; $title4 = 'Protect SEO'; $content4 = 'Save your SEO success. If search engines find malware on your site, it will be blacklisted and potentially removed from results. Protect your SEO by staying malware-free.';
$icon5 = 'fa-location'; $title5 = 'Accuracy'; $content5 = 'Protect your website with award-winning SiteLock products proven to block threats with 99.99 percent accuracy.';
$icon6 = 'fa-user-graduate'; $title6 = 'Stay informed'; $content6 = 'Be the first to know if there’s malicious activity on your site with real-time email alerts. You can also review your security results from your SiteLock Dashboard at any time.';
$benefitsBTN = 'Start Securing Your Site Now';

include('includes/benefits-bar.php');
?>

<?php include 'includes/cms-bar.php';?>

<div id="mainBody">

<div class="whiteSpace50"></div>

<h2 class="text-center font45">Website security products you'll love</h2>

<div class="row sourceBlack linkgrey">
<div class="col-md-2 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningWebsite.svg" alt="Scanning Website">
  <br>
  <a href="website-scanning"><h5>WEBSITE<br>SCANNING</h5><br></a>
</div>
</div>
</div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningCloud.svg" alt="Scanning Cloud">
  <br>
  <a href="malware-removal"><h5>MALWARE<br>REMOVAL</h5><br></a>
</div>
</div>
</div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningWAF.svg" alt="Scanning WAF">
  <br>
  <a href="web-application-firewall"><h5>WEB APPLICATION<br>FIREWALL</h5></a>
</div>
</div>
</div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningDatabase.svg" alt="Scanning Database">
  <br>
  <a href="vulnerability-patching"><h5>VULNERABILITY<br>PATCHING</h5><br></a>
</div>
</div>
</div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/shieldddos.svg" alt="DDOS Protection">
  <br>
  <a href="ddos-protection"><h5>DDOS<br>PROTECTION</h5><br></a>
</div>
</div>
</div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/pci-compliance/pciCheck.svg" alt="PCI Compliance">
  <br>
  <a href="pci-compliance"><h5>PCI<br>COMPLIANCE</h5><br></a>
</div>
</div>
</div>
</div>

<div class="whiteSpace100"></div>
</div>

<?php include 'includes/trusted-bar.php';?>

<?php include 'includes/qsr-bar.php';?>

<div id="mainGrey">
<div class="whiteSpace50"></div>
<div id="mainWhite"><div id="content90">
<div class="whiteSpace25"></div>
<div id="content">
<h2 class="text-center font45 sourceBlack">You have questions,<br>we have answers</h2>
<a id="collapsebtn1" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1"><span class="float-left">Who is SiteLock?</span> <span class="float-right"><i id="collapseone" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse1">
<br>
<p class="linkblue font18">SiteLock is the <a href="about">global leader in website security</a>. We founded the company in 2008 with a passion to protect every website on the internet. Fast forward to the present, and we now protect over 12 million websites of all sizes around the world.</p>
</div>

<a id="collapsebtn2" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2"><span class="float-left">What is website security?</span> <span class="float-right"><i id="collapsetwo" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse2">
<br>
<p class="linkblue font18">Comprehensive website security protects your website from malicious cyber threats. This includes protection of your site code and web applications. Depending on your website security package, you’ll receive daily website scans, automated malware removal and vulnerability patching, and a web application firewall to block harmful traffic from entering your site.</p>
</div>

<a id="collapsebtn3" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3"><span class="float-left">Doesn’t my hosting provider protect my website?</span> <span class="float-right"><i id="collapsethree" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse3">
<br>
<p class="linkblue font18">It’s a common <a href="https://www.sitelock.com/blog/2017/03/do-i-need-sitelock/">misconception that hosting providers protect each website they host</a>. The reality is, your web host only protects the server your website is hosted on, not the website itself. Think of it like securing an apartment building. Property management takes responsibility for securing the building, but each tenant must lock the door to their own apartment.</p>
</div>

<a id="collapsebtn4" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse4" role="button" aria-expanded="false" aria-controls="collapse4"><span class="float-left">Why do I need website security?</span> <span class="float-right"><i id="collapsefour" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse4">
<br>
<p class="linkblue font18">Today, websites are targeted a whopping 50 times per day, per website on average. A website compromise can be a heartbreaking and costly experience—potentially resulting in damage to your site visitors, revenue, and reputation. To protect your site and visitors, website security is essential. And, it’s affordable and easy to get started! <a href="pricing">Click here to protect your site today</a>.</p>
</div>

<a id="collapsebtn5" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse5" role="button" aria-expanded="false" aria-controls="collapse5"><span class="float-left">Doesn’t my SSL Certificate protect my website?</span> <span class="float-right"><i id="collapsefive" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse5">
<br>
<p class="linkblue font18">Your SSL certificate protects data <i>in transit</i>, not data <i>stored</i> on your website. To fully protect your website, additional security is highly recommended.</p>
</div>

<a id="collapsebtn6" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse6" role="button" aria-expanded="false" aria-controls="collapse6"><span class="float-left">Do SiteLock services work will all hosting providers?</span> <span class="float-right"><i id="collapsesix" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse6">
<br>
<p class="linkblue font18">Yep! SiteLock works with all website hosting providers.</p>
</div>

<a id="collapsebtn7" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse7" role="button" aria-expanded="false" aria-controls="collapse7"><span class="float-left">Does SiteLock work with all website platforms?</span> <span class="float-right"><i id="collapseseven" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse7">
<br>
<p class="linkblue font18">We sure do! SiteLock is compatible with all open source website platforms, such as WordPress, Joomla! and Drupal. We also work with custom coded websites, support many ecommerce platforms, and most WISYG site builders.</p>
</div>

<a id="collapsebtn8" class="btn-c btn-collapse" data-toggle="collapse" href="#collapse8" role="button" aria-expanded="false" aria-controls="collapse8"><span class="float-left">How can I get started?</span> <span class="float-right"><i id="collapseeight" class="far fa-chevron-circle-down"></i></span></a><br>
<div class="collapse" id="collapse8">
<br>
<p class="linkblue font18">It’s simple! Find the best website security solution for your website by <a href="products">clicking here</a>. Or, view pricing and plan <a href="pricing">options here.</a></p>
</div></div>

</div>
<div class="whiteSpace50"></div>
</div>
<div id="mainBody">
<div class="whiteSpace50"></div>
<h2 class="text-center font45">Want to do more research before making a decision?</h2>
<div class="row">
<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/siteSecureCloud.svg" alt="Scanning Cloud"><h4 class="sourceLight">Discover the right<br>solution for your needs</h4><br><a class="btn btn-ghost-blue" href="solutions">SiteLock Solutions</a></div><br></div>
</div>

<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/scanningCloud.svg" alt="Scanning Cloud"><h4 class="sourceLight">Learn more about your<br>new security products</h4><br><a class="btn btn-ghost-blue" href="products">SiteLock Products</a></div><br></div>
</div>

<div class="col-md-4 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/scanningWAF.svg" alt="Scanning Cloud"><h4 class="sourceLight">Find the security bundle<br>that’s right for your site</h4><br><a class="btn btn-ghost-blue" href="pricing">SiteLock Pricing</a></div><br></div>
</div>
</div>

<div class="whiteSpace50"></div>
</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>