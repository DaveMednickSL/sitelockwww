<?php
//DEFINITIONS
$title = "DDOS | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "DDoS Protection,DDoS Attack Protection,DDoS Website,DDoS ProtectIon Solutions,What is a DDoS Attack,Prevent DDoS Attacks,Stop DDoS Attack,Website Attack,DDoS Prevention";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div class="headerBottomGrey" id="mainHeader"><div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-6 my-auto">
<h1 class="headTxtDrop">Advanced <span class="sourceBlack">DDoS<br>Protection</span></h1>
<h3>Block Compromising Threats</h3>
<a class="btn btn-blue" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Protect Your Website Now</a>
</div>
<div class="col-lg-6">
<img class="headerimg headImgDrop" src="/img/LaptopSiteLockTrueshield.png" alt="Laptop SiteLock Dashboard">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<div id="mainTerms">

  <div class="whiteSpace50"></div>

  <div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2>DDoS protection <span class="sourceBlack">at-a-glance</span></h2>
   Experience constant website uptime with <strong>state-of-the-art DDoS protection</strong>. Today, websites are attacked approximately <u>50 times per day</u>, per website. With advanced security protecting your site—you can block targeted DDoS attacks from disrupting your business and visitor experience.
   <br><br><a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Start Blocking Cyberattacks</a></div></div>
   <div class="col-lg-5 footMobileHide"><img class="splitImg" src="/img/ddos/ddosRocket.svg" alt="DDOS Rocket" style="max-height: 250px;"></div>
  </div>
</div>

  <div class="whiteSpace100"></div>
  
<div class="row">
  <div class="col-lg-6"></div>
  <div class="col-lg-6"><img class="img90 ddosAdjust" src="/img/ddos/ddosMap.png" alt="DDOS Map"></div>
</div>
  <div id="mainGrey"><div id="content">
  <div class="whiteSpace25"></div>
  <h2><span class="sourceRegular">What is a</span> <span class="sourceBlack">DDoS attack?</span></h2>
  <div class="row">
  <div class="col-md-6">
  DDoS, short for Distributed Denial of Service, is an attempt to make a website unavailable by overwhelming it with “fake” requests and traffic from hundreds or thousands of sources. DDoS attacks are executed when multiple computers on different networks, called a botnet, send large amounts of requests to your website all at once. This takes an immense toll on a website’s resources and processing capabilities—causing websites to slow down or crash.
  </div>
  <div class="col-md-6">
  <p class="mx-auto">
  <ul>
    <li>The longest DDoS attack in recent years lasted 297 hours—more than 12 days.</li>
    <li>The real-world cost of an unmitigated attack is $40,000 per hour when loss of customer trust, data theft, intellectual property loss, and more are factored into the equation.</li>
  </ul>
  </p>
  </div>
  </div>
  <div class="whiteSpace25"></div>
  </div></div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-md-5"><img class="splitImg" src="img/ddos/ddos.svg"></div>
<div class="col-md-7 my-auto">
<h2><span class="sourceBlack">What is</span><br>DDoS Protection?</h2>
<p>SiteLock provides sophisticated web application, infrastructure, and DNS protection—all vital components for comprehensive DDoS protection. With this innovative security, you’ll have access to advanced visitor identification while never having to worry about site downtime due to malicious traffic.</p>
<a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm"s>Get DDoS Protection</a>
</div>
</div>

<div class="whiteSpace100"></div>

<ul class="row">
<div class="col-lg-7 linkgrey ulspeclist my-auto">
<h2>SiteLock <span class="sourceBlack">DDoS Protection</span></h2>
<strong>Protect your website with sophisticated web application protection, infrastructure protection, and DNS protection.</strong>
<ul>
<li>Blocks network DDoS attacks up to 16bps</li>
<li>Blocks application level DDoS attacks</li>
<li>Automatically detects DDoS attempts and deploys protections accordingly</li>
<li>Over one tbps network capacity</li>
<li>Detailed attack reports</li>
<li>Extended Validation SSL support</li>
</ul>
</div>
<div class="col-lg-5 iconFirst"><img class="splitImg" src="img/shieldddos.svg"></div>
</div>

<div class="whiteSpace100"></div>
</div>

<div id="mainBlue"><div class="text-center" id="mainBody">
<h2 class="text-center sourceBlack font55">Get DDoS protection now</h2>
<p class="text-center sourceRegular font25">Get in touch with our website secruity specialists today to start<br>building your comprehensive anti-DDoS security solution.</p>
<a class="btn btn-red" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Get Started</a>
</div></div>

<?php 
$benefitsTitle = '<h2 class="text-center font55">DDoS protection <span class="sourceBlack">benefits</span></h2>'; 
$icon1= 'fa-umbrella'; $title1 = 'Confidence in security'; $content1 = 'Keep your visitors safe by defending against the most sophisticated DDoS attacks with 99.99 percent accuracy.';
$icon2 = 'fa-gem'; $title2 = 'Constant uptime'; $content2 = 'Ensure zero disruption to your site or user experience in the event of a DDoS attack. DDoS attacks are notorious for taking sites offline for hours at a time—long enough to detract a customer or visitor.';
$icon3 = 'fa-chart-line'; $title3 = 'Cost savings'; $content3 = 'Not only can preventing a DDoS attack save you money, but SiteLock cloud-based DDoS protection eliminates the high costs that come with additional servers and load balancing infrastructure.';
$icon4 = 'fa-rocket'; $title4 = 'Advanced visitor<br>identification'; $content4 = 'Differentiate human traffic from bot traffic with advanced visitor identification. You can customize your settings and block malicious traffic from entering your site.';
$icon5 = 'fa-user-graduate'; $title5 = 'Stay informed'; $content5 = 'Stay updated on the security of your website by viewing your DDoS protection results at any time from your SiteLock Dashboard.';
$icon6 = 'fa-magic'; $title6 = 'Quick and easy setup'; $content6 = 'Setting up your DDoS protection is easy, and the SiteLock team of experts are available to help 24/7/365.';
$benefitsBTN = 'Request More Information';
?>

<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace100"></div>
<?php  echo $benefitsTitle;?>

<div class="row">
<div class="col-md-4">
<div id="benefitBox"><div id="benefitBoxContent">
<div id="benefitCircleBlue"><i class="fal <?php echo $icon1; ?> fa-3x"></i></div>
<h3 class="sourceBlack font25"><?php echo $title1; ?></h3>
<p><?php echo $content1; ?></p>
</div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleRed"><i class="fal <?php echo $icon2; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title2; ?></h3>
  <p><?php echo $content2; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleBlue"><i class="fal <?php echo $icon3; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title3; ?></h3>
  <p><?php echo $content3; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleRed"><i class="fal <?php echo $icon4; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title4; ?></h3>
  <p><?php echo $content4; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleBlue"><i class="fal <?php echo $icon5; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title5; ?></h3>
  <p><?php echo $content5; ?></p>
  </div></div>
</div>

<div class="col-md-4">
  <div id="benefitBox"><div id="benefitBoxContent">
  <div id="benefitCircleRed"><i class="fal <?php echo $icon6; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title6; ?></h3>
  <p><?php echo $content6; ?></p>
  </div></div>
</div>


</div>
<br><br>
<p class="text-center"><a class="btn btn-blue" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm"><?php echo $benefitsBTN;?></a></p>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainRed"><div class="text-center" id="mainBody">
<h2 class="text-center sourceBlack font55">Protect your website<br>from more than just DDoS</h2>
<p class="text-center sourceRegular font25">The SiteLock web application firewall (WAF) protects your website from DDoS attacks,<br>bad bots, and other malicious threats.</p>
<a class="btn btn-ghost-white" href="#HighBarrierForm" data-toggle="modal" data-target="#HighBarrierForm">Get Started</a>
</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "844.809.5476";
$hb_btn = 'Contact Us';
include 'includes/forms/channel.html';
?>

</body>
</html>
