<?php
//DEFINITIONS
$title = "SiteLock Website Security Pricing | Peace of Mind for Dollars a Day";
$description = "Protect your website with the only fully automated security solutions for less than dollars per day. Check out our award-winning website security today.";
$canonical ="bol-user-selects";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "(855) 378-6200";
?>
    <!DOCTYPE html5>
    <html lang="en">
    <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?php echo $title;?></title>
  <link rel="canonical" href="https://www.sitelock.com/<?php echo $canonical;?>" />
  <meta name="description" content="<?php echo $description;?>">
  <meta name="keywords" content="<?php echo $keywords;?>" />
  <link rel="icon" type="image/ico" href="favi.ico">
  <!-- Bootstrap -->
  <link href="landers/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="landers/css/bootstrap.min.css" rel="stylesheet">
  <link href="landers/css/style.css?v=1.0.5" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Start Visual Website Optimizer Asynchronous Code -->
  <script type='text/javascript'>
  var _vwo_code=(function(){
  var account_id=321148,
  settings_tolerance=2000,
  library_tolerance=2500,
  use_existing_jquery=false,
  /* DO NOT EDIT BELOW THIS LINE */
  f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
  </script>
  <!-- End Visual Website Optimizer Asynchronous Code -->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
</head>

        <body id="new-bol">
            <?php include 'includes/assets/ALPHA.php';?>
            <div id="menu"><div id="menu_inner">
            <div class="container">
<div class="row text-center">
<div class="col-md-6 text-left"><a href="https://www.sitelock.com/"><img class="logoNav" src="img/logos/SiteLock_red.svg" alt="sitelock logo"></a></div>
<div class="col-md-6 text-right"><h3 class="sourceBlack"><?php echo $pagephone;?></h3></div>
</div>
</div>
</div></div>

                    <!-- Top header section -->
                    <section class="p-5 image-banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <h1 class="sourceBlack blue">Website Security Designed for You</h1>
                                    <p class="grey font18">Select the only website security that automatically protects your website, reputation and visitors.</p>
                                </div>
                            </div>
<br>
 
                            <ul id="tab" class="nav justify-content-center btn-pref border-radius" role="tablist">
                                <li class="nav-item">
                                <a class="border-radius upper-font sourceBlack" data-toggle="tab" href="#tab1">Personal Sites</a>
                                </li>
                                <li class="nav-item">
                                <a data-toggle="tab" href="#tab2" class="active border-radius upper-font sourceBlack ">Business Sites</a>
                                </li>

                            </ul>


                        </div>
                    </section>






                    <!--Pick a Plan Step-->
                    <!--<section class="grey-bg-lite price-tp">
                        <div class="container text-center">

                            <div class="row">

                                <div class="col-sm-4 arrow_box padding10">
                                    <span class="blue"><strong>Step 1: Pick a Plan</strong></span>
                                </div>
                                <div class="col-sm-4 padding10">
                                    <span><strong>Step 2: Create an Account</strong></span>
                                </div>
                                <div class="col-sm-4 padding10">
                                    <span><strong>Step 3: Purchase</strong></span>
                                </div>

                            </div>
                        </div>
                    </section>-->




<section class="p-5">
<div class="container">
<div class="tab-content">
<div id="tab1" class="container tab-pane">
                                <div class="row">

                                        <div class="col-md-4 offset-md-2 text-center col-sm-12">
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h4 class="sourceRegular">Designed for <span class="sourceBlack">Blogs</span></h4>
                                                        </div>
                                                    </div>

                                                    <div id="priceBoxContent"><br>
                                                    <h5 class="sourceRegular">Secure<span class="sourceBlack">Basic</span></h5>
                                                    <p class="font14">
                                                    Designed for bloggers or simple sites owners sharing their passion with the world. Sign up today to automatically protect against future attacks, increase site speed and enhance visitor trust.

                                                        </p>
                                                        <h1 class="sourceBlack">$19.99<span class="sourceRegular font18">/month</span></h1> 

                                                       
                                                        <a href="/checkout?planID=1050" class="btn btn-red font-weight-bold width-auto">ADD TO CART</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                        </div>

                                        <div class="col-md-4 text-center col-sm-12">
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h4 class="sourceRegular">For Advanced  <span class="sourceBlack">Personal Sites</span></h4>
                                                        </div>
                                                    </div>

                                                    <div id="priceBoxContent"><br>
                                                    <h5 class="sourceRegular">Secure<span class="sourceBlack">Starter</span></h5>
                                                    <p class="font14">
                                                    Perfect for websites designed to set appointments, collect info or sell items. Sign up to get all the benefits of SecureBasic plus repair existing infections, support SSL and block DDoS.

                                                        </p>
                                                        <h1 class="sourceBlack">$29.99<span class="sourceRegular font18">/month</span></h1>

                                                       
                                                        <a href="/checkout?planID=1007" class="btn btn-blue font-weight-bold width-auto">ADD TO CART</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                        </div>

                                   
<div class="clearfix"></div>
 <!-- show-hide start -->
 <br>
                                    <div class="collapse" id="collapseExample1">
									<div class="row">
                                     

                                        <div class="col-md-4 offset-md-2 col-sm-12">
                                        <ul>
                                                <li><strong>Basic Benefits</strong>
                                                <ul>
                                                 
                                                            <li><i class="fa fa-check"></i> Worry-free automated protection. </li>
                                                            <li><i class="fa fa-check"></i> 24/7/365 phone, email and chat support. </li>
                                                            <li><i class="fa fa-check"></i> Improve visitor experience with up to a 50% increase in site speed. </li>
                                                            <li><i class="fa fa-check"></i> Prevent loss of website traffic by avoiding suspension, blacklisting and traffic theft. </li>
                                                            <li><i class="fa fa-check"></i> Establish credibility by displaying a SiteLock Trust Seal that tells visitors your site is safe and legit. </li>
                                                            <li><i class="fa fa-check"></i> Avoid reputation damaging site vandalism, redirects, data theft and more. </li>
                                                           
                                                        </ul></li>
                                                        </ul>
                                                  

                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                           
                                                <ul>
                                                <li><strong>Advanced Benefits</strong>
                                                    <ul>
                                                        <li><i class="fa fa-check"></i> Worry-free automated protection.</li>
                                                        <li><i class="fa fa-check"></i> 24/7/365 phone, email and chat support.</li>
                                                        <li><i class="fa fa-check"></i> Improve visitor experience with up to a 50% increase in site speed.</li>
                                                        <li><i class="fa fa-check"></i> Prevent loss of website traffic by avoiding suspension, blacklisting and traffic theft.</li>
                                                        <li><i class="fa fa-check"></i> Establish credibility by displaying a SiteLock Trust Seal that tells visitors your site is safe and legit.</li>
                                                        <li><i class="fa fa-check"></i> Avoid reputation damaging site vandalism, redirects, data theft and more. </li>
                                                        <li><i class="fa fa-check"></i> Instantly fix existing malware and vulnerabilities.</li>
                                                        <li><i class="fa fa-check"></i> Removal from blacklists and host suspension.</li>
                                                        

                                                    </ul>
                                                </li>
                                                        </ul>
                                         

                                        </div>
									</div>
                                    </div>
                                    
                                  
                                    <a class="collapsed text-center width-auto" data-toggle="collapse" href="#collapseExample1">
                                        <!--You can put any valid html inside these!-->
                                        <span class="if-collapsed sourceBlack"><i class="fa fa-plus"></i>See All Benefits</span>
                                        <span class="if-not-collapsed sourceBlack"><i class="fa fa-minus"></i> Hide Benefits</span>
                                    </a>
                                  





                                        </div>
                                        </div> 




 <div id="tab2" class="container tab-pane active">
 <div class="row">
    
                                        <div class="col-md-4 text-center col-sm-12">
                                        <div id="priceHatWhite">.</div>
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h4 class="sourceRegular">Best for <span class="sourceBlack">Informational Sites</span></h4>
                                                        </div>
                                                    </div>

                                                    <div id="priceBoxContent"><br>
                                                    <h5 class="sourceRegular">Secure<span class="sourceBlack">Starter</span></h5>
                                                       

                                                        <p class="font14">
                                                        Perfect for websites designed to educate or inform visitors about the services or products you offer.  Sign up today to repair existing infections, automatically protect against future attacks, increase site speed and enhance visitor trust.<br> 

                                                        </p>
                                                        <h1 class="sourceBlack ">$29.99<span class="sourceRegular font18">/month</span></h1>
                                                        <a href="/checkout?planID=1007" class="btn btn-blue font-weight-bold width-auto">ADD TO CART</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                        </div>
 
                                        <div class="col-md-4 text-center col-sm-12">
                                        <div class="sourceRegular" id="priceHatRed">Most Popular</div>
                                                <div id="priceBox">
                                                
                                                    <div id="priceGreyHead">
                                                    
                                                        <div id="priceBoxContent" style="width:100% !important;">
                                                            <h4 class="sourceRegular">Designed for Lead <span class="sourceBlack">Generation</span></h4>
                                                        </div>
                                                    </div>

                                                    <div id="priceBoxContent">
                                                    <br>
                                                       
                                                    <h5 class="sourceRegular">Secure<span class="sourceBlack">Speed</span></h5>
                                                        <p class="font14">
                                                      
                                                        Designed for websites used to drive subscribers, calls, form completions and other conversions. Sign up today to get all the benefits of SecureStarter plus block botted form completions, protect SEO rankings and safeguard ad accounts.

                                                        </p>
                                                        <h1 class="sourceBlack ">$49.99<span class="sourceRegular font18">/month</span></h1>
                                                        <a href="/checkout?planID=1008" class="btn btn-red font-weight-bold width-auto upper-font">Secure My Site</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                        </div>

                                        <div class="col-md-4 text-center col-sm-12">
                                        <div id="priceHatWhite">.</div>
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h4 class="sourceRegular">Perfect for <span class="sourceBlack">Ecommerce</span></h4>
                                                        </div>
                                                    </div>

                                                    <div id="priceBoxContent">
                                                    <br>
                                                        <h5 class="sourceRegular">Secure<span class="sourceBlack">Site</span></h5>

                                                        <p class="font14">
                                                        Best for ecommerce websites focused on collecting payment information online. Sign up today to get all the benefits of SecureSpeed plus safeguard credit card data, automatically patch ecommerce plugins and simplify PCI compliance.

                                                        </p>
                                                        <h1 class="sourceBlack ">$69.99<span class="sourceRegular font18">/month</span></h1>
                                                        <a href="/checkout?planID=1009" class="btn btn-blue font-weight-bold width-auto">ADD TO CART</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                        </div>

                                   
<div class="clearfix"></div>
 <!-- show-hide start -->
 <br>
                                    <div class="collapse" id="collapseExample1">
									<div class="row">
                                        <div class="col-md-4 col-sm-12">
                                          
                                                <ul>
                                                <li><strong>SecureStarter</strong>
                                                    <ul>
                                                        <li><i class="fa fa-check"></i> Worry-free automated protection.</li>
                                                        <li><i class="fa fa-check"></i> 24/7/365 phone, email and chat support.</li>
                                                        <li><i class="fa fa-check"></i> Improve visitor experience with up to a 50% increase in site speed.</li>
                                                        <li><i class="fa fa-check"></i> Prevent loss of website traffic by avoiding suspension, blacklisting and traffic theft.</li>
                                                        <li><i class="fa fa-check"></i> Establish credibility by displaying a SiteLock Trust Seal that tells visitors your site is safe and legit.</li>
                                                        <li><i class="fa fa-check"></i> Avoid reputation damaging site vandalism, redirects, data theft and more. </li>
                                                        <li><i class="fa fa-check"></i> Instantly fix existing malware and vulnerabilities.</li>
                                                        <li><i class="fa fa-check"></i> Removal from blacklists and host suspension.</li>
                                                        

                                                    </ul>
                                                </li>
                                                </ul>
                                          

                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                           
                                        <ul>
                                                <li><strong>SecureSpeed</strong>
                                                    <ul>
 
                                                        <li><i class="fa fa-check"></i> Worry-free automated protection. </li>
                                                        <li><i class="fa fa-check"></i> 24/7/365 phone, email and chat support. </li>
                                                        <li><i class="fa fa-check"></i> Improve visitor experience with up to a 50% increase in site speed. </li>
                                                        <li><i class="fa fa-check"></i> Prevent loss of website traffic by avoiding suspension, blacklisting and traffic theft. </li>
                                                        <li><i class="fa fa-check"></i> Establish credibility by displaying a SiteLock Trust Seal that tells visitors your site is safe and legit. </li>
                                                        <li><i class="fa fa-check"></i> Avoid reputation damaging site vandalism, redirects, data theft and more. </li>
                                                        <li><i class="fa fa-check"></i> Instantly fix existing malware and vulnerabilities.</li>
                                                        <li><i class="fa fa-check"></i> Removal from blacklists and host suspension. </li>
                                                        <li><i class="fa fa-check"></i> Avoid ad networks bans with automated malicious link detection. </li>
                                                        <li><i class="fa fa-check"></i> Secure data by blocking top 10 site vulnerabilities. </li>
                                                        <li><i class="fa fa-check"></i> Save time and improve reporting by eliminating botted form completions. </li>
                                                      
                                                    </ul>
                                                </li>
                                                </ul>

                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                           
                                        <ul>
                                                <li><strong>SecureSite</strong>
                                                    <ul>
                                                        <li><i class="fa fa-check"></i> Worry-free automated protection.</li>
                                                        <li><i class="fa fa-check"></i> 24/7/365 phone, email and chat support. </li>
                                                        <li><i class="fa fa-check"></i> Improve visitor experience with up to a 50% increase in site speed. </li>
                                                        <li><i class="fa fa-check"></i> Prevent loss of website traffic by avoiding suspension, blacklisting and traffic theft. </li>
                                                        <li><i class="fa fa-check"></i> Establish credibility by displaying a SiteLock Trust Seal that tells visitors your site is safe and legit. </li>
                                                        <li><i class="fa fa-check"></i> Avoid reputation damaging site vandalism, redirects, data theft and more. </li>
                                                        <li><i class="fa fa-check"></i> Instantly fix existing malware and vulnerabilities. </li>
                                                        <li><i class="fa fa-check"></i> Removal from blacklists and host suspension. </li>
                                                        <li><i class="fa fa-check"></i> Avoid ad networks bans with automated malicious link detection. </li>
                                                        <li><i class="fa fa-check"></i> Secure data by blocking top 10 site vulnerabilities. </li>
                                                        <li><i class="fa fa-check"></i> Save time and improve reporting by eliminating botted form completions. </li>
                                                        <li><i class="fa fa-check"></i> Safeguard credit cardholder data.</li>
                                                        <li><i class="fa fa-check"></i> 80% reduction in time spent on PCI questionnaire. </li>
                                                        <li><i class="fa fa-check"></i> Protect shoppers with automated Ecommerce plugin monitoring and patching. </li>
                                                        <li><i class="fa fa-check"></i> Simplify PCI compliance with custom policy and procedure templates. </li>

                                                    </ul>
                                                </li>
                                                </ul>
                                         

                                        </div>



									</div>
                                    </div>
                                    
                                  
                                    <a class="collapsed text-center width-auto" data-toggle="collapse" href="#collapseExample1">
                                        <!--You can put any valid html inside these!-->
                                        <span class="if-collapsed sourceBlack"><i class="fa fa-plus"></i>See All Benefits</span>
                                        <span class="if-not-collapsed sourceBlack"><i class="fa fa-minus"></i> Hide Benefits</span>
                                    </a>

                                        </div>
</div>



</div>
</div>
</section>


                    
 <!--trustpilot-->
 <section id="mainGrey" class="trustpilot p-3">
            <div class="container text-center">
            <h2 class="text-center grey sourceRegular padding-around-20">Check Out SiteLock Reviews On TrustPilot</h2>
                    <div class="row">
            
            <br />
            <div class="col-sm-12">
                <!-- TrustBox widget - Carousel -->
                <div class="trustpilot-widget" data-locale="en-US" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="5415bf7c00006400057a5178"
                    data-style-height="130px" data-style-width="100%" data-theme="light" data-stars="4,5" data-schema-type="Organization">
                    <a href="https://www.trustpilot.com/review/sitelock.com" target="_blank">Trustpilot</a>
                </div>
                <!-- End TrustBox widget -->
            </div>
            </div></div>
        </section>
                    <!--Your plan:-->
                    <section class="p-3 white-bg">
                        <div class="container">

                            <div class="row">

                                <div class="col-md-6 col-sm-12 white-bg padding-around-25">
                                <h2 class="text-left blue sourceBlack">Not sure what to choose?</h2>
                            <p>We're here to help! Just answer the following questions and we'll have a plan picked out and tailored to your website needs.</p>
                                    <p>Is your site currently infected?</p>
                                    <div id="myplan">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-plan">
                                                <input type="radio" name="options" value="no"> No
                                            </label>
                                            <label class="btn btn-plan active">
                                                <input type="radio" name="options" value="yes" checked> Yes
                                            </label>
                                        </div>

                                        <br>
                                        <p>Which term best describes you?</p>

                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-plan">
                                                <input type="radio" name="options1" value="blogger"> <i class='fas fa-pencil-alt'></i><br>Blogger
                                            </label>
                                            <label class="btn btn-plan active">
                                                <input type="radio" name="options1" value="business" checked> <i class='fas fa-shopping-basket'></i><br>Business
                                            </label>
                                            <label class="btn btn-plan">
                                                <input type="radio" name="options1" value="developer"> <i class='fas fa-code'></i><br>Web Dev
                                            </label>
                                            <label class="btn btn-plan">
                                                <input type="radio" name="options1" value="nonprofit"> <i class='fas fa-hand-holding-heart'></i></i>
                                                <br>Nonprofit
                                            </label>
                                        </div>

                                        <br>
                                        <p>What data do you collect on your site?</p>

                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-plan">
                                                <input type="radio" name="options2" value="none"><i class='fas fa-comment-slash'></i><br>None
                                            </label>
                                            <label class="btn btn-plan active">
                                                <input type="radio" name="options2" value="contact-info" checked><i class="fa fa-address-card" aria-hidden="true"></i><br>Contact info
                                            </label>
                                            <label class="btn btn-plan">
                                                <input type="radio" name="options2" value="payment-info"><i class="far fa-credit-card"></i><br>Payment info
                                            </label>
                                        </div>

                                        <br>
                                        <p>What type of SSL does your site use?</p>

                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-plan">
                                                <input type="radio" name="options3" value="none-ssl">None
                                            </label>
                                            <label class="btn btn-plan active">
                                                <input type="radio" name="options3" value="standard" checked> Standard
                                            </label>
                                            <label class="btn btn-plan">
                                                <input type="radio" name="options3" value="evssl"> EV SSL
                                            </label>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12 white-bg padding-around-25">

                                    <h3 class="sourceBlack blue">Your plan:</h3>

                                    <!-- secure-basic -->
                                    <div id="secure-basic">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h3 class="sourceRegular">Secure<span class="sourceBlack">Basic</span></h3>
                                                        </div>
                                                    </div>

                                                    <div class="text-left" id="priceBoxContent">

                                                        <h1 class="sourceLight ">$19.99</h1> Per Month/Domain

                                                        <p class="text-left font14">
                                                            <br> Designed for bloggers or sites focused on sharing their passion with the world.

                                                        </p>
                                                        <a href="/checkout?planID=1050" class="btn btn-red font-weight-bold width-auto">Sign Up Now</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <!-- <div class="col-sm-6 ">
                                                <h4 class="sourceBlack black">Benefits:</h4>

                                                <div class="font14">
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Peace of mind from ongoing automated website protection</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Improve visitor experience by increasing site speed by up to 50%. </div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Prevent lost traffic by avoiding suspensions, blacklisting and traffic theft. </div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Boost credibility with a SiteLock trust seal</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Protect your reputation against vandalism, data breaches, visitor infection, and more</div>
                                                    </div>

                                                </div>

                                            </div> -->

                                        </div>
                                    </div>

                                    <!-- secure-starter-->
                                    <div id="secure-starter">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h3 class="sourceRegular">Secure<span class="sourceBlack">Starter</span></h3>
                                                        </div>
                                                    </div>

                                                    <div class="text-left" id="priceBoxContent">

                                                        <h1 class="sourceLight ">$29.99</h1> Per Month/Domain

                                                        <p class="text-left font14">
                                                            <br>Perfect for websites designed to showcase their company, products or service

                                                        </p>
                                                        <a href="/checkout?planID=1007" class="btn btn-red font-weight-bold width-auto">Sign Up Now</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <!-- <div class="col-sm-6 ">
                                                <h4 class="sourceBlack black">Benefits:</h4>

                                                <div class="font14">
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Peace of mind from ongoing automated website protection</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Expert repair of current malware infections and vulnerabilities</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Improve visitor experience by increasing site speed by up to 50%. </div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Prevent loss of website traffic by avoiding suspension, blacklisting and traffic thef</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Establish credibility by telling visitors your site is safe and legit</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Avoid reputation damaging site vandalism, redirects, data theft and more</div>
                                                    </div>

                                                </div>

                                            </div> -->

                                        </div>
                                    </div>
                                    <!-- secure-speed-->
                                    <div id="secure-speed">
                                        <div class="row">
                                            <div class="col-sm-8">
                                            <div class="sourceRegular" id="priceHatRed">Most Popular</div>
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h3 class="sourceRegular">Secure<span class="sourceBlack">Speed</span></h3>
                                                        </div>
                                                    </div>

                                                    <div class="text-left" id="priceBoxContent">

                                                        <h1 class="sourceLight ">$49.99</h1> Per Month/Domain

                                                        <p class="text-left font14">
                                                            <br>Designed for websites used to drive subscribers, calls or leads.

                                                        </p>
                                                        <a href="/checkout?planID=1008" class="btn btn-red font-weight-bold width-auto">Sign Up Now</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <!-- <div class="col-sm-6 ">
                                                <h4 class="sourceBlack black">Benefits:</h4>

                                                <div class="font14">
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Peace of mind from ongoing automated website protection</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Expert repair of current malware infections and vulnerabilities</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Improve visitor experience by increasing site speed by up to 50%. </div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Prevent lost traffic by avoiding suspensions, blacklisting and traffic theft. </div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Establish credibility and boost conversions by telling visitors your site is safe and legit</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Avoid reputation damaging site vandalism, data theft and more</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Prevent infections that result in bans from major ad networks</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Secure data by blocking top 10 site vulnerabilities</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Save time and improve reporting by eliminating botted form completions</div>
                                                    </div>
                                                </div>

                                            </div> -->

                                        </div>
                                    </div>
                                    <!-- secure-site-->
                                    <div id="secure-site">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h3 class="sourceRegular">Secure<span class="sourceBlack">Site</span></h3>
                                                        </div>
                                                    </div>

                                                    <div class="text-left" id="priceBoxContent">

                                                        <h1 class="sourceLight ">$69.99</h1> Per Month/Domain

                                                        <p class="text-left font14">
                                                            <br>Best for business websites built to drive online purchases or collect sensitive information.

                                                        </p>
                                                        <a href="/checkout?planID=1009" class="btn btn-red font-weight-bold width-auto">Sign Up Now</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <!-- <div class="col-sm-6 ">
                                                <h4 class="sourceBlack black">Benefits:</h4>

                                                <div class="font14">
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Peace of mind from ongoing automated website protection</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Expert repair of current malware infections and vulnerabilities </div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Improve visitor experience by increasing site speed by up to 50%. </div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Prevent lost traffic by avoiding suspensions, blacklisting and traffic theft.</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Establish credibility and boost conversions by telling visitors your site is safe and legit</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Avoid reputation damaging site vandalism, data theft and more</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Prevent infections that result in bans from major ad networks</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Secure data by blocking top 10 site vulnerabilities</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Save time and improve reporting by eliminating botted form completions</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Safeguard cardholder data</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Reduce time spent on PCI questionnaire by 80%</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Protect shoppers by automatically patching Ecommerce plugins </div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Simplify PCI compliance with custom policy and procedure templates</div>
                                                    </div>

                                                </div>

                                            </div> -->

                                        </div>
                                    </div>
                                    <!-- secure-custom-->
                                    <div id="secure-custom">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h3 class="sourceRegular">Secure<span class="sourceBlack">Custom</span></h3>
                                                        </div>
                                                    </div>

                                                    <div class="text-left" id="priceBoxContent">

                                                        <!-- <h1 class="sourceLight ">$69.99</h1> Per Month/Domain -->

                                                        <p class="text-left font14">
                                                            <br>Based on your selections it is recommend you talk with one of our security experts to build a package that best suits the needs of your website. Please fill out the following form oand a member of our Sales team will be in contact with you shortly.

                                                        </p>
                                                        <a href="#HighBarrierForm" class="btn btn-red font-weight-bold width-auto" data-toggle="modal" data-target="#HighBarrierForm">Request a Quote</a>
                                                        <br>
                                                        <a href="tel:8553786200" class="btn btn-ghost-grey width-auto font-weight-bold">(855) 378-6200</a>
                                                        <a href="http://alivech.at/2akc64" target="_blank" class="btn btn-ghost-grey width-auto font-weight-bold">Chat Now</a><br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <!-- <div class="col-sm-6 ">
                                                <h4 class="sourceBlack black">Benefits:</h4>

                                                <div class="font14">
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Expert repair of current malware infections</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Peace of mind from ongoing protection</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Block theft of traffic and bandwidth.</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Boost credibility and conversions with a SiteLock trust seal</div>
                                                    </div>

                                                </div>

                                            </div> -->

                                        </div>
                                    </div>
                                    <!-- secure-devoloper-->
                                    <div id="secure-devoloper">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h3 class="sourceRegular">Secure<span class="sourceBlack">Developer</span></h3>
                                                        </div>
                                                    </div>

                                                    <div class="text-left" id="priceBoxContent">

                                                        <!-- <h1 class="sourceLight ">$69.99</h1> Per Month/Domain -->

                                                        <p class="text-left font14">
                                                            <br>Based on your seclections you may be a good fit for one of our reseller programs. Through these programs you are able to leverage SiteLock services to drive additional revenue for your business. Click send me more info and complete the form and we will reach out with more information shortly.

                                                        </p>
                                                        <a href="#HighBarrierForm" class="btn btn-red font-weight-bold width-auto" data-toggle="modal" data-target="#HighBarrierForm">Request a Quote</a>
                                                        <br>
                                                        <a href="tel:8553786200" class="btn btn-ghost-grey width-auto font-weight-bold">(855) 378-6200</a>
                                                        <a href="http://alivech.at/2akc64" target="_blank" class="btn btn-ghost-grey width-auto font-weight-bold">Chat Now</a><br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <!-- <div class="col-sm-6 ">
                                                <h4 class="sourceBlack black">Benefits:</h4>

                                                <div class="font14">
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Expert repair of current malware infections</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Peace of mind from ongoing protection</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Block theft of traffic and bandwidth.</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Boost credibility and conversions with a SiteLock trust seal</div>
                                                    </div>

                                                </div>

                                            </div> -->

                                        </div>
                                    </div>
                                    <!-- secure-nonprofit-->
                                    <div id="secure-nonprofit">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div id="priceBox">
                                                    <div id="priceGreyHead">
                                                        <div id="priceBoxContent">
                                                            <h3 class="sourceRegular">Secure<span class="sourceBlack">NonProfit</span></h3>
                                                        </div>
                                                    </div>

                                                    <div class="text-left" id="priceBoxContent">

                                                        <!-- <h1 class="sourceLight ">$69.99</h1> Per Month/Domain -->

                                                        <p class="text-left font14">
                                                            <br>SiteLock offers custom security packages at disconted prices for non-profit organizations. Contact us today to secure your special pricing.

                                                        </p>
                                                        <a href="#HighBarrierForm" class="btn btn-red font-weight-bold width-auto" data-toggle="modal" data-target="#HighBarrierForm">Request a Quote</a>
                                                        <br>
                                                        <a href="tel:8553786200" class="btn btn-ghost-grey width-auto font-weight-bold">(855) 378-6200</a>
                                                        <a href="http://alivech.at/2akc64" target="_blank" class="btn btn-ghost-grey width-auto font-weight-bold">Chat Now</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <!-- <div class="col-sm-6 ">
                                                <h4 class="sourceBlack black">Benefits:</h4>

                                                <div class="font14">
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Expert repair of current malware infections</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Peace of mind from ongoing protection</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Block theft of traffic and bandwidth.</div>
                                                    </div>
                                                    <div class="media">
                                                        <span class="float-left"><i class="fa fa-check"></i></span>
                                                        <div class="media-body">Boost credibility and conversions with a SiteLock trust seal</div>
                                                    </div>

                                                </div>

                                            </div> -->

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </section>

                    <!--Not sure which plan is right for you?-->
                    <section class="p-5">
                        <div class="container text-center">

                            <div class="row">

                                <div class="col-sm-6 ">
                                    <h4 class="sourceLight">Not sure which plan is right for you?</h4>

                                    <span class="padding-around-20"> <a href="#compare" class="btn btn-ghost-grey">Compare All Plans</a></span>
                                    <span> <a href="#HighBarrierForm" class="btn btn-ghost-grey" data-toggle="modal" data-target="#HighBarrierForm">Contact SiteLock</a></span>

                                </div>

                                <div class="col-sm-6">

                                    <h4 class="sourceLight">Looking to become a re-seller</h4>
                                    <span class="padding-around-20"> <a href="#HighBarrierForm" class="btn btn-ghost-grey" data-toggle="modal" data-target="#HighBarrierForm">Request More Information</a></span>
                                </div>

                            </div>
                        </div>
                    </section>

                    <!--Star rating bar-->

                    <section class="p-3" id="mainGrey">
                        <div class="container text-center">

                            <div class="row">

                                <div class="col-sm-3">
                                    <img class="img50" src="landers/img/reviews/bbbStars.png" alt="BBB">
                                </div>

                                <div class="col-sm-3">
                                    <img class="img50" src="landers/img/reviews/consumerAffairsStars.png" alt="Consumer Affairs">
                                </div>

                                <div class="col-sm-3">
                                    <img class="img50" src="landers/img/reviews/googleStars.png" alt="Google">
                                </div>

                                <div class="col-sm-3">
                                    <img class="img50" src="landers/img/reviews/trustPilotStars.png" alt="Trust Pilot">
                                </div>

                            </div>

                        </div>
                    </section>

                    <!--Every SiteLock Plan-->
                    <section class="p-5">
                        <div class="container text-center">
                            <h3 class="sourceRegular text-center font35">Every SiteLock Plan <span class="sourceBlack">Includes</span></h3>
                            <div class="row">

                                <div class="col-md-3">
                                    <img class="img50" src="img/scanningAlert.svg" alt="Scanning Alert">
                                    <strong>Industry-leading<br>anti-malware<br>technology</strong>
                                </div>

                                <div class="col-md-3">
                                    <img class="img50" src="img/trustSeal.svg" alt="Trust Seal">
                                    <strong>SiteLock Trust Seal<br>to boost visitor<br>confidence</strong>
                                </div>

                                <div class="col-md-3">
                                    <img class="img50" src="img/headset.svg" alt="Support">
                                    <strong>24/7 U.S.-based<br>customer service &<br>support</strong>
                                </div>

                                <div class="col-md-3">
                                    <img class="img50" src="img/rocket.svg" alt="Site Speed">
                                    <strong>Faster loading and<br>saved bandwidth<br>with our CDN</strong>
                                </div>

                            </div>
                        </div>
                    </section>

                    <!--Compare Plans Section-->
                    <a name="compare"></a>
                    <section class="p-5">
                        <div class="container">
                            <div class="row make-me-sticky border-bottom">
                                <div class="col-md-4 col-xs-12 col-sm-12 white-bg">

                                    <h2 style="margin-top:50px;">Compare <strong>Plans</strong></h2>
                                </div>

                                <div class="col-md-8 col-xs-12 col-sm-12">
                                    <div class="row white">
                                        <div class="col-3 my_planHeader blue-bg whitelink">
                                            <div class="my_planPrice">Secure <span class="sourceBlack">Basic<span></div>
                                            $19.99/mo<br>
											<a href="/checkout?planID=973">Add to Cart</a>
                                        </div>
                                        <div class="col-3 my_planHeader no-left-border blue-bg whitelink">
                                            <div class="my_planPrice">Secure <span class="sourceBlack">Starter<span></div>
                                            $29.99/mo<br>
											<a href="/checkout?planID=973">Add to Cart</a>
                                        </div>
                                        <div class="col-3 my_planHeader no-left-border blue-bg whitelink">
                                            <div class="my_planPrice">Secure <span class="sourceBlack">Speed<span></div>
                                            $49.99/mo<br>
											<a href="/checkout?planID=973">Add to Cart</a>
                                        </div>
                                        <div class="col-3 my_planHeader no-left-border blue-bg whitelink">
                                            <div class="my_planPrice">Secure <span class="sourceBlack">Site<span></div>
                                            $69.99/mo<br>
											<a href="/checkout?planID=973">Add to Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
				<div class="panel-group" id="accordion" >
               <div class="panel panel-default">
			   
			   
			   <div class="panel-heading linkblack"><a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                            <div class="row my_featureRow no-bottom-border no-top-border">
                                <div class="col-xs-12 col-sm-12 col-md-4 my_feature1 right-border sourceBlack">
                                <span class="white">Scanner</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8 sourceBlack">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Pro</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> Pro</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> Prem</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p> SMB Plus</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										</a>
								 </div>		
										
					<div id="collapse1" class="panel-collapse collapse in collapse show">
                      <div class="panel-body">
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Malware Scanning</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Daily</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Network Scan</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p> Daily</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Spam Scan</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Daily</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Vulnerability Scan</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Once</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Once</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Daily</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Application Scan</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Monthly</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Monthly</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> Monthly</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Monthly</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Platform Scan</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Daily</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Automated Malware Removal</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> Daily</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Constant</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Database Scanning</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> </p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Daily on Demand</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Automated Database Cleaning</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Daily on Demand</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Automated WP, Joomla & Drupal Patching</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> </p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Daily</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Ecommerce Patching</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> </p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Daily</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
			</div>
           </div>
		   
		    <div class="panel-heading linkblack"><a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature1 right-border sourceBlack">
                                            <span class="white">WAF</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8 sourceBlack">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Basic</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Pro</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> Prem</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>SMB Enterprise</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div></a>
										</div>
										
					<div id="collapse2" class="panel-collapse collapse in collapse show">
                      <div class="panel-body">
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>CDN</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p> Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Block Bad Bots</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Static Content Caching</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Minimizes code</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>CAPTCHA</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>SSL Support</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>DDoS Protection</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Dynamic Content Caching</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Custom Traffic Filtering</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>OWASP Top 10</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>DDoS Protection</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>PCI Compliant Network</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Blocks Back Door</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Custom SSL Support</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
				</div>
           </div>
		   
		   <div class="panel-heading linkblack"><a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature1 right-border sourceBlack">
                                            <span class="white">Expert Services</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8 sourceBlack">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div></a>
										</div>
					<div id="collapse3" class="panel-collapse collapse in collapse show">
                      <div class="panel-body">
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Emergency Manual Hacked Website Repair</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>One time</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Unlimited</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Unlimited</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										      <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Google Blacklist Removal</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>One time</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Unlimited</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Unlimited</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>24/7/365 Support</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p>Yes</p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
				</div>
           </div>
										<div class="panel-heading linkblack"><a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature1 right-border sourceBlack">
                                            <span class="white">PCI Compliance</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8 sourceBlack">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div></a>
										</div>
										
						<div id="collapse4" class="panel-collapse collapse in collapse show">
                      <div class="panel-body">
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Simplified PCI questionnaire </span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										   <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>PCI compliance audit</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										    <div class="row my_featureRow no-bottom-border">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Custom PCI compliance templates</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										     <div class="row my_featureRow">
                                            <div class="col-xs-12 col-sm-12 col-md-4 my_feature right-border">
                                                <span>Quarterly PCI compliance scans</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature right-border">
                                                        <p></p>
                                                    </div>
                                                    <div class="col-3 my_planFeature">
                                                        <p>Yes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										
										
			</div>
           </div>
										
										
		</div>
  </div>
                                        <div class="col col-md-8 offset-md-4" >
                                            <div class="row text-center">
                                                <div class="col-md-3 col-sm-3">
                                                    <a href="/checkout?planID=1050" class="btn btn-blue">Get Started</a>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <a href="/checkout?planID=1007" class="btn btn-blue">Get Started</a>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <a href="/checkout?planID=1008" class="btn btn-blue">Get Started</a>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                    <a href="/checkout?planID=1009" class="btn btn-blue">Get Started</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                    </section>
                    <!--Why More Website Owners Choose SiteLock-->

                    <section id="mainRed" class="no-padding">

                        <div class="container white">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12 text-center">

                                    <img class="img80" src="landers/img/man.png" alt="Support">
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12 text-left">
                                    <div class="whiteSpace50"></div>
                                    <p class="white sourceBlack font25 padding-around-10">Day or night, our friendly, 24/7 U.S.-based support team is here to help via phone, email or live chat.</p>
                                    <span class="padding-around-10"> <a href="tel:8553786200" class="btn btn-ghost-white whitelink">(855) 378-6200</a></span>
                                    <!-- <span class="padding-around-10"> <a href="#" class="btn btn-ghost-white">Email</a></span> -->
                                    <span class="padding-around-10"> <a href="http://alivech.at/2akc64" target="_blank" class="btn btn-ghost-white">Chat</a></span>
                                    <div class="whiteSpace25"></div>
                                </div>

                            </div>
                        </div>
                    </section>

                    <!--Frequently Asked Questions-->

                    <section id="questions" class="p-5">
                        <div class="container text-center">
                            <h2 class="grey sourceBlack">Frequently Asked Questions</h2>
                            <div class="row">

                                <div class="col-md-8 col-xs-12 offset-md-2 padding20">
                                    <div class="panel-group" id="accordion">

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                        <a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          <strong>Isn’t website security my host’s responsibility?</strong>
          <i class="indicator fa fa-chevron-up float-right" aria-hidden="true"></i></a>
                                    </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <p>
                                                        It’s a common misconception that hosting providers offer security for each website they host. However, your web host only protects the server your website is hosted on, not the website itself. Think of it like securing an apartment building. Property management takes responsibility for securing the building, but each tenant must lock the door to their own apartment.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                        <a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          <strong>What is the difference between Basic, Starter, Speed and Site?</strong>
          <i class="indicator fa fa-chevron-up float-right" aria-hidden="true"></i></a>
                                    </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="panel-body">

                                                    <p>Each of our plans is designed to meet the needs of a website by identifying the function of that website. Our base level security plan, SecureBasic is designed for blogs or personal sites that are currently free of infection. SecureStarter is designed for infected personal sites, more advanced personal sites or basic business sites. Our SecureSpeed plan is focused on websites designed to drive conversions and SecureSite is for ecommerce sites.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                        <a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          <strong>Will these plans remove a current malware infection?</strong>
          <i class="indicator fa fa-chevron-up float-right" aria-hidden="true"></i></a>
                                    </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>All of these plans, with the exception of SecureBasic, will remove an existing malware infection.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                        <a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
         <strong>I have an SSL Certificate, why do I need this?</strong>
         <i class="indicator fa fa-chevron-up float-right" aria-hidden="true"></i></a>
                                    </h4>
                                            </div>
                                            <div id="collapseFour" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> An SSL certificate is a great first step to a secure website. It decodes information passed between your site and its visitors to help prevent data theft. However, you still need to protect your site from defacement, theft or resources and other attacks.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                        <a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
          <strong>Do your services work will all hosting providers?</strong>
          <i class="indicator fa fa-chevron-up float-right" aria-hidden="true"></i></a>
                                    </h4>
                                            </div>
                                            <div id="collapseFive" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>Yes, our services work with all website hosting providers.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                        <a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
          <strong>Do your services work with all website platforms?</strong>
          <i class="indicator fa fa-chevron-up float-right" aria-hidden="true"></i></a>
                                    </h4>
                                            </div>
                                            <div id="collapseSix" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>SiteLock website security products work with all open source website platforms, such as WordPress, Joomla and Drupal. We also work with custom coded websites, support many ecommerce platforms and most WYSIWYG site builders.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                        <a class="accordion-toggle-new" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
          <strong>What type of payments do you accept?</strong>
         <i class="indicator fa fa-chevron-up float-right" aria-hidden="true"></i></a>
                                    </h4>
                                            </div>
                                            <div id="collapseSeven" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>We currently accept Visa, Mastercard, Discover and American Express. We do not accept PayPal or electronic checks currently.</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </section>

                    <!--Compatible with all kinds of websites-->

                    <section id="mainBlue">

                        <div class="container white">
                            <div class="row">
                                <div class="col-md-2 col-sm-6 col-xs-12">

                                    <img class="img50" src="landers/img/cms_icons/magentoWhite.svg">
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12 ">

                                    <img class="img50" src="landers/img/cms_icons/joomlaWhite.svg">
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">

                                    <h2 class="sourceBlack font35 text-left">Compatible with all </h2>
                                    <h2 class="sourceBlack font35 text-right">kinds of websites</h2>

                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <img class="img50" src="landers/img/cms_icons/wordpressWhite.svg">

                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">

                                    <img class="img50" src="landers/img/cms_icons/drupalWhite.svg">
                                </div>

                            </div>
                        </div>
                    </section>

                    <!--Meet a few of our customers and hear their SiteLock reviews and experiences-->
                    <section class="p-5">
                        <div class="container">
                            <h2 class="sourceLight text-center"><span class="sourceBlack">Meet a few of our customers </span>and hear their<br> SiteLock reviews and experiences</h2>
                            <div class="whiteSpace25"></div>
                            <div class="row">

                                <div class="col-sm-6 ">

                                    <div class="card flex-row flex-wrap card-border">
                                        <div class="card-header border-0">
                                            <img src="landers/
                                            img/dog.jpg">
                                        </div>
                                        <div class="card-block px-2">
                                            <div class="whiteSpace15"></div>
                                            <h4 class="card-title sourceBlack">Rentals on the Ocean<br> Recovers From a<br> Cyberattack</h4>
                                            <a href="#" class="btn btn-ghost-grey">Read More</a>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="card flex-row flex-wrap card-border">
                                        <div class="card-header border-0">
                                            <img src="landers/img/home1.jpg">
                                        </div>
                                        <div class="card-block px-2">
                                            <div class="whiteSpace15"></div>
                                            <h4 class="card-title sourceBlack">Vic’s Tree Service<br> Springs Back to Life <br>With Website Security</h4>
                                            <a href="#" class="btn btn-ghost-grey">Read More</a>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-6">

                                    <div class="card flex-row flex-wrap card-border">
                                        <iframe class="ifr" src="https://player.vimeo.com/video/259753291" width="640" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                                        <div class="card-block px-2 padding-left20 padding20">
                                            <div class="whiteSpace15"></div>
                                            <h4 class="card-title sourceBlack">Developer Can Handle <br>Cyberattacks Easily</h4>
                                            <a href="#" class="btn btn-ghost-grey">Read More</a>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </section>


                    <div class="footer">
<div id="content" style="padding-top: 10px;">


<hr class="footerHR">

<div class="row">
<div class="col-md-4" style="border-right: 1px solid #707070;">
<a href="https://www.sitelock.com/"><img class="logoFooter" src="img/logos/SiteLock_white.svg" alt="sitelock logo"></a>
</div>

<div class="col-md-4 font18">
<h3 class="sourceRegular">Contact</h3>
<p>U.S. <u>855.378.6200</u></p>
<p>Int'l <u>415.390.2500</u></p>
<!-- <p>Email <u>Support@SiteLock.com</u></p> -->
</div>

<div class="col-md-4 footerCol footconnect">
<h3 class="sourceRegular">Connect</h3>
<p>Sign up for SiteLock news & announcements</p>
<div class="form-inline footerSpace">
<form method="post" action="https://a29565.actonservice.com/acton/eform/29565/01b7e374-ab17-4ff6-a115-3f1742ab84eb/d-ext-0001">
<input type="hidden" name="AcceptsEmail" value="Yes">
<input class="form-control footerInput" type="email" name="Email" placeholder="you@email.com"><button style=" margin:0;" type="submit" class="btn-f btn-foot"><i class="far fa-arrow-right"></i></button>
</form>
</div>

</div>
</div>

<hr class="footerHR">

<p class="text-center footCopy linkblue">Copyright &copy; SiteLock <script>document.write(new Date().getFullYear())</script> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

</div>
</div>


                    <!-- DYNAMIC FORM INFO -->
                    <?php
$hb_title = "Get Started With SiteLock";
$hb_phone = "(855) 378-6200";
$hb_btn = 'Start Today';
?>

<!-- High Barrier -->
<div class="modal fade" id="HighBarrierForm" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content modalForm modalFix">
      <div class="modal-header">
        <h5 class="modal-title sourceBlack padding-left25 padding10" id="exampleModalLabel"><?php echo $hb_title; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="fal fa-times-circle" style="user-select: auto;"></i>
        </button>

      </div>
      <div class="modal-body padding25">
        <p class="redLink padding-left25">Enter your information or call us today at  <a href=""><?php echo $hb_phone; ?></a> to get started.</p>
      <form class="mt-20" method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
       <span class="form-inline form-group"><i id="validateName" class="far fa-circle grey1 fa-lg" title="First Name should be formatted as firstname"></i>&nbsp;<input name="First Name" id="HBinputName" style="width: 90%;" type="text" class="form-control" required placeholder="First Name"><label class="form-control-placeholder" for="HBinputName">First Name</label>
       </span>
       <span class="form-inline form-group"><i id="validatelastName" class="far fa-circle grey1 fa-lg" title="Last Name should be formatted as lastname"></i>&nbsp;<input name="Last Name" id="HBinputlastName" style="width: 90%;" type="text" class="form-control" required placeholder="Last Name"><label class="form-control-placeholder" for="HBinputlastName">Last Name</label>

       </span>
       <span class="form-inline form-group"><i id="validateDomain" class="far fa-circle grey1 fa-lg" title="Domain Name should be formatted as domain.com"></i>&nbsp;<input name="Website" id="HBinputDomain" style="width: 90%;" type="text" class="form-control" required placeholder="Domain"><label class="form-control-placeholder" for="HBinputDomain">Domain</label>

       </span>

       <span class="form-inline form-group"><i id="validateEmail" class="far fa-circle grey1 fa-lg" title="Email should be formatted as you@domain.com"></i>&nbsp;<input name="Email" id="HBinputEmail" style="width: 90%;" type="email" class="form-control" required placeholder="Email"><label class="form-control-placeholder" for="HBinputEmail" >Email</label>

       </span>

       <span class="form-inline form-group"><i id="validatePhone" class="far fa-circle grey1 fa-lg" title="Phone should be formatted as 0001112222"></i>&nbsp;<input name="Phone" id="HBinputPhone" style="width: 90%;" type="phone" minlength=3 class="form-control" required placeholder="Phone Number" pattern="^(?:\(\d{3}\)|\d{3})[- . ]?\d{3}[- . ]?\d{4}$"><label class="form-control-placeholder" for="HBinputPhone">Phone Number</label>

       </span>

     <div class="custom-control custom-checkbox" style="font-size: 14px; padding-left:25px !important; width: 100%;">
       <input type="checkbox" class="custom-control-input" id="customCheck1">
       <label class="custom-control-label" for="customCheck1">Email me the latest website security news</label>
     </div>
     <input type="hidden" name="Form ID" value="<FORM ID>">
            <input type="hidden" name="Lead Source" value="Act-On Form">
            <input type="hidden" name="Lead Group" value="Organic Web">
            <input type="hidden" name="Accepts Email" value="Accepts Email">
            <input type="hidden" name="Division" value="Retail">
            <input type="hidden" name="Lead Type" value="Buy Online">
            <input type="hidden" name="Lead Campaign" value="<CAMPAIGN ID>">
      <br>
      <p class="text-center"><button type="submit" class="btn btn-primary"><?php echo $hb_btn; ?></button></center>
      </form>
      </div>
      <div class="modal-footer">
        <p class="form-notice">By submitting this form you confirm that you have read and accepted our <a href="terms" target="_blank">Terms & Conditions</a> and <a href="privacy-policy" target="_blank">Privacy Policy</a></p>
      </div>
    </div>
  </div>
</div>
<!-- END DYNAMIC FORM INFO -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/custom.js?v=1.0.0"></script>
<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
<script>
 //FAQ
 function toggleChevron(e) {
  $(e.target)
      .prev('.panel-heading')
      .find("i.indicator")
      .toggleClass('fa-chevron-down fa-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);



$(document).ready(function() {
  $(".btn-pref .btn").click(function() {
      $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
      // $(".tab").addClass("active"); // instead of this do the below
      $(this).removeClass("btn-default").addClass("btn-primary");
  });
});

 //My Plan
 $(function() {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').show();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();

$('#myplan input').on('change', function() {

  var first = $('input[name=options]:checked', '#myplan').val();
  var second = $('input[name=options1]:checked', '#myplan').val();
  var third = $('input[name=options2]:checked', '#myplan').val();
  var four = $('input[name=options3]:checked', '#myplan').val();

  if(first == "yes" && second == "blogger"  && third == "none"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "blogger"  && third == "contact-info"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "blogger"  && third == "payment-info"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').show();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "blogger"  && third == "none"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "blogger"  && third == "contact-info"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "blogger"  && third == "payment-info"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').show();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "blogger"  && third == "none"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "blogger"  && third == "contact-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "blogger"  && third == "payment-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "blogger"  && third == "none"  && four == "none-ssl") {
  $('#secure-basic').show();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "blogger"  && third == "contact-info"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "blogger"  && third == "payment-info"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').show();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "blogger"  && third == "none"  && four == "standard") {
  $('#secure-basic').show();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "blogger"  && third == "contact-info"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "blogger"  && third == "payment-info"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').show();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "blogger"  && third == "none"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "blogger"  && third == "contact-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "blogger"  && third == "payment-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "business"  && third == "none"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "business"  && third == "contact-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').show();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "business"  && third == "payment-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').show();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "business"  && third == "none"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "business"  && third == "contact-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').show();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "business"  && third == "payment-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').show();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "business"  && third == "none"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "business"  && third == "contact-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "yes" && second == "business"  && third == "payment-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "business"  && third == "none"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "business"  && third == "contact-info"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').show();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "business"  && third == "payment-info"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').show();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "business"  && third == "none"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').show();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "business"  && third == "contact-info"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').show();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "business"  && third == "payment-info"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').show();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "business"  && third == "none"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "business"  && third == "contact-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();
  }
if(first == "no" && second == "business"  && third == "payment-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').show();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').hide();

  }
if(first == "yes" && second == "nonprofit"  && third == "none"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "yes" && second == "nonprofit"  && third == "contact-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "yes" && second == "nonprofit"  && third == "payment-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "yes" && second == "nonprofit"  && third == "none"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "yes" && second == "nonprofit"  && third == "contact-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "yes" && second == "nonprofit"  && third == "payment-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "yes" && second == "nonprofit"  && third == "none"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "yes" && second == "nonprofit"  && third == "contact-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "yes" && second == "nonprofit"  && third == "payment-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "no" && second == "nonprofit"  && third == "none"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "no" && second == "nonprofit"  && third == "contact-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "no" && second == "nonprofit"  && third == "payment-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "no" && second == "nonprofit"  && third == "none"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "no" && second == "nonprofit"  && third == "contact-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "no" && second == "nonprofit"  && third == "payment-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "no" && second == "nonprofit"  && third == "none"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "no" && second == "nonprofit"  && third == "contact-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "no" && second == "nonprofit"  && third == "payment-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').hide();
  $('#secure-nonprofit').show();

  }
if(first == "yes" && second == "developer"  && third == "none"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "yes" && second == "developer"  && third == "contact-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "yes" && second == "developer"  && third == "payment-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "yes" && second == "developer"  && third == "none"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "yes" && second == "developer"  && third == "contact-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "yes" && second == "developer"  && third == "payment-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "yes" && second == "developer"  && third == "none"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "yes" && second == "developer"  && third == "contact-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "yes" && second == "developer"  && third == "payment-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "no" && second == "developer"  && third == "none"  && four == "none-ssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "no" && second == "developer"  && third == "contact-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "no" && second == "developer"  && third == "payment-info"  && four == "none-ssl") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "no" && second == "developer"  && third == "none"  && four == "standard") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "no" && second == "developer"  && third == "contact-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "no" && second == "developer"  && third == "payment-info"  && four == "standard") 

{
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "no" && second == "developer"  && third == "none"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "no" && second == "developer"  && third == "contact-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();

  }
if(first == "no" && second == "developer"  && third == "payment-info"  && four == "evssl") {
  $('#secure-basic').hide();
  $('#secure-starter').hide();
  $('#secure-speed').hide();
  $('#secure-site').hide();
  $('#secure-custom').hide();
  $('#secure-devoloper').show();
  $('#secure-nonprofit').hide();
  }
});
});
</script>
</body>
</html>