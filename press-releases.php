<?php
//DEFINITIONS
$title = "SiteLock News articles";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div class="text-center" id="mainBody">
<div class="whiteSpace25"></div>
<h1 class="sourceBlack">SiteLock Press Release Archive</h1>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainGrey"><div id="mainBody">

<div class="whiteSpace25"></div>

<div class="whitebk linkgrey" div="mainBody"><div id="content"  style="max-width: 90%;">
<br><br>
<span id="prbox"></span>
<br>
</div></div>

<div class="whiteSpace50"></div>

</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<script>
fetch('includes/data/press_releases.json')
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }

      // Examine the text in the response
      response.json().then(function(prdata) {
        console.log(prdata.slice(-500));
        var prbox = "";
        for(index in prdata.slice(-500)) {
          for(pr in prdata[index]) {
            prbox += "<div id='pr' style='color: #000; border-bottom: 3px solid #000;'>"
            + "<span class='font14 capitalize'>" + prdata[index][pr]['month'] + " " + prdata[index][pr]['day'] + ", " + prdata[index][pr]['year'] + "</span>"
            + "<h3 class='sourceBlack font18'>" + prdata[index][pr]['title'] + "</h3>"
            + "<a href='" + prdata[index][pr]['source'] + "' target='_blank'>Read more</a>"
            + "</div></div>";
          }
        }
        document.getElementById("prbox").innerHTML = prbox;
      });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });
</script>
</body>
</html>