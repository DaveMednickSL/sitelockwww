<?PHP
//DEFINITIONS
$title = "404 | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$canonical ="404";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?php echo $title;?></title>
  <link rel="canonical" href="https://www.sitelock.com/<?php echo $canonical;?>" />
  <meta name="description" content="<?php echo $description;?>">
  <meta name="keywords" content="<?php echo $keywords;?>" />
  <link rel="icon" type="image/ico" href="favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>
<div class="desnav" id="menu"><div id="menu_inner">
<p id="top_menu" class="top_menu"><a class="btn btn-red btn-sm" href="currently-infected">My Site is Hacked</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-ghost-grey btn-sm" href="https://secure.sitelock.com/login">LOGIN</a></p>

<nav class="navbar navbar-expand-lg linknav">
<a style="width: 200px;" href="/"><img class="logoNav" src="img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="far fa-bars" style="color: #000 !important;"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <a href="solutions" class="spock kirk">SOLUTIONS</a>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <br>
          
          <div class="container">
            <div class="row">
              <div class="col-md-4">
                <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="personal-site">Personal Sites & Blogs</a><span class="navDesc">Affordable security options</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="business-solutions">Business Sites</a><span class="navDesc">Protect your bottom line</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="developer-solutions">Web Developers</a><span class="navDesc">Keep your clients secure</span>
                </li>
                <li class="nav-item ">
                  <a class="nav-link nav-adj text-uppercase" href="wordpress-security">WordPress</a><span class="navDesc">Tailored WordPress security</span>
                </li>
              </ul>
              </div>
              <!-- /.col-md-4  -->
              <div class="col-md-4">
                <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="hosting-providers">Hosting Providers</a><span class="navDesc">Protect your customers’ sites</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="msp-telco">MSP/Telco</a><span class="navDesc">Grow profits and reduce churn</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="agencies">Agencies</a><span class="navDesc">Boost customer satisfaction</span>
                </li>
              </ul>
              </div>
              <!-- /.col-md-4  -->
              <div class="col-md-4 my-auto text-center">
              <a href="currently-infected">
                <center><img src="img/nav/navPromo3.png" alt="" class="img-fluid img80" style="padding-bottom: 25px;"></center>
                </a>
              </div>
              <!-- /.col-md-4  -->
            </div>
          </div>
          <!--  /.container  -->


        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <a href="products" class="spock kirk">PRODUCTS</a>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <br>

          <div class="container">
            <div class="row">
              <div class="col-md-4">
                
                <ul class="nav flex-column">
                <li class="nav-item">
                <a class="nav-link nav-adj active text-uppercase" href="website-scanning">Website Scanning</a><span class="navDesc">Find harmful cyberthreats</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="malware-removal">Malware Removal</a><span class="navDesc">Automated hack removal</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="vulnerability-patching">Vulnerability Patching</a><span class="navDesc">Automatically fix CMS flaws</span>
                </li>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="vpn">VIRTUAL PRIVATE NETWORK</a><span class="navDesc">Browse the internet safely</span>
                </li>
              </ul>
              </div>
              <!-- /.col-md-4  -->
              <div class="col-md-4">
                <ul class="nav flex-column">
                <li class="nav-item">
                <a class="nav-link nav-adj text-uppercase" href="web-application-firewall">Web Application Firewall</a><span class="navDesc">Block malicious traffic</span>
                </li>
                <li class="nav-item">
                <a class="nav-link nav-adj text-uppercase" href="ddos-protection">DDoS Protection</a><span class="navDesc">Stop DDoS attacks</span>
                </li>
                <li class="nav-item">
                <a class="nav-link nav-adj text-uppercase" href="pci-compliance">PCI Compliance</a><span class="navDesc">Protect payment card data</span>
                </li>
              </ul>
              </div>
              <!-- /.col-md-4  -->
              <div class="col-md-4 my-auto">
                <a href="vpn">
                <center><img src="img/nav/navPromo2.png" alt="" class="img-fluid img80" style="padding-bottom: 25px;"></center>
                </a>

              </div>
              <!-- /.col-md-4  -->
            </div>
          </div>
          <!--  /.container  -->


        </div>
      </li>

      <li class="nav-item mainnav">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <a class="kirk mccoy sourceLight" href="pricing">PRICING</a>
        </a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <a href="resources" class="spock kirk">RESOURCES</a>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <br>

          <div class="container">
            <div class="row">
              <div class="col-md-4">
                
                <ul class="nav flex-column">
                <li class="nav-item">
                <a class="nav-link nav-adj active text-uppercase" href="help-center">Help Center</a><span class="navDesc">FAQs, blogs, videos and more</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="security-report">Security Report</a><span class="navDesc">Today’s cyberthreats explained</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="case-studies">Case Studies</a><span class="navDesc">Customer success stories</span>
                </li>
              </ul>
              </div>
              <!-- /.col-md-4  -->
              <div class="col-md-4">
                <ul class="nav flex-column">
                <li class="nav-item">
                <a class="nav-link nav-adj text-uppercase" href="downloads">Downloads</a><span class="navDesc">Reports, whitepapers, and more</span>
                </li>
                <li class="nav-item">
                <a class="nav-link nav-adj text-uppercase" href="community">Community Hub</a><span class="navDesc">Involvement in the community</span>
                </li>
              </ul>
              </div>
              <!-- /.col-md-4  -->
              <div class="col-md-4 my-auto">
                <a href="https://www.sitelock.com/blog/2018/05/case-study-amanda-naor-photography/">
                <center><img src="img/nav/navPromo1.png" alt="" class="img-fluid img80" style="padding-bottom: 25px;"></center>
                </a>

              </div>
              <!-- /.col-md-4  -->
            </div>
          </div>
          <!--  /.container  -->


        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <a href="about" class="spock kirk">OUR COMPANY</a>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <br>

          <div class="container">
            <div class="row">
              <div class="col-md-4">
               <ul class="nav flex-column">
                <li class="nav-item">
                <a class="nav-link nav-adj active text-uppercase" href="about">About SiteLock</a><span class="navDesc">Get to know SiteLock</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="reviews">Reviews</a><span class="navDesc">Why customers trust SiteLock</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="awards">Awards & Honors</a><span class="navDesc">Industry and culture recognition</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="sitelock-user-experience">SiteLock Experience</a><span class="navDesc">Your journey as a customer</span>
                </li>
              </ul>
              </div>
              <!-- /.col-md-4  -->
              <div class="col-md-4">
              <ul class="nav flex-column">
                <li class="nav-item">
                <a class="nav-link nav-adj active text-uppercase" href="giving-back">Giving Back</a><span class="navDesc">Supporting the future of STEM</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="news">News</a><span class="navDesc">Breaking news and coverage</span>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-adj text-uppercase" href="careers">Careers</a><span class="navDesc">Join the SiteLock team</span>
                </li>
              </ul>
              </div>
              <!-- /.col-md-4  -->
              <div class="col-md-4 my-auto">

                <a href="awards">
                <center><img src="img/nav/navPromo4.png" alt="" class="img-fluid img80" style="padding-bottom: 25px;"></center>
                </a>
                
              </div>
              <!-- /.col-md-4  -->
            </div>
          </div>
          <!--  /.container  -->


        </div>
      </li>

    <li class="nav-item mainnav">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <a class="kirk mccoy sourceLight" href="blog">BLOG</a>
        </a>
      </li>

      <li class="nav-item mainnav navHide">
        <span class="nav-link mccoy sourceLight colorWhite">.</span>
      </li>
    </ul>
  </div>


</nav>
</div></div>


<!-- MOBILE NAV -->
<nav class="mobnav navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/"><img src="/img/logos/SiteLock_red.svg" style="width: 150px;"></a>
  
  <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="whiteSpace20"></div>
  <div class="collapse navbar-collapse text-center" id="navbarNavDropdown">
    <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          SOLUTIONS <i class="fas fa-caret-down"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="solutions">ALL SOLUTIONS</a>
          <a class="dropdown-item" href="personal-site">PERSONAL SITES & BLOGS</a>
          <a class="dropdown-item" href="business-solutions">BUSINESS SITES</a>
          <a class="dropdown-item" href="developer-solutions">WEB DEVELOPERS</a>
          <a class="dropdown-item" href="wordpress-security">WORDPRESS</a>
          <a class="dropdown-item" href="hosting-providers">HOSTING PROVIDERS</a>
          <a class="dropdown-item" href="msp-telco">MSP/TELCOS</a>
          <a class="dropdown-item" href="agencies">AGENCIES</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          PRODUCTS <i class="fas fa-caret-down"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="products">ALL PRODUCTS</a>
          <a class="dropdown-item" href="website-scanning">WEBSITE SCANNING</a>
          <a class="dropdown-item" href="malware-removal">MALWARE REMOVAL</a>
          <a class="dropdown-item" href="vulnerability-patching">VULNERABILITY PATCHING</a>
          <a class="dropdown-item" href="web-application-firewall">WEB APPLICATION FIREWALL</a>
          <a class="dropdown-item" href="ddos-protection">DDOS PROTECTION</a>
          <a class="dropdown-item" href="pci-compliance">PCI COMPLIANCE</a>
          <a class="dropdown-item" href="vpn">VIRTUAL PRIVATE NETWORK</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="pricing">PRICING</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          RESOURCES <i class="fas fa-caret-down"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="help-center">HELP CENTER</a>
          <a class="dropdown-item" href="security-report">SECURITY REPORT</a>
          <a class="dropdown-item" href="case-studies">CASE STUDIES</a>
          <a class="dropdown-item" href="downloads">DOWNLOADS</a>
          <a class="dropdown-item" href="community">COMMUNITY HUB</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          OUR COMPANY <i class="fas fa-caret-down"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="about">ABOUT SITELOCK</a>
          <a class="dropdown-item" href="reviews">REVIEWS</a>
          <a class="dropdown-item" href="awards">AWARDS & HONORS</a>
          <a class="dropdown-item" href="the-sitelock-experience">SITELOCK EXPERIENCE</a>
          <a class="dropdown-item" href="giving-back">GIVING BACK</a>
          <a class="dropdown-item" href="news"">NEWS</a>
          <a class="dropdown-item" href="careers">CAREERS</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="blog">BLOG</a>
      </li>
      <li class="nav-item">
        <a class="btn btn-red" href="currently-infected">My Site Is Hacked</a>
      </li>
      <li class="nav-item">
      <br>
      <a class="btn btn-ghost-grey" href="https://secure.sitelock.com/login">Login</a>
      <br><br>
      </li>
    </ul>
  </div>
</nav>

<div class="headerspace" id="mainGrey"><div id="mainBody">
<br>
<h1 class="sourceBlack">404 | Page Not Found</h1>
<h4 class="font18">Oops! We can’t seem to find the page you’re looking for. </h4>
<div class="whiteSpace25"></div>
<hr>
<h3 class="font25">Can we point you in the right direction?</h3>
<div class="row">
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><br><i class="far fa-home fa-5x red"></i><hr><h4 class="sourceLight">Visit the<br>SiteLock homepage</h4><br><a class="btn btn-ghost-blue font-weight-bold" href="https://sitelock.com">SiteLock Home</a></div><br></div></div>
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><br><i class="far fa-lock-alt fa-5x red"></i><hr><h4 class="sourceLight">Check Out Our<br>Website Security Products</h4><br><a class="btn btn-ghost-blue font-weight-bold" href="https://sitelock.com/products">See Products</a></div><br></div></div>
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><br><i class="far fa-question-circle fa-5x red"></i><hr><h4 class="sourceLight">Browse Our<br>Help Center</h4><br><a class="btn btn-ghost-blue font-weight-bold" href="https://sitelock.com/help-center">Help Center</a></div><br></div></div>
</div>
<div class="whiteSpace50"></div>
</div></div>

<div class="footer">
<div id="content" style="padding-top: 10px;">
<img class="logoFooter" src="img/logos/SiteLock_white.svg" alt="sitelock logo">
<p class="footerNav">
<span class="footerNavItem"><a href="/currently-infected">GET HELP NOW</a></span>
<span class="footerNavItem"><a href="/products">PRODUCTS</a></span>
<span class="footerNavItem"><a href="/pricing">PRICING</a></span>
<span class="footerNavItem"><a href="/about">OUR COMPANY</a></span>
<span class="footerNavItem"><a href="/blog">BLOG</a></span>
<span class="footerNavItem"><a href="/careers">CAREERS</a></span>
<span class="footerNavItem"><a href="/help-center">HELP CENTER</a></span>
<span class="footerNavItem"><a href="/sitemap">SITEMAP</a></span>
</p>

<hr class="footerHR">

<div class="row">
<div class="col-md-4">
<h3 class="sourceRegular">Search</h3>
<p>Looking for something?</p>
<div class="form-inline footerSpace">
<form method="post" action="search-sitelock">
<input class="form-control footerInput" type="text" name="term" placeholder="Enter Keyword">
<button class="btn btn-ghost-blue btn-sm"  style="width: 100%;">Search</button>
</form>
</div>
</div>

<div class="col-md-4">
<hr class=" footerHR footMobileShow">
<h3 class="sourceRegular">Contact</h3>
<p>Reach us 24/7</p>
<p>
<table class="colorWhite font18 linkblue">
<tr><td>U.S&nbsp;&nbsp;</td><td><a href="tel:8553786200">(855) 378-6200</a></td></tr>
<tr><td>Int'l&nbsp;&nbsp;</td><td><a href="tel:4153902500">+1 (415) 390-2500</a></td></tr>
<tr><td>Email&nbsp;&nbsp;</td><td><a href="contact">Contact Us</a></td></tr>
</table>
</p>
</div>

<div class="col-md-4">
<hr class=" footerHR footMobileShow">
<h3 class="sourceRegular">Stay in touch</h3>
<p>Sign up for SiteLock news & announcements</p>
<div class="form-inline footerSpace">
<form method="post" action="https://a29565.actonservice.com/acton/eform/29565/f370b272-2264-4afa-bfea-f962cfb91e3d/d-ext-0002">
<input class="form-control footerInput" type="text" placeholder="you@email.com" required>
<button class="btn btn-ghost-blue btn-sm" style="width: 100%;">Sign Up</button>
</div>
</form>
</div>



</div>
</div>

<hr class="footerHR">

<h3 class="text-center sourceRegular">Connect</h3>

<div id="mainBody"><div id="content90" style="max-width: 350px;"><div class="row">
<div class="col text-center">
<a href="https://www.facebook.com/SiteLock/" target="_blank"><span class="fa-stack fa-2x">
  <i class="fal fa-circle fa-stack-2x"></i>
  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
</span></a>
</div>

<div class="col text-center">
<a href="https://twitter.com/SiteLock" target="_blank"><span class="fa-stack fa-2x">
  <i class="fal fa-circle fa-stack-2x"></i>
  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
</span></a>
</div>

<div class="col text-center">
<a href="https://www.linkedin.com/company/sitelock/" target="_blank"><span class="fa-stack fa-2x">
  <i class="fal fa-circle fa-stack-2x"></i>
  <i class="fab fa-linkedin-in fa-stack-1x fa-inverse"></i>
</span></a>
</div>

<div class="col text-center">
<a href="https://www.instagram.com/sitelock/" target="_blank"><span class="fa-stack fa-2x">
  <i class="fal fa-circle fa-stack-2x"></i>
  <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
</span></a>
</div>
</div></div></div>

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock 2008-<span id="COPYRIGHT"></span> | <a href="privacy-policy">Privacy Policy</a> | <a href="terms">Terms of Use</a> | <a href="affiliate-tos">Affiliates Terms of Service</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

</div>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/custom.js?v=1.0.1"></script>

</body>
</html>