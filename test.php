<?php
//DEFINITIONS
$title = "TEST PAGE";
$description = "This is a  test page";
$keywords = "test, page";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="sourceRegular"><span class="sourceBlack">SiteLock</span><br>TEST PAGE</h1>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace50"></div>

<div class="row linkblue text-center">
<div class="col-md-3"><a href="test?about">About</a></div>
<div class="col-md-3"><a href="test?dashboard">Dashboard</a></div>
<div class="col-md-3"><a href="test?newcustomer">New Customer Setup</a></div>
<div class="col-md-3"><a href="test?all">All</a></div>
<div class="col-md-12">
<br>
<form method="get" action="test">
<input class="form-control forms" style="border: 2px solid #000 !important;" type="text" name="term">
<button class="btn btn-red btn-sm" type="submit">Search</button>
</form>
</div>
</div>

<div class="whiteSpace50"></div>

<div id="response">Content is loading...</div>

<div class="whiteSpace50"></div>
</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

<script>
//URL VARIABLE
var currentURL = (document.URL);
var cat=(cat=(cat=(cat=currentURL.split("?")[1]).replace("term=","")).replace("+"," ")).toLowerCase();

//PULL FROM JSON
fetch('includes/data/helpcenter.json')
  .then(
    function(response) {if (response.status !== 200) {console.log('Looks like there was a problem. Status Code: ' +response.status);return;}

      // Examine the text in the response
      response.json().then(function(data) {
        var display = "";
        for(index in data.slice(-500)) {
          for(helpcenter in data[index]) {
            rowcat = data[index][helpcenter]['category'];
            if (rowcat.indexOf(cat) >= 0) {
              //display += "<div><h1>" + data[index][helpcenter]['question'] + "</h1><p>" + data[index][helpcenter]['answer'] + "</p><hr></div>";
              display += "<div class='card bg-light mb-3'><div class='card-header font22'>" + data[index][helpcenter]['question'] + "</div><div class='card-body'><p class='card-text'>" + data[index][helpcenter]['answer'] + "</p></div></div><br>"
              } else {display += "";}
          }   
        }
        document.getElementById("response").innerHTML = display + "<p class='text-center font18'>Couldn't find the right solution?<br> We can help! Call support at <a href='tel:8775632832'>(877) 563-2832</a></p>";
      });
    }
  )
  .catch(function(err) {console.log('Fetch Error :-S', err);});
</script>

</body>
</html>