<?php
//DEFINITIONS
$title = "Contact | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainblue" style="margin-top:111px; max-height: 10000px;"><div id="mainTerms"><div id="content">

<div class="row">
<div class="col-md-4 linkwhite">
<h1 class="sourceBlack font35" style="margin-top: 20px;">Contact Us</h1>
<hr class="hrWhite">
<p class="font22">SOLUTIONS TEAM<br>
<a class="sourceBlack" href="">(XXX) XXX-XXXX</a><br>
<a class="sourceBlack" href="">Chat Now</a>
</p>

<hr class="hrWhite">
<p class="font22">SUPPORT TEAM<br>
<a class="sourceBlack" href="">(XXX) XXX-XXXX</a><br>
<a class="sourceBlack" href="">Chat Now</a>
</p>

<hr class="hrWhite">
<p class="font22">BILLING TEAM<br>
<a class="sourceBlack" href="">(XXX) XXX-XXXX</a><br>
<a class="sourceBlack" href="">Chat Now</a>
<hr  class="hrWhite">
</p>
</div>

<div class="col-md-1"></div>

<div class="col-md-7"><div class="whitebk" id="mainBody"><div class="grey" id="content" style="max-width: 90%;">
<h1 class="sourceBlack font35" style="margin-top: 10px;">Let Us Contact You</h1>
<p>What department are you trying to reach?</p>



<form method="post" action="https://sitelock.sugarondemand.com/rest/v11/ms-lead-routing">
      
    <fieldset id="dept">
    <div id="deptBox"><div class="row text-center">
    
    <div class="col-md-4"><label class="cursor"><span class="cselect1 cchange1">Solutions</span> <input class="hidden" type="radio" value="Solutions" name="dept"></label></div>
    <div class="col-md-4"><label class="cursor"><span class="cselect2 cchange2">Support</span> <input class="hidden" type="radio" value="Support" name="dept"></label></div>
    <div class="col-md-4"><label class="cursor"><span class="cselect3 cchange3">Billing</span> <input class="hidden" type="radio" value="Billing" name="dept"></label></div>
     
    </div></div>
    </fieldset>   
    
    <span class="form-inline form-group"><i id="validateName" class="far fa-circle grey fa-lg"title="First Name should be formatted as firstname"></i>&nbsp;<input id="CTinputName" style="width: 90%;" type="text" class="form-control" placeholder="First Name" required><label class="form-control-placeholder" for="CTinputName">First Name</label></span>
    <span class="form-inline form-group"><i id="validateLast" class="far fa-circle grey fa-lg"title="Last Name should be formatted as lastname"></i>&nbsp;<input id="CTinputLast" style="width: 90%;" type="text" class="form-control" placeholder="Last Name" required><label class="form-control-placeholder" for="CTinputLast">Last Name</label></span>
    <span class="form-inline form-group"><i id="validateDomain" class="far fa-circle grey fa-lg" title="Domain Name should be formatted as domain.com"></i>&nbsp;<input id="CTinputDomain" style="width: 90%;" type="text" class="form-control" placeholder="Domain" required><label class="form-control-placeholder" for="CTinputDomain">Domain</label></span>
    <span class="form-inline form-group"><i id="validateEmail" class="far fa-circle grey fa-lg" title="Email should be formatted as you@domain.com"></i>&nbsp;<input id="CTinputEmail" style="width: 90%;" type="email" class="form-control" placeholder="Email" required><label class="form-control-placeholder" for="CTinputEmail" >Email</label></span>
    <span class="form-inline form-group"><i id="validatePhone" class="far fa-circle grey fa-lg"title="Phone Number should be formatted as 0001112222"></i>&nbsp;<input id="CTinputPhone" style="width: 90%;" type="text" minlength=3 class="form-control" placeholder="Phone Number" required><label class="form-control-placeholder" for="CTinputPhone">Phone Number</label></span>

    <textarea class="form-control-text" name="Message" placeholder="Message (Optional)"></textarea>

    <input type="hidden" name="Lead Source" value="Act-On Form">
    <input type="hidden" name="Lead Group" value="Organic Web">
    <input type="hidden" name="Accepts Email" value="Accepts Email">
    <input type="hidden" name="Division" value="Retail">
    <input type="hidden" name="Lead Type" value="General">
      
    <div class="custom-control custom-checkbox" style="font-size: 14px;">
    <input type="checkbox" class="custom-control-input" id="customCheck1">
    <label class="custom-control-label" for="customCheck1">Email me the latest website security news</label>
    </div>
      <br>
      <p class="text-center"><button type="submit" class="btn btn-red">Submit</button>
      </form>
      <p class="form-notice linkred">By submitting this form you confirm that you have read and accepted our <a href="terms" target="_blank">Terms & Conditions</a> and <a href="privacy-policy" target="_blank">Privacy Policy</a></p>
</div><br></div></div>
</div>

</div></div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>