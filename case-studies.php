<?php
//DEFINITIONS
$title = "Case Studies | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div id="mainBody">
<div class="row">
<div class="col-md-6 my-auto">
<h1>Customer<br><span class="sourceBlack">Success Stories</span></h1>
<h3 class="font18">Meet these security superheroes (our customers!) and learn how they worked with SiteLock to save their websites after a cyberattack struck.</h3>
<a class="btn btn-red" href="pricing">Browse Plans</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-ghost-white" href="contact">Contact Us</a>
<div class="whiteSpace50 tabshow"></div>
</div>
<div class="col-md-6 my-auto">
<div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/288605026" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
</div>
</div>
</div>
</div>

<div id="mainGrey"><div id="mainBody"><div id="content">
<h1 class="text-center"><span class="sourceBlack">Meet a few of our customers</span> and hear their SiteLock reviews and experiences</h1>


<div id="caseBox">
<div class="row">
<div class="col-md-6">
<div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/288594809" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 5px;">A Photographer Bounces Back After a Website Compromise</h3>
<p class="font18" style="padding: 5px;">After a website compromise, Amanda turned to SiteLock for help. Now, Amanda’s website is back in action and there isn’t anything standing in between her, her clients, and her photography.</p>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/09/sitelock-video-reviews-website-malware/">Read More</a><br><br>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
<div id="caseBox">
<div class="row">
<div class="col-md-6">
<img class="img100" src="img/community/vic.png" alt="Vic's Tree Service">
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 5px;">Vic’s Tree Service Springs Back to Life With Website Security</h3>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/09/sitelock-reviews-tree-trimming-business-case-study/">Read More</a><br><br>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div id="caseBox">
<div class="row">
<div class="col-md-6">
<img class="img100" src="img/community/marlowes.png" alt="Marlowe's BBQ">
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 5px;">Memphis Restaurant’s Joomla! Site Stays Safe</h3>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/05/sitelock-reviews-marlowesmemphis/">Read More</a><br><br>
</div>
</div>
</div>
</div>
</div>

<div id="caseBox">
<div class="row">
<div class="col-md-6">
<div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/288599817" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 5px;">Developer Can Handle Cyberattacks Easily</h3>
<p class="font18" style="padding: 5px;">Brett protects every single one of his clients automatically with SiteLock, keeping his costs low while increasing customer satisfaction. Watch how his job is easier with website security.</p>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/09/sitelock-video-reviews-remove-malware/">Read More</a><br><br>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
<div id="caseBox">
<div class="row">
<div class="col-md-6">
<img class="img100" src="img/community/rochelles.jpg" alt="Vic's Tree Service">
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 5px;">Rochelle Interiors Redecorates after a Website Attack</h3>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/11/sitelock-case-study-interior-designer-smb/">Read More</a><br><br>
</div>
</div>
</div>
</div>

<div class="col-md-6">
<div id="caseBox">
<div class="row">
<div class="col-md-6">
<img class="img100" src="img/community/dawn.png" alt="Marlowe's BBQ">
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 5px;">Dawn Gets Her Website Back in Shape</h3>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/01/sitelock-case-study-shapewear-smb/">Read More</a><br><br>
</div>
</div>
</div>
</div>
</div>

<div id="caseBox">
<div class="row">
<div class="col-md-6">
<div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/288600995" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
</div>

<div class="col-md-6 my-auto">
<h3 class="font25 sourceBlack" style="padding: 5px;">A Whale Watcher's Path to Success</h3>
<p class="font18" style="padding: 5px;">Melissa’s website compromise damaged her company’s bottom line and reputation. Watch as she shares her SiteLock review and experience about her path to success after a cyberattack.</p>
<a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/09/sitelock-video-reviews-malicious-redirect/">Read More</a><br><br>
</div>
</div>
</div>

</div></div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>