<?php
//DEFINITIONS
$title = "SiteLock Resources Hub";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div class="text-center" id="mainBody">
<div class="whiteSpace25"></div>
<h1>Welcome to the SiteLock<br><span class="sourceBlack">Resource Center</span></h1>
<h2 class="font25">Whether you’re a cybersecurity newbie or a seasoned expert, find everything you need to know about website security.</h2>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainBody">
<div class="whiteSpace25"></div>
<div class="row sourceBlack">
<div class="col-md-1"></div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/siteSearch.svg" alt="Blogs">
  <h5>Help<br>Center</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey btn-sm" href="help-center">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/qsrRed.svg" alt="Blogs">
  <h5>Website Security Report</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey btn-sm" href="security-report">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/resources/resourcesCaseStudies.svg" alt="Blogs">
  <h5>Case<br>Studies</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey btn-sm" href="case-studies">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/siteResources.svg" alt="Blogs">
  <h5>Downloadable<br>Assets</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey btn-sm" href="downloads">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-2 text-center">
  <div id="darkBorderBox">
  <div class="linkgrey" id="darkBorderBoxContent">
  <img class="imgHeight130" src="/img/resources/resourcesCommunity.png" alt="Blogs">
  <h5>Community<br>Hub</h5>
</div>
<div id="darkBorderBoxContent">
<br><a class="btn btn-ghost-grey btn-sm" href="community">Learn More</a><br><br>
</div>
</div>
</div>

<div class="col-md-1"></div>

<div class="whiteSpace50"></div>

</div>
</div>
</div>

<div id="mainRed" style="padding: 0 !important;"><div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-sm-5"><img class="headerimg" src="/img/channel/hostingSupport.png" alt="twenty-four hour support"></div>
<div class="col-sm-7 my-auto">
<h2 class="font35 sourceBlack">Day or night, our friendly, 24/7 U.S.-based support team is here to help via phone, email or live chat.</h2>
<a class="btn btn-ghost-white" href="tel:8553786200">(855) 378-6200</a>&nbsp;&nbsp;&nbsp; <a class="btn btn-ghost-white" href="contact">email</a>&nbsp;&nbsp;&nbsp; <a class="btn btn-ghost-white" href="">chat</a>
<div class="whiteSpace25"></div>
</div>
</div>
</div></div></div>


<div id="mainBody"><div id="content">

<div class="row">
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><br><img  class="img70" src="img/resources/resourceAwards.svg"><br><h4>See SiteLock<br>Honors and Awards</h4><br><a class="btn btn-ghost-blue" href="https://sitelock.com/awards">See Accolades</a></div><br></div></div>
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><br><img class="img70" src="img/scanningCloud.svg"><br><h4>Find the Best Plan<br>for Your Website</h4><br><a class="btn btn-ghost-blue" href="https://sitelock.com/pricing">Compare Plans</a></div><br></div></div>
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="img70" src="img/channel/hostingComment.svg"><h4>Read SiteLock<br>Reviews</h4><br><a class="btn btn-ghost-blue" href="https://sitelock.com/reviews">Check Us Out</a></div><br></div></div>
</div>
<div class="whiteSpace50"></div>

</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>