<?php
//DEFINITIONS
$title = "Website Scanning | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "Website Scanner,Website Virus Scanner,Malware Scanner,Website Security Scan,Scan Website for Malware,Check Website for Malware";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div class="headerBottomGrey" id="mainHeader"><div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-6 my-auto">
<h1 class="headTxtDrop">Website <span class="sourceBlack">Malware Scanner</span></h1>
<h3>Instantly check your website for malware, viruses, and other threats</h3>
<a class="btn btn-blue font-weight-bold" href="#onpagepricing">Gain Control of Your Site Now</a>
</div>
<div class="col-lg-6">
<img class="headerimg headImgDrop" src="/img/website-scanning/scanningLaptop.png" alt="SiteLock Dashboard">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<div id="mainBody">
<div class="whiteSpace25"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2 class="sourceLight">What is <span class="sourceBlack">Website Scanning?</span></h2>Your scanners check your site daily for things that shouldn’t be there, like malware and vulnerabilities. If anything harmful is identified, you’ll be alerted right away. You can think of website scanning like an alarm system for your website—when threats enter, you’re the first to know. You can take scanning one step further by selecting a solution that automatically fixes issues on the fly—so you never have to worry.<br><a class="btn btn-red font-weight-bold" href="#onpagepricing">Find and Fix Threats Now</a></div></div>
   <div class="col-lg-5 iconFirst footMobileHide"><img class="splitImg" src="/img/scanningAlert.svg" alt="Scanning Calendar"></div>
  </div>
</div>

<div class="whiteSpace100"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-md-5"><img class="splitImg" src="/img/scanningCalendar.svg" alt="Scanning Alert"></div>
  <div class="col-md-7 whitebk splitPadding"><div id="content"><h2 class="sourceLight">Daily <span class="sourceBlack">website scans</span></h2>Interested in scanning your website daily for threats? You’ve come to the right place. <strong>Discover all of the scans and capabilities available to you as part of your website scanner purchase.</strong><br><a class="btn btn-red font-weight-bold" href="#onpagepricing">Start Scanning for Threats</a></div></div>
 </div>
</div>
<div class="whiteSpace50"></div>
</div>

<?php include 'includes/awards-bar.php';?>

<div id="mainTerms">
<div class="whiteSpace100"></div>
<h1 class="text-center font55 font-weight-bold">SiteLock website scans available to you</h1>
<h5 class="text-center">Choose your SiteLock scanner and reap all the benefits of a clean website.<br><strong>The scans listed below are included in your website scanner product package.</strong></h5>
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-7 linkred">
<h2 id="malware-scan"><span class="sourceBlack">Malware</span> Scan</h2>
<strong>Your industry-leading malware scan analyzes website content, flagging suspicious and malicious content and notifying you immediately of any issues to correct.</strong>
<ul style="width: 90%;">
<li>Daily outside-in scanning checks for over 10 million threats</li>
<li>Stops search engine blacklisting by alerting you to issues before they’re detected by crawlers</li>
<li>Stops malware infections that cause slow load times<br>and attacks on website visitors</li>
</ul>
Want to remove malware automatically?<br>
<strong><a href="malware-removal">Check out SiteLock SMART</a></strong>
</div>
<div class="col-lg-5 iconFirst"><img class="splitImg" src="img/website-scanning/scanningMalware.png"></div>
</div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-md-5"><img class="splitImg" src="img/website-scanning/scanningEmail.png"></div>
<div class="col-md-7 linkred my-auto">
<h2 id="spam-scan"><span class="sourceBlack">Spam</span> Scan</h2>
<strong>Websites and IP addresses that are listed in spam blacklist databases may have their emails blocked or sent to recipients’ spam folders. A spam scan ensures your website isn’t listed on one of these databases.</strong>
  <ul>
  <li>Daily scans of your website’s IP address and domain name against multiple spam databases</li>
  <li>Alerts you to “bad neighbor” blocks—when a website shares an IP address with another domain sending spam on a shared hosting platform, all websites on that IP address are sometimes blocked</li>
  <li>If an issue is detected, you are immediately notified, allowing you<br>to correct the issue before your recipients’ experience errors</li>
  </ul>
  <strong><a href="malware-removal">Learn how SiteLock SMART protects your website from spam</a></strong>
</div>
</div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-lg-7 linkred my-auto">
<h2 id="network-scan"><span class="sourceBlack">Network</span> Scan</h2>
<strong>The network scan in your SiteLock Dashboard scans the server your website is hosted on for misconfigurations and open network ports.</strong>
  <ul>
  <li>Daily scans of your hosting environment to ensure it is secured properly</li>
  <li>Automated alerts break down open ports and how they are being used</li>
  <li>Email alerts empower you to contact your hosting provider in the event an issue is discovered, before a compromise occurs</li>
  </ul>
  <strong><a href="malware-removal#malware-scan">Start scanning your website now with SiteLock SMART</a></strong>
</div>
<div class="col-lg-5 iconFirst"><img class="splitImg" src="img/website-scanning/scanningNetwork.png"></div>
</div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-md-5"><img class="splitImg" src="img/website-scanning/scanningWordPress.png"></div>
<div class="col-md-7 linkred my-auto">
<h2 id="app-scan"><span class="sourceBlack">Application</span> Scan</h2>
<strong>The application scan examines the server’s core applications, including operating system, database, and language versions such as PHP and MySQL.</strong>
  <ul>
  <li>Monthly scans of your web hosting environment</li>
  <li>Checks for vulnerabilities in operating systems and application versions</li>
  <li>If issues are detected, you are notified immediately, allowing you to contact your hosting provider to ensure your website is hosted in a secure environment</li>
  </ul>
  <strong><a href="malware-removal#smart">Check out SiteLock SMART</a></strong>
 
</div>
</div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-lg-7 linkred my-auto">
<h2 id="xss-scan"><span class="sourceBlack">Cross-site Scripting (XSS) & SQL Injection (SQLi)</span> Scan</h2>
<strong><a href="https://www.sitelock.com/blog/2015/11/xss-vulnerability-part-1/">Cross-site scripting</a> and <a href="https://www.sitelock.com/blog/2018/07/cyberattack-types/">SQL injection</a> vulnerabilities are commonly used by cybercriminals to gain unauthorized access to your website. SiteLock checks for vulnerabilities and if any are found, you are notified immediately.</strong>
  <ul>
  <li>Scans website pages daily for XSS and SQLi vulnerabilities</li>
  <li>Sends an immediate email alert in the event that a vulnerability is detected</li>
  </ul>
  <strong><a href="malware-removal">Check out SiteLock SMART</a></strong>
</div>
<div class="col-lg-5 iconFirst"><img class="splitImg" src="img/website-scanning/scanningInjection.png"></div>
</div>

</div>

<div id="onpagepricing"></div>
<div id="mainBody">
<div class="whiteSpace100"></div>
<h1 class="sourceLight text-center font55">Don’t wait, protect your website now.<br><span class="sourceBlack">Buy your website security plan today!</span></h1>

<?php include('includes/price-bar-1.php');?>

<?php include('includes/includes-bar.php');?>

</div>

<?php include('includes/solutions-bar.php');?>

<?php 
$benefitsTitle = '<h1 class="text-center font55">Website scanning <span class="sourceBlack">benefits</span></h1>'; 
$icon1= 'fa-umbrella'; $title1 = 'Peace of mind'; $content1 = 'Rest assured knowing your site is protected from malware 24/7. SiteLock is the only security provider to offer automated malware removal the moment threats hit your site.';
$icon2 = 'fa-rocket'; $title2 = 'Enhance site speed'; $content2 = 'Don’t let malware slow down your site. When a site is infected with malware, it could freeze, crash, or the appearance could change. Keep your site clean to maintain constant uptime.';
$icon3 = 'fa-crown'; $title3 = 'Increase visitor trust'; $content3 = 'Build customer confidence with a SiteLock Trust Seal. Your malware removal solution includes a security badge that lets your visitors know your site is secure and malware-free.';
$icon4 = 'fa-chart-line'; $title4 = 'Improve<br>SEO'; $content4 = 'Never worry about a website compromise again. With ongoing, proactive protection, we’ll find and fix threats before you even know they exist.';
$icon5 = 'fa-badge-check'; $title5 = 'Worry-free<br>protection'; $content5 = 'Day or night, our friendly, U.S.-based team of security experts is available to help 24/7/365 via phone or live chat.';
$icon6 = 'fa-magic'; $title6 = 'Immediate alerts<br>and updates'; $content6 = 'Always be informed about the security of your site with automated alert emails and live results from your SiteLock Dashboard.';
$benefitsBTN = 'Choose Your Scanner Now';

include('includes/benefits-bar.php');?>

<?php include('includes/cms-bar.php');?>


<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>
