<?php
//DEFINITIONS
$title = "SiteLock News articles";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeader"><div class="text-center" id="mainBody">
<div class="whiteSpace25"></div>
<h1 class="sourceBlack">SiteLock News Archive</h1>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainGrey"><div id="mainBody">

<div class="whiteSpace25"></div>

<div class="whitebk" div="mainBody"><div id="content" style="max-width: 90%;">
<br><br>
<span id="news"></span>
<br>
</div></div>

<div class="whiteSpace50"></div>

</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<script>
fetch('includes/data/news.json')
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }

      // Examine the text in the response
      response.json().then(function(data) {
        console.log(data.slice(-500));
        var news = "";
        for(index in data.slice(-500)) {
          for(post in data[index]) {
            news += "<div class='linkgrey' id='news'><div class='row'>"
            + "<div class='col-md-2 my-auto'><img class='newsImg' src='img/news/" + data[index][post]['image'] + "' alt='Article Source'></div><div class='col-md-10'>"
            + "<span class='capitalize'>" + data[index][post]['month'] + "</span> " + data[index][post]['day'] + ", " + data[index][post]['year']
            + "<h3 class='font18'>" + data[index][post]['title'] + "</h3>"
            + "<a class'font14' href='" + data[index][post]['source'] + "' target='_blank'>Keep reading</a>"
            + "</div></div></div>";
          }
        }
        document.getElementById("news").innerHTML = news;
      });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });
</script>
</body>
</html>