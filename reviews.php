<?php
//DEFINITIONS
$title = "SiteLock Reviews";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace25"></div>
<h1 class="sourceBlack">SiteLock Reviews</h1>
<h2 class="font25">See Why Over 12 million Websites Trust SiteLock</h2>
<a href="#ReviewForm" class="btn btn-red" data-toggle="modal" data-target="#ReviewForm">Leave Your SiteLock Review</a>
<div class="whiteSpace50"></div>
</div></div>

<?php include 'includes/rating-bar.php';?>

<div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<h2 class="font45 text-center"><span class="sourceBlack">Real SiteLock Reviews</span> by Real SiteLock Customers</h2>
<h3 class="font25 text-center">Learn why customers choose SiteLock.</h3>
<div class="whiteSpace25"></div>
<div class="row">
<div class="col-md-3">
<div id="darkBorderGrey"><div id="content"><br>
<p class="text-center"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"></p>
<p>My experience was excellent. The information provided to me was clear and helpful. I definitely would recommend the service to others. The person that assisted me with my file made the process stress free.</p>
<br><br>
<p class="font-weight-bold">Petra</p>
</div></div>
</div>

<div class="col-md-3">
<div id="darkBorderGrey"><div id="content"><br>
<p class="text-center"><img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"></p>
<p>My SiteLock representative provided me with exceptional and informative service. I really appreciate the comprehensive explanation of my website security options and package offerings.</p>
<br><br>
<p class="font-weight-bold">Lisa</p>
</div></div>
</div>

<div class="col-md-3">
<div id="darkBorderGrey"><div id="content"><br>
<p class="text-center"><img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"></p>
<p>My representative was very personable, answered my questions and conducted himself like a true professional. Explained the nuances of my current plan and value added that I receive through SiteLock. Excellent customer service.</p>
<br>
<p class="font-weight-bold">Drew</p>
</div></div>
</div>

<div class="col-md-3">
<div id="darkBorderGrey"><div id="content"><br>
<p class="text-center"><img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"> <img class="reviewStar" src="img/reviews/starRed.png"></p>
<p>I appreciate my rep's polite, professional and to-the-point service. He took the time to politely explain what my website needed for security. I now feel better about spending the money to allow my site to be more secure. Thank you for your service.</p>
<p class="font-weight-bold">Jenifer</p>
</div></div>
</div>
</div>

<div class="whiteSpace50"></div>

<p class="text-center"><a href="#ReviewForm" class="btn btn-blue" data-toggle="modal" data-target="#ReviewForm">Leave Your SiteLock Review</a></p>
<p class="text-center font-italic font14">Your review may not be posted if it contains sensitive account information or offensive content.<br>We do not alter reviews unless it is necessary to do so to protect our employees.</p>

<div class="whiteSpace100"></div>

<div id="mainSplitLeft">
    <div class="row splitSpace">
     <div class="col-md-5 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2 class="sourceBlack">The SiteLock experience</h2>Let us walk you through the SiteLock experience and show you why over 12 million websites trust us with their security.</div></div>
     <div class="col-md-7 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/259753291" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
    </div>
  </div>
</div>

<div class="whiteSpace100"></div>

<h2 class="text-center font55"><span class="sourceBlack">Customer success</span> stories</h2>
<h3 class="text-center font25">From small businesses to large enterprises, SiteLock protects websites of all sizes. Read our case studies to learn how we can help you quickly recover from the aftermath of a website compromise, improve your online reputation, and prevent cyberattacks from occurring again.</h3>

<div class="row">
<div class="col-md-4">

<div id="darkBorderGrey">
<img class="img100" src="img/reviews/success1.png" alt="Customer Success Naor">
<div id="content">
<div id="benefitCircleReviewRed"><img src="img/fa_icons/icon-Code.svg"></div>
<p class="font-italic">I never thought this could happen to me. Ignorance is bliss up until the point where you see all of your hard work in the hands of someone else. If you have a website, you need to have website security because anyone is susceptible!</p>
<p class="font-weight-bold">amandanaorphotography.com</p>
<p class="text-center"><a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/05/case-study-amanda-naor-photography/">Read More</a></p>
</div></div>
</div>

<div class="col-md-4">
<div id="darkBorderGrey">
<img class="img100" src="img/reviews/success2.png" alt="Customer Success BBQ">
<div id="content">
<div id="benefitCircleReviewBlue"><img src="img/fa_icons/icon-Code.svg"></div>
<p class="font-italic">The SiteLock technicians were easy to work with, listening to my questions and providing clear explanations of security risks…I would definitely recommend SiteLock as a cost-effective solution to website protection.</p>
<br>
<p class="font-weight-bold">MarlowesMemphis.com</p>
<p class="text-center"><a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/05/sitelock-reviews-marlowesmemphis/">Read More</a></p>
</div></div>
</div>

<div class="col-md-4">
<div id="darkBorderGrey">
<img class="img100" src="img/reviews/success3.png" alt="Customer Success Bucking">
<div id="content">
<div id="benefitCircleReviewRed"><img src="img/fa_icons/icon-Code.svg"></div>
<p class="font-italic">With SiteLock protecting my website, my traffic and page views have surpassed my initial expectations. My visitors are leaving their email information to the subscription list with confidence.</p>
<br>
<p class="font-weight-bold">buckingstocktalk.com</p>
<p class="text-center"><a class="btn btn-ghost-grey" href="https://www.sitelock.com/blog/2018/05/sitelock-reviews-buckingstocktalk/">Read More</a></p>
</div></div>
</div>
</div>

<div class="whiteSpace100"></div>

</div></div>

<div id="mainGrey"><div id="mainBody"><div id="content">

<div class="whiteSpace50"></div>
<h2 class="font55 text-center"><span class="sourceBlack">Customer</span> Testimonials</h2>

<div class="row">
<div class="col-md-4">
<div id="benefitBox"><div id="content">
<div id="benefitCircleReviewRed"><img src="img/fa_icons/icon-Comment.svg"></div>
<p>Always professional and friendly. Dedicated to assisting with any problem. Very supportive. Fixed issue(s) quickly and efficiently. Would certainly continue to recommend to others for service.</p>
<p class="font-weight-bold">Essy</p>
</div></div>
</div>

<div class="col-md-4">
<div id="benefitBox"><div id="content">
<div id="benefitCircleReviewBlue"><img src="img/fa_icons/icon-Comment.svg"></div>
<p>Completely independent, works without any need for extra work. Very efficient and effective at working as advertised. Great customer service.</p>
<br>
<p class="font-weight-bold">Rich H.</p>
</div></div>
</div>

<div class="col-md-4">
<div id="benefitBox"><div id="content">
<div id="benefitCircleReviewRed"><img src="img/fa_icons/icon-Comment.svg"></div>
<p>Staff are extremely knowledgeable and friendly. They helped fix my issues in record time. Highly recommend their services. Thanks again! :)</p>
<br>
<p class="font-weight-bold">Chelsea</p>
</div></div>
</div>
</div>
<div class="whiteSpace50"></div>
</div></div></div>

<div id="mainBody"><div id="content">

<div class="whiteSpace50"></div>
<h2 class="font55 text-center">Check Out SiteLock <span class="sourceBlack">Reviews On TrustPilot</span></h2>
<div class="whiteSpace25"></div>
<!-- TrustBox widget - Carousel -->
<div class="trustpilot-widget" data-locale="en-US" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="5415bf7c00006400057a5178" data-style-height="130px" data-style-width="100%" data-theme="light" data-stars="4,5" data-schema-type="Organization">
<a href="https://www.trustpilot.com/review/sitelock.com" target="_blank">Trustpilot</a>
</div>
<!-- End TrustBox widget -->

<div class="whiteSpace50"></div>

<p class="text-center"><a href="#ReviewForm" class="btn btn-red" data-toggle="modal" data-target="#ReviewForm">Leave Your SiteLock Review</a></p>

<div class="whiteSpace50"></div>

</div></div>

<?php include 'includes/awards-bar.php';?>

<div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-6 my-auto">
<br><img class="img90" src="/img/logos/SiteLock_logo_cloud.svg" alt="SiteLock Logo Cloud">
</div>

<div class="col-lg-6">
<h1 class="font55"><span class="sourceBlack">Who</span> is SiteLock?</h1>
<p class="font18">SiteLock is the global leader in website security. Founded in 2008, we protect over 12 million websites worldwide. We’re committed to helping site owners find, fix, and prevent threats. Our solutions are fast, affordable and easy to implement—all backed by 24/7 U.S.-based support.</p>
<p class="text-center"><a class="btn btn-red" href="pricing">Explore Pricing and Plans</a></p>
</div>
</div>
<div class="whiteSpace100"></div>
</div>


<div id="mainRed" style="padding: 0 !important;"><div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-5"><img class="headerimg" src="/img/channel/hostingSupport.png" alt="twenty-four hour support"></div>
<div class="col-lg-7 my-auto">
<h2 class="font35 sourceBlack">Day or night, our friendly, 24/7 U.S.-based support team is here to help via phone, email or live chat.</h2>
<a class="btn btn-ghost-white" href="">(xxx) xxx-xxxx</a>&nbsp;&nbsp;&nbsp; <a class="btn btn-ghost-white" href="contact">Contact</a>&nbsp;&nbsp;&nbsp; <a class="btn btn-ghost-white" href="">chat</a>
<div class="whiteSpace25"></div>
</div>
</div>
</div></div></div>

<div id="mainBody"><div id="content">

</div></div>

<div id="mainBody"><div id="content">
<div class="row">
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><br><img  class="img70" src="img/resources/resourceAwards.svg"><br><h4>See SiteLock<br>Honors and Awards</h4><br><a class="btn btn-ghost-blue" href="awards">See Accolades</a></div><br></div></div>
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="img70" src="img/siteResources.svg"><h4>Check out our<br>Resources Center</h4><br><a class="btn btn-ghost-blue" href="downloads">Start Exploring</a></div><br></div></div>
<div class="col-md-4 text-center"><div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="img70" src="img/qsrRed.svg"><h4>Download the SiteLock<br>Website Security Insider</h4><br><a class="btn btn-ghost-blue" href="security-report">Read the Report</a></div><br></div></div>
</div>
<div class="whiteSpace50"></div>
</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<?php include 'includes/forms/review.html';?>
<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
</body>
</html>