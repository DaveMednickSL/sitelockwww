<!DOCTYPE html>
<html lang="en">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<head>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5DMG67');</script>
  <!-- End Google Tag Manager -->
<?php include($root.'/includes/gt.php'); ?>
<title>Create Your Account</title>
<link rel="icon" type="image/ico" href="favi.ico">
<link rel="canonical" href="https://www.sitelock.com/checkout" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/fontawesome-all.min.css" rel="stylesheet">
<link href="css/checkout.css" rel="stylesheet">
<script>
////////////////////////////////////////////////////////

//API FUNCTIONS
function runAjax( ) {
    // create the form data object to send through the ajax call
    var formData = new FormData();

    // populate the form data
    formData.append("offer_id", document.getElementById('offer_id').value.trim());
    formData.append("new_login", document.getElementById('new_login').value.trim());
    //formData.append("new_website", document.getElementById('new_website').value.trim());
    //formData.append("selected_bundle", document.getElementById('selected_bundle').value.trim());

    if ((apiTrigger) === "bundle") {
        formData.append("selected_bundle", document.getElementById('selected_bundle').value.trim());
        formData.append("new_website", document.getElementById('new_website').value.trim());
    } else if ((apiTrigger) === "vpn") {
        formData.append("selected_vpn", document.getElementById('selected_vpn').value.trim());
    } else {
      console.log("SYSTEM ERROR - API FAULT");
    }

    // create open and send the ajax request
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "https://secure.sitelock.com/checkout/validate_fields_AJAX", false);
    xmlhttp.send(formData);

    // get and return the resonse data
    if (xmlhttp.status === 200) {
        return xmlhttp.responseText;
    }

}

function runValidation() {
    // Send the ajax request and get the response data
    var returnedData = runAjax();

    var info = JSON.parse(returnedData);
    document.getElementById("errors").innerHTML = ""; // clearing any existing errors being displayed for when submitting a second time.

    // Check each of the response options for false values and if false set the error text
    if (info.login_is_available == false) {
        document.getElementById("errors").innerHTML += "<div class='alert alert-danger' role='alert'>The username you’ve selected is not available. Please select an alternative.</div>";
    }
    if (info.domain_is_available == false) {
        document.getElementById("errors").innerHTML += "<div class='alert alert-danger' role='alert'>This domain is already protected with SiteLock services. Login to your SiteLock Dashboard or call us at <a href='tel:8332638629'>833.263.8629</a> to review your account and products.</div>";
    }
    if (info.discount_is_valid == false) {
        document.getElementById("errors").innerHTML += "<div class='alert alert-danger' role='alert'>We’re sorry, the discount code you entered is no longer valid.</div>";
    }
    if (info.plans_are_valid == false) {
        document.getElementById("errors").innerHTML += "<div class='alert alert-danger' role='alert'>Something didn’t work right – give us a call so we can help you out. <a href='tel:8332638629'>833.263.8629</a></div>";
    }

    if (document.getElementById("errors").innerHTML == "") {  // No Errors Found
        return true;
    }

    return false;
}
</script>
<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=321148,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
/* DO NOT EDIT BELOW THIS LINE */
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');
if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body>

<div id="content">
<div class="row" style="margin-top: 15px; margin-bottom: 15px;">
<div class="col-sm-2"></div>

<div class="col-sm-8">
  <img class="img-fluid imgCenter" src="img/logos/SiteLock_red.svg" alt="SiteLock Logo" style="max-width: 274px;">
  <p class="text-center" style="margin-top: 10px; font-size:14px;">International: (415) 390-2500 | Toll-Free: (877) 257-9263</p>
</div>

<div class="col-sm-2">
<div class="float-right" id="7d494ce9-2e39-4a4f-a1ee-be6081ee3bd1">
<script type="text/javascript" src="//privacy-policy.truste.com/privacy-seal/SiteLock,-LLC/asc?rid=7d494ce9-2e39-4a4f-a1ee-be6081ee3bd1"></script><a href="//privacy.truste.com/privacy-seal/SiteLock,-LLC/validation?rid=3fc6f9d5-9959-423e-a8a7-b516ce9bdef4" title="TRUSTe European Safe Harbor certification" target="_blank"><img style="border: none" src="//privacy-policy.truste.com/privacy-seal/SiteLock,-LLC/seal?rid=3fc6f9d5-9959-423e-a8a7-b516ce9bdef4" alt="TRUSTe European Safe Harbor certification"></a>
</div>
</div>
</div>


</div>

<div id="main">
<div class="checkoutPadding" id="content">

<div id="checkoutForm">
<div id="errors"></div>
<div class="row">

<div class="col-md-3"></div>

<div class="col-md-6 checkoutBorder">
<form action="https://secure.sitelock.com/checkout/promo" method="post" onsubmit="return runValidation();">
<h3 class="text-center checkoutHeader"><i class="far fa-user-check"></i> Create Your Account</h3>

<div id="buyid"></div>

<div id="domainvisible"></div>

Your Name<br>
<input class="form-control" type="text" name="new_name" placeholder="John Doe" required>

Pick A Login Username<br>
<input pattern=".{8,}" class="form-control blue" type="text" name="new_login" id="new_login" placeholder="johndoe89 (8 or more characters)" oninvalid="Please ensure your username is at least 8 characters')" title="Please ensure your username is at least 8 characters" required>

Email Address<br>
<input class="form-control" pattern="[-.\w]+\@[\dA-Za-z]+\.[A-Za-z]{2,3}(?:\.[A-Za-z]{2,3})?" type="email" name="new_email" placeholder="johndoe89@gmail.com" oninvalid="email should be formatted as email@domain.com')" title="email should be formatted as email@domain.com" required>

Phone Number<br>
<input class="form-control" type="tel" name="new_phone" placeholder="(877) 257-9263" required>

Country<br>
<select class="form-control" name="new_country">
  <option value="AFG">Afghanistan</option>
  <option value="ALA">Aland Islands</option>
  <option value="ALB">Albania</option>
  <option value="DZA">Algeria</option>
  <option value="ASM">American Samoa</option>
  <option value="AND">Andorra</option>
  <option value="AGO">Angola</option>
  <option value="AIA">Anguilla</option>
  <option value="ATA">Antarctica</option>
  <option value="ATG">Antigua</option>
  <option value="ARG">Argentina</option>
  <option value="ARM">Armenia</option>
  <option value="ABW">Aruba</option>
  <option value="AUS">Australia</option>
  <option value="AUT">Austria</option>
  <option value="AZE">Azerbaijan</option>
  <option value="BHS">Bahamas</option>
  <option value="BHR">Bahrain</option>
  <option value="BGD">Bangladesh</option>
  <option value="BRB">Barbados</option>
  <option value="BLR">Belarus</option>
  <option value="BEL">Belgium</option>
  <option value="BLZ">Belize</option>
  <option value="BEN">Benin</option>
  <option value="BTN">Bhutan</option>
  <option value="BMU">Bermuda</option>
  <option value="BOL">Bolivia</option>
  <option value="BES">Bonaire</option>
  <option value="BIH">Bosnia</option>
  <option value="BWA">Botswana</option>
  <option value="BVT">Bouvet Island</option>
  <option value="BRA">Brazil</option>
  <option value="BGR">Bulgaria</option>
  <option value="BRN">Brunei Darussalam</option>
  <option value="BFA">Burkina Faso</option>
  <option value="BDI">Burundi</option>
  <option value="CPV">Cabo Verde</option>
  <option value="KHM">Cambodia</option>
  <option value="CMR">Cameroon</option>
  <option value="CAN">Canada</option>
  <option value="CYM">Cayman Islands</option>
  <option value="CAF">Central African Republic</option>
  <option value="TCD">Chad</option>
  <option value="CHL">Chile</option>
  <option value="CHN">China</option>
  <option value="CXR">Christmas Island</option>
  <option value="CCK">Cocos Islands</option>
  <option value="COL">Colombia</option>
  <option value="COM">Comoros</option>
  <option value="COG">Congo</option>
  <option value="COD">Congo</option>
  <option value="COK">Cook Islands</option>
  <option value="CRI">Costa Rica</option>
  <option value="CIV">Cote dIvoire</option>
  <option value="HRV">Croatia</option>
  <option value="CUW">Curacao</option>
  <option value="CYP">Cyprus</option>
  <option value="CZE">Czechia</option>
  <option value="DNK">Denmark</option>
  <option value="DJI">Djibouti</option>
  <option value="DMA">Dominica</option>
  <option value="DOM">Dominican Republic</option>
  <option value="ECU">Ecuador</option>
  <option value="EGY">Egypt</option>
  <option value="SLV">El Salvador</option>
  <option value="GNQ">Equatorial Guinea</option>
  <option value="ERI">Eritrea</option>
  <option value="EST">Estonia</option>
  <option value="ETH">Ethiopia</option>
  <option value="FLK">Falkland Islands</option>
  <option value="FRO">Faroe Islands</option>
  <option value="FJI">Fiji</option>
  <option value="FIN">Finland</option>
  <option value="FRA">France</option>
  <option value="GAB">Gabon</option>
  <option value="GMB">Gambia</option>
  <option value="GEO">Georgia</option>
  <option value="DEU">Germany</option>
  <option value="GHA">Ghana</option>
  <option value="GIB">Gibraltar</option>
  <option value="GRC">Greece</option>
  <option value="GRL">Greenland</option>
  <option value="GRD">Grenada</option>
  <option value="GLP">Guadeloupe</option>
  <option value="GUM">Guam</option>
  <option value="GTM">Guatemala</option>
  <option value="GGY">Guernsey</option>
  <option value="GIN">Guinea</option>
  <option value="GNB">Guinea-Bissau</option>
  <option value="GUY">Guyana</option>
  <option value="HTI">Haiti</option>
  <option value="HMD">Heard Island</option>
  <option value="VAT">Holy See</option>
  <option value="HND">Honduras</option>
  <option value="HKG">Hong Kong</option>
  <option value="HUN">Hungary</option>
  <option value="ISL">Iceland</option>
  <option value="IND">India</option>
  <option value="IDN">Indonesia</option>
  <option value="IRQ">Iraq</option>
  <option value="IRL">Ireland</option>
  <option value="IMN">Isle of Man</option>
  <option value="ISR">Israel</option>
  <option value="ITA">Italy</option>
  <option value="JAM">Jamaica</option>
  <option value="JPN">Japan</option>
  <option value="JEY">Jersey</option>
  <option value="JOR">Jordan</option>
  <option value="KAZ">Kazakhstan</option>
  <option value="KEN">Kenya</option>
  <option value="KIR">Kiribati</option>
  <option value="KOR">Korea</option>
  <option value="KWT">Kuwait</option>
  <option value="KGZ">Kyrgyzstan</option>
  <option value="LAO">Lao</option>
  <option value="LVA">Latvia</option>
  <option value="LSO">Lesotho</option>
  <option value="LBR">Liberia</option>
  <option value="LIE">Liechtenstein</option>
  <option value="LTU">Lithuania</option>
  <option value="LUX">Luxembourg</option>
  <option value="MAC">Macao</option>
  <option value="MKD">Macedonia</option>
  <option value="MDG">Madagascar</option>
  <option value="MWI">Malawi</option>
  <option value="MYS">Malaysia</option>
  <option value="MDV">Maldives</option>
  <option value="MLI">Mali</option>
  <option value="MLT">Malta</option>
  <option value="MHL">Marshall Islands</option>
  <option value="MTQ">Martinique</option>
  <option value="MRT">Mauritania</option>
  <option value="MUS">Mauritius</option>
  <option value="MYT">Mayotte</option>
  <option value="MEX">Mexico</option>
  <option value="FSM">Micronesia</option>
  <option value="MDA">Moldova</option>
  <option value="MCO">Monaco</option>
  <option value="MNG">Mongolia</option>
  <option value="MNE">Montenegro</option>
  <option value="MSR">Montserrat</option>
  <option value="MAR">Morocco</option>
  <option value="MOZ">Mozambique</option>
  <option value="MMR">Myanmar</option>
  <option value="NAM">Namibia</option>
  <option value="NRU">Nauru</option>
  <option value="NPL">Nepal</option>
  <option value="NLD">Netherlands</option>
  <option value="NCL">New Caledonia</option>
  <option value="NZL">New Zealand</option>
  <option value="NIC">Nicaragua</option>
  <option value="NER">Niger</option>
  <option value="NGA">Nigeria</option>
  <option value="NIU">Niue</option>
  <option value="NFK">Norfolk Island</option>
  <option value="MNP">Northern Mariana Islands</option>
  <option value="NOR">Norway</option>
  <option value="OMN">Oman</option>
  <option value="PAK">Pakistan</option>
  <option value="PLW">Palau</option>
  <option value="PSE">Palestine</option>
  <option value="PAN">Panama</option>
  <option value="PNG">Papua New Guinea</option>
  <option value="PRY">Paraguay</option>
  <option value="PER">Peru</option>
  <option value="PHL">Philippines</option>
  <option value="PCN">Pitcairn</option>
  <option value="POL">Poland</option>
  <option value="PRT">Portugal</option>
  <option value="PRI">Puerto Rico</option>
  <option value="QAT">Qatar</option>
  <option value="REU">Reunion</option>
  <option value="ROU">Romania</option>
  <option value="RUS">Russian Federation</option>
  <option value="BLM">Saint Barthelemy</option>
  <option value="RWA">>Rwanda</option>
  <option value="SHN">Saint Helena</option>
  <option value="KNA">Saint Kitts</option>
  <option value="LCA">Saint Lucia</option>
  <option value="MAF">Saint Martin</option>
  <option value="SPM">Saint Pierre</option>
  <option value="WSM">Samoa</option>
  <option value="VCT">Saint Vincent</option>
  <option value="SMR">San Marino</option>
  <option value="STP">Sao Tome and Principe</option>
  <option value="SAU">Saudi Arabia</option>
  <option value="SEN">Senegal</option>
  <option value="SRB">Serbia</option>
  <option value="SLE">Sierra Leone</option>
  <option value="SYC">>Seychelles</option>
  <option value="SGP">Singapore</option>
  <option value="SXM">Sint Maarten</option>
  <option value="SVN">Slovenia</option>
  <option value="SVK">Slovakia</option>
  <option value="SLB">Solomon Islands</option>
  <option value="ZAF">South Africa</option>
  <option value="SGS">South Georgia</option>
  <option value="SSD">South Sudan</option>
  <option value="ESP">Spain</option>
  <option value="LKA">Sri Lanka</option>
  <option value="SUR">Suriname</option>
  <option value="SJM">Svalbard and Jan Mayen</option>
  <option value="SWE">Sweden</option>
  <option value="SWZ">Swaziland
  <option value="CHE">Switzerland</option>
  <option value="TJK">Tajikistan</option>
  <option value="TWN">Taiwan</option>
  <option value="TZA">Tanzania</option>
  <option value="THA">Thailand</option>
  <option value="TLS">Timor-Leste</option>
  <option value="TGO">Togo</option>
  <option value="TKL">Tokelau</option>
  <option value="TON">Tonga</option>
  <option value="TTO">Trinidad and Tobago</option>
  <option value="TUN">Tunisia</option>
  <option value="TUR">Turkey</option>
  <option value="TKM">Turkmenistan</option>
  <option value="TCA">Turks and Caicos Islands</option>
  <option value="TUV">Tuvalu</option>
  <option value="UGA">Uganda</option>
  <option value="UKR">Ukraine</option>
  <option value="ARE">United Arab Emirates</option>
  <option value="GBR">United Kingdom</option>
  <option value="USA" selected>United States</option>
  <option value="UMI">United States Minor Outlying Islands</option>
  <option value="URY">Uruguay</option>
  <option value="UZB">Uzbekistan</option>
  <option value="VUT">Vanuatu</option>
  <option value="VEN">Venezuela</option>
  <option value="VNM">Viet Nam</option>
  <option value="VGB">Virgin Islands (British)</option>
  <option value="VIR">Virgin Islands (U.S.)</option>
  <option value="WLF">Wallis and Futuna</option>
  <option value="ESH">Western Sahara</option>
  <option value="YEM">Yemen</option>
  <option value="ZMB">Zambia</option>
  <option value="ZWE">Zimbabwe</option>
</select>

<i id="plus" class="far fa-plus"></i><a class="promoshow" style="cursor: default;" onclick="toggle('offer_id'); toggle('minus'); toggle('plus');"> Enter a promo code</a><br>
<!--<button class="promoshow" onclick="toggle('promoBox');">I have a promo code</button>-->
<input class="form-control" id="offer_id" style="display:none;" type="text" name="offer_id" value="" placeholder="Promo Code">
<hr>
<center><span class="input-group-addon"><input type="checkbox" name="new_promotional" value="yes"></span>
Email me website security news and special offers</center><br>
<center><button class="btn btn-danger oswald" type="submit" id="submit" >CONTINUE TO PAYMENT DETAILS</button></center>
<br>
<p style="font-size: 12px;text-align: center;">By submitting this form you confirm that you have read and accepted our <a href="https://sitelock.com/terms" target="_blank">Terms & Conditions</a> and <a href="https://sitelock.com/privacy-policy" target="_blank">Privacy Policy</a></p>
</form>
</div>

<div class="col-md-3"></div>

</div>
</div>

</div>
</div>

<div id="footer"></div>

<script type="text/javascript">
//VERIFY PLAN ID
var url_string = window.location.href;
  var url = new URL(url_string);
    var id = url.searchParams.get("planID");
    var promo = url.searchParams.get("promo");

    if ((id) === "959") {
        var planname = "SecureStarter";
        var term = "Yearly";
        var annualprice = "$588.00";
        var formname = "selected_bundle";
    } else if ((id) === "960") {
        var planname = "SecureSpeed";
        var term = "Yearly";
        var annualprice = "$888.00";
        var formname = "selected_bundle";
    } else if ((id) === "961") {
        var planname = "SecureSite";
        var term = "Yearly";
        var annualprice = "$1188.00";
        var formname = "selected_bundle";
    } else if ((id) === "969") {
        var planname = "SecureStarter";
        var term = "Yearly";
        var annualprice = "$240.00";
        var formname = "selected_bundle";
    } else if ((id) === "970") {
        var planname = "SecureSpeed";
        var term = "Yearly";
        var annualprice = "$420.00";
        var formname = "selected_bundle";
    } else if ((id) === "971") {
        var planname = "SecureSite";
        var term = "Yearly";
        var annualprice = "$600.00";
        var formname = "selected_bundle";
    } else if ((id) === "963") {
        var planname = "SecureEXPRESS";
        var term = "Yearly";
        var annualprice = "$119.88";
        var formname = "selected_bundle";
    } else if ((id) === "972") {
        var planname = "SecureStarter";
        var term = "Yearly";
        var annualprice = "$239.88";
        var formname = "selected_bundle";
    } else if ((id) === "958") {
        var planname = "SecureALERT";
        var term = "Yearly";
        var annualprice = "$2999.00";
        var formname = "selected_bundle";
    } else if ((id) === "973") {
        var planname = "SecureALERT";
        var term = "Yearly";
        var annualprice = "$0.00";
        var formname = "selected_bundle";
    } else if ((id) === "1010") {
        var planname = "SecureALERT";
        var term = "Monthly";
        var annualprice = "$10.00";
        var formname = "selected_bundle";
    } else if ((id) === "1007") {
        var planname = "SecureSTARTER";
        var term = "Monthly";
        var annualprice = "$30.00";
        var formname = "selected_bundle";
    } else if ((id) === "1008") {
        var planname = "SecureSPEED";
        var term = "Monthly";
        var annualprice = "$50.00";
        var formname = "selected_bundle";
    } else if ((id) === "1009") {
        var planname = "SecureSITE";
        var term = "Monthly";
        var annualprice = "$70.00";
        var formname = "selected_bundle";
    } else if ((id) === "1011") {
        var planname = "SecureALERT";
        var term = "Monthly";
        var annualprice = "$6.00";
        var formname = "selected_bundle";
    } else if ((id) === "1012") {
        var planname = "SecureALERT";
        var term = "Monthly";
        var annualprice = "$0.00";
        var formname = "selected_bundle";
    } else if ((id) === "13179") {
        var planname = "VPN Monthly";
        var term = "Monthly";
        var annualprice = "$0.00";
        var formname = "selected_vpn";
    } else if ((id) === "13180") {
        var planname = "VPN Yearly";
        var term = "Yearly";
        var annualprice = "$0.00";
        var formname = "selected_vpn";
    } else if ((id) === "13181") {
        var planname = "VPN Multi Year";
        var term = "Multi Year";
        var annualprice = "$0.00";
        var formname = "selected_vpn";
    } else {
      window.location.href = 'https://sitelock.com/pricing';
    }
    //document.getElementById("plan").innerHTML = (planname);
    //document.getElementById("price").innerHTML = (annualprice);
    //document.getElementById("term").innerHTML = (term);
    //$("input[name=offer_id]").val("promo");
    document.getElementById("buyid").innerHTML = '<input type="hidden" name="' + (formname) + '" value="' + (id) + '" id="' + (formname) + '">';
    document.getElementById("offer_id").value = promo;

    if ((formname) === "selected_bundle") {
        var apiTrigger = "bundle";
        document.getElementById("domainvisible").innerHTML = 'Domain<br><input pattern="(?:[a-zA-Z0-9-]+\.)?[a-zA-Z0-9.-]+\.[A-Za-z]{2,}(?:\.[A-Za-z]{2,3})?" class="form-control" type="text" name="new_website" id="new_website" placeholder="example.com" oninvalid="format as \'domain.com\', no http:// or https://\')" title="format as \'domain.com\', no http:// or https://" required>';
    } else if ((formname) === "selected_vpn") {
        var apiTrigger = "vpn";
    } else {
      console.log("SYSTEM ERROR - POST FAULT");
    }

////////////////////////////////////////////////////////

//SHOW DISCOUNT CODE FIELD
function toggle(obj) {
    var item = document.getElementById(obj);
    if(item.style.display == 'block') { item.style.display = 'none'; document.getElementById('plus').classList.add('fa-plus');document.getElementById('plus').classList.remove('fa-minus'); }
    else { item.style.display = 'block'; document.getElementById('plus').classList.add('fa-minus');document.getElementById('plus').classList.remove('fa-plus'); }
}
</script>

</body>
</html>
