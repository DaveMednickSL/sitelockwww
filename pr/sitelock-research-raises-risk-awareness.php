<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Research  Raises Risk Awareness for National Cyber Security Awareness Month</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-research-raises-risk-awareness" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Research  Raises Risk Awareness for National Cyber Security Awareness Month</h1>
            <h3 class="font22">AI Based Statistical Model Can Predict Risk of Compromise</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; October 15, 2015</strong></p> 
                <p>Websites with increased popularity and a high number of features are 12 times more likely to be hacked, according to research released to raise awareness during National Cyber Security Awareness Month. SiteLock, the global leader in website security solutions partnered with faculty from the University of Pennsylvania's Wharton School of Business, to analyze one million SiteLock-scanned sites from February through April 2015. The analysis was used to develop an artificial-intelligence-based statistical model that can predict the risk of website compromise for any website.</p>

                <p>The report found that the key feature of a website that predicted its likelihood of compromise was its overall complexity, rather than its specific functionality. As a site offers more features to engage and retain its users, the importance of preventative website security increases.</p>  

                <p>"Websites and web applications are a very visible and vulnerable part of a company's infrastructure, and businesses can no longer assume they're immune from cyber attacks," said Neill Feather, President of SiteLock. "With National Cyber Security Awareness Month under way, these findings remind organizations — from small businesses to enterprises — to protect themselves by frequently checking vulnerabilities."</p>

                <p>The research also found that website visibility, as measured through its social media presence, was associated with the likelihood of compromise. A possible explanation is that a website's social media profile is positively associated with its popularity. The more popular the site, the more attractive it is to an attacker. Despite increased threat from visibility, organizations can successfully mitigate risk by having a proactive solution and securing all access points.</p>

                <p>For this research, SiteLock used both well-established statistical models in their analyses, as well as cutting-edge data-mining/prediction engines. This project was undertaken from February through April 2015. SiteLock had daily data files that included the features of 1.153 million websites from 12/5/2014 through 3/18/2015 and the compromise status from 1/13/2015 to 3/24/2015.</p>

                <p>SiteLock detects malware the minute it hits. After identifying malicious content, it automatically neutralizes and removes the threats. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.</p> 

                <h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 5,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>
                
<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>