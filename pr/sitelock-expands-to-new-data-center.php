<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Expands to New Data Center</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-expands-to-new-data-center" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Expands to New Data Center</h1>
            <hr>

                <p><strong>TEMPE, ARIZONA &mdash; April, 2010</strong></p> 
                <p>On the brink of its two-year anniversary, Internet security firm SiteLock has announced plans to expand its footprint, adding a second data center to support its growth. The company cited its expansion over 60,000 domains protected as a primary driver for this decision. General Manager Josh Lesavoy commented, "The expansion is really driven by our desire to ensure the highest quality scans and performance for our customers. We are thrilled that our message is getting out there and people are adopting our security solutions, driving this need to expand."</p>

                <p>SiteLock General Manager Josh Lesavoy described the partnership as "mutually beneficial" and went on to say, "We're excited to form this partnership with a leader in the hosting space like HostGator. This will provide a great value for their customers, and allows us to continue to provide security to greater numbers of small businesses and sites across the web."</p>

                <h4>About SiteLock</h4>
                <p>SiteLock provides web security services that ensure a safe and productive Internet environment for online businesses. SiteLock solutions provide a comprehensive suite of security products with packages tailored to meet the specific needs of small business. Unlike competitive offerings, SiteLock's Business Verification and Security services are combined to provide the most complete and affordable business security solution available.</p>

                <p>SiteLock's offices are located in Tempe, Arizona. For more information, visit the website at <a href="https://www.sitelock.com">www.sitelock.com</a> or call 877-257-9263.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>