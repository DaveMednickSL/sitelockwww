<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Inc. Magazine Unveils 36th Annual List Of America’s Fastest-Growing Private Companies</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/fastest-growing-company-inc-magazine" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35"><em>Inc.</em> Magazine Unveils 36th Annual List Of America’s Fastest-Growing Private Companies—The Inc.&nbsp;5000</h1>
            <h3 class="font22">SiteLock Ranks No. 760 On The 2017 Inc. 5000 With Three-Year Sales Growth Of&nbsp;598%</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; August 16, 2017</strong></p> 
                <p><em>Inc.</em> magazine today ranked SiteLock number 760 on its 36th annual Inc. 5000, the most prestigious ranking of the nation's fastest-growing private companies. The list represents a unique look at the most successful companies within the American economy’s most dynamic segment— its independent small and midsized businesses. Companies such as Microsoft, Dell, Domino’s Pizza, Pandora, Timberland, LinkedIn, Yelp, Zillow and many other well-known names gained their first national exposure as honorees of the Inc. 5000.</p> 
                <p>“We’re honored to be included in this year’s Inc. 5000 list,” said Neill Feather, president of SiteLock. “It has been both motivating and humbling to watch the company grow from two team members to over 200 employees protecting more than 12 million websites worldwide. We’re proud of our progress and eager to continue on our journey toward protecting every website on the internet.”</p>
                <p>The 2017 Inc. 5000, unveiled online at Inc.com and with the top 500 companies featured in the September issue of Inc. (available on newsstands August 16) is the most competitive crop in the list’s history. The average company on the list achieved a mind-boggling three-year average growth of 481%. The Inc. 5000’s aggregate revenue is $206 billion, and the companies on the list collectively generated 619,500 jobs over the past three years. Complete results of the Inc. 5000, including company profiles and an interactive database that can be sorted by industry, region, and other criteria, can be found at <a href="https://www.inc.com/inc5000" target="_blank">www.inc.com/inc5000</a>.</p>

                <p>"The Inc. 5000 is the most persuasive evidence I know that the American Dream is still alive,” says Inc. President and Editor-In-Chief Eric Schurenberg. “The founders and CEOs of the Inc. 5000 tell us they think determination, risk taking, and vision were the keys to their success, and I believe them.”</p>
                <p>The annual Inc. 5000 event honoring all the companies on the list will be held from October 10 through 12, 2017 at the JW Marriott Desert Springs Resort &amp; Spa in Palm Desert, CA. Speakers include some of the greatest entrepreneurs of this and past generations, such as former Ford president Alan Mullaly, FUBU CEO and founder and “Shark Tank” star Daymond John, Dollar Shave Club founder Michael Dubin, researcher and #1 New York Times bestseller Brené Brown, and Gravity Payments’ founder and CEO Dan Price.</p>
                <p>
 
				<h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

                <h5>Contact</h5>
                <p>Katie Pesek<br /><a href="tel:7037951928">703.795.1928</a><br /><a href="mailto:pesek@merrittgrp.com">pesek@merrittgrp.com</a></p>

				<h4>More About Inc. And The Inc. 5000</h4>
                <h5>Methodology</h5>
                <p>The 2017 Inc. 5000 is ranked according to percentage revenue growth when comparing 2013 to 2016. To qualify, companies must have been founded and generating revenue by March 31, 2013. They had to be U.S.-based, privately held, for profit, and independent—not subsidiaries or divisions of other companies—as of December 31, 2016. (Since then, a number of companies on the list have gone public or been acquired.) The minimum revenue required for 2013 is $100,000; the minimum for 2016 is $2 million. As always, Inc. reserves the right to decline applicants for subjective reasons. Companies on the Inc. 500 are featured in Inc.'s September issue. They represent the top tier of the Inc. 5000, which can be found at <a href="https://www.inc.com/inc5000" target="_blank">https://www.inc.com/inc5000</a>.</p>

                <h5About Inc. Media</h5>
                <p>Founded in 1979 and acquired in 2005 by Mansueto Ventures, Inc. is the only major brand dedicated exclusively to owners and managers of growing private companies, with the aim to deliver real solutions for today's innovative company builders. Winner of the National Magazine Award for General Excellence in both 2014 and 2012. Total monthly audience reach for the brand has grown significantly from 2,000,000 in 2010 to over 18,000,000 today.  For more information, visit www.inc.com.</p>
                <p>The Inc. 5000 is a list of the fastest-growing private companies in the nation. Started in 1982, this prestigious list of the nation's most successful private companies has become the hallmark of entrepreneurial success. The Inc. 5000 Conference &amp; Awards Ceremony is an annual event that celebrates their remarkable achievements. The event also offers informative workshops, celebrated keynote speakers, and evening functions.</p>
                <p>For more information on Inc. and the Inc. 5000 Conference, visit <a href=" http://conference.inc.com/" target="_blank">http://conference.inc.com/</a>.</p>
                <h5>For More Information Contact:</h5>
                <p>Inc. Media<br />Drew Kerr<br /><a href="tel:2128498250">212.849.8250</a><br /><a href="mailto:drew@four-corners.com">drew@four-corners.com</a></p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>