<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Parallels APS-Certified Application</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-aps-certified-parallels-application" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Parallels APS-Certified Application</h1>
            <h3 class="font22">Package Gives Service Providers Access to Comprehensive Web Site Security</h3>
            <hr>

                <p><strong>ORLANDO, FLORIDA and JACKSONVILLE, FLORIDA &mdash; February 16, 2012</strong></p> 
                <p>SiteLock LLC (www.sitelock.com), a global leader in Web site security solutions for online businesses, today announced a partnership with Parallels, the hosting and cloud services enablement leader. This agreement makes SiteLock's award-winning cloud-based security package available to more than 5,000 Parallels service providers and resellers, who serve more than 10 million small and medium-sized businesses (SMBs). The announcement was made in Orlando during Parallels Summit 2012, an event that attracts a global audience of over 1,200 leading participants from across the cloud and hosting industry.</p>
                
				<p>APS-certified applications create valuable new revenue opportunities for the hosting and Web site security markets. Once packaged as an APS-certified plug-in by a software vendor, an application can easily be integrated into the infrastructure of any hosting provider using Parallels Automation or Parallels Plesk Panel. The ability to quickly and easily add new, attractive APS packages enables service providers to generate higher average revenue per customer.</p>
						
				<p>"SiteLock is very excited to launch this partnership with Parallels via our new APS-certified package," said Neill Feather, President of SiteLock." With the number of malware attacks continuing to grow each day, providing comprehensive, trustworthy Web site security solutions to small business has never been more important. We place great value on protecting not only SiteLock subscribers, but also their customers. Their protection is proven whenever the SiteLock Trust Seal is displayed. Partnering with Parallels through APS is a great way for us to extend our reach and deliver this valuable service through a platform that's convenient for our small business audience."</p>
				
				<p>For additional information about the Parallels APS ecosystem, visit <a href="http://www.parallels.com/products/saas/isv/" target="_blank">www.parallels.com/products/saas/isv/</a>. For additional information about SiteLock Web site security technology and services, visit www.sitelock.com.</p>
					
				<h4>About SiteLock</h4>
				<p>Established in 2008, SiteLock has helped small businesses protect their Web sites and reputations through Web site security services and Web site scanning. SiteLock offers the most affordable and complete Web site security solution available on the market, protecting against malware, spam, viruses and other vulnerabilities. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million pages daily for threats. Many of these customers are online merchants that rely on their Web site as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona.  For more information, visit www.SiteLock.com or call 877-257-9263.</p>
                
<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>