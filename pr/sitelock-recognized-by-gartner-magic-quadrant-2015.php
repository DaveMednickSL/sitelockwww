<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Has Been Recognized by Gartner As Part Of Its 2015 Magic Quadrant for Application Security Testing</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-recognized-by-gartner-magic-quadrant-2015" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Has Been Recognized by Gartner As Part Of Its 2015 Magic Quadrant for Application Security Testing</h1>
            <h3 class="font22">Report Reveals Niche Players Address Subsets More Efficiently of Overall Market</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; September 9, 2015</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a>, the global leader in website security solutions, announced today that they have been positioned in the Gartner Inc.'s 2015 Magic Quadrant for Application Security Testing for the first time.</p>

                <p>Application security testing (AST) is an integral component of security as it analyzes and tests applications for security compromises. There are various types of AST products and services which can be delivered as a tool or subscription service. With data breaches making headlines daily, the need to identify and address vulnerabilities is at an all-time high.</p>

                <p>Positioned as a Niche Player, the report notes these organizations "offer viable, dependable solutions that meet the needs of specific buyers" and they "may address subsets of the overall market, and often can do so more efficiently than the Leaders." They also "fare well when considered for business and technical cases that match their focus."</p>

                <p>"We provide optimal Web application security testing combining both DAST and SAST analysis to midsize customers," notes Neill Feather, President of SiteLock. "SiteLock believes that being recognized in the Magic Quadrant for Application Security Testing for the first time underscores our ability to provide real solutions and excellent customer service to our clients, and that SiteLock's specialization in SaaS delivery of comprehensive testing for web apps results in a unique offering."</p>

                <p>According to Gartner, "Enterprises are increasingly understanding the need to implement a comprehensive, life cycle approach to application security…. They tend to pick Niche Players when the focus is on a few important functions, on specific vendor expertise, or when they have an established relationship with the vendor."</p>

                <p>SiteLock detects malware the minute it hits. After identifying malicious content, it automatically neutralizes and removes the threats. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.</p> 

                <p>The report evaluated vendors on 12 criteria based on an ability to execute and on completeness of vision. For further insight, view the full Gartner Magic Quadrant for Application Security Testing report <a href="https://www.sitelock.com/gartner">here</a>.</p>

                <cite>Reference: Gartner, Magic Quadrant for Application Security Testing – Neil MacDonald, Joseph Feiman. August 6, 2015</cite>

                <h4>About the Magic Quadrant</h4> 
                <p>Gartner does not endorse any vendor, product or service depicted in its research publications, and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner's research organization and should not be construed as statements of fact. Gartner disclaims all warranties, expressed or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the global leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company currently protects over 5 million websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>