<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Offers Customers Payment Card Industry (PCI) Compliance</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-offers-payment-card-industry-compliance" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Offers Customers Payment Card Industry (PCI) Compliance</h1>
            <h3 class="font22">SiteLock Adds Online Credit Card Transactions to its List of Website Security Offerings</h3>
            <hr>

            <p><strong>SCOTTSDALE, ARIZONA &mdash; June 6, 2012</strong></p> 
            <p>SiteLock LLC (www.sitelock.com), a global leader in website security solutions for small and midsize businesses, announced today that it will offer online merchants the ability to obtain Payment Card Industry (PCI) Compliance.

            <p>PCI Compliance is a security requirement created by major credit card brands in an attempt to reduce fraud and bring all companies who accept credit cards up to a standard level of security. Online businesses are now able to achieve compliance with SiteLock PCI, a software solution that guides merchants through a very simple online application process, including the completion of the PCI Self-Assessment Questionnaire (SAQ) and network vulnerability scanning.</p>

            <p>SiteLock's PCI program includes a user-friendly, streamlined version of the Self-Assessment Questionnaire; a dynamically created security policy; a remediation plan; and guides for setup on businesses' network environments.</p> 
	                            
	       <p>The challenge faced by many small business owners to achieve compliance is the time and complexity of the traditional application process. The SiteLock PCI process is designed specifically to help business owners complete the process in four guided online steps.</p>

	       <p>"PCI compliance is a natural extension to SiteLock's existing line of website security technology and services," explains Neill Feather, President of SiteLock. "We are well aware of the costly penalties that companies can face when they are found to be out of compliance with PCI regulations, so we want to make it easy for online businesses to manage all of their security solutions in one place for a fraction of the standard price. When companies become compliant, they not only prevent fines, but also provide their customers with trust in their business - which is one of our primary goals at SiteLock."</p>

	       <p>SiteLock protects small businesses. The rapid acceleration of malware development and malicious attacks on business websites is making this a near epidemic challenge that puts small and midsize companies at the greatest risk for website security threats.</p>

            <p>For additional information about SiteLock website security technology and services, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

	       <h4>About SiteLock</h4>
            <p>Established in 2008, SiteLock has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam and other forms of threats from hackers. The SiteLock Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
