<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock and IX Web Hosting Unite to Secure New Online Businesses</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-and-ix-web-hosting-unite-to-secure-new-online-businesses" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock and IX Web Hosting Unite to Secure New Online Businesses</h1>
            <h3 class="font22">IX Web Hosting Now Includes SiteLock Website Security in Hosting Plans</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; July 8, 2014</strong></p> 
                <p>SiteLock LLC <a href="https://www.sitelock.com">(www.sitelock.com)</a>, a global leader in website security and PCI Compliance, announced today that IX Web Hosting now offers SiteLock to current and new customers of their premium shared hosting plans.</p>

                <p>IX Web Hosting has been providing complete website hosting and online services for 15 years and serves more than 115,000 customers with 470,000 unique websites. IX Web Hosting specializes in supporting companies who use common web platforms such as WordPress, Drupal, and Joomla – running on Linux or Windows. They also include tools such as site builders and Ecommerce plans at no additional cost.</p>

                <p>Customer service is a key focus for IX Web Hosting, and they include 24 x 7 support through multiple channels to make it easy to get help. Many small business owners are not highly technical and IX Web Hosting provides a simple control panel and unlimited bandwidth, disk space and other resources so their users can focus on their business without worrying about their website.</p>

                <p>The addition of SiteLock website security services is the perfect fit for customer peace of mind. Because IX Web Hosting cares about creating a safe and secure online experience for website owners and their visitors, they offer the SiteLock basic plan at sign-up, which includes daily malware scans that provide instant notification when malware is detected. Customers can upgrade to plans that include automatic malware removal and a web application firewall as their online business needs change.</p>

                <p>“At IX Web Hosting, we deliver premium services that have everything our customers need to run their online business without worries about technology or limitations,” states Michael Harris, Vice President of Information Technology, IX Web Hosting. “We realize that online risks are rapidly increasing, and it’s very important that we do our part to keep our customers’ websites and their visitors secure. And we chose to partner with SiteLock, as the global leader in website security, to protect our customers.”</p>

                <p>“The approach that IX Web Hosting takes to providing their customers with easy-to-use products and services is directly in-line with our goals at SiteLock,” adds Tom Serani, Executive Vice President of Business Development for SiteLock. “The challenge is that, the more common and widely used the web platform, the more vulnerable it is to threats. It’s for this reason that SiteLock places such a high priority on securing websites across all popular, as well as custom, hosting and website development platforms.”</p>

                <p>IX Web Hosting uses h-Sphere, cPanel and Plesk to integrate and offer SiteLock services to their customer base.</p>

                <p>For additional information regarding the SiteLock website security suite, visit www.sitelock.com. For additional information about IX Web Hosting, visit <a href="http://www.ixwebhosting.com" target="_blank">www.ixwebhosting.com</a>.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a hef="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>