<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock to Promote National Cyber Security Awareness Month with National Cyber Security Awareness</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-to-promote-national-cyber-security-awareness />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock to Promote National Cyber Security Awareness Month with National Cyber Security Awareness</h1>
            <h3 class="font22">SiteLock Will Help Educate on the Importance of Website Security</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; September 28, 2012</strong></p>
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a leading provider of website security solutions for online businesses around the world, announced today that it has volunteered to be a National Cyber Security Awareness Month champion as part of the company's ongoing commitment to cyber security.</p>

				<p>Organized through the National Cyber Security Alliance, National Cyber Security Awareness Month (NCSAM) is held in October 2012 and takes the time to examine the importance of maximizing digital security in a world that is becoming more web-based by the day. The premise of NCSAM focuses around "Stop. Think. Connect." which encourages Internet users to stop and take the time to learn about risks, think about the paths of their online actions, and connect only with the confidence that the right steps have been taken to safeguard against dangers. Each week of October, NCSAM focuses on a different cyber security issue, including digital literacy, law enforcement and business website safety.</p>

				<p>As an NCSAM champion, SiteLock is planning upcoming events and activities to help reach and educate customers, employees and communities about the importance of maintaining a safe and secure online presence.</p> 

				<p>"We're extremely excited to be able to participate in National Cyber Security Awareness Month with StaySafeOnline.org," says Neill Feather, President of SiteLock. "Our whole company is focused on teaching others about different online threats and making sure their online data is as safe as possible. We are looking forward to contributing our knowledge of the industry to the cause."</p>
 
				<p>For additional information about SiteLock website security technology and services, visit www.sitelock.com.</p>

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (www.sitelock.com / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security and PCI compliance solutions available on the market. SiteLock helps protect websites against malware, spam, viruses and other forms of online threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 3 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

				<h4>About National Cyber Security Alliance</h4>
                <p>The National Cyber Security Alliance is a non-profit organization. Through collaboration with the government, corporate, non-profit and academic sectors, the mission of the NCSA is to empower a digital citizenry to use the Internet securely and safely protecting themselves and the technology they use and the digital assets we all share. NCSA works to create a culture of cyber security and safety through education and awareness activities. NCSA board members include: ADP, AT&T, Bank of America, EMC Corporation, ESET, Facebook, Google, Intel, McAfee, Microsoft, PayPal, Science Applications International Corporation (SAIC), Trend Micro, Symantec, Verizon and Visa. Visit www.staysafeonline.org or www.stopthinkconnect.org for more information.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>