<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Ranked Arizona’s Fastest Growing Software Company</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/deloitte-fastest-growing-company" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Ranked Arizona’s Fastest Growing Software Company on Deloitte’s 2017 Technology Fast 500™ For Third Year in a Row</h1>
            <h3 class="font22">Attributes Growth to Continued Product Innovation and Focus on Direct Business</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; November 9, 2017</strong></p> 
                <p>SiteLock today announced it was named to the Deloitte Technology Fast 500™, a ranking of the 500 fastest growing technology, media, telecommunications, life sciences and energy tech companies in North America. The company also garnered recognition as Arizona’s fastest growing software company for the third consecutive year, and earned distinction as the fastest growing company in Arizona overall, boasting a growth rate of 603 percent during a three-year period.</p> 
                <p>SiteLock President, Neill Feather, credits the company’s continued growth and recognition to a deliberate focus on product innovation and targeted customer acquisition strategy, both driven by exceptional customer service and support. Software innovations include SMART® DataBase, a scanning tool designed to automatically find and remove known malware from WordPress databases, and the SiteLock Risk Assessment, a proprietary predictive model used to determine a website’s likelihood of attack.</p> 
                <p>“We are honored to be included once again in the Deloitte Technology Fast 500. Earning recognition as the fastest growing software company in Arizona for the third consecutive year is a direct result of our continued focus on addressing a growing need in the market, and delivering superior technology specifically designed to help small and medium sized businesses thrive on the internet.” said Feather. “Our mission is to protect every website on the internet. As cyber threats and breaches continue to dominate headlines, helping website owners become proactive about website security has never been more important.” </p>
                <p>“The Deloitte 2017 North America Technology Fast 500 winners underscore the impact of technological innovation and world class customer service in driving growth, in a fiercely competitive environment,” said Sandra Shirai, vice chairman, Deloitte Consulting LLP and U.S. technology, media and telecommunications leader. “These companies are on the cutting edge and are transforming the way we do business. We extend our sincere congratulations to all the winners for achieving remarkable growth while delivering new services and experiences for their customers.”</p>
                <p>“Emerging growth companies are powering innovation in the broader economy. The growth rates delivered by the companies on this year’s North America Technology Fast 500 ranking are a bright spot for the capital markets and a strong indicator that the emerging growth technology sector will continue to deliver a strong return on investment,” said Heather Gates, national managing director of Deloitte &amp; Touche LLP’s emerging growth company practice. “Deloitte is dedicated to supporting the best and brightest companies of the future in the emerging growth company sector. We are proud to acknowledge the significant accomplishments of this year’s Fast 500 winners.” </p>
                <p>SiteLock was included on the 2015 and 2016 Deloitte Technology Fast 500™ and also recognized as the fastest growing software company in Arizona both years as well. </p>
				
                <h5>About Deloitte’s 2017 Technology Fast 500™</h5>
                <p>Deloitte’s Technology Fast 500 provides a ranking of the fastest growing technology, media, telecommunications, life sciences and energy tech companies — both public and private — in North America. Technology Fast 500 award winners are selected based on percentage fiscal year revenue growth from 2013 to 2016.</p>
                <p>In order to be eligible for Technology Fast 500 recognition, companies must own proprietary intellectual property or technology that is sold to customers in products that contribute to a majority of the company's operating revenues. Companies must have base-year operating revenues of at least $50,000 USD, and current-year operating revenues of at least $5 million USD. Additionally, companies must be in business for a minimum of four years and be headquartered within North America.</p>
                <p>Deloitte refers to one or more of Deloitte Touche Tohmatsu Limited, a UK private company limited by guarantee (“DTTL”), its network of member firms, and their related entities. DTTL and each of its member firms are legally separate and independent entities. DTTL (also referred to as “Deloitte Global”) does not provide services to clients. In the United States, Deloitte refers to one or more of the US member firms of DTTL, their related entities that operate using the “Deloitte” name in the United States and their respective affiliates. Certain services may not be available to attest clients under the rules and regulations of public accounting. Please see www.deloitte.com/about to learn more about our global network of member firms.</p>
           
                <h4>About SiteLock</h4>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million customers worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>