<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Partners with Dominios</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-partnership-with-dominios" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">­SiteLock Partners with Dominios.pt to Integrate Website Security Within Hosting Services</h1>
            <h3 class="font22">Dominios.pt to offer full suite of website security solutions</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; January 17, 2018</strong></p> 
                <p>SiteLock, the global leader in website security solutions, announced today its partnership with Dominios.pt, the Portuguese market leader in domain registration and hosting services. The SiteLock security portfolio offered by Dominios.pt will provide security monitoring and prevention for all current and new customers, including access to Find, Fix & Prevent and SiteLock 911, the company’s industry leading prevention and remediation products.</p>
				<p>"Dominios.pt takes the website security of our customers very seriously and are excited to partner with SiteLock to provide security protection across all of our product ranges,” said Nuno Matias, General Manager of Dominios.pt. “Online threats continue to increase dramatically on a daily basis and our focus is to guarantee the highest level of security to all our customer’s websites and their visitors. A partnership with SiteLock, the global leader in website security, was a natural decision.”</p>
                <p><a href="https://www.sitelock.com/blog/2017/12/website-security-insider-q3-2017/" rarget="_blank">Recent data indicates</a> that websites experience on average, 59 attacks per day. By providing security monitoring to all Dominios.pt customers, SiteLock can work to educate customers on the importance of good website security practices. Find, Fix & Prevent will offer customers malware scanning and remediation services, as well protects against the top 10 Open Web Application Security Project (OWASP) threats, while SiteLock 911 can remove malware immediately from infected websites. As a leader in domain registration and hosting services, Dominios.pt provides an excellent opportunity to promote website security solutions.</p>
                <p>“We are excited to partner with Dominios.pt and offer website security solutions to their customers,” said Tom Serani, Executive Vice President of Business Development for SiteLock. “With security monitoring and access to Find, Fix & Prevent and SiteLock 911, SiteLock is able to offer Dominios.pt customers a proactive security solution package to ensure their websites run safely and smoothly. Through combined efforts, we can make it easy for Dominios.pt to integrate security into its hosting services and prevent against future attacks.”</p>

                <h5>About SiteLock</h5>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>
           
                <h5>About Dominios.pt </h5>
                <p>Dominios.pt has been operating in the Portuguese market since 2001, and is currently the market leader in domain registration, website hosting and servers. The management of our servers and our technical support service are provided by our own team, present in the same building of the datacenter, which allows us a faster response and a better quality in the service and services available.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>