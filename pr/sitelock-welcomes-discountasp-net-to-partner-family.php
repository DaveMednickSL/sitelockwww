<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Website Security Leader, SiteLock, Welcomes DiscountASP.NET to the Partner Family</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-welcomes-discountasp-net-to-partner-family" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font22">Website Security Leader, SiteLock, Welcomes DiscountASP.NET to the Partner Family</h1>
            <hr>
                <p><strong>SCOTTSDALE, ARIZONA &mdash; March 20, 2013</strong></p> 
                <p>SiteLock LLC (www.sitelock.com), a global leader in website security solutions for online businesses, announced today that DiscountASP.NET has joined the partner program and will offer SiteLock to its extensive web developer community.</p>

	            <p>DiscountASP.NET is an ASP.NET hosting provider that supports the latest Microsoft web stack for building powerful web sites and web applications on the Windows platform. They built their own hosting automation system and control panel using .NET, which demonstrates their expertise in the .NET environment. The technology is highly flexible, which enables them to launch new Microsoft technologies faster than through many standard control panels that require significant development effort to add new applications.</p>

	            <p>The speed at which DiscountASP.NET enables developers to deliver new applications increases the need for security around those applications and websites. DiscountASP.NET will now offer the SiteLock website security solutions to their customers to protect their users from malware, hackers, cross-site scripting, and many other forms of attack. The addition of SiteLock security package to DiscountASP.NET’s service portfolio provides an unprecedented level of protection for their customers and partners.</p>

	            <p>“Our development community relies on us to provide them quick, innovative technologies to support their web applications,” states Takeshi Eto, VP Marketing and Business Development at DiscountASP.NET. “Our relationship with SiteLock will now enable us to also protect our customers and give them peace of mind that they are producing their applications in a secure environment, as well as creating a safe experience for their online customers.”</p>

	            <p>“Online security is gaining attention and priority because online risks continue to increase, as do the sources that generate them,” adds Tom Serani, Executive Vice President of Business Development for SiteLock. “Complete security starts with a clean, safe environment for developers to produce good code, and we’re thrilled to extend this level of protection to the global DiscountASP.NET community.”</p>

	            <p>The partnership will bring a more safe and secure environment for the .NET developer community and their users in the online marketplace as well as increase the website security footprint for SiteLock throughout DiscountASP.NET’s US and European presence. DiscountASP.NET will integrate with SiteLock through the OpenSRS API to provide website security products to their customers.</p>

                <p>For additional information regarding the SiteLock website security suite, visit www.sitelock.com. For additional information about DiscountASP.NET, visit <a href="http://www.discountasp.net" target="_blank">http://www.discountasp.net</a>. </p> 

	            <h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (@SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security and PCI compliance solutions available on the market. SiteLock helps protect websites against malware, spam, viruses and other forms of online threats from hackers. SiteLock's Expert Services team specializes in malware removal and hardening website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 10+ million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">https://www.sitelock.com</a>.</p>

	            <h4>About DiscountASP.NET</h4>
              <p>DiscountASP.NET is a leader in hosting solutions for Microsoft developers. A Microsoft Partner with the Gold Hosting Competency and a Visual Studio Partner member, DiscountASP.NET offers both shared and managed Team Foundation Server hosting solutions. As a leader in cutting-edge ASP.NET web hosting, DiscountASP.NET supports the latest Microsoft web stack including Windows 2012. Through strong word-of-mouth and their commitment to ASP.NET developers, DiscountASP.NET has become the choice of more than 30,000 customers.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>