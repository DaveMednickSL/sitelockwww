<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Industry Leaders Join Forces to Introduce New Sales Channel for eCommerce Businesses via Secure Chat</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/industry-leaders-join-forces-to-introduce-new-sales-channel" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">Industry Leaders Join Forces to Introduce New Sales Channel for eCommerce Businesses via Secure Chat</h1>
            <h3 class="font22">SiteLock, WebsiteAlive and SecureBuy Form Partnership to Introduce AliveSecure</h3>
            <hr>

            <p><strong>SCOTTSDALE, ARIZONA &mdash; August 27, 2013</strong></p> 
            <p>SiteLock, WebsiteAlive and SecureBuy, in a joint statement today, announced the release of AliveSecure, an innovation in secure online communications and transaction processing. They are launching the new technology as an online Software-as-a-Service (SaaS) designed to secure credit card payments through a website or mobile chat interface.</p>

              <p>AliveSecure is a combined effort of development and custom integration of patented and patent-pending products provided by SiteLock, the global leader in website security; WebsiteAlive, a leading provider of online chat and click-to-call communications; and SecureBuy, the global leader in credit card fraud prevention technology. The PCI Compliant SaaS solution is the first to market and only service available that automates 100% of secure payment processing steps – from point of purchase through signature – in an online environment.</p>

              <p>According to the “2013 Online Fraud Report” recently published by CyberSource Corporation, a Visa company, $3.5 billion is estimated as lost revenue this year due to online payment fraud. The report also indicates that the average ticket value for a fraudulent order is $200, approximately 25% greater than the average value for valid transactions. It further claims that eCommerce sales by the end of 2013 are expected to increase 12% compared to last year. Add to that the consumer impact and their demand for a safe, yet convenient, online purchase process. Nearly 820 million consumers globally were victims of online fraud in 2011. And as indicated in recent research conducted by WorldPay, 75% of consumers identify security of their personal and financial information as their number one priority. This data suggests that the potential revenue loss for many online businesses could severely cripple, if not close them, and consumers are prepared to do whatever it takes to protect their information.</p>

              <p>To address this risk of revenue loss resulting in chargebacks and credits issued, AliveSecure manages the entire checkout process in a CHAT session that is held online or via a web-enabled mobile session. Within the chat session, the customer is prompted to enter their credit card information in a secure environment. The credit card data is verified using a risk-based authentication process and then the card data is tokenized to avoid the need for companies to store full credit card data. Upon completion of the payment step, the customer is prompted with a form that captures the biometric signature of the customer, which is also securely stored as part of the transaction. In addition to the revenue loss prevention, the automated solution can reduce staffing costs and research time required to manually investigate fraudulent and suspicious transactions.</p>

              <p>“At SiteLock, we see thousands of websites impacted by online threats, including potential fraud, every day,” states Neill Feather, President of SiteLock. “So, we are thrilled to be part of this unique solution for online interactions that include payments, which helps protect our customers from revenue loss.”</p>

              <p>“WebsiteAlive continuously looks for ways to extend the value of online conversations by providing a convenient method for consumers to buy online and an effective sales channel for eCommerce companies,” continues Adam Stass, CEO of WebsiteAlive. “Adding the integration of security and payment processing to online communications is the logical next step for growing eCommerce businesses that protects their customers at the same time.”</p>

              <p>“SecureBuy is all about game changing technology that stabilizes commerce,” adds Greg Wooten, CEO of SecureBuy. “We are very excited about our participation in AliveSecure. With our history of payment fraud prevention through online and mobile commerce, the extension of our proprietary technology to online chat and click-to-call conversations opens exciting new revenue opportunities for online businesses.”</p>

              <p>The newly integrated product, AliveSecure will be available first to current customers of the three companies and will follow shortly thereafter to broader markets. Learn more about this groundbreaking sales channel at <a href="http://www.AliveSecure.com" target="_blank">http://www.AliveSecure.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>