<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Receives Best Website Security Award from Hosting-Review</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-receives-best-website-security-award" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Receives Best Website Security Award from Hosting-Review</h1>
            <h3 class="font22">Hosting-Review Presented the Best in Show Awards and Honors SiteLock</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; November 7, 2013</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a>, a global leader in website security solutions for online businesses, announced today that it is honored to accept the Best Website Security Award from Hosting-Review.</p> 

                <p>Hosting-Review presented “Best in Show” awards in categories including, but not limited to: best windows web hosting; best value web hosting; best secure hosting, in addition to best website security. SiteLock accepted the award at HostingCon 2013 in Austin, Texas in June. The award winners were selected based on technology, reliability, performance, customer feedback, and value among other key factors.</p>

                <p>SiteLock was recognized as a leader in website security and selected for the award because of their commitment to offering reliable and high-quality services to their more than 1,000,000 customers. SiteLock partners with leading web hosting and online service providers to continually educate their customers about online threats and how they can protect their websites and visitors. SiteLock’s comprehensive services include website scanning to find malware and vulnerabilities, automatic malware removal, web application firewall and a global content delivery network, as well as PCI compliance. When businesses care about their website and use it to drive customer engagement and online purchases, having website security is not optional, it’s critical.</p>

                <p>At the rate cybercrime is currently increasing, roughly 26% year-over-year, according to the recent Ponemon Institute report, “Cost of Cyber Crime”, for many small businesses it’s not a question of “if” they will come under attack, it’s more likely “when”. And because there are so many online small businesses, the pool of targets is nearly limitless for hackers. While for online shoppers, the presence of a website security tool and/or their trust seal is often a distinguishing factor as they determine where to browse and buy. SiteLock has conducted numerous studies among their customer base and seen consistent 10-15% increases in lead and sales conversions.</p>

                <p>“We are honored to accept the Best Website Security Award from Hosting-Review,” says Neill Feather, president of SiteLock. “We view the award as a source of external validation of our continuous focus on protecting online businesses and helping them succeed.”</p>

                <p>“SiteLock is clearly a leader in website security services and offers a variety of tools to help small and midsize businesses evade potential threats, while not affecting a website’s performance,” describes Dave Price, senior partner and manager at Hosting-Review. “We congratulate SiteLock on their award as well as their dedication to delivering website security tools to the growing SMB market at an affordable price.”</p>

                <p>Learn more about SiteLock at www.sitelock.com. Learn more about the Hosting-Review Best of Show awards at <a href="http://www.hosting-review.com/awards/best-in-show-2013.shtml" target="_blank">http://www.hosting-review.com/awards/best-in-show-2013.shtml</a>.</p>  

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @sitelock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%.  As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About Hosting-Review</h4>
                <p>Hosting-Review (<a href="https://www.hosting-review.com" target="_blank">www.hosting-review.com</a>) is an independent source for finding the best web host to match online business needs. There are hundreds of web hosts available on the marketplace, and the choice can be very confusing. Hosting-Review is here to act as a complete source, and our professional webmasters regularly review hosting companies and their packages. We are looking for safe, reliable web hosts that offer excellent value to our members.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>