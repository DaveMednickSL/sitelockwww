<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock WordPress Plugin Available for TrueShield<</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-wordpress-plugin-available-for-trueshield" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock WordPress Plugin Available for TrueShield</h1>
            <h3 class="font22">New Plugin Launched for TrueShield, Web Application Firewall</h3>
            <hr>
                <p><strong>SCOTTSDALE, ARIZONA &mdash; June 3, 2013</strong></p> 
                <p>SiteLock LLC, the global website protection company, announced the launch of its WordPress plugin for use with its latest website security product, TrueShield, a web application firewall. Just announced in March of this year, TrueShield is already protecting thousands of websites from targeted and malicious attacks. The new plugin ensures that WordPress websites run optimally when using the SiteLock firewall.</p>

	            <p>When website owners use the SiteLock WordPress plugin, they will experience no change to the originating IP address of website visitors while also using TrueShield. The plugin sets the value of $_SERVER['REMOTE_ADDR'] according to the client IP value reported by TrueShield. The plugin will automatically position itself to execute first so that all other plugins that require the originating IP continue to operate as expected.</p>

	            <p>“With more than 66 million WordPress sites in the world, we know that it’s a very popular platform for new and emerging online business owners, including our customers, to use,” states Neill Feather, President of SiteLock. “And more than 6 times that number represent the number of people who visit WordPress sites. The volume of activity, combined with the WordPress platform’s ease of use, makes these sites prime targets for attacks. Because we believe that the web should be a secure environment for online businesses and their customers, we place high value and priority on making sure that our security tools and services integrate easily in the WordPress environment and protect their users and visitors."</p>

	            <p>TrueShield, the SiteLock web application firewall, requires only a simple DNS change and it instantly becomes a reverse proxy for all incoming website traffic. Its purpose is to identify human versus bot traffic, and further distinguish between good bots, like search engines, and malicious bots. TrueShield prevents malicious bots and a multitude of other attacks on websites to keep the website and the visitor experience secure. </p>

                <p>Version 1.2 of the SiteLock WordPress plugin is available for <a href="http://wordpress.org/plugins/sitelock/">download</a>. To learn more about SiteLock‘s complete website protection, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

	            <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%.  As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
