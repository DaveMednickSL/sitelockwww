<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Named One of the 2017 '100 Best Companies in Arizona</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-named-one-of-2017-best-companies" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Named One of the 2017 '100 Best Companies in Arizona</h1>
            <hr>

                <p><strong>PHOENIX, ARIZONA &mdash; March 2, 2017</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a>, a leader in website security solutions, today announced that it has been named one of the '100 Best Companies in Arizona' by BestCompaniesAZ as part of its 15th anniversary serving Arizona's business community. SiteLock is recognized within the "Best of Cool" category, which honors Arizona's top organizations who exemplify strong, unique corporate cultures that create exceptionally happy workplaces through creativity and innovation.</p>

               <p>"When you walk through our office, it's clear we have a unique workplace that embodies our corporate culture," said Neill Feather, president of SiteLock. "From our open floor plan to our lunchtime ping pong matches, we strive to create a positive work environment that fosters collaboration and drives performance. In addition to company-sponsored activities, employee benefits and other fun perks, our "people-first" mentality helps us attract and retain some of the best local talent, which is the backbone of our continued growth and success."</p>
               <p>In 2016, for the second year in a row, SiteLock received the prestigious Deloitte Technology Fast 500 award, where it was recognized as the fastest growing software company in Arizona. In addition, SiteLock was named to the Online Trust Alliance Honor Roll in 2016, the Top Companies to Work for in Arizona in 2014, and the Best Places to Work in Phoenix list in 2013.</p>
               <p>"BestCompaniesAZ has been at the forefront of recognizing sought-after employer brands and creating prestigious workplace awards programs in Arizona since 2002. As we celebrate 15 years, we are proud to recognize companies across our state who are making a positive difference in the lives of their employees and our communities," said Denise Gredler, Founder and CEO of BestCompaniesAZ. </p>
               <p>The BestCompaniesAZ's 15th anniversary list honors well-known employers throughout Arizona. For the full list, visit <a href="https://bestcompaniesaz.com/100-best-companies-in-arizona/" target="_blank">http://bestcompaniesaz.com/100-best-companies-in-arizona/</a></p>
                <h4>About SiteLock</h4>
                <p>SiteLock, the global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>
                <h4>About BestCompaniesAZ:</h4>
                <p>BestCompaniesAZ is a consulting firm dedicated to identifying, developing and promoting great workplaces. BestCompaniesAZ has been at the forefront of building sought-after employer brands and prestigious workplace awards programs since 2002, including the "2003 Best Companies to Work for in the Valley" program (published by the Phoenix Business Journal), Arizona's Most Admired Companies (published by AZ Business Magazine and BestCompaniesAZ), and Top Companies to Work for in Arizona (published by azcentral and Republic Media). BestCompaniesAZ provides consulting services for organizations committed to workplace excellence, including employee surveys, best practices, national and local public relations, employer branding, culture development and promotional services. For more information, visit <a href="https://bestcompaniesaz.com/" target="_blank">http://www.bestcompaniesaz.com</a> or phone 480-545-5151.</p>
                
<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>