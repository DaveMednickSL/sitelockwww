<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Welcomes SoftCom to Global Partner Community</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-welcomes-softcom-to-global-partner-community" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Welcomes SoftCom to Global Partner Community</h1>
            <h3 class="font22">SoftCom, an Ingram Micro Company, Adds Website Security to Hosting and Email Services</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; April 2, 2014</strong></p> 
                <p>SiteLock LLC <a href="https://www.sitelock.com">(www.sitelock.com)</a>, a global leader in website security and PCI Compliance, announced today that global service provider, SoftCom™ will add SiteLock services to their comprehensive portfolio of web hosting and Internet services for current and new customers in the SME market.</p>

                <p>SoftCom takes the approach of building a community while serving customers who need everything from domain names to email, web hosting, VPS Hosting and collaboration tools. They are also a leading technology integrator. As a Microsoft Certified Hosting Partner, as well as a partner to Cisco, Dell, and NetApp, SoftCom strives to provide everything its customers need to run their online business under one roof. And they know that as their customers’ businesses become more complex, complete website security is a requirement. To meet that need, they are now integrating with SiteLock.</p>

                <p>In fact, according to a recent study conducted by the National Small Business Chamber, up to 50% of targeted attacks are aimed at the SME market. While many small and midsize companies blame cost as the primary reason they are not proactive about website security, the truth is that the cost of an attack could be far worse.</p>

                <p>Unlike some tools that claim to provide reliable protection, when in fact they test only a sample of the website code against a segment of known malware, SiteLock scans websites from the outside in, and the inside out. This complete process identifies any malicious code, and even other weaknesses that could expose sites for easier attack. The service also has an automatic malware removal tool and a web application firewall that add layers of protection for online businesses.</p>

                <p>“I have watched and participated in the evolution of websites as R&amp;D projects in IT departments to now simple deployment of a few online tools managed by marketing departments or business owners,” describes Turker Sokullu, Executive Director and GM at SoftCom. “This dramatic change in code management and business objective for a company’s online presence has simultaneously increased the risk for website attack. Our goal is to keep the management process simple for any user, while making sure they have the most complete level of security at the same time. And that’s why we chose to partner with SiteLock.”</p>

                <p>“While there are many providers of hosting and online services, few take a relationship-building approach that truly put the customer first,” adds Tom Serani, Executive Vice President of Business Development for SiteLock. “As we have the same focus, we are very excited to welcome SoftCom to the SiteLock family and serve their customers with complete website security solutions.”</p>

                <p>SoftCom will leverage the Parallels APS platform to integrate and offer SiteLock services to their customer base. The program was just made available to the market during the last week.</p>

                <p>For additional information regarding the SiteLock website security suite, visit <a href="https://www.sitelock.com">www.sitelock.com</a>. For additional information about SoftCom, visit <a href="https://www.softcom.com" target="_blank">www.softcom.com</a>.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About Softcom</h4>
                <p>Founded in 1997, SoftCom Inc. is an industry leading provider of cloud hosting and business communication services to more than 2.5 million users in 140 countries worldwide with support in 26 languages. SoftCom also manages the myhosting.com and mail2web.com brands. The company is a Parallels Platinum Service Provider Partner, Microsoft Certified Partner and member of Research in Motion (RIM) Blackberry Alliance program. SoftCom is a wholly owned subsidiary of Ingram Micro, the world’s largest technology distributor.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
