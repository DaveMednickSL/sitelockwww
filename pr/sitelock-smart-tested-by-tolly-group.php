<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Independent Testing Shows SiteLock Web-Based Malware Protection Outperforms Traditional Endpoint Solutions</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-smart-tested-by-tolly-group" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">Independent Testing Shows SiteLock Web-Based Malware Protection Outperforms Traditional Endpoint Solutions</h1>
            <hr>
                <p><strong>SCOTTSDALE, ARIZONA &mdash; October 5, 2016</strong></p> 
                <p>A recent report from the Tolly Group confirms that traditional virus scanning applications and endpoint security solutions are not designed to detect or remove web-based cyber threats and malware. According to the study commissioned by SiteLock, a global leader in website security, traditional endpoint security solutions miss more than 90 percent of web-based malware attacks when used alone. This study comes as the Department of Homeland Security launches Cyber Security Awareness Month (NCSAM).</p>

                <p>Testing a group of nearly 3,000 web-based malware examples to determine effectiveness, the independent testing group compared the SiteLock SMART (Secure Malware Alert &amp; Removal Tool) web-based malware protection solution alongside McAfee Complete Endpoint Protection.</p> 

                <p>Additional findings include:</p>

                <ul>
                    <li><span>Traditional virus scanning applications and endpoint security solutions are not designed to detect and effectively counter web-based cyber threats and malware</span></li>
                    <li><span>The endpoint security solution tested missed over 90 percent of web-based malware</span></li>
                    <li><span>SiteLock SMART provided 100 percent PHP and JavaScript coverage versus less than 6 percent coverage by McAfee</span></li>
                    <li><span>Relying solely on endpoint security solutions increase the likelihood of security breaches</span></li>
                </ul>

                <p>Tests showed that the SiteLock solution detected and cleaned 100 percent of the samples provided. Traditional anti-virus endpoint solutions have focused on threats that arrive as executable programs, failing to address newer web-based attacks. SiteLock SMART offers real-time, non-disruptive continuous scanning for website and web applications.</p>  

                <p>"Millions of websites get hacked and don't know it. We work tirelessly to get ahead and stay ahead of cyber criminals, developing a solution that can not only detect malware and vulnerabilities but remove threats in real-time," said Neill Feather, President of SiteLock. "There is a misperception regarding the role of traditional solutions and customers don't realize they can be left vulnerable. The Tolly Report provides third-party validation of the unique work that SiteLock SMART delivers, and helps us to continue our journey to detect and prevent future website attacks."</p>

                <p>SiteLock can detect malware the minute it hits and scans source code of more than 200,000 websites and web applications each month. After identifying malicious content, it automatically neutralizes and removes the threats—cleaning over 50,000 applications per month. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.</p>

                <p>To view the complete report, visit: <a href="https://www.sitelock.com/tolly">https://www.sitelock.com/tolly.</a> To join the NCSAM conversation, follow @SiteLock on Twitter and use #CyberAware.</p>

                <h4>About The Tolly Group</h4>
                <p>The Tolly Group is the industry's leading provider of third-party testing and validation services for IT products, services and components. Tolly's expertise spans wireless, storage, virtualization/thin clients, switching, security (including but not limited to: endpoint security, NAC, mobile security, backup and recovery and deduplication), networking, and cloud services. For more information, please visit <a href="https://www.tolly.com" target="_blank">www.tolly.com.</a></p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6,000,000 customers worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com.</a></p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>