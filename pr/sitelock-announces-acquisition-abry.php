<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Acquisition by ABRY Partners</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-acquisition-abry" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Acquisition by ABRY Partners</h1>
            <h3 class="font22">Investment from prominent private equity firm positions SiteLock to accelerate company growth and product innovation</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; April 12, 2018</strong></p>
                <p>SiteLock, a global leader in website security solutions, today announced its acquisition by ABRY Partners (“ABRY”), a leading private equity firm.</p>
                <p>Founded in 2008, SiteLock’s mission is to protect organizations from ever evolving cyber security threats. SiteLock is the leading provider of cloud-based, comprehensive website security solutions, and its innovative and award-winning products protect more than 12 million websites globally. In 2017, SiteLock was named by Deloitte as the fastest growing software company in Arizona for the third consecutive year, and the fastest growing company in Arizona overall. </p>
                <p>“We are excited to partner with SiteLock to support their continued innovation of new products and solutions both organically and inorganically,” said Brian St. Jean, Partner at ABRY Partners. “As threats become more complex and frequent, organizations need a comprehensive and reliable solution to protect their online presence. We believe SiteLock is well positioned to continue to be the go-to partner for website security needs.”   </p>
                <p>“The investment from ABRY Partners is a testament to our culture of innovation and dedication to building a reliable platform to protect organizations across the globe,” said SiteLock Chief Executive Officer Neill Feather. “Cyber security attacks continue to increase at an unprecedented pace. During the fourth quarter of 2017 the average website experienced more than 44 attacks per day, and there was a 90 percent rise in the number of businesses targeted by ransomware for a total $5 billion financial impact. We are excited to work with the team at ABRY to continue to develop new products and solutions to support and protect our customers.”</p>
                <p>“We’re grateful to all our employees and to Unitedweb for their investment in our growth,” continued Feather. “Together with the Unitedweb team we have pioneered the website security industry and made solutions and protection accessible to businesses of all sizes and types.” </p>
                <p>“We’re proud to have supported SiteLock for more than 10 years as it has grown to become the leading provider of website security solutions,” said Unitedweb Chief Executive Officer Tomas Gorny. “We’re excited to see what’s next for their growing team and what they accomplish in partnership with ABRY Partners.”</p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company has an industry leading threat database and protects over 12 million websites worldwide. For more information, please visit sitelock.com.</p>
                                
                <h4>About ABRY Partners</h4>
                <p>ABRY is one of the most experienced media, communications, and business and information services sector focused private equity investment firms in North America. Since their founding in 1989, they have completed over $62 billion of leveraged transactions (including many roll-up investment strategies) and other private equity, mezzanine or preferred equity investments. Currently, they manage over $5.0 billion of capital in their active funds.</p>

                <h4>About Unitedweb</h4>
                <p>Unitedweb was founded in 2008. Unitedweb owns diversified group of technology companies. It primary focuses on early stage investments.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>