<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Ranked Arizona's Fastest Growing Software Company</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-deloitte-fast-500-2016" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Ranked Arizona's Fastest Growing Software Company on Deloitte Technology Fast 500<sup>&trade;</sup></h1>
            <h3 class="font22">Growth Attributed to Product Innovation and Strategic Partnerships</h3>
            <hr>

               <p><strong>SCOTTSDALE, ARIZONA &mdash; November 17, 2016</strong></p>
               <p>SiteLock today announced it was named to the Deloitte Technology Fast 500, a ranking of the 500 fastest growing technology, media, telecommunications, life sciences and energy tech companies in North America, for the second year in a row. The company, which is part of Arizona’s rapidly expanding “Silicon Desert,” grew 479 percent over the three year period measured, making it the state’s fastest growing software company.</p> 

               <p>SiteLock president, Neill Feather, attributes the company’s continued growth and recognition to its targeted customer acquisition strategy, product innovation, and a deliberate focus on building strategic partnerships driven by exceptional customer service and support. Software innovations include SiteLock Infinity<sup>&trade;</sup> which is the only automated, state-of-the-art malware and vulnerability remediation of its kind.</p>

                <p>"It is an honor to be recognized among Deloitte’s Technology Fast 500. Being the fastest growing software company in Arizona for the second consecutive year is a testament to the effectiveness of our products and commitment to customer service," said Feather. “This recognition comes at a time when the risk of a security breach has never been greater. It is critical for website owners to not only understand website application security, but to take the necessary steps to protect their customers and their businesses.”</p>

                <p>Arizona, and the Phoenix metro area in particular, is quickly becoming a hub for tech innovation, with a growing community of technology companies of all kinds, earning it the “Silicon Desert” moniker. As a leader in the state’s cybersecurity cluster, SiteLock and its growing base of employees are an integral part of that community.</p> 

                <p>“Today, when every organization can be a tech company, the most effective businesses not only foster the courage to explore change, but also encourage creativity in using and applying existing assets in new ways, as resourcefully as possible,” said Sandra Shirai, principal, Deloitte Consulting LLP and U.S. technology, media and telecommunications industry leader. “This ingenious approach to innovation calls for the encouragement of curiosity and collaboration both within and outside the office walls.” </p>
 
                <p>“This year’s Fast 500 winners showcase that when organizations are open to diverse perspectives and insights, they are able to create an environment for their employees and customers to see the possibilities and ingenious solutions that might lie ahead,” added Jim Atwell, national managing partner of the emerging growth company practice, Deloitte &amp; Touche LLP. “Entrepreneurial environments foster change and innovation within businesses, and we look forward to watching these companies continue to drive change across all sectors.”</p>  

               <p>SiteLock was included on the 2015 Technology Fast 500<sup>&trade;</sup> and also recognized as the fastest growing technology company in Arizona.</p>

               <h4>About Deloitte’s 2016 Technology Fast 500<sup>&trade;</sup></h4>
                <p>Deloitte’s Technology Fast 500 provides a ranking of the fastest growing technology, media, telecommunications, life sciences and energy tech companies &mdash; both public and private &mdash; in North America. Technology Fast 500 award winners are selected based on percentage fiscal year revenue growth from 2012 to 2015.</p>

                <p>In order to be eligible for Technology Fast 500 recognition, companies must own proprietary intellectual property or technology that is sold to customers in products that contribute to a majority of the company's operating revenues. Companies must have base-year operating revenues of at least $50,000 USD, and current-year operating revenues of at least $5 million USD. Additionally, companies must be in business for a minimum of four years and be headquartered within North America.</p>

               <p>As used in this document, “Deloitte” means Deloitte LLP and its subsidiaries. Please see <a href="http://www.deloitte.com/us/about" target="_blank">www.deloitte.com/us/about</a> for a detailed description of the legal structure of Deloitte LLP and its subsidiaries. Certain services may not be available to attest clients under the rules and regulations of public accounting.</p>


               <h4>About SiteLock</h4>
               <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p></p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>