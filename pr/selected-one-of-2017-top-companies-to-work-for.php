<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Selected As One Of The 2017 AZCentral.com Top Companies</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/selected-one-of-2017-top-companies-to-work-for" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Selected As One Of The 2017 AZCentral.com Top Companies To Work For In&nbsp;Arizona</h1>
            <h3 class="font22">Global Website Security Leader Recognized For Thriving Workplace &amp; Building Great Careers</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; July 5, 2017</strong></p>
                <p>AZCentral.com and Republic Media (The Arizona Republic, azcentral.com, La Voz) announced today that SiteLock has earned a highly coveted spot on the prestigious list of 2017 azcentral.com<sup>&reg;</sup> Top Companies to Work for in Arizona.</p>

				<p>"We are proud to be honored as one of Arizona's 'Top Companies to Work for,'" said Neill Feather, President of SiteLock. "At SiteLock, we strive to build careers rather than jobs for our employees, and developing a positive working environment is a key component for achieving that goal."</p>

                <p>All participating companies completed a two-part assessment process conducted by the independent workplace research firm, Best Companies Group (BCG). Part one of process was the Employer Questionnaire, where BCG asked about benefits, HR policies and the fun things the employers do for their employees. Part two was the Employee Engagement and Satisfaction Survey which measured the employee experience in 8 core focus areas: Leadership and Planning, Corporate Culture and Communications, Role Satisfaction, Work Environment, Relationship with Supervisor, Training, Development and Resources, Pay and Benefits, and Overall Engagement. Using the combined data from both parts of the survey process, BCG determined the top 100 organizations that made the grade.</p>
                
                <p>"This Top Company list gets more competitive each year," says Denise Gredler, Founder and CEO of BestCompaniesAZ and consulting partner for the program. "The list of winners had very impressive employee survey results averaging an overall favorable rate of 90 percent and an overall employee engagement score of 92 percent, which is more than double the national average. These winners should be very proud of their engaged workforces. This presents a powerful opportunity for award-winners to promote their company culture to retain and attract the best talent."</p>

                <p>The 100 “Top Companies" were honored earlier this morning at an awards breakfast reception held at the Fairmont Scottsdale Princess. They will also be recognized in a special supplement in the July 2, 2017 issue of The Arizona Republic, as well as online at <a href="http://topcompanies.azcentral.com" target="_blank">topcompanies.azcentral.com</a> and <a href="https://bestcompaniesaz.com" target="_blank">www.BestCompaniesAZ.com</a>. For complete details visit www.sitelock.com or topcompanies.azcentral.com.</p>
				
				<h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

                <h4>About Republic Media</h4>
                <p>Republic Media is a consultative media company that provides you with the means to deliver your message to the right audience in the most effective way. From online to print to direct mail, Republic Media encompasses an array of products including The Arizona Republic, azcentral.com, and La Voz. Since 1890, The Arizona Republic continues to be Arizona's most trusted and most read newspaper. <a href="http://www.republicmedia.com/about-us" target="_blank">http://www.republicmedia.com/about-us</a> </p>

                <h4>About BestCompaniesAZ &mdash; Arizona’s Employer Branding Partner</h4>
                <p>BestCompaniesAZ specializes in helping corporate clients develop, strengthen and market their unique employer brands through a variety of events, awards and communications programs. Recognition and promotion for award-winning clients is supported through a variety of marketing programs, including recruitment marketing, search engine optimization, public relations and social media outreach. For more information contact <a href="mailto:dgredler@bestcompaniesaz.com">dgredler@bestcompaniesaz.com</a> or call 480-545-5151. </p>

                <h4>About Best Companies Group</h4>
                <p>Best Companies Group is dedicated to establishing Best Places to Work programs to distinguish leaders in workplace excellence. As a research firm, BCG collects data about participating companies as well as employee feedback, analyzes that data, and produces a “Best” distinction that makes each recognized organization, the selected region (or industry) and the organizing partners proud. Best Companies Group identifies and recognizes places of employment that lead the way in defining the employee experience of the 21st century.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>