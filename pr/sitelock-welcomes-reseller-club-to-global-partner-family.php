<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Welcomes ResellerClub, a Directi Company, to Global Partner Family</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-welcomes-reseller-club-to-global-partner-family" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Welcomes ResellerClub, a Directi Company, to Global Partner Family</h1>
            <h3 class="font22">The SiteLock Community Extends to Asia with New Alliance and Integration with ResellerClub</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; September 18, 2013</strong></p> 
                <p>SiteLock LLC (www.sitelock.com), a global leader in website security solutions for online businesses, announced today that it has partnered with ResellerClub (www.resellerclub.com) and its parent company Directi (www.directi.com) to add website security to their comprehensive set of web products that serve millions of customers in more than 230 countries around the globe.</p>
 
                <p>Shridhar Luthria, General Manager & Business Head at ResellerClub said, “We have over 5 million domains and 100,000 web hosts using our platform to run a variety of websites. We take a great deal of effort to ensure that our clients have a secure web presence which is crucial for any successful e-commerce business. To accomplish this, we have been on the lookout for a reputed website security solutions provider.”</p>
 
                <p>The need for website security and proactive protection of online businesses has never been greater than it is now, and unfortunately, the threats increase each year. According to PwC, cybercrime ranks as one of the top four global economic crimes. And it doesn’t discriminate based on size, industry or location. All companies are at risk, and in addition to the financial impact, companies that are cybercrime victims can also suffer significant brand mistrust and reputation damage.</p>

                <p>SiteLock partners with leading web service providers and hosting companies that take a similar approach to market education about online threats and how new website owners can protect themselves. Because individuals and companies that want to take their business online first need a home for their site, their primary source of information and options about what they need to make their website successful is their hosting provider. When hosting providers offer SiteLock to their customers, they are demonstrating the importance of having an alarm system for website malware and vulnerabilities, as well as options for malware removal and the addition of a protective web application firewall that prevents harmful traffic from visiting their customers’ websites.</p>
 
                <p>“We see our partnership with ResellerClub and their parent company, Directi, as extremely exciting as it extends SiteLock’s global presence to India and to the millions of customers and resellers in the Directi family,” adds SiteLock Executive Vice President of Business Development, Tom Serani. “When companies make a commitment to an online strategy, they need to know they are working with industry leaders from technology, expertise, and service perspectives. SiteLock and ResellerClub provide highly complementary services that support small business success online.”</p>
 
                <p>“At Directi, our goal is to continually open new possibilities for individuals and entities to succeed with their online presence,” states Bhavin Turakhia, Founder and CEO of Directi. “We believe that new businesses should have the same opportunities and advantages as larger organizations and security is a necessity that no business should be without. Our partnership with SiteLock enables us to introduce this valuable benefit from day one when they launch their online business.”</p>

                <p>Learn more about SiteLock at www.sitelock.com. Learn more about ResellerClub at www.resellerclub.com.</p>
 
                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @sitelock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%.  As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About ResellerClub</h4>
                <p>The ResellerClub platform powers some of the World’s most popular Web Hosts, Domain Resellers, Web Designers and Technology Consultants. ResellerClub provides scalable and secure Shared Hosting, Reseller Hosting as well as VPS solutions, in addition to a comprehensive suite of gTLDs, ccTLDs and other essential Web Presence Products.</p>

                <p>Current Partners: Over 100,000<br>
                Domains Served: Over 5 Million<br>
                Countries Served: Over 200+<br>
                Team Strength: 100+<br></p>


                <h4>Media Contact:</h4>
                <p>Mary Miller<br>
                Director, Marketing<br>
                SiteLock<br>
                mmiller@sitelock.com</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>