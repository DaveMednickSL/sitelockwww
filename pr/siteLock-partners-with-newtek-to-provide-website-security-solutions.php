<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Partners with Newtek to Provide Website Security Solutions for Businesses of All Sizess</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/siteLock-partners-with-newtek-to-provide-website-security-solutions" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Partners with Newtek to Provide Website Security Solutions for Businesses of All Sizes</h1>
            <h3 class="font22">SiteLock named the exclusive website security provider of Newtek</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; December 14, 2018</strong></p>
                <p>SiteLock, the global leader in website security solutions, today announced a partnership with Newtek Business Services Corp., a distributor of business services and financial products to the small and medium-sized markets. With this partnership, SiteLock will become the exclusive website security provider for Newtek, offering industry-leading scanning and remediation solutions to an expanded channel of customers worldwide.</p>

                <p>Newtek will now offer the full portfolio of SiteLock website security solutions to cover the needs of the small to medium-sized business (SMB) and to the enterprise-level customer, including eCommerce and financial institutions. This will include SiteLock industry-leading scanners to automatically find and fix malware and vulnerabilities, along with a web application firewall (WAF) that includes DDoS mitigation and a content delivery network (CDN). Products will be available on an individual basis, as well as through packages designed to meet specific customer needs.</p>

                <p>"We are extremely pleased to partner with SiteLock, the market leader in website security," said Barry Sloane, President and CEO of Newtek Business Services Corp. "The addition of SiteLock solutions rounds out our security portfolio to both SMB and enterprise-level customers, ensuring they have the necessary proactive website security to combat cyberattacks. With this partnership, we will become a one-stop-shop security provider for both our small business and large enterprise customers. These businesses will not only be protected from today’s constant cyberthreats but also educated on the specific risks their websites face through SiteLock’s suite of solutions."</p>

                <p>Cyberattacks are continuing to evolve at a rapid pace, becoming increasingly difficult to detect. In fact, website attacks increased 16 percent on average throughout 2018, indicating the ongoing importance of proactive website security. By partnering with SiteLock, Newtek customers gain access to the most advanced cloud security solutions on the market, as well as industry-leading tools, such as a personalized website risk score to identify their sites’ likelihood of compromise and the level of security needed to remain protected.</p>

                <p>"We are excited to partner with Newtek as their exclusive website security provider, increasing their security portfolio for companies of all sizes," said Tom Serani, Chief Channel Officer at SiteLock. "As cybertheats continue to advance at an increasing rate, all businesses need website security solutions with a proven track record of defense and remediation. As the largest business solution provider, partnering with Newtek will ensure more businesses have easy and affordable access to top-tier website security."</p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

                <h4>About Newtek Business Services Corp.</h4>
                <p><a href="https://www.newtekone.com/">Newtek Business Services Corp.</a>, Your Business Solutions Company&reg; , is an internally managed BDC, which along with its controlled portfolio companies, provides a wide range of business services and financial products under the Newtek&reg;  brand to the small-and medium-sized business ("SMB") market. Since 1999, Newtek has provided state-of-the-art, cost-efficient products and services and efficient business strategies to SMB accounts across all 50 states to help them grow their sales, control their expenses and reduce their risk.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>