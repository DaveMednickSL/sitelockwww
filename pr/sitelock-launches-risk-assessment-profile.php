<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Launches Security Risk Assessment Profile</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-launches-risk-assessment-profile" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Launches Security Risk Assessment Profile</h1>
            <h3 class="font22">SiteLock Enhances Awareness of Online Threats among Business Owners</h3>
            <hr>

            <p><strong>SCOTTSDALE, ARIZONA &mdash; June 19, 2013</strong></p> 
            <p>SiteLock LLC, (www.sitelock.com) the global website security leader, announced today that it has launched a new online tool to help website owners understand security risks and potential threats, as well as tips and best practices to address them. The interactive tool asks a series of questions and generates a Risk Assessment Profile (RAP) that website owners and managers can use as a guide to remediate their security gaps.</p> 

	        <p>On the heels of the recent report on small business website security vulnerabilities and their costs, SiteLock continues to educate the market regarding the constant pressures and exposures that they face to secure customer data. The SiteLock RAP takes a consultative approach to guiding website owners through their security knowledge, practices, policies and technology currently in place. The profile report rates the level of security based on the website owners‘ responses and provides them with steps to protect their online businesses in the future.</p>

	        <p>“Based on our in-depth knowledge and experience with small business websites, we developed the Risk Assessment Profile to reinforce the message that whether we, or they, want to acknowledge it or not, the unfortunate truth is that SMB websites are under massive, targeted attack – every day,” explains Neill Feather, President of SiteLock. “We believe that they should have just as much access to insight, technolgy and services to address such threats as large enterprises, even if they don’t have extensive IT staff or resources in place.”</p>

	        <p>Having recently been named to the OTA Online Trust Honor Roll by the OTA Alliance, SiteLock practices what they preach and exposes its own business and website to the same or even greater level of scrutiny to ensure they do everything possible to protect their customers‘ and partners‘ websites, data and online experience. The launch of the RAP demonstrates another example of their commitment to data protection and online security.</p>

	        <p>To complete the SiteLock security Risk Assessment Profile or learn more about their 360-degree website security, visit www.sitelock.com. </p>

	        <h4>About SiteLock</h4>
            <p>SiteLock (www.sitelock.com / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services includes malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%.  As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
