<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Welcomes DADA to the Global Partner Family</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-welcomes-DADA-group-to-global-partner-family" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Welcomes DADA to the Global Partner Family</h1>
            <h3 class="font22">DADA Brands Add Complete Website Security to Brand Protection Product Line</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; April 1, 2014</strong></p> 
                <p>SiteLock LLC <a href="https://www.sitelock.com">(www.sitelock.com)</a>, a global leader in website security solutions, announced today that it has formed a valuable alliance with DADA (www.dada.eu), an international leader in professional digital services. The DADA product portfolio is aimed at building the online presence of their customers and includes domain registration, hosting, servers, website creation, e-commerce and online advertising services.</p>

                <p>As an internationally growing company, DADA provides all aspects of website management and marketing, delivering on their promise to be the “gateway to the digital presence” for their customers. A snapshot of the DADA brands in this business includes register.it, names.co.uk, amen, nominalia, poundhost and register365. Together, these companies enable online businesses to create, promote, publish content, socialize and drive traffic to their websites. The DADA portfolio manages 1.7 million domain registrations for 520,000 customers throughout Europe.</p>

                <p>DADA added SiteLock to its services for customers so they could protect their online investments. As small and midsize businesses rely more heavily on their websites as a source of lead generation and sales, the security of their site and visitor data becomes critically important. According to a recent Forbes article, 30,000 websites are hacked each day, with the majority representing legitimate small businesses that are unaware of the infection. By adding SiteLock website security, DADA can support their customers with malware identification and automatic removal as well as vulnerability detection and a web application firewall that blocks malicious traffic and virtually patches risk exposures that could be exploited. Customers can automatically add the SiteLock Trust Seal to display on their website once they are verified as secure.</p>

                <p>“At DADA, we are always looking for ways to help our small and medium businesses promote their brands online,” states Claudio Corbetta, Chief Executive Officer at DADA. “It is not enough to give them the tools to host, build and market their websites. Their businesses, brands and customers rely on us for stability, integrity and security. We looked to SiteLock to strengthen security in our international offer, making it easy for our customers to start with full protection from day one of their online presence.”</p>

                <p>“Our partnership with DADA and its family of brands demonstrates our commitment at SiteLock to fully support the global SMB market, given their presence across Europe,” adds SiteLock Executive Vice President of Business Development, Tom Serani. “It’s very refreshing when partners take a holistic approach to empowering their customers to build, promote and protect their online brand, going beyond traditional hosting services and educating companies about the importance of website security and online trust.”</p>

                <p>Learn more about SiteLock at <a href="https://www.sitelock.com">www.sitelock.com</a>. Learn more about the DADA family of brands, at <a href="http://www.dada.eu" target="_blank">www.dada.eu</a>.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About DADA</h4>
                <p>DADA S.p.A. – listed on the STAR segment of the Italian Stock Exchange – is an international leader in professional online presence services (domains, hosting, servers, online brand protection) and in a number of advanced online advertising solutions. With more than 520 thousand business clients and 1.7 million domains under management, DADA is one of the leading names in the European Domain & Hosting sector and is a key player in the markets where it is active: in Italy through its established brand Register.it, as well as Spain, the UK, Ireland, France, Portugal and Holland under the brands Nominalia, Namesco, PoundHost, Register365 and Amen, respectively. With regard to online advertising, DADA is active at an international level in the Performance Advertising business.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>