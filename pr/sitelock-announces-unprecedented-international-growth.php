<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Unprecedented International Growth</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-unprecedented-international-growth" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Unprecedented International Growth</h1>
            <h3 class="font22">Global Website Security Leader Expands to Australia, Greece, and Across Europe</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; June 19, 2014</strong></p> 
                <p>SiteLock LLC <a href="https://www.sitelock.com">(www.sitelock.com)</a>, the global leader in cloud-based website security, announced that its year-over-year growth has seen the business more than double each year since inception in 2008. A key driver of this growth has been international expansion, spurred by partnerships with technology market leaders such as GoDaddy, Melbourne IT, Register.IT, UsableWeb, Strato, and others.</p>

                <p>As the online threat landscape evolves faster than most consumers can post a tweet, the challenge for security tools is to keep pace and prevent those threats from affecting online communities – businesses and customers alike. According to the 2014 Data Breach Investigation Report, published by Verizon, “As economic and social activities continue to go online, criminals will follow in order to exploit the soaring amount of data that can be (all too easily) converted to cash.”</p>

                <p>These risks are not subject to geographic, demographic or size discrimination. Any website, anywhere in the world can be a target. That’s why partnerships with leading providers around the globe is so important to help spread awareness of security services that can protect their online businesses. The companies that have joined the SiteLock partner family in the last year include some of the largest in the market, adding credence to message that security should be built in to a website from the very beginning, just like putting locks on doors and windows of a brick and mortar retailer.</p>

                <p>To prepare for the onslaught of online attacks, SiteLock has expanded its product portfolio to include a web application firewall powered by a global CDN, dynamic application security testing (DAST) and most recently, static application security testing (SAST) to enable companies to thoroughly test web applications and correct vulnerabilities prior to the application launch.</p>

                <p>“At SiteLock, we know that we engaged in a rapidly-changing environment, as online risks continue to increase in volume and complexity,” says Neill Feather, President of SiteLock. “Our goal is to stay ahead of the attacks to prevent them from affecting our customers. Since we protect more than a million websites, we learn about new malware and vulnerabilities very quickly and automatically add them to our detection and removal solutions. And our partnerships with leading global service providers helps increase business awareness of online risks and the solutions available for them.”</p>

                <p>Learn more about SiteLock and their complete website security services at <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
