<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Partners With 123 Reg For Website Security Solutions</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-partners-with-123-reg" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock<sup class="reg_format">&reg;</sup> Partners With 123 Reg For Website Security Solutions</h1>
            <hr>

               <p><strong>SCOTTSDALE, ARIZONA &mdash; October 6, 2016</strong></p>
               <p><a href='https://www.sitelock.com'>SiteLock,</a> the global leader in website security solutions, announced today its partnership with the UK's largest domain registrar, <a href='https://www.123-reg.co.uk' target='_blank'>123 Reg.</p>
                <p>SiteLock and 123 Reg will offer existing customers <a href='https:/www.sitelock.com/malware-removal'>website security solutions</a> to find, fix, and prevent website vulnerabilities. The partnership will also provide automatic malware detection and remediation to further secure the infrastructure. </p>
                <p>"SiteLock understands how critical website security is for our customers and their businesses," said Richard Winslow, 123 Reg Brand Director. "Google has discovered that malicious code infects approximately 30,000 sites every day. This wrecks website performance, puts website visitors at risk and has the potential to destroy the reputation of small businesses. By partnering with SiteLock, small business customers now have access to best-of-breed security solutions that deliver proactive and reliable protection from internet threats and vulnerabilities."</p>
                <p>The recent partnership with 123 Reg is a testament to SiteLock's ongoing commitment to provide flexible solutions that ensure complete website security. Through a unique combination of automatic malware detection and removal, web application firewall and more, SiteLock delivers the critical protection required to guard against today's evolving cyberattacks.</p>
                <p>"As the UK's leading domain registrar, 123 Reg understands the importance of online security for its customers," said Tom Serani, Executive Vice President of Business Development for SiteLock. "Our partnership will ensure that websites run safely and smoothly, and will further secure the infrastructure in the UK. Through our combined efforts and commitment, we can make it easy for customers to seamlessly integrate security into their sites and prevent future attacks."</p>
                <p>SiteLock can detect known malware the minute it hits. After identifying malicious content, it automatically neutralizes and removes the threats. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.</p>
                <h4>About SiteLock</h4>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6 million customers worldwide. For more information, please visit <a href='https://www.sitelock.com'>sitelock.com.</a></p>
                <h4>About 123 Reg</h4>
                <p>123 Reg is one of the UK's largest domain registrars, and has been every year for the last 15 years. 123 Reg has more than 800k customers, manages over 3.5 million domain names and is connected to over 1m websites in the UK. 123 Reg provides easy to use and intuitive products for all website needs, with a particular focus on providing online services to Britain's small business community. The philosophy is simple: the internet should be for the many and not the few which, is why every single product is designed for the small business owner looking for an easy and hassle free way of getting online. 123 Reg enables SME's to maximise their potential online to significantly grow their business through expanding and improving their online presence, helping businesses every step of the way with excellent local and friendly customer support.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>