<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Ranked Arizona’s Fastest Growing Software Company On Deloitte Technology Fast 500</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-ranked-arizonas-fastest-growing-software-company" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Ranked Arizona’s Fastest Growing Software Company On Deloitte Technology Fast 500<sup>&trade;</sup> for the Fourth Consecutive Year</h1>
            <h3 class="font22">Website security company attributes growth to continued product innovation and accelerated market demand</h3>
            <hr>
                
                <p><strong>SCOTTSDALE, ARIZONA &mdash; November 16, 2018</strong></p> 
                <p>SiteLock, the global leader in website security solutions, today announced it was named to the Deloitte Technology Fast 500<sup>&trade;</sup>, an annual ranking of the 500 fastest-growing technology, media, telecommunications, life sciences and energy tech companies in North America. The company also earned recognition as Arizona’s fastest growing software company for the fourth consecutive year, boasting a growth rate of 306 percent during a three-year period.</p>

                <p>SiteLock CEO Neill Feather credits the company’s continued growth to its unwavering commitment to product innovation, as well as substantial increases indemand for affordable, powerful, cloud-based website security solutions designed for small-to-medium-sized businesses (SMBs).</p>

                <p>"It is an honor to once again be included in the Deloitte Technology Fast 500. Our continued growth demonstrates our commitment to delivering innovative solutions that address the evolving needs of small and medium sized businesses," said Feather. "Cyber threats are continuing to advance at a rapid pace and show nosigns of slowing down. Our solutions help business owners of all sizes guard against these threats, while proactively protecting their websites from future attacks."</p>

                <p>"Congratulations to the Deloitte 2018 Technology Fast 500 winners on this impressive achievement," said <a href="https://www2.deloitte.com/us/en/profiles/sshirai.html">Sandra Shirai</a>, vice chairman, Deloitte LLP, and U.S.technology, media and telecommunications leader. "These companies are innovators who have converted their disruptive ideas into products, services and experiences that can captivate new customers and drive remarkable growth."</p>

                <p>"Software, which accounts for nearly two of every three companies on the list, continues to produce some of the most exciting technologies of the 21st century, including innovations in artificial intelligence, predictive analytics and robotics," said <a href="https://www2.deloitte.com/us/en/profiles/mdissanayake.html">Mohana Dissanayake</a>, partner, Deloitte &amp; Touche LLP, and Industry Leader for technology, media and telecommunications, within Deloitte’s audit and assurance practice. "This year’s ranking demonstrates what is likely a national phenomenon, where many companies from all parts of America are transforming the way we do business by combining breakthrough research and development, entrepreneurship and rapid growth."</p> 

                <p>SiteLock was previously included in the 2015, 2016 and 2017 Technology Fast 500<sup>&trade;</sup> and was recognized as the fastest growing software company in Arizona all three years.</p>

                <h4>About Deloitte's 2015 Technology Fast 500<sup>&trade;</sup></h4>
                <p>Deloitte's Technology Fast 500 provides a ranking of the fastest growing technology, media, telecommunications, life sciences and energy tech companies - both public and private - in North America. Technology Fast 500 award winners are selected based on percentage fiscal year revenue growth from 2014 to 2017.</p>

                <p>In order to be eligible for Technology Fast 500 recognition, companies must own proprietary intellectual property or technology that is sold to customers in products that contribute to a majority of the company's operating revenues. Companies must have base-year operating revenues of at least $50,000 USD, and current-year operating revenues of at least $5 million USD. Additionally, companies must be in business for a minimum of four years and be headquartered within North America.</p>

                <h4>About SiteLock</h4>     
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million customers worldwide. For more information, please visit <a href="https://www.sitelock.com/">sitelock.com</a>.</p>
            
                <h4>About Deloitte</h4>
                <p>Deloitte refers to one or more of Deloitte Touche Tohmatsu Limited, a UK private company limited by guarantee (“DTTL”), its network of member firms, and their related entities. DTTL and each of its member firms are legally separate and independent entities. DTTL (also referred to as “Deloitte Global”) does not provide services to clients. In the United States, Deloitte refers to one or more of the US member firms of DTTL, their related entities that operate using the “Deloitte” name in the United States and their respective affiliates. Certain services may not be available to attest clients under the rules and regulations of public accounting. Please see <a href="https://www2.deloitte.com/us/en/pages/about-deloitte/articles/about-deloitte.html">www.deloitte.com/about</a> to learn more about our global network of member firms.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>