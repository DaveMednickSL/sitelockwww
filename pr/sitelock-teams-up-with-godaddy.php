<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Teams Up With GoDaddy For Website Security Solutions</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-teams-up-with-godaddy" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Teams Up With GoDaddy For Website Security Solutions</h1>
            <h3 class="font22">WordPress Plugin Allows Users to Manage Website Security within Dashboard</h3>
            <hr>
                
                <p><strong>SCOTTSDALE, ARIZONA &mdash; December 3, 2015</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a>, the global leader in website security solutions, announced today their partnership with <a href="https://www.godaddy.com" target="_blank">GoDaddy</a>, the world's largest technology provider dedicated to small businesses, introducing a new plugin for WordPress.</p>

                <p>SiteLock and GoDaddy have been working to bring millions of GoDaddy customers complete website security since April 1st, 2014. Both parties are excited for this new offering, proving that when companies put their customers first, they can join forces and make amazing things happen.</p>

                <p>As GoDaddy creates a seamless hosting experience for WordPress users, SiteLock makes it easy to keep tabs on a website's security with one click. And now for over one million SiteLock customers (and growing) on WordPress, managing website security services has never been more simple or affordable. Designed with web developers and designers in mind, SiteLock plugin gives you an at-a-glance view of scan results right in your WordPress dashboard. Users can now access their SiteLock Dashboard from within WordPress, eliminating the need to move between systems for daily updates.</p>

                <p>"GoDaddy understands the importance of online security for our customers and their websites," said GoDaddy General Manager, Security Products, Wayne Thayer. "As one of the world's largest managed WordPress hosting providers, GoDaddy is always looking for ways to let customers quickly and easily make their websites more secure, and that's exactly what SiteLock's new WordPress plugin does."</p>

                <p>"As the hosting space continues to evolve, we wanted to offer a strategic solution through a trusted small business advisor and partner like GoDaddy. We worked together to make it easy for customers to seamlessly integrate security into their sites," notes Tom Serani, Executive Vice President of Business Development for SiteLock.</p>

                <p>SiteLock can detect malware the minute it hits. After identifying malicious content, it automatically neutralizes and removes the threats. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 5,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

                <h4>About GoDaddy</h4>
                <p>GoDaddy's mission is to radically shift the global economy toward small businesses by empowering people to easily start, confidently grow and successfully run their own ventures. With more than 13 million customers worldwide and more than 61 million domain names under management, GoDaddy gives small business owners the tools to name their idea, build a beautiful online presence, attract customers and manage their business. To learn more about the company, visit <a href="https://www.godaddy.com" target="_blank">www.GoDaddy.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>