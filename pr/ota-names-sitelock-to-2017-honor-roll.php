<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Online Trust Alliance Names SiteLock to the 2017 Online Trust Honor Roll</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/ota-names-sitelock-to-2017-honor-roll" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font22">Online Trust Alliance Names SiteLock to the 2017 Online Trust Honor Roll</h1>
            <h3 class="font22">SiteLock honored for its website security and privacy</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; June 21, 2017</strong></p>
                <p>SiteLock, the global leader in website security solutions, announced today it has received Honor Roll status for the <a href="https://otalliance.org/HonorRoll" target="_blank">2017 Online Trust Alliance (OTA) Audit &amp; Honor Roll</a> for the fifth consecutive year. The Audit &amp; Honor Roll is the de facto standard for recognizing excellence in consumer protection, data security and responsible privacy practices for the world’s top companies.</p>

				<p>“We’re honored to once again be named to Online Trust Alliance’s (OTA) 2017 Honor Roll. This continued recognition is a testament of our commitment to promoting online trust, security and privacy. By providing best-in-class security solutions and partnering with organizations like OTA, we’re able to help businesses mitigate risk and stay a step ahead of cyber attackers,” said SiteLock President Neill Feather. </p>

                <p>As the only comprehensive, independent online trust benchmark study, the ninth annual OTA Online Trust Audit evaluates websites in three categories: consumer protection, responsible privacy practices and security. Based on a composite weighted analysis, sites that scored 80 percent or better overall, without failing in any one category, received Honor Roll status. </p>

                <p>“The websites that made the Honor Roll should be proud of their accomplishments because it shows that they value the security and privacy of their customers just as much as profits,” said OTA Founder and Chairman Emeritus, Craig Spiezle. “SiteLock is ‘walking the talk’ by both advocating best practices and their commitment to collaboration, while achieving Honor Roll status for the 5th year. All recipients of the Honor Roll should be commended for going beyond the status quo embracing security best practices and respecting customers’ information.” </p>

                <p>OTA’s ninth annual Online Trust Audit &amp; Honor Roll analyzed approximately 1,000 predominantly consumer-facing websites for their website and email security and privacy practices. The audit, powered in part by data and analysis provided by SiteLock, revealed that 52 percent of analyzed websites qualified for the Honor Roll, a five percent improvement over 2016. However, OTA also observed emergence of an alarming three-year trend: sites either qualify for the honor roll or fail the audit. In other words, sites increasingly either take privacy and security seriously and do well in the audit, or they lag the industry significantly in one or more critical areas. The complete 2017 Online Trust Audit &amp; Honor Roll report is at <a href="https://otalliance.org/TrustAudit" target="_blank">https://otalliance.org/TrustAudit</a></p>

                <p>SiteLock is the only web security solution to offer complete, cloud-based website protection and the ability to detect malware the minute it hits. After identifying malicious content, SiteLock’s Secure Malware Alert and Removal Tool (SMART) automatically neutralizes and removes the threats. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.</p>

                <p>For more information on SiteLock’s security offerings, visit: <a href="https://www.sitelock.com/products">https://www.sitelock.com/products</a></p>
				
				<h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>