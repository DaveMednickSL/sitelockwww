<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock, a Leader in Website Security, Announces Tucows Partnership</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-tucows-partnership" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock, a Leader in Website Security, Announces Tucows Partnership</h1>
            <hr>

                <p><strong>TEMPE, ARIZONA &mdash; September 28, 2011</strong></p> 
                <p>SiteLock LLC, a global leader in website security solutions for online businesses, announced today a new partnership with Tucows Inc., (NYSE AMEX:TCX, TSX:TC) a global provider of domain names, email and other Internet services. This partnership will enable Tucows Inc. to extend SiteLock's security services to its hosting customers at reduced rates, further expanding the range of value added services Tucows provides clients.</p>

                <p>"At OpenSRS, we're always on the lookout for services like SiteLock that help our resellers grow customer relationships by offering innovative and useful products and services," said Adam Eisner, Director of OpenSRS Product Management, for Tucows. "SiteLock fits perfectly within our existing Trust Service lineup and gives resellers a complete suite of services so they can provide the right product to satisfy the diverse security needs of their small to mid-sized business customers."</p>

                <p>SiteLock's professional website security services include Deep 360-Degree security scanning, instant threat alerts, reputation management, verifiable website trust seals, and expert site hardening services. These services have never been more critical for Tucows online business clients, as hacking, brute force attacks, malware, and data theft is at an all time high.</p>

                <p>"We're excited to form this partnership with a leader in the hosting space like Tucows. This will provide a great value for their customers, and allows us to continue to provide security to greater numbers of small businesses and sites across the web,” said Neill Feather, President of SiteLock.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock provides website security services and website malware scanning that ensure a safe and productive Internet environment for online businesses. SiteLock solutions provide a comprehensive suite of security products with packages tailored to meet the specific needs of small business. Unlike competitive offerings, SiteLock's Business Verification and Security services are combined to provide the most complete and affordable business security solution available.</p>

                <p>SiteLock's offices are located in Scottsdale, Arizona. For more information, visit the website at <a href="https://www.sitelock.com">www.sitelock.com</a> or call 877-257-9263.</p>

                <h4>About Tucows</h4>
                <p>Tucows is a global Internet services company. OpenSRS managers over eleven million domain names and millions of email boxes through a reseller network of over 12,000 web hosts and ISPs. Hover is the easiest way for individuals and small businesses to manage their domain names and email addresses. Yummy names owns premium domain names that generate revenue through advertising or resale. Butterscotch.com is an online video network building on the foundation of Tucows Downloads. More information can be found at <a href="http://tucows.com" target="_blank">http://tucows.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>