<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock to Offer Website Security through Partnership with Exabytes</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-to-offer-website-security-through-partnership-with-exabytes" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock to Offer Website Security through Partnership with Exabytes</h1>
            <h3 class="font22">Exabytes to Package Website Protection within Hosting Services</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; October 3, 2012</strong></p>
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced that it is partnering with Exabytes (www.exabytes.com), a leading hosting provider, to offer website security scanning packages to consumers.

				<p>Exabytes is the largest hosting company in Malaysia, serving over 60,000 customers around the world with web hosting services. They maintain a solid history of network and server up time of over 99.5% as well as a 24/7 technical support team that offers friendly customer service with a satisfaction guarantee. Exabytes delivers domain names, eCommerce hosting, instant website design, cloud hosting, dedicated servers and much more.</p>

				<p>Exabytes will integrate with SiteLock through Tucows' OpenSRS API (www.opensrs.com) to provide SiteLock website security products to customers. New online businesses will have the ability to purchase the SiteLock Basic scanning software for their website, and current customers will be able to purchase select website scanning tools through the Exabytes control panel.</p>

				<p>Designed for small business websites, SiteLock Basic scanning package includes an initial website scan to thoroughly check all applications for infiltrations, then performs daily malware scans to ensure all of the code and files on the website remain clean. Once a customer's website is determined to be malware-free, they are able to display a SiteLock Trust Seal on the webpages as visual proof that the site is protected.</p>

				<p>"Website security is a crucial tool in today's world and we're glad to be able to offer it to our customers," says Chan Kee Siak, founder and CEO of Exabytes. "The combination of the SiteLock service and the OpenSRS platform creates an innovative, comprehensive benefit for our customers. We are confident they will appreciate the added value to their hosting package and be able to increase their conversions by through use of the Trust Seal."</p>

				<p>"Exabytes brings an unparalleled expertise to the hosting market and the global industry, and their customers deserve the most complete website security available," says Tom Serani, SiteLock's executive vice president of business development. "We are looking forward to protecting their customers' business websites across the Internet on a global scale."</p>

				<p>For additional information about SiteLock website security technology and services, visit www.sitelock.com.</p>

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security and PCI compliance solutions available on the market. SiteLock helps protect websites against malware, spam, viruses and other forms of online threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 3 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

				<h4>About Exabytes</h4>
                <p>Exabytes has specialized in providing web hosting services since year 2001. Ranked No. 1 in Malaysia since 2005, Exabytes now serves over 60,000 customers (from individuals, students, small & medium sized businesses, to government and public listed companies) in 121 countries and manages over 600 servers with 60,000 registered domains. Exabytes' commitments to customers are 100% customer satisfaction guarantee, 100-day money back guarantee, 99.9% network uptime guarantee and 99.5% server uptime guarantee. The company's passion is providing services that always leave customers feeling delighted. Exabytes' hallmark is a reputable, professional and 24Ã—7 Technical Support as well as a friendly customer service team. For more information, visit www.exabytes.com. </p>

				<h4>About Tucows</h4>
                <p>Tucows Inc. (NYSE AMEX:TCX, TSX:TC) is a global Internet services company. OpenSRS (<a href="http://opensrs.com" target="_blank">http://opensrs.com</a>) manages over 11 million domain names and millions of email boxes through a reseller network of over 12,000 web hosts and ISPs. Hover (<a href="http://hover.com" target="_blank">http://hover.com</a>) is the easiest way for individuals and small businesses to manage their domain names and email addresses. Ting (<a href="http://ting.com" target="_blank">http://ting.com</a>) is a mobile phone service provider dedicated to bringing clarity and control to U.S. mobile phone users. YummyNames (<a href="http://yummynames.com" target="_blank">http://yummynames.com</a>) owns premium domain names that generate revenue through advertising or resale. More information can be found on Tucows' corporate website (<a href="http://tucows.com" target="_blank">http://tucows.com</a>).</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>