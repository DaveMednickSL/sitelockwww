<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Builds Strategic Partnership with BaseKit</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-builds-strategic-partnership-with-basekit" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Builds Strategic Partnership with BaseKit</h1>
            <h3 class="font22">World-leading Website Builder Adds Global Security Technology to Portfolio</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; July 17, 2012</strong></p> 
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced today that it is partnering with BaseKit to extend its secure presence to entrepreneurs at the point where they create and design their websites. </p>

				<p>Given SiteLock's focus of providing complete website security for the SMB market with an inside out as well as outside in development approach, it makes perfect sense that they would integrate with such a highly respected and accepted platform. BaseKit has received international acclaim as an all-in-one website builder that lets SMB's build a site that "looks as good as most commercial sites" (The Sunday Times, 23 Oct 2011). Both companies share a passion for serving the frequently underserved market of small business owners. They often lack technical support and design resources, so it's critical that the online tools they can access are easy to use and carry all of the design prestige and security protection afforded by large enterprises.</p>  

				<p>"At SiteLock, we are continually looking for ways to make small business solutions affordable, while providing exceptional quality and technology at the same time," states Tom Serani, EVP of Business Development. "By partnering with BaseKit, we are enabling companies of all sizes, in any market, to truly build security into their website design from day one."</p>

				<p>"We aim to provide a website builder that both the most novice and most experienced users could learn and adapt to their business needs," claims BaseKit's CEO Juan Lobato. "We have found that the more comprehensive the service we provide to cover all of their website aspects, particularly the critical element of security, the more confident our small business customers will become in their website's ability to meet their company's objectives."</p> 

				<p>SiteLock protects small businesses online. BaseKit enables the same audience to build and design websites with such a professional and classy interface that they are often perceived as and able to compete with much larger organizations. At the rate of acceleration of online threats and attacks, website security tools are no longer optional. Now, with the powerful combination of a site builder with sizzle, neither design nor security needs to be compromised for small and midsize business websites. BaseKit will make SiteLock website security services available through their wholesale partner and retail API. Website builders who choose SiteLock through BaseKit will also be able to display the SiteLock Trust Seal on their sites, which assures consumers that their experience will be safe.</p>

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a>  / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other forms of threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>
                
				<h4>About BaseKit</h4>
                <p>BaseKit (<a href="https://www.basekit.com" target="_blank">www.basekit.com</a>) was founded in 2008 and has become one of the world's leading website builders. The BaseKit editor is available in the following languages: English, Spanish, French, German, Turkish, Portuguese, Russian, and Greek. The company now has over 75 employees, with offices in London, Bristol, Madrid and Sao Paulo. It aims to help small businesses and web designers create beautiful websites quickly and easily. BaseKit won Seedcamp in 2008 and has subsequently received investment from Eden Ventures, NESTA and Nauta Capital.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
