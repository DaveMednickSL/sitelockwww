<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Private Beta Of SMART&reg; Database At WordCamp Europe</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/announces-private-beta-of-smart-db" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Private Beta Of SMART<sup>&reg;</sup> Database At WordCamp Europe</h1>
            <h3 class="font22">SMART Database Removes Malware and Spam from WordPress Databases</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; June 16, 2017</strong></p>
                <p>SiteLock, the global leader in website security solutions, announced today a private beta program to evaluate SMART<sup>&reg;</sup> Database (SMART/DB<sup>&trade;</sup>), a new scanning tool designed to protect websites by identifying and automatically removing malware and spam from WordPress website databases.</p>

				<p>"SMART/DB provides websites &mdash; both large and small &mdash; with an effective yet simple solution for website security. We're able to couple expert evaluation with emerging malware monitoring technology to give users the benefit of automated removal of potentially unwanted content (PUC), paired with the freedom to personalize what content and users are whitelisted or blacklisted," said Neill Feather, president of SiteLock.</p>

                <p>SiteLock customers and all WordPress users can apply to participate in a private beta program to take advantage of SMART/DB's cutting-edge technology, and provide feedback on specific features, functionality and overall experience. To qualify for participation, users must have a publicly accessible WordPress site. Selected participants will have access to new technology before generally available and will be able to provide input on product functionality and enhancements.</p>
                <p>Applications for SiteLock's beta program will be accepted beginning June 15 and may be completed online here: <a href="https://www.sitelock.com/smartdb_beta">https://www.sitelock.com/smartdb_beta</a>. For more information, visit the SiteLock booth at WordCamp Europe.</p>

				
				<h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>