<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Launches TrueCode to Conduct Static Application Security Testing</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-launches-truecode-to-conduct-static-application-security-testing" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1  class="font35">SiteLock Launches TrueCode to Conduct Static Application Security Testing</h1>
            <h3 class="font22">The Global Website Security Leader Enhances Application Vulnerability Detection</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; June 10, 2014</strong></p> 
                <p>SiteLock LLC <a href="https://www.sitelock.com">(www.sitelock.com)</a>, the global leader in website security and PCI compliance, announced the launch of TrueCode, a new service that conducts Static Application Security Testing (SAST) to automatically detect vulnerabilities in applications, before or after they are put into production.</p>

                <p>Application development security testing, particularly for small to midsize companies, is often overlooked. Or, in many cases, it’s considered a luxury due to the price tag that has historically accompanied such comprehensive testing. And industry research suggests that 87-90% of web applications fail to address the OWASP top 10 threats, many of which are detected by SAST. In addition, of more than 1,300 data breaches last year, the biggest category (25%) was web applications, according to the Data Breach Investigation Report recently published by Verizon.</p>

                <p>TrueCode, the SiteLock static web application testing solution, acts like spell-check for application code and identifies the source of the problem, or potential problem, while highlighting the exact line(s) of code that need to be addressed. The service is entirely cloud-based and extracts a copy of the application code, storing it temporarily on SiteLock servers while testing so that there is no resource load or downtime risk on the client side. For companies with limited technical understanding or development staff, the SiteLock expert services team can even help address the application vulnerabilities before returning the secure code to the client. Making sure that application code is clean before production launch can accelerate the clients’ time to market and increase the ability to meet regulatory compliance requirements, while reducing the risk of infection for website visitors and application users.</p>

                <p>Static application security testing is particularly important for small and mid-size businesses, because it eliminates the trade-off previously required between comprehensive testing and the server load for those who host their websites in multi-tenant environments that have restrictions or fees for excessive usage of IO, bandwidth and other server resources.</p>

                <p>"At SiteLock, we are continuously looking for the next area of risk exposure for our customers’ online businesses to help protect them," said Neill Feather, President of SiteLock. "As we have seen the rise of web application development, particularly by using third-party and open-source code, we identified a gap in vulnerability detection that was in great demand across the SMB market. There are so few tools and services designed for them at an affordable price and at SiteLock, we want to break that mold. We believe that security should be available to every online business and the introduction of TrueCode is another step toward that goal."</p>

                <p>TrueCode is now available to the SiteLock customer base, which is more than 1 million websites strong, and can be purchased by new customers either stand-alone or as part of a customized security package.</p>

                <p>For additional information regarding the SiteLock website security suite, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>