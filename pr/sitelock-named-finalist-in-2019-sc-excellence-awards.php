<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Named Finalist in 2019 SC Excellence Awards for Best SME Security Solution</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/announces-partnership-with-enom" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Named Finalist in 2019 SC Excellence Awards for Best SME Security Solution</h1>
            <h3 class="font22">Website security company SiteLock recognized for providing superior products to the cybersecurity industry</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; Decemeber 19, 2018</strong></p>
                <p>SiteLock, the global leader in website security solutions, today announced that SiteLock SecureSite has been recognized as an Excellence Award finalist in the Best SME Security Solution category for the 2019 SC Awards. Finalists are recognized for outstanding leadership and providing superior security products to the cybersecurity industry. Winners will be announced at the SC Awards ceremony on March 5, 2019 in San Francisco.</p>
                <p>The Best SME Security Solution includes tools and services from all product sectors specifically designed to meet the requirements of small-to-midsized businesses. The winning solution will have been a leading solution during the last two years, having helped to strengthen the IT security industry’s continued evolution.</p>
                <p>“It is an honor to be recognized as a 2019 Excellence Finalist by SC Media,” said Neill Feather, CEO of SiteLock. “From the beginning, SiteLock has been dedicated to protecting every website on the internet by making enterprise-level security accessible and affordable to businesses of all sizes. As cyber threats continue to evolve at a rapid pace with no signs of slowing down, proactive website security is critical to keeping businesses and websites online. SiteLock SecureSite is designed with the SME in mind, helping to guard against these threats and prevent future attacks.”</p>
                <p>“Every new year brings with it an unpredictable mix of adversity and opportunity for information security professionals,” said Illena Armstrong, VP, editorial, SC Media. “In 2018, we watched as ransomware took down entire city governments, popular online platforms were accused of mishandling user data, and technology giants announced an unprecedented industry-wide effort to solve the Spectre and Meltdown CPU vulnerabilities. Through it all, this year’s SC Awards finalists found ways to break boundaries, overcome challenges and contribute fresh new ideas to the world of cybersecurity.”</p>
                <p>Now in its 22nd year, SC Awards is recognized as the industry gold standard of accomplishment for cybersecurity professionals, products and services. Winners in the Excellence category are determined by an expert panel of judges with extensive knowledge and experience in the cybersecurity industry. The Excellence Award honors the professionals, products and services that have proven to be the best in the industry for protecting today’s corporate world from an array of risks and threats.</p>
                <p>“Combatting today’s top security threats requires a combination of innovation, collaboration and education,” added Illena Armstrong of SC Media. “SiteLock is a shining example of a company that’s firmly committed to these key tenets as it helps protect organizations big and small from wave after wave of attack.”</p>
                <p>The SC Awards gala honoring the winners attracts top professionals in the cybersecurity community and provides an invaluable opportunity for networking. To register for the 2019 SC Awards gala, please visit <a href="https://scawards2019.splashthat.com/">https://scawards2019.splashthat.com/</a>.</p>

                <h4>About SC Media</h4>
                <p><i>SC Media</i> is cybersecurity. For 30 years, they have armed information security professionals with in-depth and unbiased information through timely news, comprehensive analysis, cutting-edge features, contributions from thought leaders, and independent product reviews in partnership with and for top-level information security executives and their technical teams.</p>
                <p>In addition to their comprehensive website, SC Media offers magazines, ebooks, and newsletters. They also host digital and live events such as SC Awards and <a href="https://www.google.com/url?q=https://www.risksecny.com/&ust=1545274440000000&usg=AFQjCNFjqbgWmgfZcm3tYa28uGo0OfpGzw&hl=en&source=gmail">RiskSec NY</a> to provide cybersecurity professionals all the information needed to safeguard their organizations and contribute to their longevity and success.</p>
                <p>
                Friend us on Facebook: <a href="http://www.facebook.com/SCMag">http://www.facebook.com/SCMag</a><br>
                Follow us on Twitter: <a href="http://twitter.com/scmagazine">http://twitter.com/scmagazine</a>
                </p>
                <p>
                Event Information:<br>
                Anna Naumoski, Events Manager<br>
                <a href="mailto:anna.naumoski@haymarketmedia.com">anna.naumoski@haymarketmedia.com</a><br>
                <a href="tel:6466386015">646.638.6015</a><br>
                </p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>