<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Expands Leadership Roster to Strengthen In-house Operations</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-expands-leadership-roster" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Expands Leadership Roster to Strengthen In-house Operations, Lead Product Innovation and Grow Channel Partnerships</h1>
            <h3 class="font22">SiteLock appoints Tom Heiser to board of directors, onboards new channel director and vice presidents of product, finance and HR</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; August 22, 2018</strong></p>
                <p><a href="https://sitelock.com">SiteLock</a>, the global leader in website security solutions, today announced the appointment of Tom Heiser to its board of directors. A seasoned technology leader and former EVP of EMC Corporation, Heiser will serve as executive chairman of the board. </p>

                <p>During his 30-year tenure at EMC Corporation, Heiser held key senior executive roles across the organization, including EVP of cloud strategy and execution. He has also served as president of RSA Security, CEO of ClickSoftware and interim CEO for Arxan. He currently sits on the board of directors for multiple technology companies, including Arxan, AtScale, and formerly sat on the board of directors for CyberArk.</p>

                <p>“Tom brings an unparalleled level of sales, marketing and executive leadership experience to the table,” said Neill Feather, CEO of SiteLock. “Tom’s ability to grow companies, ranging from startups to complex multinational businesses, makes him a natural fit for our growing SiteLock team.”</p>

                <p>Heiser’s appointment is a strategic next step for SiteLock following its <a href="https://www.sitelock.com/pr/sitelock-announces-acquisition-abry">acquisition by ABRY Partners</a> ("ABRY") earlier this year. SiteLock continues to engage seasoned expertise to reach the next level as a leading website security provider. Heiser will accelerate SiteLock’s product innovation with his go-to-market experience and customer-centric focus.</p>

                <p>“SiteLock’s mission to secure every website is an ambitious one, but we are primed for the challenge,” said Heiser. “The company has developed a comprehensive solution to the growing threat of malware, and I’m excited to chair the board at this pivotal moment of company growth.“</p>

                <p>This appointment dovetails with additional strategic hires for the Scottsdale-based company. The following executives and leaders will work alongside Feather, Heiser and the larger executive team. Their expertise is pivotal to the company’s focus on innovation, portfolio expansion and new partnerships as well as SiteLock’s ongoing commitment to strengthen website owners’ security strategies:</p>

                <p><ul>
                  <li style="color: #000; padding-bottom: 10px;"><strong>Brian Sargent, VP, product:</strong> In his role, Sargent will bring visibility and insight to SiteLock’s development roadmap. With more than 15 years of experience leading product teams, Brian has the experience to establish strategic product direction to drive business units forward. Prior to SiteLock, Brian was VP of product management at Docutech.</li>

                  <li style="color: #000; padding-bottom: 10px;"><strong>Iain Saunders, VP, finance:</strong> Iain joins SiteLock from Iron Mountain, formerly IO Data Centers. While at Iron Mountain, and in past roles at Vereit and Fender, Iain led the finance and accounting teams. Iain has more than 15 years of executive experience in accounting, financial planning and auditing. Iain will help lead the finance function at SiteLock.</li>

                  <li style="color: #000; padding-bottom: 10px;"><strong>Robyn Jordan, VP, HR:</strong> With more than 10 years of executive experience, Robyn will oversee the development of SiteLock’s growing career paths and maintain SiteLock as one of Arizona’s top places to work. Before joining SiteLock, Robyn managed all aspects of employee engagement and HR functions at Banner Health and in previous roles at Radial and Five Below.</li>

                  <li style="color: #000; padding-bottom: 10px;"><strong>Greg Howard, channel director:</strong> Greg will lead SiteLock into new markets like managed service providers and telecommunication, capitalizing on the growing demand and opportunity in these channels for security providers. Greg has more than 20 years of experience in channel cloud sales and enablement. In previous roles at Acronis, Parallels, VeriSign, Web.com and Stratoscale, Greg has seen the birth and rise of cloud computing from many vantage points. He is responsible for new channel partner acquisition, growth and strategy. </li>
                </ul></p>

                <p>Founded in 2008, SiteLock protects organizations from ever evolving cyber security threats. SiteLock provides comprehensive cloud-based website security solutions, and its award-winning products protect more than 12 million websites globally.</p>

                <p>SiteLock was also recently named to the <a href="https://www.inc.com/profile/sitelock">Inc. 5000 list</a> for the second consecutive year. In 2017, SiteLock was named by <a href="https://www.sitelock.com/pr/deloitte-fastest-growing-company">Deloitte</a> as the fastest growing software company in Arizona for the third consecutive year, and the fastest growing company in Arizona overall.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million websites worldwide. For more information, please visit sitelock.com.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>