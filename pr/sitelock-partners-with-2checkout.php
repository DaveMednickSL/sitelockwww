<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Partners with 2Checkout</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-partners-with-2checkout" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Partners with 2Checkout</h1>
            <hr>

                    <p><strong>Jacksonville, FLORIDA &mdash; November 28, 2011</strong></p>
					<p>SiteLock and 2Checkout are excited to announce a strategic partnership that will make selling products online safer, easier, and more secure. The combination of SiteLock's industry-leading total website security matched with 2Checkout's existing international customer base and global online payment solutionswill benefit each party's existing customer bases, while creating unique customer acquisition possibilities.</p>
					
					<p>While 2Checkout is known for providing merchants with a safe and easy way to accept payments online, SiteLock provides online businesses with a proactive approach to protecting their website, visitors and business reputation.  With security now top of mind for consumers, over 70% of online customers look for a verifiable 3rd party certification before providing personal data*.  The SiteLock Trust Seal provides customer confidence in knowing that the site is safe and that the necessary steps to provide protection have been taken.  SiteLock can verify the business's reputation through domain ownership, business phone and address verification, and SSL certification validity.</p>
					
					<h4>About SiteLock</h4>
					<p>SiteLock provides website security services and website malware scanning to ensure a safe and productive internet environment for online businesses. SiteLock offers a comprehensive suite of security products with packages tailored to meet the specific needs of online businesses and merchants. Unlike competitive offerings, SiteLock's Business Verification and Security services are combined to provide the most complete and affordable business security solution available.  SiteLock currently protects over 400,000 websites worldwide.  Many of these customers are merchants that sell online.  Each subscription of the service includes SiteLock's Trust Seal, which is proven to increase sales by over 10% through higher conversion rates.</p>
					
					<p>SiteLock's offices are located in Scottsdale, Arizona and Jacksonville, Florida.  For more information, visit the website at <a href="https://www.SiteLock.com">http://www.SiteLock.com</a> or call 877-257-9263.</p>
					<h4>About 2Checkout.com</h4>
					<p>2Checkout.com (2CO), a Columbus, Ohio -based company, is a worldwide leader in payments and e-commerce services. 2CO powers online sellers with a global platform of payment methods and a world-class fraud prevention service on secure and reliable PCI-compliant payment pages.  2Checkout's payments platform bundles a payment gateway and merchant account into one single offering with no need to contract with a merchant bank or manage separate agreements to accept forms of payment.  In addition, 2CO provides industry leading recurring billing services, call center support, full SSL certification, and the system is translatable in 15 languages and 26 international currencies for buyers and sellers in over 200 countries.</p>
					<p>Visit <a href="http://www.2checkout.com" target="_blank">http://www.2checkout.com</a> to learn more information 2Checkout.com's e-commerce and payment solutions.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>