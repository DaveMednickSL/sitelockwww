<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Launch of Mobile Application</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-launch-of-mobile-application" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1>SiteLock Announces Launch of Mobile Application</h1>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; April 4, 2013</strong></p> 
                <p>SiteLock LLC (<a href="http://www.sitelock.com/">www.sitelock.com</a>), a leading provider of website security solutions for online businesses, announced today that it has developed a mobile application that allows customers to monitor their website security status and resolve identified issues on the go. </p>
	
	            <p>With an increasing number of business decision makers working on mobile devices, SiteLock took the proactive approach of providing a way for its customers to monitor their website security status from anywhere. The mobile application supports multiple websites in the same SiteLock account and the website owner will have the options to start a scan, drill down into the identified issue, contact SiteLock support for assistance or ignore the message if it is determined not to be a problem.</p>
	
	            <p>"As the SiteLock customer base has grown to more than 700,000 online businesses around the globe, we have identified the need to support them in innovative ways that align with the way they run their businesses,” states Neill Feather, President of SiteLock. “The mobile application is a logical extension of our website security dashboard and gives our customers peace of mind that threat details and expert support are available at their fingertips."</p>
	
	            <p>SiteLock is committed to empowering website owners and business managers with real-time insight into the health of their business. When websites are the primary business storefront, as is often the case for small and midsize companies, they cannot afford to have issues that affect their customers’ online experience. SiteLock is dedicated to finding, fixing and helping to prevent malware and other threats from impacting online business. The mobile app complements the comprehensive website and application scanning portfolio as a real-time response service for their global customers.</p>
	
                <p>The SiteLock mobile application is available for the Apple iPhone in the App Store at no cost and can be downloaded here: https://itunes.apple.com/us/app/sitelock-dashboard/id621361478?mt=8 (no longer available on iTunes). For additional information regarding the enhanced SiteLock website security technology and services, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>
	
		        <h4>About SiteLock</h4> 
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security scanning and firewall protection technology. SiteLock offers the most affordable and complete website security and PCI compliance solutions available on the market. SiteLock helps protect websites against malware, spam, viruses and The OWASP Top Ten online threats. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 10 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>