<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Expands Strategic Partnership with Plesk to Offer Fully Integrated Portfolio of Website Security Solutions</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-expands-strategic-partnership-with-plesk" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Expands Strategic Partnership with Plesk to Offer Fully Integrated Portfolio of Website Security Solutions</h1>
            <h3 class="font22">SiteLock named a preferred website security provider of Plesk</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; December 5, 2018</strong></p>
                <p>SiteLock, the global leader in website security solutions, today announced a partnership expansion with Plesk, the leading WebOps platform and hosting control panel to build, secure and run websites, applications and hosting businesses. With this expansion, SiteLock will become a preferred website security provider for Plesk, simplifying security for web professionals and service providers through its industry-leading malware protection and remediation solutions.</p>

				<p>"Cyberattacks are continuing to make headlines as hackers evolve and their attacks become more sophisticated. Proactive website security is essential to combating these bad actors, and stopping attacks before they happen," said Lukas Hertig, Senior Vice President of Business Development & Strategic Alliances at Plesk. "By partnering with SiteLock, the market leader in website security, we have access to industry-leading tools, such as a customized risk score, that will empower both hosting and service providers to proactively educate their customers on key vulnerabilities or potential threats within their websites. Through our deep integration with SiteLock, our hosting partners will be able to quickly deploy tools that ensure the highest level of threat protection."</p>
                
                <p>The average website is attacked <a href="https://www.sitelock.com/blog/2018/09/website-security-insider-q2-2018/" target="_blank">58 times per day</a>. That’s one cyberattack approximately every 25 minutes. As the threat landscape continues to evolve – it’s no longer a matter of if you’ll be attacked, but when, making website security a top priority. Many service providers rely on providers, like Plesk, to provide a one-stop shop for all their hosting needs. With the integration of SiteLock website security solutions, web hosts now have access to a full service offering that simplifies and streamlines the go-to-market process with a single click deployment.</p>

                <p>Through this unique collaboration, web hosts can quickly and easily deploy SiteLock solutions from the Plesk ecosystem without the need for an extension. Plesk customers will now have access to Patchman, which automatically detects and patches vulnerabilities at the server level, as well as the SiteLock suite of powerful website security solutions including industry-leading scanners to automatically find and fix malware and vulnerabilities, along with a Web Application Firewall (WAF).</p>

                <p>"We are excited to expand our partnership with Plesk as a preferred provider of website security solutions," said Tom Serani, Chief Channel Officer at SiteLock. "In the face of an ever-evolving threat landscape, today’s web hosts require proven cybersecurity solutions that mitigate risk in order to stay competitive and drive new business. Working together, Plesk and SiteLock create a unique opportunity to provide innovative and proactive website security to web hosts worldwide."</p>
				
				<h4>About SiteLock</h4>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million customers worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

                <h4>About Plesk</h4>
                <p>Plesk is the leading WebOps platform to build, secure and run applications, websites and hosting businesses. Available in more than 32 languages across 140 countries in the world. 50% of the top 100 worldwide service providers are partnering with Plesk today. Plesk is managing and securing more than 380,000 servers, automating 11M+ websites and at least 19M mailboxes. Besides simplifying complexity and saving time through automation for developers and sysadmins, the rich and open ecosystem over 100 Plesk extensions provides access to even more relevant features targeted at specific audiences.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>