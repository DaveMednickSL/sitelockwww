<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>p To 10,000 Small Business Websites Discovered Each Day with Serious Security Vulnerabilities</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/ten-thousand-business-websites-discovered-with-vulns" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">Up To 10,000 Small Business Websites Discovered Each Day with Serious Security Vulnerabilities, According to Security Firm</h1>
            <h3 class="font22">Half of Vulnerable Websites Already Infected With Malware.</h3>
            <hr>
                <p><strong>SCOTTSDALE, ARIZONA &mdash; May 21, 2013</strong></p> 
                <p>SiteLock LLC (www.sitelock.com) the global website protection company that protects more than 700,000 online businesses, today released a report which provides clear evidence that small businesses are now squarely in the sights of hackers.</p>

	            <p>SiteLock scans hundreds of thousands of small business websites every day in search of a variety of vulnerabilities. In just the month of April 2013 alone, the company identified between 5,000 and 10,000 small business websites every single day, mainly in the U.S., that had serious security vulnerabilities that could be or already had been exploited by hackers.</p>

 	            <p>“Our daily scanning for a wide variety of security vulnerabilities confirms that not only are small business websites exposed to serious risks, in many cases these vulnerabilities have already been discovered and exploited by hackers,” according to Neill Feather, President of SiteLock. “Small business owners need to understand how website risks are business viability risks, and should be addressed with urgency.”</p>

	            <p>In any one day, SiteLock finds an average of 7,000 to 10,000 small business websites with serious vulnerabilities, and nearly half of those sites have already been hijacked and have malware installed. This malware is typically planted by hackers using automated tools to identify vulnerable websites. The malware is then used to infect visitors to those websites, send phishing e-mails, infect other computers, and even attack other websites through Distributed Denial of Service Attacks. </p>

	            <p>“My web site was being hit all the time, costing me up to $30,000 every time. Not to mention how many customers I lost,” said A.G., a SiteLock customer who doesn’t want to be identified for fear of retaliation. After losing more than $300,000 to web site attacks and malware that resulted in blacklisting by search engines, he turned to SiteLock for help. “I had no idea the damage malware could do to my web site. Since I started using the SiteLock website firewall, I haven’t had a single security incident.”</p>

	            <p>SiteLock’s report comes on the heels of other studies which found that nearly a third of all cyber-attacks in 2012 were targeted at small businesses, nearly double the previous year and a very worrying trend.</p>

	            <p>The Top 3 most common security issues discovered by SiteLock were:</p>
                <ol>
                    <li>Malware</li>
                    <li>Cross-Site Scripting</li>
                    <li>SQL injections</li>
                </ol>

	            <p>Malware that hides on websites to infect visitors is known as a drive-by download, and in January 2013 the European Network and Information Security Agency (ENISA) identified the drive-by download as the number one cyber threat worldwide.</p>

	            <p>“A website is a valuable asset, but an unprotected website is a vulnerability that small business owners can’t afford to ignore,” said Neal O’Farrell, an expert on small business cybersecurity and an advisor to SiteLock “Not only are they exposing their livelihood and their customers to serious risk, they also create a national security risk by providing a platform for malware to spread to other computers and users and even potentially attack the nation’s infrastructure.”</p>

	            <h4>SiteLock offers the following tips to help small business owners protect their websites:</h4>
                <ol>
                    <li>Make sure you’re scanning your website around the clock so you can quickly find and plug vulnerabilities before hackers find and exploit them.</li>
                    <li>Create a security plan and policy that specifically addresses website security and makes it easier to remind yourself about routine security tasks.</li>
                    <li>Be careful about the way you manage passwords, and especially about how employees store FTP and other website passwords on their computers and other devices.</li>
                    <li>Don’t just rely on traditional defenses like antivirus protection to detect malware. SiteLock’s studies have found that advanced website scanning detects at least 30% more malware than these conventional security tools.</li>
                    <li>Regularly update any third-party programs or plugins you use on your website, and consider uninstalling any plugins that are no longer supported or which have known security vulnerabilities.</li>
                </ol>

                <p><a href="/pr/smbs-easy-prey-for-cybercriminals" rel="nofollow">Read the complete report.</a></p>

	            <h4>About SiteLock</h4>
                <p>SiteLock is a global website security technology and services leader, protecting more than 1,000,000 websites. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. </p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>