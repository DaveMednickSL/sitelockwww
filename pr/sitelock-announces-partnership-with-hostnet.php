<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Partnership with Hostnet</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-partnership-with-hostnet" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Partnership with Hostnet</h1>
            <h3 class="font22">Global Website Security Provider Extends Leadership Position in the Netherlands</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; March 23, 2012</strong></p> 
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced today that it has joined forces with Hostnet, a leader in the Dutch hosting space. Together, the companies will be able to deliver a one-stop shop for small and midsize online businesses. The partnership was announced at WorldHostingDays in Rust, Germany.</p>

                <p>Hostnet is highly regarded as a leader in the Dutch hosting space, offering services such as domain registration, email, and web hosting for business and private markets. They also help customers create brand awareness and grow their business. Hostnet provides low-cost domains, hosting and other solutions including hosted exchange and SSL certificates. For large and enterprise customers, Hostnet makes available high performance hosting services such as virtual private servers (VPS), dedicated servers and cloud hosting.  </p>

                <p>With more than 13 years in the website hosting business, the company has a dedicated focus on service, value and expertise that enables them to serve hundreds of thousands of customers. Hostnet will offer SiteLock website security technology and services to their current customer base as well as new customers during the onboarding process.</p>

                <p>"SiteLock is thrilled to welcome Hostnet to our partner community," says Tom Serani, executive vice president of business development for SiteLock. "Their expertise in the hosting space as well as the Dutch market is a perfect fit for taking the much-needed website security tools, insight and remediation services provided by SiteLock to small and medium online businesses in the Netherlands."</p>

                <p>"As a hosting company that has built our customer base on the foundation of excellent service, quality and value for our clients, we believe that our partnership with SiteLock makes our online business solution the most complete package available," says Merijn de Brabander, business manager at Hostnet. "While there are a number of website security providers, SiteLock fits best with our approach to comprehensive, innovative technology as well as personal service available for customers when they need help."</p>

                <p>For additional information about Hostnet, visit www.hostnet.nl. For additional information about SiteLock website security technology and services, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other vulnerabilities. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About Hostnet</h4>
                <p>Hosting and domain name service provider Hostnet bv is established in 1999 and has since then became one of the largest and most reliable providers in the Netherlands. Services such as domain registrations, email packages, web hosting, virtual private servers (VPS) and dedicated servers are offered to both business and private market. In over the 12 years that Hostnet exists, it has almost 700,000 domains registered for over 165,000 clients. This growth is mainly due to the following three points: service, reliability and accessibility. Hostnet is a profitable, healthy and independent company based in Amsterdam and has over 50 employees. More information about Hostnet and its services can be found at <a href="https://www.hostnet.nl" target="_blank">www.hostnet.nl</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
