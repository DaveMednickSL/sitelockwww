<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>MochaHost Announces Partnership with SiteLock</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/mochahost-announces-partnership-with-sitelock" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">MochaHost Announces Partnership with SiteLock</h1>
            <hr>

                <p><strong>San Jose, CA &mdash; September 29, 2011</strong></p> 
                <p>MochaHost, web search giant and hosting company recently announced its partnership with the web security firm SiteLock. This partnership gives MochaHost customers access to special pricing on SiteLock's innovative line of security products and services. This helps site owners solidify the security and credibility of their online properties. The partnership expands the range of value-added services MochaHost provides its clients, and continues the impressive growth of the security firm, SiteLock.</p>
					
				<p>SiteLock President, Neill Feather commented, "We're very excited about this new partnership with MochaHost . It's a great opportunity for us to partner with such a well-known and respected hosting company and to offer their customers access to a great value-added website security service to protect their website investments."</p>
				<h4>About SiteLock</h4>
				<p>SiteLock provides web security services that ensure a safe and productive Internet environment for online businesses. SiteLock solutions provide a comprehensive suite of website security products with packages tailored to meet the specific needs of small business. Unlike competitive offerings, SiteLock's security services are combined to provide the most complete and affordable website security solution available.</p>
					
                <p>SiteLock's offices are located in Scottsdale, Arizona. For more information, visit <a href="https://www.sitelock.com">https://www.SiteLock.com</a> or call 877-257-9263; internationally at (415) 390-2500.</p>
					
               <h4>About MochaHost</h4>
                <p>MochaHost is located in San Jose, California, the heart of Silicon Valley. MochaHost specializes in providing professional hosting, web design, E-commerce, and Internet promotion services. MochaHost's plans are designed to tailor to specific needs - from personal use/small businesses to sophisticated E-commerce sites with higher requirements for bandwidth and disk space. For more information about the company, please visit <a href="http://www.mochahost.com" target="_blank">http://www.mochahost.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
