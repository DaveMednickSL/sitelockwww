<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Reveals Enhanced Website Scanning Technology</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-reveals-enhanced-website-scanning-technology" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Reveals Enhanced Website Scanning Technology</h1>
            <h3 class="font22">Secure Malware Alert and Removal Tool (SiteLock SMART<sup>&trade;</sup>)</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; July 17, 2012</strong></p> 
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced today that it is offering a new website scanning package to its customers. Unlike most website scanning products that look at the website front-end from an outside-in approach, the SiteLock SMART technology is designed to evaluate website files from both outside-in and inside-out perspectives. The SiteLock SMART (Secure Malware Alert and Removal Tool) is the company's most comprehensive malware scanning package yet.</p>

				<p>The SMART technology performs deep website scans at set intervals to detect malware and automatically remove it. This process ensures that the website maintains a safe and secure environment for visitors. In addition, the SMART feature offers SiteLock customers an additional layer of security to ensure peace of mind. SMART identifies any file changes and immediately categorizes them as suspicious or malicious, giving website owners full visibility into all changes in their web environment.</p>

				<p>"If your website has been compromised by hackers injecting malware, they may be using your website to distribute virus-infected software to your visitors without your knowledge," explains SiteLock President, Neill Feather. "Most of our customers are small and medium sized businesses who cannot afford to have their websites taken down. The SMART scan works independently to keep websites clean and maintain a consistently safe environment, with minimal effort required from the website owner."</p>

				<p>In addition to the enhanced, intelligent scan types, SMART also performs fuzzy logic to identify potentially harmful code, even if it's not on an identified list of "known suspects". It also conducts a file comparison to detect any changes that have been implemented on the site. The automated alerts notify customers of issues that could become problems so they can be fixed before allowing a site to be infected.</p> 

				<p>The SiteLock SMART package, as well as all of SiteLock website security products, can be easily controlled through the SiteLock dashboard. The dashboard is now available in multiple languages including English, French, Dutch, Spanish, German, Italian and Brazilian-Portuguese.</p> 

				<p>SiteLock protects small businesses. The rapid acceleration of malware and other malicious attacks on business websites is making this a near epidemic challenge that puts small and midsize companies at the greatest risk for website security threats, and SiteLock helps prevent them.</p>

                <p>For additional information about SiteLock website security technology and services, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other forms of threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>