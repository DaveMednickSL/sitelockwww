<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Joins Forces with Savvis, a CenturyLink Company</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-joins-forces-with-savvis" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Joins Forces with Savvis, a CenturyLink Company</h1>
            <h3 class="font22">Website Security Now Bundled with savvisdirect Webhosting</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; May 17, 2013</strong></p> 
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced today its integrated alliance with Savvis (www.savvis.com), a CenturyLink company and global leader in cloud infrastructure and hosted IT solutions. The SiteLock Basic security plan, which includes daily malware scans, a comprehensive vulnerability scan and business reputation management, is now automatically included in the Webhosting Advanced packages available through the savvisdirect web portal.</p>  

	            <p>According to a recent research study conducted by the Ponemon Institute, 55 percent of small businesses have suffered some kind of data breach, with a primary source of attack being attempts to steal credit card data from business websites. Many small business owners have little or no technical resources to address potential website security threats, putting them at an increased risk for attack. Combined with the open source technology platforms on which their websites are frequently built, small online businesses become a prime target for cybercrime, which unfortunately can put millions of consumers’ data at risk for fraud and theft. In fact, SiteLock identifies thousands of websites each day that have malware and other vulnerabilities, typically finding 140% more malware than traditional virus scanners and 170% more than search engine blacklists.</p>

	            <p>As a leader in the cloud hosting market, Savvis builds security into the foundation of its cloud service offerings throughout its global data center footprint. Bundling SiteLock website security as part of savvisdirect Webhosting Advanced packages enhances the level of protection for cloud customers.</p>

	            <p>“Companies that host their websites in the cloud want to focus on growing their businesses – they don’t want to worry about cybercrime,” said Steve Robinson, director of product management at Savvis. “Bundling website security services like SiteLock into savvisdirect serves as a highly effective, user-friendly way we can deliver peace of mind to both businesses and their website visitors.”</p>

	            <p>“At SiteLock, our approach is to make security simple and readily available for online businesses,” says Tom Serani, executive vice president of business development for SiteLock. “Our alliance with Savvis provides a unique and seamless way to deliver website security directly to their hosting customers and at the same time enable them to display the SiteLock Trust Seal, which can help increase online conversions, sales and visitor trust.”</p>

                <p>For additional information about SiteLock website security technology and services, visit <a href="https://www.sitelock.com">www.sitelock.com</a>. To learn more about savvisdirect, visit <a href="http://www.savvisdirect.com" target="_blank">http://www.savvisdirect.com</a>.</p>

	            <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%.  As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit www.sitelock.com.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
