<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Partners With Media Temple To Offer Comprehensive Hosting Packages</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-partners-with-media-temple" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Partners With Media Temple To Offer Comprehensive Hosting Packages</h1>
            <h3 class="font22">SiteLock's Best-In-Class Security Solutions Now Offered with Web Host's Managed Web Hosting Solutions</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; October 22, 2015</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a>, the global leader in website security solutions, announced they entered a partnership with Media Temple, a Los Angeles-based company that provides web and cloud hosting solutions to web designers, developers and creative agencies, to automatically include topnotch website security and malware protection.</p>

                <p>According to industry research, 80% of website attacks are aimed at web applications1 and many companies aren't prepared for possible compromises. Through the partnership, SiteLock website protection is now automatically offered with Media Temple's managed VPS hosting (Virtual Private Server) offering as well as with Grid, the company's shared hosting platform, and its upper tier Managed WordPress plans.</p>

                <p>"We continue to grow our business in the hosting space with excellent service and the ability to provide peace of mind to users. Websites hosted through Media Temple will now benefit from this partnership," notes Tom Serani, Executive Vice President of Business Development for SiteLock.</p>

                <p>"We take security very seriously, which is why exclusively partnering with the industry leader in website security solutions, SiteLock, was a very logical choice for Media Temple. With security features like SiteLock's automated malware scan and removal, our customers can spend more time running and scaling their business and less time worrying about the security of their sites or web apps," says Zuzana Byrum, Senior Product Manager at Media Temple.</p>

                <p>SiteLock can detect malware the minute it hits. After identifying malicious content, it automatically neutralizes and removes the threats. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 5,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p> 

                <cite><a href="https://www.scmagazine.com/web-apps-account-for-80-percent-of-internet-vulnerabilities/article/555592/" target="_blank">1. Cenzic report focused on vulnerability disclosures for various commercial off-the-shelf and open-source software, 2009.</a></cite> 

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html> 