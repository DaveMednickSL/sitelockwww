<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Lycos Partnership</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-lycos-partnership" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Lycos Partnership</h1>
            <hr>

                <p><strong>TEMPE, ARIZONA &mdash; April 14, 2010</strong></p> 
                <p>Web Security firm SiteLock announced today its partnership with web search giant and hosting company, Lycos. This partnership gives Lycos customers access to special pricing on SiteLock's innovative line of security products and services, which will help these site owners solidify the security and credibility of their online properties. The partnership expands the range of value-added services Lycos provides its clients, and continues the impressive growth of security firm, SiteLock.</p>

                <p>SiteLock General Manager Josh Lesavoy commented, "We're thrilled to add Lycos as a partner. It's a great opportunity for us to partner with a company that has such a storied history and fantastic reputation. It also gives Lycos customers access to a great value-added security service."</p>
                
                <p>SiteLock General Manager Josh Lesavoy described the partnership as "mutually beneficial" and went on to say, "We're excited to form this partnership with a leader in the hosting space like HostGator. This will provide a great value for their customers, and allows us to continue to provide security to greater numbers of small businesses and sites across the web."</p>

                <h4>About SiteLock</h4>
                
                <p>SiteLock provides web security services that ensure a safe and productive Internet environment for online businesses. SiteLock solutions provide a comprehensive suite of security products with packages tailored to meet the specific needs of small business. Unlike competitive offerings, SiteLock's Business Verification and Security services are combined to provide the most complete and affordable business security solution available.</p>

                <p>SiteLock's offices are located in Tempe, Arizona. For more information, visit the website at <a href="https://www.sitelock.com">www.sitelock.com</a> or call 877-257-9263.</p>

                <h4>About Lycos</h4>
                <p>Lycos is one of the original (est. 1995) and most widely-known Internet brands in the world, evolving from one of the first search engines on the web, into a comprehensive network of social media web sites that foster online communities. Lycos's award-winning products and services include tools for video viewing and sharing, social networking, blogging, web publishing and hosting, online games, e-mail, and search.</p>

                <p>Lycos consistently averages 12 - 15 million monthly unique visitors in the U.S., and is a top 25 Internet destination worldwide, reaching nearly 60 million unique visitors globally. The Lycos Network of sites and services include Lycos.com, Tripod, Angelfire, HotBot, The Lycos 50™, Gamesville, WhoWhere, Lycos Mail, Lycos Cinema and Webon. Integrated, these sites help individual users retrieve, manage, consume and create information tailored to meet his or her personal interests.</p>

                <p>Lycos Domains makes it easy and affordable for anyone to have their own unique web address. For only $12.95 a year, users can pick a domain for their personal or family web site, online business, sports team, classroom and more.</p>

                <p>Lycos is a wholly-owned subsidiary of Daum Communications Corp., a leading search and Internet portal in Korea, and has its U.S. headquarters in Waltham, Massachusetts.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>