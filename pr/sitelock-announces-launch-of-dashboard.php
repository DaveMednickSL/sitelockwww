<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Launch of New Website Security Technology and Dashboard</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-launch-of-dashboard" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Launch of New Website Security Technology and Dashboard</h1>
            <h3 class="font22">Enhanced Scanning and Interface Show Renewed Focus on Ease of Use for Small Business</h3>
            <hr>

            <p><strong>JACKSONVILLE, FLORIDA &mdash; May 30, 2012</strong></p> 
            <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced today that it has released an enhanced, simplified version of its website security scanning technology and client dashboard.</p> 

            <p>The website security technology enhancements include a more sophisticated, comprehensive level of application scanning; real-time updates to the threats that are detected; and a highly sensitive search for actual threats that minimizes false positives. Business owners need to be aware of the real impacts to their websites, not empty threats that distract their focus on running their companies. The service is more lightweight than ever, designed for any hosting environment and can be customized based on partner requirements. In addition, the results of the newly updated technology are now displayed on the enhanced real-time business dashboard.</p>

            <p>The new online tool now provides multi-lingual support for German and Spanish languages, making website security risks and infections accessible at business owners' fingertips in multiple global locations. It provides a single source for detected issues, steps to fix them, and a direct connection with experts who can quickly solve problems that are beyond the business owners' technical resources. The website security dashboard now displays an extensive list of business-focused messages that notify users of identified issues, while simplifying the technical details. Dashboard results and reports can also be easily exported to CSV files for further analysis.</p>

            <p>"SiteLock is committed to our focus of delivering website security solutions that make sense for small business owners," says Neill Feather, president of SiteLock. "The enhancements to our scanning technology as well as the client dashboard interface will streamline the process of maintaining a secure online presence for our clients."</p>

            <p>For additional information or to request a demo of the enhanced SiteLock website security technology and services, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

            <h4>About SiteLock</h4>
            <p>Established in 2008, SiteLock has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other vulnerabilities. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>