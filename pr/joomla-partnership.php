<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Partnership with Joomla!</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/joomla-partnership" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Partnership with Joomla!</h1>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; November 14, 2017</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a>, the global leader in website security solutions, announced today its exclusive partnership with Joomla!, a leading Content Management System (CMS).</p>
                <p>SiteLock and Joomla! are pleased to announce a new partnership where Joomla will be using and promoting SiteLock’s products and services such as industry-leading solutions for website security and acceleration including: automatic malware detection and remediation, vulnerability scanning, website application firewall (WAF) and content delivery network (CDN).</p>
                <p>“Joomla! understands how critical website security is for our users, and we are excited to work with SiteLock to promote these solutions,” said Robert Jacobi, President of Joomla!. “Joomla! is designed to be so simple and easy to use that everyone from small business owners to sophisticated web designers and developers can be empowered. By partnering with SiteLock, we will be able to expose our users to the fast, affordable website vulnerability detection, automatic malware removal and threat prevention service that SiteLock offers.”</p>
                <p>“Joomla! is a key player in the open source content management system environment that now powers approximately three percent of all the website on the internet,” said Tom Serani, Executive Vice President of Business Development for SiteLock. “Joomla! understands the importance of online security for its users, and we are excited that our partnership will help ensure that more websites run safely and smoothly.”</p>
                <p>Joomla!, which accounts for around seven percent of the CMS market, is an excellent partnership opportunity to promote security solutions to more website owners who need it now more than ever. With websites experiencing an average of 63 attacks per day, which is nearly 23,000 attacks per year, website security can no longer be overlooked. SiteLock will provide security solutions to users for a safe website experience in an open source environment where flexibility and ease of use are imperative. In addition, Joomla! will be included in the SiteLock platform scan, which proactively reviews website plugins and themes within content management systems to identify malicious threats and vulnerabilities.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million customers worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

                <h4>About Joomla!</h4>
                <strong>The Flexible Platform - Empowering Website Creators</strong>
                <p>Joomla! is an award-winning content management system (CMS) used to build beautiful web sites and powerful online applications. A global volunteer community of developers has been building and improving Joomla! since the first version was released in 2005. This immense effort has made Joomla! easy to use, stable and very secure. Joomla! is search engine and mobile friendly, multilingual, flexible and extensible. Offering unlimited design possibilities alongside industry leading security, Joomla! also has thousands of third party extensions and templates allowing further customization to meet specific needs. Best of all, Joomla! is an open source solution that is freely available to everyone. Find out more at <a href="https://www.joomla.org/" target="_blank">www.joomla.org</a></p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
