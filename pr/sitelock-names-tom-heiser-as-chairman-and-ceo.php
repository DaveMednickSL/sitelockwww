<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Names Tom Heiser as Chairman and Chief Executive Officer</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/ota-names-sitelock-to-2017-honor-roll" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font22">SiteLock Names Tom Heiser as Chairman and Chief Executive Officer</h1>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; February 15, 2019</strong></p>
                <p><a href="https://sitelock.com">SiteLock</a>, the global leader in website security solutions, today announced the appointment of Tom Heiser as chairman and chief executive officer, effective immediately. Heiser was brought on as <a href="https://www.sitelock.com/pr/sitelock-expands-leadership-roster">executive chairman of the SiteLock board</a> in August of 2018 and has since helped the company refine its strategy and streamline go-to-market operations. As Chairman and CEO, he will help lead the company through its next stages of growth.</p>

				<p>Heiser is a seasoned technology leader with more than 30 years of experience accelerating innovation and scaling high-growth companies, ranging from startups to complex multinational businesses. He has held notable positions throughout his career including EVP of EMC Corporation, president of RSA Security, and CEO of ClickSoftware. In addition to SiteLock, Heiser also sits on the board of directors for multiple technology companies, including Arxan, AtScale, and NuSpire, and formerly sat on the board of directors for CyberArk and ZSclaer.</p>

                <p>“SiteLock has successfully established itself as a proven market leader with a track record of consistent and impressive growth,” said Heiser. “The company has done a tremendous job anticipating the needs of the cybersecurity industry, driving continued innovation, and delivering simple, affordable, powerful solutions to over 12M customers worldwide. I’m thrilled to join the SiteLock team to build on this momentum, and help take the company to the next level of growth and scale.”</p>

                <p>Current SiteLock President and CEO Neill Feather will transition to the role of Chief Innovation Officer. Feather will fuel the company’s growth strategy through aggressive organic and inorganic expansion of product capabilities. This acceleration of technology innovation will deliver additional value to customers and partners and expand SiteLock’s market reach.</p>

                <p>“Since it was founded 10 years ago, SiteLock has grown quickly and is well-positioned to scale into the future,” said Feather. “Doing so requires that we maintain industry leadership through new technology partnerships and significant innovation, anticipating our customers’ and partners’ future needs. I’m excited to work with Tom, our talented leadership team, and our external stakeholders to help SiteLock deliver a holistic portfolio of security solutions for the SMB market.”</p>

                <p>“We are both fortunate and grateful to have Neill’s deep industry knowledge, technological leadership, and strategic expertise to help drive SiteLock forward in its mission to protect every website on the internet,” said Heiser. “Building upon its strong foundation as a trusted security provider, SiteLock is well positioned to help shape the future of cybersecurity.”</p>
			
				<h4>About SiteLock</h4>
                <p>SiteLock, the global leader in business website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million websites worldwide. For more information, please visit sitelock.com.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>