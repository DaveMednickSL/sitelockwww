<?php 
include('../includes/top.php'); 
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SiteLock to Serve as Data Privacy Day Champion for National Cyber Security Alliance | January 07, 2013</title>
<link rel="canonical" href="https://www.sitelock.com/pr/sitelock-to-serve-as-data-privacy-day-champion"/>
<?php include($root.'/includes/metadataRS.php'); ?>
</head>

<body class="press-releases">
   
    <?php include($root.'/includes/gt.php'); ?>
    <?php include($root."/includes/_headerPushy.php"); ?>
    <div class="container" id="container">
	<section class="module content">
	
        <br />
        <div class="ctrDiv">
            <h1>SiteLock to Serve as Data Privacy Day Champion for National Cyber Security Alliance</h1>
            <h2>SiteLock Will Help Educate Businesses About Responsible Data Protection Practices</h2>
            <div class="clearFloat forty"></div>
            <div class="pr-body">

                <p><strong>JACKSONVILLE, FLORIDA &mdash; January 07, 2013</strong></p>
                <p>SiteLock LLC (<a href="https://www.sitelock.com">https://www.sitelock.com</a>), a leading provider of website security solutions for online businesses around the world, announced today that it has volunteered to participate in the fifth annual Data Privacy Day as part of the company's ongoing commitment to cyber security.

                <p>Organized through the National Cyber Security Alliance, Data Privacy Day (DPD) is held on January 28th and serves as a reminder of the importance of protecting the private and financial data of all online citizens. The celebration will focus around the need to be aware of the large amount of personal data that is transmitted through the online world each day, and ways to remain vigilant and proactive about protecting it.</p>

                <p>As a DPD champion, SiteLock plans to support the initiative within the company and in the market by educating customers, employees and communities about the importance of keeping data safe from hackers. SiteLock also sponsors and attends multiple industry events, during which the company demonstrates its website security technology and shares insight about the many online risks that can affect consumer data.</p>

                <p>"We continually look to the National Cyber Security Alliance, as an industry leader and advocate for online safety, to establish website security and data privacy practices that are relied upon by consumers and businesses alike," says Neill Feather, President of SiteLock. "Our focus at SiteLock is to ensure that businesses with an online presence are keeping their customers' data safe, so we look forward to being able to contribute our website security expertise in support of Data Privacy Day."</p>

                <h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (@SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security and PCI compliance solutions available on the market. SiteLock helps protect websites against malware, spam, viruses and other forms of online threats from hackers. SiteLock's Expert Services team specializes in malware removal and harden website platforms to help prevent future attacks. SiteLock currently protects over 600,000 websites worldwide and scans over 3 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="http://www.sitelock.com">http://www.sitelock.com</a>.</p>

                <h4>About National Cyber Security Alliance</h4>
                <p>The National Cyber Security Alliance is a non-profit organization. Through collaboration with the government, corporate, non-profit and academic sectors, the mission of the NCSA is to empower a digital citizenry to use the Internet securely and safely protecting themselves and the technology they use and the digital assets we all share. NCSA works to create a culture of cyber security and safety through education and awareness activities. NCSA board members include: ADP, AT&T, Bank of America, EMC Corporation, ESET, Facebook, Google, Intel, Microsoft, PayPal, Science Applications International Corporation (SAIC), Trend Micro, Symantec, Verizon and Visa. Visit <a href="http://www.staysafeonline.org" target="_blank">http://www.staysafeonline.org</a> for more information.</p>
                
            </div><!--End 'ctrDiv'-->
        </div><!--End 'pr-body'-->
    </section>
    <div class="clearFloat forty"></div>
    <?php include($root.'/includes/_footerPushy.php'); ?>
    </body>
    </html>