<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Teams with Lycos to Heighten Online Business Security</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-teams-with-lycos-to-heighten-online-business-security" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Teams with Lycos to Heighten Online Business Security</h1>
            <h3 class="font22">SiteLock and Lycos Complete Analysis of 3 Million Websites</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; July 10, 2012</strong></p> 
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced today that it teamed with Lycos, Inc (www.lycos.com), a search engine turned website and community hosting provider, to evaluate the security status of clients' websites.</p>

	           <p>Lycos, having evolved its business model in recent years, is a global online network that has become a go-to source for web search and navigation, multiplayer real-time games, homepage building and web community services. Integrated, these sites help individual users locate, retrieve and manage information tailored to his or her personal interests. Included in Lycos' products are free web hosting platforms that support more than three million websites.</p>

	           <p>SiteLock and Lycos joined forces to evaluate the security risks of those millions of websites to identify potential problems such as malware, terms of service abuse, phishing and link farming. Due to the free nature of the sites being hosted, there was an inherent risk of users building websites in the environment with the intent of distributing malicious or potentially harmful code. Lycos actively recognized this risk and secured a relationship with SiteLock to identify all of the websites that were affected by such threats.</p> 

	           <p>The patent-pending 360-degree scans performed by SiteLock identified thousands of websites that were infected and could be harmful to their visitors. Lycos' goal was to clean up their online community and, therefore, took down the problematic sites. At the same time, SiteLock gained extensive insight into risks, hacks and potential threats that they were able to add to their technology's knowledge base to help prevent other online businesses from being affected.</p>

	           <p>"At Lycos, we build online communities. Our customers can vary greatly from business owners and bloggers, to gamers and social users," describes Rob Balazy, CEO of Lycos. "It's imperative that those experiences are conducted in a safe environment. We took an active approach through our partnership with SiteLock to identify the problem areas in our online neighborhoods. Based on the results of the SiteLock scanning, we were able to clean up the communities on the website hosting platforms of Tripod.com and Angelfire.com, so that our customers and their visitors are protected from online threats."</p>

	           <p>"At SiteLock, we believe in making the web a safer place for our business customers, their visitors, and our partners," states Neill Feather, President of SiteLock. "We have a high degree of respect for Lycos, an online community leader, who took the proactive step to secure their customers' websites and keep the 'bad guys' out in the future. We believe that this partnership will enable Lycos and SiteLock to stay ahead of the threat curve and build a repeatable model for heightened detection and awareness of online risks."</p>

	           <p>SiteLock and Lycos plan to repeat the comprehensive scanning on a quarterly basis to continually remove the identified websites with problems.</p>
 
	           <h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other forms of threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

	           <h4>About Lycos</h4>
                <p>Established in 1995, Lycos was founded as one of the first search engines. In stride with the changing Internet, Lycos has gone through propitious transformations, acquiring Gamesville.com, Tripod.com, and Angelfire.com, to build the established network that it is today. Headquartered in Waltham, Massachusetts Lycos is a wholly owned subsidiary of Ybrant Digital, the end-to-end global digital marketing company. For more information, visit: <a href="http://www.lycos.com" target="_blank">www.lycos.com</a> and <a href="http://www.ybrantdigital.com" target="_blank">www.ybrantdigital.com</a></p>

                <p>Author: Mary Miller</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>