<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Partnership with CloudFlare</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-partnership-with-cloudflare" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Partnership with CloudFlare</h1>
            <h3 class="font22">SiteLock Joins the CloudFlare Community to Safely Accelerate Online Businesses' Connection with their Visitors</h3>
            <hr>

                <p><strong>ST. JOHNS, FLORIDA &mdash; April 20, 2012</strong></p> 

                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced today that it has partnered with CloudFlare, a web performance and security company. Together, the companies will deliver top-rated website performance and security for small and midsize online businesses.</p>

				<p>This announcement comes as SiteLock makes its website security and monitoring solution available on CloudFlare's application platform. CloudFlare offers the SiteLock Premium and SMB Enterprise security packages through its global network and application delivery platform. These comprehensive website security services are designed for websites with up to 500 pages and 2500 pages, respectively. These versions offer ideal protection for all types of websites, from content-only to large e-commerce websites.</p>    

				<p>"Last year alone, SiteLock fixed over 1100 business websites that were hacked. With the severity and number of threats targeting small businesses increasing daily, we continue to see proof that security should be treated as a multi-layered process and not accomplished with one particular product or service," said Givonn Jones, business development manager of SiteLock. "Given that many small businesses have limited time and IT resources, the powerful combination of CloudFlare's speed and SiteLock's security provide the peace of mind that their websites are meeting their business needs and keeping their visitors and customers safe."</p> 

				<p>"SiteLock provides an additional layer of security beyond the existing protection of CloudFlare to ensure your investment is protected and your reputation is safe," said John Roberts, Platform Lead at CloudFlare. "By partnering with SiteLock, we are continuing to provide our customers with tools to make their sites faster and safer. We are excited to welcome them as a CloudFlare application partner."</p>

                <p>SiteLock is now available to all CloudFlare customers via the CloudFlare application platform: <a href="https://www.cloudflare.com/apps/sitelock" target="_blank">https://www.cloudflare.com/apps/sitelock</a></p>

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (www.sitelock.com / @sitelock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other vulnerabilities. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona. For more information, call 1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

				<h4>About CloudFlare</h4>
                <p>CloudFlare, Inc. (<a href="https://www.cloudflare.com" target="_blank">www.cloudflare.com</a> / @cloudflare) makes sites twice as fast, protects them from attacks, ensures they are always online, and makes it simple to add web apps with a single click. CloudFlare supercharges websites regardless of size or platform with no need to add hardware, install software, or change a line of code. The CloudFlare community gets stronger as it grows; every new site makes the network smarter. Thanks to our awesome sauce technology, every month hundreds of millions of people experience a faster, safer, better Internet. CloudFlare is recognized by the World Economic Forum as a Technology Pioneer and was named the 2011 Most Innovative Network &amp; Internet Technology Company of the Year by the Wall Street Journal. CloudFlare is based in San Francisco, California, USA.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>