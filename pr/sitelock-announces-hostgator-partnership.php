<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces HostGator Partnership</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-hostgator-partnership" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces HostGator Partnership</h1>
            <hr>

                <p><strong>TEMPE, ARIZONA &mdash; February 28, 2010</strong></p> 
                <p>Web Security firm SiteLock announced today its partnership with web hosting leader, HostGator. This partnership will enable HostGator to provide SiteLock's security services to its web hosting customers, further solidifying the security and credibility of individual customer sites. The partnership expands the range of value-added services HostGator provides its clients, and continues the impressive growth of security firm, SiteLock.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock General Manager Josh Lesavoy described the partnership as "mutually beneficial" and went on to say, "We're excited to form this partnership with a leader in the hosting space like HostGator. This will provide a great value for their customers, and allows us to continue to provide security to greater numbers of small businesses and sites across the web."</p>

                <p>SiteLock provides web security services that ensure a safe and productive Internet environment for online businesses. SiteLock solutions provide a comprehensive suite of security products with packages tailored to meet the specific needs of small business. Unlike competitive offerings, SiteLock's Business Verification and Security services are combined to provide the most complete and affordable business security solution available.</p>

                <p>SiteLock's offices are located in Tempe, Arizona. For more information, visit the website at <a href="https://www.sitelock.com">www.sitelock.com</a> or call 877-257-9263.</p>

                <h4>About HostGator</h4>
                <p>HostGator.com LLC is a world leading provider of shared, reseller, and dedicated web hosting. Privately held and based in Houston, Texas, the company was founded in 2002 by current Founder and Chairman Brent Oxley, who started the company from his dorm room at Florida Atlantic University. Since then, HostGator has grown from a small hosting company with just three servers into a world leading and industry recognized hosting provider with more than 7,000 servers under management. The over two million domains hosted by HostGator make up approximately 1% of the world's Internet traffic.</p>

                <p>As a technology and product innovator, HostGator is able to provide its more than 225,000 customers with innovative products and services designed to complement their existing businesses. HostGator serves customers ranging from individual freelancers to Fortune 500 companies in more than 170 countries.</p>

                <p>In 2008, HostGator was recognized by Inc. Magazine as one of the fastest growing private companies in America. According to Inc. Magazine, HostGator is the 21st fastest growing private company in America, the second fastest growing private company in the "business services" sector, and the fastest growing private company in Texas.</p>

                <p>In a June 2008 customer satisfaction survey, approximately 90% of HostGator customers reported being satisfied with HostGator and the services it provides. Industry-leading customer service and support is at the forefront of what HostGator does on a day-to-day basis. The company is constantly working to improve and refine that service and support and achieve its goal of becoming the world leader in the global web hosting market.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>