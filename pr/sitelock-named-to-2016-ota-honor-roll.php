<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Named To The 2016 Online Trust Alliance Honor Roll</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-named-to-2016-ota-honor-roll" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font25">SiteLock Named To The 2016 Online Trust Alliance Honor Roll</h1>
            <h3 class="font22">SiteLock Included in Prestigious List for Fourth Consecutive Year</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; June 14, 2016</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a>, the global leader in website security solutions, today announced that it has been named to the 2016 Online Trust Alliance's (OTA) Honor Roll listing, marking the fourth consecutive year the company has earned this distinction for its excellent leadership and a commitment to data protection, privacy and security.</p>

                <p>The Online Trust and Honor Roll Audit is the only independent online trust benchmark study. Through its comprehensive study, it evaluates companies in three major categories: a company's consumer protection, data and site security and adherence to responsible privacy practice.</p>

                <p>"We're proud to earn a place on the 2016 Online Trust Alliance's (OTA) 2016 Honor Roll for the fourth consecutive year. This recognition underscores our commitment to providing best-in-class solutions that protect consumers' security and privacy. Cybercriminals are evolving, but together with industry leaders and the OTA, we can advance best practices that address both current and future security challenges," said <a href="https://www.sitelock.com">Neill Feather, President of SiteLock.</a></p>

                <p>In total, 1,000 consumer facing websites were analyzed, including the Internet Retailer Top 500, FDIC 100 banks, top social networking companies, top 50 news and media companies, government agencies and leading Internet of Things (IoT) providers focused on home automation and wearable technologies.</p>

                <p>To qualify for the Honor Roll, companies had to receive a composite score of 80% or better and a score of at least 55 in each of the three categories. The 2016 scoring has been expanded and enhanced with additional weight and granularity given to key practices. One of the major changes for 2016 involved scoring sites which fail any major component of the site security assessment (normally equating to a "C" or lesser grade) as an automatic fail for the overall Audit—demonstrating that a site's security is only as strong as its weakest link.</p>

                <p>"For the past several years SiteLock has provided significant insights to the security challenges impacting web sites worldwide. Their commitment to collaboration and advancing best practices today are paying dividends for the internet tomorrow," said Craig Spiezle, Executive Director and President Online Trust Alliance. "As a recipient of the Honor Roll for the fourth consecutive year, they are walking the talk and embracing essential security, consumer protection and responsible privacy practices."</p>

                <p>To review the full 2016 Online Trust Honor Roll Report, please visit <a href="https://otalliance.org/HonorRoll" target="_blank">https://otalliance.org/HonorRoll</a>.</p>

                <p>SiteLock can detect malware the minute it hits. After identifying malicious content, it automatically neutralizes and removes the threats. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock provides comprehensive, cloud-based website security to all businesses. The company offers a suite of products that help businesses defend against malicious activity and harmful requests. Founded in 2008, the company currently protects over six million websites worldwide. For more information, please visit <a href="https://www.sitelock.com">SiteLock.com</a>.</p>

                <h4>About OTA</h4>
                <p>The Online Trust Alliance (OTA) is a non-profit with the mission to enhance online trust and user empowerment while promoting innovation and the vitality of the Internet. Its goal is to help educate businesses, policy makers and stakeholders while developing and advancing best practices and tools to enhance the protection of users' security, privacy and identity. OTA supports collaborative public-private partnerships, benchmark reporting, and meaningful self-regulation and data stewardship. Its members and supporters include leaders spanning the public policy, technology, ecommerce, social networking, mobile, email and interactive marketing, financial, service provider, government agency and industry organization sectors.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>