<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Ranked Number 85 Fastest Growing Company in North America on Deloitte's 2015 Technology Fast 500</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-ranked-85-fastest-growing-company" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Ranked Number 85 Fastest Growing Company in North America on Deloitte's 2015 Technology Fast 500<sup>&trade;</sup></h1>
            <h3 class="font22">Attributes 1046 Percent Revenue Growth to Increased Recognition of the Importance of Security Protections</h3>
            <hr>
                
                <p><strong>SCOTTSDALE, ARIZONA &mdash; November 17, 2015</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a> today announced it ranked No. 85 on Deloitte's Technology Fast 500<sup>&trade;</sup>, a ranking of the 500 fastest growing technology, media, telecommunications, life sciences and energy tech companies in North America. SiteLock grew 1046 percent during this period.</p>

                <p>"We are honored to be recognized among this year's Deloitte Technology Fast 500," said Neill Feather, President of SiteLock. "This recognition is a testament to the success of SiteLock's excellent products, customer service, and support that are unrivaled in the security space. We look forward to continuing the growth that has been integral to our success as we help more customers seamlessly integrate security into their sites."</p>

                <p>Feather credits wider understanding of the importance of web application security among website owners and extraordinary relationships with strategic partners in the service provider space for the company's 1046 percent revenue growth.</p>

                <p>"Amid a fierce business climate, there seems to be no shortage of new and established companies that are unlocking a seemingly unlimited potential for growth and advancement through technology's continued disruption and proliferation across industries," said Sandra Shirai, principal, Deloitte Consulting LLP and U.S. technology, media and telecommunications leader. "It is inspiring to witness the innovative ways that companies are incorporating emerging technologies for business gains, be it cognitive computing, or the Internet of Things. We congratulate all those ranked on this year's Fast 500 and look forward to seeing their continued growth into 2016."</p>

                <p>"Through the efforts and utilization of new and emerging technologies from these companies, we are witnessing greater business demands from across almost all industries," added Jim Atwell, national managing partner of the emerging company practice, Deloitte &amp; Touche LLP. "We look forward to the opportunity to serve these companies as they strive to grow to the next level –  be it towards introducing new solutions or entering new markets – and with it make important and long lasting impressions on the technology market as a whole."</p> 

                <p>SiteLock's growth of 1046 percent comes in above the 2015 Technology Fast 500™ companies achieved revenue average growth of 850 percent.</p>

                <h4>About Deloitte's 2015 Technology Fast 500<sup>&trade;</sup></h4>
                <p>Deloitte's Technology Fast 500 provides a ranking of the fastest growing technology, media, telecommunications, life sciences and energy tech companies – both public and private – in North America. Technology Fast 500 award winners are selected based on percentage fiscal year revenue growth from 2011 to 2014.</p>

                <p>In order to be eligible for Technology Fast 500 recognition, companies must own proprietary intellectual property or technology that is sold to customers in products that contribute to a majority of the company's operating revenues. Companies must have base-year operating revenues of at least $50,000 USD or CD, and current-year operating revenues of at least $5 million USD or CD. Additionally, companies must be in business for a minimum of four years and be headquartered within North America.</p>

                <h4>About SiteLock</h4>     
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 5,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>