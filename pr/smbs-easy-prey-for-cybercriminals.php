<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SMBs: Easy Prey for Cybercriminals?</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/smbs-easy-prey-for-cybercriminals" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1>SMBs: Easy Prey for Cybercriminals?</h1>
            <hr>

                <p><strong>Seems just about everybody loves the small business. And who wouldn’t? The small business is both the engine and the jewel of the U.S. economy, accounting for more than 99% of all businesses in America. They’re a job engine too, generating 64% of net new jobs over the past 15 years, representing 44% of total U.S. private payroll and 40% of high tech workers. And they’re the innovation engine, producing 13 times more patents per employee than large firms.</strong></p>

               <p>But amongst the crowds of adoring fans hides a growing legion of more sinister admirers. They’re the hackers, identity thieves, and cyber crooks who now see your small business as their big payday.</p>

               <p>Smaller firms seem to be under attack from all sides – their computers, their bank accounts, and even their websites. A 2011 study by Verizon of more than 800 investigated data breaches found that the vast majority of these breaches occurred not at multinational giants but at small businesses. </p>

               <p>In its report, Verizon concluded that "Small to medium-sized businesses represent prime attack targets for many hackers, who favor highly automated, repeatable attacks against these more vulnerable targets.” This finding is supported by Visa which claims that 95% of its credit breaches occur at its smallest businesses.</p>

               <p>Data isn’t the only thing the crooks are after, and money is still a respected currency in the cybercrime underground. According to Tom Wills, fraud analyst with Javelin Strategy & Research "The low-hanging fruit for these overseas criminal syndicates is clearly small and medium-sized businesses, which, because of inadequate and antiquated security controls at 99 percent of U.S. banks, combined with the larger bank balances that businesses typically hold, represent much better financial yields to the fraudsters than when consumers are targeted.”</p>

               <p>So why is everyone picking on the little guy? Security experts point to a variety of factors:</p>

               <ul>
                    <li>Larger enterprises are getting better at protecting their data and borders, forcing cybercrooks to focus on smaller targets.</li>
                     <li>There are more than 27 million small businesses in the U.S. To criminals, this translates to millions of untrained employees using millions of unprotected computers, devices and networks that could expose hundreds of millions of personal records.</li>
                     <li>Evolving technologies, including social networking, cloud computing, and mobile computing are only adding to the significant risks and exposures faced by small businesses.</li>
                     <li>Small businesses typically don’t have the time, manpower, skills, resources, funds, and inclination to focus on security.</li>
                     <li>Small businesses still see cybersecurity as low risk/low priority, and see security as an interruption and not an enabler.</li>
                     <li>Small businesses represent low risk and little chance of exposure for thieves – small businesses typically lack the monitoring, forensics, logs, audits, reviews, penetration testing and other security defenses and warning systems that would alert them to a breach.</li>
                     <li>Small businesses tend to use popular open-source applications to help them deploy and manage their online presence in a simple manner. This can help businesses immensely in time savings, but the popularity of some of these platforms makes them vulnerable to mass infections, given that they all are based on the same code.</li>
              </ul>

              <p>Unlike larger enterprises, a single security incident, even a minor one, could be the death knell for a small business. According to research firm the Ponemon Institute, a single data breach or exposure of just 1,000 customer records costs the breached entity an average of $194 per compromised record, or $194,000. Few small businesses could afford such a loss.</p>

              <h4>Your website may be your weakest link</h4>

              <p>For most small businesses, their website is their store front. For hackers, the website is a back door, and an often-too-easy way to break into a business from the comfort of an armchair half way around the world - hackers who want every last piece of customer, employee, and personal information that might be accessed through that website.</p>

              <p>And a poorly protected website is not just an easy way for hackers to steal customer and employee information, it’s also one of the most favored ways to spread malware to surfers and shoppers. Known as drive-by downloads, hackers will jump at the chance to install sophisticated malware on unprotected websites. This hidden payload will then silently infect any visitors to that website with a variety of dangerous malware.</p>

              <p>Not only is it an easy way to infect thousands of web users, but guess who gets the blame if the malware is detected? And even if your business is not directly punished for this lapse, it could sure feel like it. Major search engines like Google and Bing scan millions of websites every day looking for sites that might be unwittingly hosting malware.</p>

              <p>And what will they do if they find such malware on your website? They’ll start by sending your business to the sin bin - blacklisting it so your infected website won’t appear in any search results. Blacklisting protects surfers from catching whatever bugs might be lying in wait on your website, but it also stops your customers from finding your site. Which makes the point of having a website, well, pointless.</p>

              <p>And it’s only going to get worse. As users become more wary of emails laden with infected attachments, hackers are increasingly turning to drive-by downloads instead. And security experts are worried. In January 2013 the European Network and Information Security Agency (ENISA) identified the drive-by download as the number one cyber threat worldwide.</p>

              <p>Small businesses are especially vulnerable to this threat because they’ve never really had many good options when it comes to securing their websites. Until recently they either had to choose between enterprise level security solutions that were very expensive and required a lot of technical knowledge and oversight. Or they had to settle for more affordable but bare-bones protection that provided little real resistance to even amateur hackers.</p>

              <p>But all that’s beginning to change. Thanks to innovators like SiteLock<sup>&reg;</sup>, small businesses can now have the best of all possible worlds: access to enterprise-class web scanning services that can detect malware hiding on a website, probe for security vulnerabilities, and even make sure the website is not faced by the dreaded blacklist; advanced security that still requires no technical knowledge or resources and no effort or time on the part of the business; and best of all, so affordable that even the smallest business would find it hard to resist.</p>

              <h4>One Business Owner’s Story</h4>

              <p>With so many small businesses now being impacted by cyber attacks, it’s no longer difficult to find a small business owner with a harrowing story.</p>

              <p>Take Anthony. It’s not his real name because he’s still scared of retaliation, and he’s even reluctant to identify the nature of his business or even the state he’s based in. Anthony started his business a little over four years ago and does all of his business online, delivering his products directly to consumers and other small businesses.</p>

              <p>Starting nearly three years ago, Anthony suffered the first in a series of devastating attacks on his website that were so severe, he ended up being blacklisted by search engines. No matter how hard Anthony tried, hackers were still able to infect his website with dangerous malware that was capable of infecting any visitors to the website.</p>

              <p>That resulted in his site and business being blacklisted or blocked by the major search engines. That’s because in order to protect their reputation and surfers, search engines constantly scan the internet for small businesses that have significant vulnerabilities or have malware lurking on them.</p>

              <p>Because search engines lead visitors to these websites, they don’t want to lead them to infected websites. So they blacklist those sites, at least temporarily, so that visitors won’t be able to find the site until the security problem has been fixed.</p>

              <p>It’s bad enough if it happens once, but if it happens repeatedly, as in Anthony’s case, it can be devastating. Being blacklisted means your customers simply can’t find your website, even though they know exactly who and where you are. </p>

              <p>For Anthony, his website isn’t just his storefront, it is also where he generates his leads. He was investing more than 40% of his marketing budget in lead generation, through everything from Google search engine marketing to social media. But those leads were now worthless because they couldn’t find that storefront.</p>

              <p>Each attack, which Anthony believed was targeted and malicious, put the web side of his business on hold for up to a week. And each time that happened, it would cost him up to $30,000 in downtime and lost business. Not to mention the long-term damage to his reputation and goodwill.</p>

              <p>And as if things couldn’t get any worse, the search engines don’t forget. Once placed on a list, a business can stay on that list. The search engines want to make sure that when a security hole is plugged, it’s plugged properly and permanently. But being on that list also pushed Anthony’s business further down the search rankings. Which is a double whammy because he’s still losing more business while at the same time having to spend even more marketing dollars to claw his way back up the rankings just to get back to where he used to be.</p>

              <p>In spite of spending more than $2,000 to hire a security company he found on the internet, the attacks kept coming. So far his losses have topped $300,000. When he finally found SiteLock, they recommended he use something called a web application firewall, or WAF, that helps filter out such attacks in the first place.</p>

              <p>Finally, something worked. For just a couple of dollars a day, Anthony has been able to stop the attacks cold. And while he still worries that it can happen again, at least he’s free to start rebuilding – not just his business and website traffic, but the trust of his customers and the search engines. It’s a slow process but one that many thousands of businesses may also be facing. And like Anthony, are too afraid to come forward for fear the publicity will do even more harm.</p>

              <h4>So what can you do to protect your website?</h4>

              <p>Security doesn’t have to be difficult in order to work. In fact, as security technologies become more advanced they become more automated – meaning that technically challenged business owners don’t have to worry about learning new skills or technologies.</p>

              <p>There are at least a dozen simple steps any business owner can take and repeat that will minimize the risk of an attack on their website. Here are some tips to help online businesses get started.</p>

              <ul>
                 <li>Create a web security plan. This is the most important step you can take in protecting your website. A security plan should clearly state your security goals, priorities, rules, standards, expectations and so on. The plan will help you create a road map to security, help you review and measure how well you’re doing in achieving your security goals, remind you of what work remains to be completed, and helps you share your security expectations with employees and other key security partners.</li>
                 
                <li><b>Engage and educate your employees about your security plan and policies.</b> Your employees can either be sentries or vulnerabilities – it all comes down to how vigilant and security-aware they are. So it will pay dividends to have an on-going awareness program in place to help employees understand the security rules, the roles they have to play in security, and how they can avoid easy common mistakes that can make your website vulnerable.</li>
     
                 <li><b>Make sure password management is part of the plan and training.</b> Passwords still remain one of the basic defenses against intruders, which is why intruders will focus on mistakes in the way passwords are created and stored. Every password plan should include simple rules for how passwords should be created, how complex they should be (the more complex they are, the more secure they are), and how often they should be changed. You should also create rules or guidelines for how passwords should be stored, and whether employees are allowed to share important passwords with others. And pay particular attention to the passwords you use to manage access to your website.</li>
     
                 <li><strong>Regularly scan your website for vulnerabilities.</strong> This not only enables you to stay one step ahead of hackers, it also allows you to see your website as hackers do. Good scanning services will continuously check your website for a wide range of security gaps and vulnerabilities, and some of the better services will even help you fix any holes they find. Automated scanning services are now very affordable for the small business, and offer the same level of security that bigger businesses enjoy.</li>
     
                 <li><strong>Invest in a web application firewall to protect against growing threats like bots and botnets.</strong> Just like using a personal firewall to protect your home computer, a web application firewall will help protect your website against a variety of threats that focus on stealing visitor information or serving up malicious code to your website visitors.</li>
     
                 <li><strong>Familiarize yourself with the most common and dangerous vulnerabilities.</strong> It won’t come as much comfort if the first time you hear about a common vulnerability or exploit is when you’re informed a hacker has used it against you.</li>
     
                 <li><strong>Regularly verify and update any third-party plugins and applications you use on your website.</strong> These plugins are only as secure as the companies that created them, so make sure you’re using the latest version with all known vulnerabilities patched.</li>
     
                 <li><strong>Be especially wary of remote employee access.</strong> Malware can easily slip into your website through computers and other devices used to access your website from less-than-secure locations. Pay particular attention to employees who have permission to access your website from home or while on the road.</li>
              </ul>

                  <p>One of the easiest and most affordable is a web scanning service. SiteLock, for example, provides comprehensive and automated website protection, that’s enterprise grade, for as little as $10 per month. And it’s always-on security, meaning your website can be watched and guarded all day every day, and any vulnerabilities found are acted on quickly.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock is a global website security technology and services leader, protecting more than 700,000 online businesses. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>