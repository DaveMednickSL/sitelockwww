<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Launches Partnership with HostUpon to Offer Website Security</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-launches-partnership-with-hostupon" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Launches Partnership with HostUpon to Offer Website Security</h1>
            <h3 class="font22">HostUpon to Bundle Website Protection within Hosting Packages</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; September 25, 2012</strong></p> 
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announces that it has joined forces with HostUpon (<a href="https://www.hostupon.com">www.hostupon.com</a>), an industry leading shared and VPS web hosting solutions provider, to offer website scanning security to new customers.

				<p>Toronto-based HostUpon provides secure, reliable and user-friendly website and cloud hosting to customers around the world. HostUpon differentiates themselves from other hosting providers by offering a business model based around zero outsourcing, easy-to-use interfaces, and a strong technological architecture. HostUpon will bundle the SiteLock Basic scanning package into its VPS and cloud-based hosting packages for customers to automatically receive website security upon registering their domain.

				<p>Designed for small business websites, the SiteLock Basic scanning package includes an initial website scan to thoroughly check all applications for infiltrations, then performs daily malware and network scans to ensure all of the code and files on the website remain secure. Once a customer's website is determined to be secure, they are able to display a SiteLock Trust Seal on the webpages as visual proof that the site is protected.

                <p>Through this partnership, HostUpon will also offer SiteLock's website scanning through the company's two related brands, HostHero (www.hosthero.com) and UptimeHost (<a href="https://www.uptimehost.com" target="_blank">www.uptimehost.com</a>). Together, the three hosting brands maintain over 17,000 domains worldwide that will now receive the SiteLock Basic package as part of their hosting package.

				<p>"We're really excited about partnering with SiteLock, since they're known to be a heavy hitter with a remarkable reputation in the industry," says HostUpon CEO, Alfez Velji. "The driving factor for us is the additional security they will bring forward. Website security is a big focus for us, since our clients have always been paramount within our mission. Giving our clients the ability to use SiteLock to enhance website security is the number one priority for our quality, service and overall offering."

				<p>"We're happy to be able to offer enhanced website security to HostUpon's customers," says Givonn Jones, SiteLock business development manager. "HostUpon offers a fast and reliable web hosting solution, which is now also a safe and clean environment free of malware for their hosting customers."

                <p>For additional information about HostUpon, visit www.hostupon.ca. For additional information about SiteLock website security technology and services, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security and PCI compliance solutions available on the market. SiteLock helps protect websites against malware, spam, viruses and other forms of online threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 3 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit www.sitelock.com.</p>

				<h4>About HostUpon</h4>
                <p>HostUpon (<a href="https://www.hostupon.com" target="_blank">www.hostupon.com</a> / @HostUpon) offers shared web hosting that is designed to fit the needs of beginner to advanced websites. Whether starting a personal website or a small to medium sized business website, the HostUpon shared hosting plans provide all the best web hosting features. With the ever increasing popularity of social media, music, video, and dating websites, their FFmpeg service provides the core features needed for video hosting, social networking, and much more. Consistently ranked amongst the top Canadian web hosts, HostUpon continues to raise the bar for shared and VPS hosting providers through it's Toronto head office. For more information please visit <a href="https://www.hostupon.com" target="_blank">www.hostupon.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
