<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock to Sponsor and Attend 2012 cPanel Hosting Conference</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-2012-cpanel-conference" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock to Sponsor and Attend 2012 cPanel Hosting Conference</h1>
            <h3 class="font22">SiteLock Will Educate Attendees on the Importance of Website Security</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; October 2, 2012</strong></p>
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, is pleased to announce its participation in the cPanel conference. The 7th annual conference will be held at The Westin Galleria in Houston, Texas from October 8-10, 2012 and is designed to teach attendees strategies and tactics to automate their operations, as well as tips on how to free up resources and ways to compete more effectively. The event will draw in those looking to enhance their knowledge of the hosting industry, as well as entrepreneurs who are new to the industry.</p>

				<p>While there, SiteLock will host a booth in the exhibit hall where attendees can learn about threats to online businesses and the importance of having a secure, trustworthy company website. SiteLock representatives will also offer visitors new ways to increase profitability and provide peace-of-mind to their online customers while demonstrating how SiteLock integrates through the cPanel and WHM platforms.</p>

				<p>Attendees that represent hosting companies will be able to learn about how easily they can integrate SiteLock into what they offer their small and midsized business customers. By integrating SiteLock website scanning, automatic malware removal, and ongoing website monitoring into their online services, hosting providers and other attendees will be able to differentiate themselves in the market by partnering with a known leader in website protection.</p>

				<p>"We're really looking forward to networking with potential business partners in the conference's interactive and educational environment," says Tom Serani, Executive Vice President of business development for SiteLock. "We see the growth in the cPanel and WHM platforms as an ideal opportunity to build website security in to online businesses from their inception."</p>

				<p>SiteLock will be available to meet with partners and those interested in becoming a partner during the exhibit. Meetings can be arranged in advance by visiting http://www.sitelock.com/company-events-cpanel-conference.php. For more information on the cPanel Conference 2012, visit http://conference.cpanel.net/.  For additional information about SiteLock website security technology and services, visit www.sitelock.com.</p>

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security and PCI compliance solutions available on the market. SiteLock helps protect websites against malware, spam, viruses and other forms of online threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 3 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>