<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Named a "Best Place to Work" in Phoenix</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-named-a-best-place-to-work-in-phoenix" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Named a "Best Place to Work" in Phoenix</h1>
            <h3 class="font22">Phoenix Business Journal honors SiteLock with Top Employer Award</h3>
            <hr>

            <p><strong>JACKSONVILLE, FLORIDA &mdash; November 30, 2012</strong></p>
            <p>SiteLock LLC (<a href="http://www.sitelock.com/">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced today that its Phoenix, Arizona office has been named one of the "Best Places to Work" by <em>Phoenix Business Journal.</em></p>
    
            <p>The "Best Places to Work" honorees are determined through confidential surveys that are completed by applicants' employees regarding the benefits, culture, communication, policies, and mission of each company. The "Best Place to Work" title is given to select companies who exhibit above average offerings and have built cohesive staffs and strong company morale.</p>
        
            <p>SiteLock employees strive to abide by a "work hard, play hard" mentality which inspires them to go above and beyond to help customers, while maintaining a positive attitude and having fun. The SiteLock team enjoys an employee lounge complete with a pool table, ping pong, and video games; free meals each day; a casual dress environment; and monthly social and incentive events.</p>
            <p>"We're honored to receive this award," says SiteLock President, Neill Feather. "From our employee game room and daily breakfast to our sales contests and great benefits, we have worked hard to create an energized, engaging culture so that our employees enjoy coming to work every day."</p>

               <p>The SiteLock team's dedication is evidenced by the numerous <a href="http://www.sitelock.com/reviews">customer testimonials</a> and references they receive. The company's position is that the more energized and empowered employees are, the more effectively they will serve customers.  This approach is part of the culture that makes SiteLock a great place to work.</p>
        
                <p>Learn more about SiteLock at <a href="http://www.sitelock.com/">www.sitelock.com</a>. Applicants interested in becoming a SiteLock employee should visit <a href="http://www.sitelock.com/careers">www.sitelock.com/careers</a>.</p>

       
                <h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="http://www.sitelock.com/">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security scanning and services. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other forms of threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="http://www.sitelock.com/">www.sitelock.com</a></p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
