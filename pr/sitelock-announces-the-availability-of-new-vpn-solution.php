<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Named Finalist in 2019 SC Excellence Awards for Best SME Security Solution</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/announces-partnership-with-enom" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces the Availability of<br>New VPN Solution</h1>
            <h3 class="font22">Website security leader expands product portfolio to protect business and consumer data, empower safe internet browsing</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; February 19, 2018</strong></p>
                <p><a href="https://www.sitelock.com">SiteLock</a>, the global leader in website security solutions, today announced the expansion of its product offering with the launch of SiteLock VPN. SiteLock’s virtual private network offering will help businesses protect all of their online activity from any location.</p>
                <p>A VPN is a vital layer of protection that eliminates concerns around employee use of public Wi-Fi, protects data transmitted online, allows businesses to view competitor ad campaigns, and removes content restrictions. For consumers, a VPN is also critical for personal data privacy and protection on public networks, eliminating data throttling and enabling access to content without restriction.</p>
                <p>“Websites are the modern small business’s storefront, and protecting those sites from malicious actors has been SiteLock’s core focus,” said Brian Sargent, Vice President of Product at SiteLock. “As hackers become increasingly advanced and remote work grows in popularity, however, the security needs of businesses and their employees continue to expand. We are excited to bring our industry-leading expertise to the VPN market to help our customers respond to these changes with another layer in their cybersecurity strategy.”</p>
                <p>SiteLock VPN will help businesses protect their online activities while offering improved security and anonymity. It offers unlimited access across all supported devices to provide an internet free of privacy concerns and content restrictions.</p>
                <p>Features of SiteLock VPN include:<br>
                    <ul>
                    <li><strong>Superior privacy:</strong> No activity logs, DNS leak protection, IP address randomization and 256-bit military-grade encryption ensure user privacy.</li>
                    <li><strong>Fast and easy browsing:</strong> More than 1,000 servers in 40+ locations means SiteLock VPN users can browse securely in seconds.</li>
                    <li><strong>Unlimited, multi-device usage:</strong> Connect to any SiteLock VPN server at any time, without restrictions or download caps, and use up to five devices at once without having to pay for multiple VPN services.</li>
                    <li><strong>Affordability and support:</strong> Combining SiteLock VPN with website security assures the best price on a powerful VPN, as well as access to SiteLock’s U.S.-based support team available 24/7/365.</li>
                    </ul>
                </p>
                <p>SiteLock VPN, combined with the company’s industry-leading website security solutions, provides an additional layer of protection for business battling the evolving threat landscape. To learn more about SiteLock VPN and the benefits of a virtual private network for business and consumer security, please visit https://www.sitelock.com/.</p>

                <h4>About SiteLock</h4>
                <p><a href="https://www.sitelock.com">SiteLock</a>, the global leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million websites worldwide. For more information, please visit sitelock.com.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>