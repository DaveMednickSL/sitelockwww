<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Releases Custom Rendition of "The Twelve Days of Christmas"</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-twelve-days-of-christmas" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Releases Custom Rendition of "The Twelve Days of Christmas"</h1>
            <h3 class="font22">A Satirical yet Educational Video about Online Risks and Website Security</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; December 17, 2013</strong></p> 
                <p>SiteLock LLC <a href="https://www.sitelock.com">(www.sitelock.com)</a>, a global leader in website security and PCI Compliance, announced today that in the midst of all the serious online threats, particularly at this time of year, they are introducing a lighter side of the story, and taking a humorous yet educational approach to website security awareness. The message is delivered in their video “The 12 Days of Christmas, by SiteLock.”</p>

                <p>The final chorus, so as to spare readers the continuous build of each verse, goes like this: “On the twelfth day of Christmas a hacker gave to me, twelve search ranks-a-dropping, eleven bills outstanding, ten days blacklisted, nine threatening letters, eight phishers phishing, seven spammers spamming, six zombie botnets, five days offline…four angry shoppers, three banking Trojans, two drive-by downloads and a bad case of anxiety.”</p>

                <p>Throughout the video, captions educate viewers on the meaning and impact of online threats, such as, “A drive-by download is a program that is automatically installed on your computer when you go to a booby-trapped website,” and “some botnets have included more than 10 million infected computers.” At the same time, the element of fun is introduced in captions like, “referred to by meat historians as the Guggenham, Hormel’s SPAM Museum gets 100,000 visitors each year.”</p>

                <p>“We know that website owners have a great deal of stress to manage, in addition to running their business, during the holidays and as they close the year,” states Neill Feather, President of SiteLock. “And we think that they can use a bit of comic relief while we help them understand how they can protect their businesses and customers.”</p>

                <p>While presented much like a comedy sketch, the video will have viewers laughing but also remembering the message of importance of having website security. The video can be seen here: http://www.youtube.com/watch?v=gEklYdqT4iU.</p>

                <p>To read the post related to the video, visit sitelock.com/blog. For additional information regarding SiteLock website security, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @sitelock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>