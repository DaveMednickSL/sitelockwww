<?php 
include('../includes/top.php'); 
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SiteLock Joins Forces with Web.com to Promote SMB Success Online | January 21, 2014</title>
<link rel="canonical" href="https://www.sitelock.com/pr/sitelock-joins-forces-with-webcom"/>
<?php include($root.'/includes/metadataRS.php'); ?>
</head>

<body class="press-releases">
    <?php include($root.'/includes/gt.php'); ?>
    <?php include($root."/includes/_headerPushy.php"); ?>
    <div class="container" id="container">
	<section class="module content">
        <br />
        <div class="ctrDiv">
            <h1>SiteLock Joins Forces with Web.com to Promote SMB Success Online</h1>
            <h2>Web.com to Offer Website Security to Small Businesses</h2>
            <div class="clearFloat forty"></div>
            <div class="pr-body">

                <p><strong>SCOTTSDALE, ARIZONA &mdash; January 21, 2014</strong></p> 
                <p>SiteLock LLC <a href="https://www.sitelock.com">(www.sitelock.com)</a>, a global leader in website security solutions for online businesses, announced today that it has teamed with Web.com<sup>&reg;</sup> (<a href="http://www.web.com" target="_blank">www.web.com</a>) to integrate website security and protection into the Web.com comprehensive portfolio of offerings which currently includes services such as website hosting, online marketing and eCommerce solutions.</p>

                <p>According to recent research published on ITProPortal.com, 90 percent of websites are vulnerable to an attack. Additional findings state that 95 percent of those weaknesses could lead to information loss or leaks. With the majority of business websites launched being small or brand new businesses; there are often limitations in technical or security expertise. To add insult to injury, many small businesses cannot afford the potential revenue loss or reputation damage that could follow a website attack.</p>

                <p>SiteLock partners with innovative web hosting and related service providers that share their focus of helping online businesses succeed. To further provide proactive protection to their customers, Web.com will manage the setup of TrueShield, SiteLock’s web application firewall that prevents malicious bots and targeted attacks from affecting their customers’ websites.</p>

                <p>“At Web.com, we continually look to provide better services to our customers and with the addition of the SiteLock services, we can now better safeguard our customers,” states Amit Mathradas, senior vice president, Marketing at Web.com. “We take a customized, consultative approach with of our clients that need full-service or managed programs. And we’re pleased to include SiteLock’s complete website security program to provide our customers with peace of mind that their online business is safe and secure for their visitors.”</p>

                <p>“We strongly believe in the approach that Web.com takes in the small business market,” adds SiteLock Executive Vice President of Business Development, Tom Serani. “We share a common goal of helping companies get off the ground while being prepared for success, initially and for the longer term. Part of that growth is contingent upon the trust and relationships that new website owners build with their customers. Together, SiteLock and Web.com provide complete website launch and promotion services as well as security and preventative protection that can accelerate conversions for online businesses.”</p>

                <p>SiteLock services will be available through Web.com as part of their website hosting plans in the coming weeks. Learn more about SiteLock at www.sitelock.com. Learn more about Web.com at <a href="http://www.web.com">www.web.com</a>.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About Web.com</h4>
                <p>Web.com Group, Inc. (Nasdaq: WWWW) provides a full range of Internet services to small businesses to help them compete and succeed online. Web.com is owner of several global domain registrars and further meets the needs of small businesses anywhere along their lifecycle with affordable, subscription-based solutions including website design and management, search engine optimization, online marketing campaigns, local sales leads, social media, mobile products, eCommerce solutions and call center services. For more information, please visit www.web.com; follow Web.com on Twitter @webdotcom or on Facebook at www.facebook.com/web.com. For additional online marketing resources and small business networking, please visit Web.com’s Small Business Forum.</p>

            </div><!--End 'pr-body'-->
        </div><!--End 'ctrDiv'-->
    </section>
    <div class="clearFloat forty"></div>
    <?php include($root.'/includes/_footerPushy.php'); ?>
    </body>
    </html>