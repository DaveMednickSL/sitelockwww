<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Teams with Aruba - Italy's Largest Hosting Provider</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/announces-partnership-with-enom" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Teams with Aruba &mdash; Italy's Largest Hosting Provider</h1>
            <h3 class="font22">The Aruba Group Adds Online Security Products to Service Portfolio</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; May 2, 2012</strong></p> 

                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announces a partnership with Aruba (<a href="http://hosting.aruba.it/index.asp" target="_blank">http://hosting.aruba.it/index.asp</a>), the largest website hosting provider in Italy. Aruba offers website hosting and registered domain names to their clients and will now expand their service portfolio to include SiteLock website security tools to protect the reputations of their clients' online businesses. By partnering with SiteLock, Aruba will be able to offer online security products and services to its more than 2 million customers.</p>

                <p>The Aruba Group provides a wide variety of online services including domain name registration, online advertising, application housing, eCommerce and extensive hosting solutions ranging from simple packages to complete, professional level website hosting under their global brands. With offices in eight countries across southwestern Europe, the company is included in the world's Top 10 list of number of active websites in hosting with over 1.25 million pages and roughly 25,000 new web hosting clients per month.</p>

                <p>The partnership agreement will enable the Aruba Group to offer SiteLock's website security services - which include malware detection and removal, blacklist monitoring, application and vulnerability scanning, and other website security products and services - to their web hosting clients, managed server owners, and e-mail account members - a combined base of over 8 million customers. SiteLock will be the preferred provider for Aruba Group's online security solutions and the services will be made available to all Aruba hosting clients.</p>

                <p>"Website security tools are a must-have in today's online world, and we're thrilled to partner with the Aruba Group expand our online security footprint in the European market," says Executive VP of Business Development for SiteLock, Tom Serani.  "Aruba's history and expertise in the hosting community is the perfect match for our Internet security products, and we are happy to be the ones to help complete their comprehensive, business-focused online services."</p>

                <p>"At the Aruba Group, we strive to offer our customers the highest quality, extensive range of services that small businesses need to maintain a successful online presence," says Stefano Cecconi, CEO at Aruba Group. "We believe that our partnership with SiteLock will increase the value for our customers by delivering more than hosting, but a secure website environment for online businesses. Combining the SiteLock service and the OpenSRS wholesale platform gives us the best product on the market for our customers and the best reseller feature set for our business."</p>

                <p>Aruba will use Tucows' OpenSRS platform to deliver and manage the SiteLock service.  </p>

                <h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other vulnerabilities. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona. For more information, call +1.877.257.9263 or visit www.sitelock.com.</p>

                <h4>About Aruba Group</h4>
                <p>The Aruba Group is a website hosting and registered domain provider with offices and datacenters in Italy, France, Germany, Hungary, Poland, the Czech Republic, and Slovakia.  The Aruba Group currently serves over 2 million customers, over 2 million registered and managed domain names, 1.25 million active websites in hosting, over 6 million managed e-mail accounts, and over 12,000 managed servers.  The Aruba Group offers over a dozen brands that each specialize in specific areas of Internet services, from e-commerce to advertising to housing to web promotions and more.  For more information, call +39.0575.0505  or visit <a href="http://hosting.aruba.it" target="_blank">http://hosting.aruba.it</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>