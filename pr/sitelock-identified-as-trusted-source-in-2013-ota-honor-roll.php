<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Identified as Trusted Source in the OTA 2013 Online Trust Honor Roll</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-identified-as-trusted-source-in-2013-ota-honor-roll" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Identified as Trusted Source in the OTA 2013 Online Trust Honor Roll</h1>
            <h3 class="font22">SiteLock Recognized for Demonstrating Leadership in Brand Protection, Online Security and Privacy Practices</h3>
            <hr>

            <p><strong>SCOTTSDALE, ARIZONA &mdash; June 5, 2013</strong></p> 
            <p>SiteLock LLC, the global website protection company, announced that it has been named to the Online Trust Alliance (OTA) 2013 Online Trust Honor Roll for  demonstrating exceptional data protection, privacy and security in an effort to better protect their customers from the increased threats of cybercriminals. </p>

	        <p>OTA, a nonprofit organization that works collaboratively with industry leaders to enhance online trust, completed comprehensive audits analyzing more than 750 domains and privacy policies, approximately 10,000 web pages and more than 500 million emails for this report. The composite analysis included more than a dozen attributes focusing on 1) site & server security, 2) domain, brand, email and consumer protection, and 3) privacy policy and practices.   In addition to the in-depth analysis of their web sites, Domain Name Systems (DNS), outbound emails, and public records were analyzed for recent data breach incidents and FTC settlements. Key sectors audited include the Internet Retailer 500, FDIC 100, Top 50 Social Sites as well as OTA members.</p>

	        <p>“Consumers are willing to risk billions of pieces of personal data, providing them in exchange for desired services. They rely on the integrity of the businesses collecting and storing this information to protect them,” said Craig Spiezle, president and executive director of the Online Trust Alliance.  “We are very pleased with the voluntary level of adoption many consumer-facing websites implemented this year that went above and beyond baseline compliance.”</p>

	        <p>"With our company foundation in website security for small businesses, we recognize the importance of building online trust for our customers and their clients,“ says Neill Feather, President of SiteLock. “We are proud to be recognized as an example of a company dedicated to keeping the online experience safe, protecting visitor data, and promoting online trust,  security and privacy. We are delighted and grateful to be named to the OTA Online Trust Honor Roll.“</p>

	        <p>Nearly a third of the companies reviewed made the Honor Roll, including SiteLock. The report indicates that company size and/or sales are not true measures of the level of security and privacy a company implements. “All companies are equally evaluated by the same criterion regardless of size. We have seen large e-retailers with significant sales fail to make the Honor Roll; conversely we have seen small to mid-size companies taking top grades,” said Spiezle.</p>  

	        <p>Started in 2005 as an effort to drive adoption of best practices, the objectives of the Honor Roll are to 1) recognize leadership and commitment to best  practices which aid in the protection of online trust and confidence in online services, 2) Enable businesses to enhance their security, data protection and privacy practices, 3) Move from compliance to stewardship, demonstrating support of meaningful self-regulation, and 4) Promote security & privacy as part of a company’s brand promise and value proposition. </p>

	        <p>Being named to the 2013 Honor Roll is a significant achievement considering the large number of companies that received failing marks for inadequate domain and consumer protection (14%), insecure websites (7%), and inadequate privacy policies or data collection practices (36%).</p>

            <p>To review the full 2013 Honor Roll report, please download a free copy (no longer available on otalliance.org). To learn more about SiteLock complete website protection, visit <a href="https://www.sitelock.com">www.sitelock.com</a>. </p>

             <h4>About SiteLock</h4>
            <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%.  As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit www.sitelock.com.</p>
	       <h4>About The Online Trust Alliance (OTA)</h4>
            <p>The Online Trust Alliance (OTA) is a non-profit with the mission to enhance online trust, while promoting innovation and the vitality of the internet. Our goal is to help educate businesses, policy makers and stakeholders while developing and advancing best practices and tools to enhance the protection of users' security, privacy and identity. OTA supports collaborative public-private partnerships, benchmark reporting, meaningful self-regulation and data stewardship. For more information, visit: <a href="https://otalliance.org" target="_blank">https://otalliance.org</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>