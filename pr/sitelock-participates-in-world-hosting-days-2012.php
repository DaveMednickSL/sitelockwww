<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock to Sponsor and Host Website Security Session at WorldHostingDays</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-participates-in-world-hosting-days-2012" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock to Sponsor and Host Website Security Session at WorldHostingDays</h1>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; March 13, 2012</strong></p> 
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, is pleased to announce its participation in the upcoming global hosting event, WorldHostingDays. As an event sponsor, SiteLock is looking to establish relationships with potential partners, resellers and experts in markets that support website security. The global event promises to draw the largest gathering in the hosting and Internet industry with more than 3,500 attendees during the three-day conference taking place in Europa-Park in Rust, Germany from March 20-23.</p>
					
				<p>While there, SiteLock will facilitate an interactive session on the topic of online risks to small businesses. The rapid acceleration of malware development and malicious attacks on business websites is making this a near epidemic challenge that puts small and midsize companies at the greatest risk for website security threats. The SiteLock session at the event will explore the impact and potential remedies for such issues.</p>
					
				<p>"SiteLock is committed to making the web a safer place for small and midsize businesses," says Neill Feather, president of SiteLock. "Our active participation in the WorldHostingDays event is an example of our goal to partner with the right organizations that share our passion for helping business grow and achieve their success objectives. Website security is a very important tool that enables them to do just that."</p>
					
				<p>SiteLock will be available to meet with partners at Booth 30 during the event and meetings can be arranged in advance by visiting http://www.sitelock.com/company-events-whd.php [no longer a live page]. For additional information about the WorldHostingDays event, visit <a href="http://www.worldhostingdays.com" target="_blank">http://www.worldhostingdays.com</a>. For additional information about SiteLock website security technology and services, visit www.sitelock.com.</p>
					
				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other vulnerabilities. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>
           
<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>