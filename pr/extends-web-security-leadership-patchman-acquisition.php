<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Extends Web Security Leadership with Acquisition of Patchman</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/extends-web-security-leadership-patchman-acquisition" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Extends Web Security Leadership with Acquisition of&nbsp;Patchman</h1>
            <h3 class="font22">Global website security leader broadens malware and vulnerability detection technology offering to accelerate growth</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; July 25, 2017</strong></p>
                <p><a href="https://www.sitelock.com">SiteLock</a>, a global leader in business website security solutions, today announced it has acquired <a href="http://patchman.co/" target="_blank">Patchman</a>, a Dutch web security startup that automatically detects and patches vulnerabilities for hosting providers. With this acquisition, SiteLock will amplify its global reach, expanding its threat database for faster identification and remediation of threats. In addition, the company is now well positioned to accelerate its growth in web security by enabling greater support and services to the hosting provider community.</p>

				<p>“The vision of Patchman aligns with SiteLock very well. We’re excited to join forces and execute on our shared goal of securing every website on the internet,” said Neill Feather, president of SiteLock. “This acquisition will double the number of websites we protect, allowing us to increase our knowledge base, better identify malware patterns and more effectively mitigate threats. In addition, we can now provide more options for hosting providers around the world to work with us, and ensure high quality solutions are available to their end customers.”</p>
                <p>Launched in 2014, Patchman proactively secures websites at the server-level, patching the security vulnerabilities found in commonly used content management systems such as WordPress, Drupal and Joomla. With the addition of Patchman, SiteLock will now have more than 500 global partners, including Host Papa, UK2 Group and more.</p>
                <p>“The Patchman team is thrilled to join forces with SiteLock, as combining our unique technologies presents many new and exciting opportunities,” said Wouter de Vries, founder of Patchman. “We look forward to accelerating our already impressive growth and continuing to deliver top notch web security solutions worldwide.”</p>
                <p>With this acquisition, SiteLock will offer expanded options for hosting providers, including educational resources and high quality security solutions. For more on SiteLock’s products, visit: <a href="https://www.sitelock.com/products">https://www.sitelock.com/products</a>.</p>
                <h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12,000,000 websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
