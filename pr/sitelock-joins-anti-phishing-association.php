<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Joins Anti-Phishing Association (APWG)</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-joins-anti-phishing-association" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Joins Anti-Phishing Association (APWG)</h1>
            <h3 class="font22">Business Website Security Leader Advocates Anti-phishing &amp; Online Safety</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; March 12, 2013</strong></p> 
                <p>SiteLock LLC, a global leader in website security for online businesses, announced a strategic partnership with Internet Anti-Phishing Working Group (APWG). The APWG works toward the goal of combatting cybercrime, phishing and online abuse.</p> 

				<p>As a member of the group, SiteLock strives to advocate business education regarding the risks they consistently face and empower them to resolve issues while improving their website visitors‘ experience. The APWG aims to connect leading providers of Internet security solutions with each other and the businesses they support. At the same time, a common goal among the organization and its members is to promote consumer awareness and data protection.</p> 

				<p>The Anti-Phishing Working Group focuses on strengthening the global response to cybercrime. SiteLock will actively participate in initiative groups and contribute its SMB expertise and insight as well as resources such as research findings, white papers and other educational tools to group members and online consumers.</p> 

				<p>“Joining the APWG member companies is an honor for SiteLock and we look forward to participating in the effort to reduce cybercrime,” said Neill Feather, President of SiteLock. “This partnership will increase the exposure of the threats and attacks that our website security technology identifies. At the same time, we hope to raise awareness among businesses and consumers of the malware and other vulnerabilities that are targeted at business websites and how we can help prevent them."</p>

				<p>APWG Secretary General Peter Cassidy said, “APWG is pleased and honored to receive SiteLock into our membership corps. The company’s focus and expertise in SME enterprise security will be of enormous benefit to our research into managing enterprise threats against cybercrime. That kind of insight is key to making cybercrime a predictable and manageable threat.“</p> 

				<p>Together, the companies will pool their industry-leading resources to help protect online businesses and their customers from hackers, phishing, malicious software, as well as many other ecrime attacks.</p> 

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock LLC (www.sitelock.com / @sitelock) helps small businesses protect their websites and reputations through website security scanning technology and services. SiteLock offers the most affordable and complete website security solution, protecting against malware, spam, viruses and other vulnerabilities. SiteLock protects more than 700,000 websites worldwide and scans over 10 million web pages daily for threats. Many of these customers rely on their website as their business storefront. To support these online merchants, SiteLock provides a quick, intuitive PCI Compliance application and approval process. Each website security subscription includes the SiteLock Trust Seal, which is proven to increase online conversions by more than 10%. With 24/7, U.S.-based support, SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

				<h4>About APWG</h4>
                <p>The APWG (www.apwg.org) is a worldwide coalition unifying the global response to cybercrime across industry, government and law-enforcement sectors. APWG’s membership of more than 2000 institutions worldwide is as global as its outlook, with its directors, managers and research fellows advising: national governments; global governance bodies like ICANN; hemispheric and global trade groups; and multilateral treaty organizations such as the European Commission, Council of Europe's Convention on Cybercrime, United Nations Office of Drugs and Crime, Organization for Security and Cooperation in Europe and the Organization of American States.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>