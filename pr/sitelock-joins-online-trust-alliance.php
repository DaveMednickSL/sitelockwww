<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Joins Online Trust Alliance (OTA)</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-joins-online-trust-alliance" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Joins Online Trust Alliance (OTA)</h1>
            <hr>

               <p><strong>SCOTTSDALE, ARIZONA &mdash; March 1, 2013 </strong></p>
				<p>SiteLock LLC, a global leader in website security for online businesses, announced a strategic alliance with nonprofit internet security and trust organization Online Trust Alliance (OTA). </p>
					
				<p>Online Trust Alliance works to make the internet safer through the promotion of trust, accountability, and online integrity. The OTA has a fundamental goal of affecting public policy through security recommendations, data stewardship, self-regulation and innovation, with consistent participation from its members. SiteLock and the OTA share similar principles, in that they both strive to protect organizations and consumers who function in the online marketplace. </p>
					
				<p>"We welcome SiteLock as a new OTA member and partner. Their commitment to establish and provide best practices to small business aligns well with OTA’s goals to help educate businesses, policy makers and stakeholders and advance best practices to enhance the protection of users' security, privacy and identity,“ said Craig Spiezle, Executive Director, OTA.</p>
					
				<p>For SiteLock, the partnership with OTA demonstrates their commitment to security best practices and the concept of building security into the design of websites, web applications and the online business infrastructure. SiteLock will participate in anti-malware and small and mid-size business focused initiatives and communities as part of its membership.</p> 
					
				<p>"As the pioneer in the space of website security for small businesses, joining the enterprise leaders in privacy and security-related policy is an exciting step for SiteLock,” says Neill Feather, President of SiteLock. “Within just a few years, we have established partnerships with global leaders in the web hosting market and we look to our relationship with the OTA to help us expand our footprint within larger organizations as we share our small business expertise and gain insight from other members who serve mid-to-large enterprises within their respective markets.” </p>
					
                <p>The collaboration between SiteLock and the OTA is a critical step toward raising member awareness about the impact of security risks and breaches in the SMB market. At the same time, because there are many hurdles that larger organizations have successfully addressed, it is highly valuable for the small business online community to learn from the insights of  thousands of enterprise-level companies. From educating businesses to corresponding with policymakers, OTA brings a strong influence to SiteLock’s online presence.</p>
 
<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
