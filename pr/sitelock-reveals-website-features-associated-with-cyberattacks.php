<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Reveals Website Features Associated With Increased Likelihood of Cyberattacks</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-reveals-website-features-associated-with-cyberattacks" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Reveals Website Features Associated With Increased Likelihood of Cyberattacks</h1>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; Oct. 11, 2016</strong></p>
                <p>Each year, over 760,000 websites are breached, resulting in the compromise of highly sensitive and private information including financial records, customer data, email communications, and photos. This National Cyber Security Awareness Month, SiteLock advocates the importance of being #CyberAware with the publication of new website security data. </p>

                <p>SiteLock scans over six million websites and web applications daily. To raise awareness about the need for increased website protection and dispel common misperceptions about cyber security, the company shared proprietary data revealing a distinct correlation between website popularity and an increased likelihood of cyberattacks.</p>

                <p>Often considered a company's most valuable asset, a strategically developed website is key to increasing profitability, establishing credibility, and building valuable brand equity. In order to achieve these objectives, many websites offer highly engaging and complex features such as social media, ecommerce and custom web applications. However, as more popular "bells and whistles" are added, website vulnerability is significantly amplified.</p>

                <h4>Threats</h4>
                <p>The more popular a website and its applications, the greater the risk of security threats. For example, a website is <strong>2.5 times more likely to be compromised</strong> than the average website if it links to a Twitter account that has 10,000-20,000+ Twitter followers. A website with 10-20 plug-ins is two times more likely to be hacked, while a site with 20 or more plug-ins is <strong>three times more likely to be compromised.</strong> Even more concerning, certain combinations of features will further increase a websites risk, up to 12 times more than the average.

                <h4>Playing Defense</h4>
                <p>Whether a small business, a large enterprise, or an individual managing your social media platforms and countless daily online interactions, you can be #CyberAware and protect your digital assets.</p>

                <p>This month, take stock of your security solutions by <strong>knowing what to look for.</strong>  There are clear warning signs that a cyberattack is underway, from slow load speeds to questionable data and obvious spam. For example, <strong>backdoor files account for 40 percent of all malware infections</strong> and give hackers the ability to alter a site's content.</p>

                <p><strong>Be vigilant about security.</strong> By identifying and implementing the right security solutions, you can significantly decrease your risk of external threats. Use a <strong>website scanner</strong> to check for <strong>malware and vulnerabilities on your site.</strong> You will be alerted if the scanner finds anything suspicious or malicious, and some scanners will find and automatically remove malware. A <strong>web application firewall (WAF)</strong> can help block attacks from automated bots or human attackers.</p>

                <p>To learn more, visit SiteLock.com or <a href="https://www.sitelock.com/blog/2016/10/popular-but-insecure-infographic/" target="_blank">here.</a> To join the NCSAM conversation, follow @SiteLock on Twitter and use #CyberAware.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6 million websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com.</a></p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>