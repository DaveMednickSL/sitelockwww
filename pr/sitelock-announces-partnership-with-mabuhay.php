<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Partnership with Mabuhay Web Hosting</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-partnership-with-mabuhay" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Partnership with Mabuhay Web Hosting</h1>
            <hr>

                    <p><strong>Jacksonville, FLORIDA &mdash; November 27, 2011</strong></p>
					<p>SiteLock LLC, a global leader in website security solutions for online businesses announced today a new partnership with Mabuhay Hosting, a premier hosting provider located in the Philippines. This partnership will enable Mabuhay Hosting to extend SiteLock's security services to its hosting customers at special rates, further expanding the range of online protection provided to them to solidify the security and credibility of their websites.</p>
					<p>"Providing top level security to our customers is the highest priority for us.  Partnering with SiteLock gives us an easy way to do it without huge demands on our staff or resources," said Ivan Rivera, CEO and Founder of Mabuhay Web Hosting. "SiteLock is a perfect fit for our business because it enhances our mission of providing state of the art internet technology to every individual and company at the lowest possible price with the highest possible quality."</p>
					<p>SiteLock's professional website security services include deep 360-degree security scanning, instant threat alerts, reputation management, verifiable website trust seals, and expert site hardening services. These services have never been more critical for online business clients in the Philippines, as hacking, brute force attacks, malware, and data theft is at an all-time high.</p>
					<p>"We're excited to form this partnership with a company like Mabuhay Hosting because they truly care about every single customer. This partnership allows us to continue to provide security to more small businesses and websites on a global scale," said Givonn Jones, Business Development Manager of SiteLock.</p>
					<h4>About SiteLock</h4>
					<p>SiteLock provides website security services and website malware scanning that ensure a safe and productive internet environment for online businesses. SiteLock solutions provide a comprehensive suite of security products with packages tailored to meet the specific needs of small business. Unlike competitive offerings, SiteLock's Business Verification and Security services are combined to provide the most complete and affordable business security solution available.  SiteLock currently protects over 400,000 websites worldwide.</p>
					<p>SiteLock's offices are located in Scottsdale, Arizona and Jacksonville, Florida. For more information, visit the website at <a href="https://www.sitelock.com">http://www.sitelock.com</a> or call 877-257-9263.</p>
					<h4>About Mabuhay Web Hosting</h4>
					<p>Mabuhay Web Hosting &amp; Development Service PHILIPPINES is owned and operated by IVAINS.com.  The company has been dedicated to delivering end-to-end web development and IT services to small businesses based in the Philippines.  Mabuhay's services range from website hosting and design to virtual private servers, cloud services and support.  All of Mabuhay's services are backed by a 100% satisfaction guarantee and 24X7 support.</p>
					<p>Mabuhay's main office is located in Malabanias, Pampanga in the Philippines.  More information on Mabuhay Web Hosting can be found at <a href="http://www.mabuhayhosting.com" target="_blank">http://www.mabuhayhosting.com</a></p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>