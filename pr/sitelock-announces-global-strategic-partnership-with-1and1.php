<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Global, Strategic Partnership with 1&1</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-global-strategic-partnership-with-1and1" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Global, Strategic Partnership with 1&amp;1</h1>
            <h3 class="font22">SiteLock Joins with 1&amp;1 to Delivery Website Security to Online Businesses</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; December 10, 2012</strong></p>
                <p>SiteLock LLC (www.sitelock.com), a global leader in website security solutions for online businesses, announced that it has partnered with 1&1 Internet Inc. (www.1and1.com), a global provider of web hosting services. Among the largest hosting providers in the world, 1&1 gives private users and small-to-medium businesses a range of innovative, high quality web hosting solutions that allow them to affordably expand their Internet presence. </p>

				<p>SiteLock performs website security scans that check for malware and vulnerabilities. SiteLock also provides blacklist monitoring to identify any sites that are blocked by search engines. Once customers' websites are verified, they are given a SiteLock Trust Seal to display on their website. The SiteLock scan package being provided to 1&1 customers is a custom plan, developed specifically for the needs of 1&1 and its customers. All customers will have access to a dashboard that is powered by SiteLock which provides real-time notification and alerts when any issues are identified. </p>

				<p>The 1&amp;1 SiteLock scanning packages will evaluate the need for security modifications to web applications, identify the potential for data breaches, recognize if a website has been infiltrated, and can detect if a website itself is being used to distribute malicious content. If an issue is detected, customers will be directed to where the problems lie so that the issues can be resolved immediately and their online presence can maintain the best possible online security.</p>

				<p>"We see this partnership as a great opportunity to offer a valuable service to our customers by delivering a secure environment for their personal and business websites," says Michael Frenzel of 1&amp;1. "We strive to offer our customers the highest quality of hosting services in the industry, and believe that the SiteLock product will add more value for our customers".</p>

				<p>Tom Serani, Executive VP of Business Development at SiteLock, sees this as a promising partnership. "1&1 and SiteLock both understand that it is more important than ever to protect online businesses, and we have the utmost respect for 1&1's position of global leadership as we work together to elevate the value of website security in the market," he says.</p>

				<p>This global partnership reinforces SiteLock's leadership position in the small business website security market, providing scanning or malware removal for more than 700,000 websites worldwide. With a strong presence in markets including the United States, Austria, Canada, France, Germany, the United Kingdom, Italy, Poland, Romania, Argentina and Spain, 1&amp;1 will likely expedite SiteLock's penetration in these markets that are in need of comprehensive website security. </p>
 
				<p>Learn more about SiteLock at www.sitelock.com. Learn more about 1&1 at www.1and1.com.</p>

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (www.sitelock.com / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other forms of threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit www.sitelock.com.</p>

				<h4>About 1&amp;1 Internet Inc.</h4>
                <p>1&amp;1 Internet Inc. is a subsidiary of United Internet, a profitable public company with a market cap of $3 billion. 1&amp;1 was founded in 1988 and hosts more than 11 million domain names, while more than 70,000 servers run in the company's five state-of-the-art, green data centers. 1&amp;1's global community is approximately 11 million customer contracts strong. The company's U.S. headquarters is located in Chesterbrook, Pa. For more information, visit www.1and1.com or success.1and1.com  or contact the company at 1-877-GO-1AND1, on Facebook, or Twitter @1and1.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>