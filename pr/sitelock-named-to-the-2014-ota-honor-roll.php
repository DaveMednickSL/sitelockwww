<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Named to the OTA 2014 Online Trust Honor Roll for 2nd Consecutive Year</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-named-to-the-2014-ota-honor-roll" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Named to the OTA 2014 Online Trust Honor Roll for 2nd Consecutive Year</h1>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; June 11, 2014</strong></p> 
                <p>SiteLock LLC, the global leader in website security, announced that it has been named to the Online Trust Alliance (OTA) Online Trust Honor Roll for the second straight year. As the rate of security risks and threats increases faster than ever, SiteLock demonstrates its commitment to protecting online business store fronts and their visitors‘ data.</p>

                <p>Now in its 5th year, the Honor Roll has expanded to over 800 sites including OTA members, the Internet Retailer 500, the FDIC top 100 banks, and the top 50 news, social media, and government organizations. The OTA is a nonprofit organization that works collaboratively with industry leaders to enhance online trust, data privacy, and to instill the need for website security to be part of the structure and design of online businesses.</p>

                <p>The composite analysis included more than two-dozen attributes focusing on 1) site &amp; server security, 2) domain, brand, email and consumer protection, and 3) privacy policy and practices. In addition to the in-depth analysis of their web sites, Domain Name Systems (DNS), outbound emails, and public records were analyzed for recent data breach incidents and FTC settlements.</p>

                <p>“Consumer privacy, data security and brand protection are increasingly issues every site owner must address, said Craig Spiezle, president and executive director of the Online Trust Alliance. “Companies need to shift focus and become stewards of consumer data and adopt best practices to earn consumer trust while promoting innovation. We are very pleased to see an increased adoption of such standards by hundreds of companies that have recognized the need of making data security and privacy their differentiators.“</p>

                <p>"With our company‘s foundation in website security, we recognize the importance of building trust for all online businesses and consumers,“ says Neill Feather, President of SiteLock. “We are proud to be recognized for the second consecutive year as a company that leads the industry in promoting online security standards. We are dedicated to keeping the online experience safe, protecting visitor data, and promoting online trust, security and privacy. We are delighted and grateful to be named again to the OTA Online Trust Honor Roll.“</p>

                <p>Started in 2005 as an effort to drive adoption of best practices, the objectives of the Honor Roll are to 1) recognize leadership and commitment to best practices which aid in the protection of online trust and confidence in online services, 2) Enable businesses to enhance their security, data protection and privacy practices, 3) Move from compliance to stewardship, demonstrating support of meaningful self-regulation, and 4) Promote security &amp; privacy as part of a company’s brand promise and value proposition.</p>

                <p>Being named to the 2014 Honor Roll is a significant achievement considering that only 30.2% of the 800 plus sites evaluated made the Honor Roll, with 12% making the list for the past three years and 22% for the past two years. Conversely, nearly 53% failed in one of the three categories. Data breach incidents occurred in 7.1% of the organizations evaluated and impacted all sectors. The News 50 had the lowest rate (2%) of breaches, followed by the IR 500 (4.4%). The highest rate of breaches was in the Social 50 (18%).</p>

                <p>The OTA Honor Roll awards were presented at the IRCE Conference opening reception on Tuesday, June 10, 2014 in Chicago, Illinois. Neill Feather proudly accepted the SiteLock honor.</p>

                <p>To review the full 2014 Honor Roll report, please download a free copy. <a href="https://otalliance.org/HonorRoll.html" target="_blank">https://otalliance.org/HonorRoll.html</a>. To learn more about complete website security from SiteLock, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About the Online Trust Alliance (OTA)</h4> 
                <p>The Online Trust Alliance (OTA) is a 501c3 charitable non-profit with the mission to enhance online trust and empower users, while promoting innovation and the vitality of the internet. OTA's goal is to help educate businesses, policy makers and stakeholders while developing and advancing best practices and tools to enhance the protection of users' security, privacy and identity. OTA supports collaborative public-private partnerships, benchmark reporting, meaningful self-regulation and data stewardship. For more information, visit: <a href="https://otalliance.org/HonorRoll.html" target="_blank">https://otalliance.org/HonorRoll.html</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
