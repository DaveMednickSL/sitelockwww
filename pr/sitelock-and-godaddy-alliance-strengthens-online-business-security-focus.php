<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock and GoDaddy Alliance Strengthens Online Business Security Focus</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-and-godaddy-alliance-strengthens-online-business-security-focus" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock and GoDaddy Alliance Strengthens Online Business Security Focus</h1>
            <h3 class="font22">GoDaddy, the Largest US-based Web Technology and Service Provider, Now Offers SiteLock</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; June 18, 2014</strong></p> 
                <p>SiteLock LLC <a href="https://www.sitelock.com">(www.sitelock.com)</a>, the global leader in website security and PCI compliance, announced today that its partnership with GoDaddy can change the way small business owners think about their websites and customer experience. Both companies have been dedicated to helping small businesses succeed since their respective inceptions. This alliance solidifies that message and highlights the importance of complete website security as a success metric.</p>

                <p>GoDaddy provides all of the tools necessary for website owners to build, manage and grow their online presence. With the focus of making business easy for their customers, GoDaddy offers solutions ranging from domains to a variety of web hosting plans to professional services. The addition of the SiteLock website security service completes their portfolio and is now available for their 12 million customers through the GoDaddy control panel.</p>

                <p>The 2014 Data Breach Investigation Report, just released by Verizon, indicates that approximately 35% of all breaches were attacks aimed at web applications. This category is also the fastest growing breach type. SiteLock can automatically remove malware and proactively protect web applications, with the proper security plan. SiteLock scans more than a million websites daily to keep their customers’ websites and visitors safe.</p>

                <p>"Online security is vital to any small business, which is why we partnered with SiteLock," said GoDaddy Vice President and General Manager, Security Products Wayne Thayer. "SiteLock makes it easy for our customers to keep their websites safe and secure, allowing them to focus on their business goals and on living their passion."</p>

                <p>"GoDaddy was looking to enhance their solution set with complete website security for their customers to support their goal of helping businesses attract and retain customers," said SiteLock Executive Vice President of Business Development Tom Serani. "Security is such an important component of customer acquisition and retention. And this strategic relationship is directly in line with our goal of quickly increasing awareness among small and mid-size businesses about the risk of online threats and steps they can take to protect their brands."</p>

                <p>Already showing strong adoption among GoDaddy customers, SiteLock’s automatic malware removal tool (SMART) is currently active for nearly 40% of their joint customers. Together, the companies will be able to quickly identify new malware strains and protect Internet users from these threats, keeping thousands of websites clean and safe.</p>

                <p>For additional information regarding the SiteLock website security suite, visit <a href="https://www.sitelock.com">www.sitelock.com</a>. For additional information about GoDaddy, visit <a href="https://www.godaddy.com" target="_blank">www.godaddy.com</a>.

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%. As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit www.sitelock.com.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>