<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Connects with WebsiteAlive&reg; in New Alliance</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-connects-with-website-alive-in-new-alliance" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Connects with WebsiteAlive<sup>&reg;</sup> in New Alliance</h1>
            <h3 class="font22">SiteLock and WebsiteAlive Join Forces to Keep Online Chat Experiences Secure</h3>
            <hr>

            <p><strong>SCOTTSDALE, ARIZONA &mdash; June 18, 2013</strong></p> 
            <p>SiteLock LLC, the global website protection company, announced today it has established a unique partnership with WebsiteAlive®, a leader in Live Chat, Click-to-Call and online communications and its industry-leading Alive Chat™ and Alive Dial™ solutions, to secure the online chat experience. With the explosion of web chat, the risk for hackers and other threats to attempt to compromise information that is entered through the quick communication interface increases exponentially. </p>

	        <p>The transformation from traditional website visitor interaction is taking place &mdash; right now. What used to require a form submission, a.k.a. a web lead form, and delayed response time, or a telephone call that sometimes requires multiple transfers, digital option selection or the dreaded voicemail has evolved into an instant communications connection with someone who can provide website visitors with the information they are looking for in a matter of seconds. </p>

	        <p>According to WebsiteAlive, it’s estimated 50% of web visitors—typically a company‘s best prospects—abandon a website without live chat. It’s like having a retail store without a clerk or a restaurant without a server. When online businesses add live chat to their websites, it invites visitors, both customers and prospects, into a two-way conversation that‘s designed to meet and greet them, answer their questions, help them better navigate the site, build rapport, and encourage the next step toward a purchase decision.</p>

	        <p>Due to the real-time nature of the chat experience, one that often leads to a purchase – the buyer’s information, both personal and financial, suddenly becomes exposed for potential cybercrime in yet another forum. The direct integration of the patent-pending technology from WebsiteAlive and the online protection and PCI compliance from SiteLock fills the growing merchant demand of online merchants to instantly transform their chat windows into shopping carts. Online shoppers will be able to securely enter their personal and credit card data into the chat window. The combination of these two innovations makes the first-ever payment processing chat interface available to any online business.</p>

	        <p>“SiteLock is very excited about our new partnership with WebsiteAlive, as it opens the doors for website security through an additional communication platform,” explains Neill Feather, President of SiteLock. “As online businesses become more sophisticated and take advantage of all of the tools to turn their websites into storefronts, it’s our goal to keep each area of their business protected – for the company and their visitors or customers.”</p>

	        <p>“WebsiteAlive has built our business on the principle that conducting conversations and transactions online should be simple, always available, and secure,” adds Adam J. Stass, Managing Partner and CEO of WebsiteAlive. “As we have enhanced our technology integration, we look for new and innovative ways to serve our customers. SiteLock shares our commitment to keeping online conversations and information private and secure, and they provide our customers with the peace of mind their customers are safe.”</p>

	        <p>The integrated product, first announced at HostingCon 2013, is available now in early release directly from WebsiteAlive and will soon be accessible through partner channels. For more information about WebsiteAlive, visit www.websitealive.com. To learn more about SiteLock’s website security solutions, visit http://www.sitelock.com. </p>

	        <h4>About SiteLock</h4>
            <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%.  As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

	        <h4>About WebsiteAlive® </h4>
            <p>WebsiteAlive® (<a href="http://www.websitealive.com" target="_blank">www.websitealive.com</a>) is a fast and forward thinking software developer and online communications provider, dedicated to developing the most flexible and agile innovations delivered in a straightforward and streamlined approach. Since 2004, our feature-rich, industry-leading, and award-winning Live Chat and Click-To-Call solutions — AliveChat™ and AliveDial™ — are being used to meet and greet website visitors, answer questions, increase leads and close sales. Trusted by more than 11,000 web and mobile sites, including the San Francisco Giants and many other major sports teams, Loews Hotels & Resorts, CA Technologies and PCMall &mdash; to name a few, WebsiteAlive prides itself on its innovation, simplicity, flexibility and value that help Internet-based organizations of all sizes thrive.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>