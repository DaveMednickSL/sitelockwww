<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Partnership with UK2 Group</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-partnership-with-uk2-group" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="35">SiteLock Announces Partnership with UK2 Group</h1>
            <h3 class="font22">SiteLock Joins the UK2 Group Community with Website Security Offering</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; November 5, 2012</strong></p>
                <p>SiteLock LLC (www.sitelock.com), a global leader in website security solutions for online businesses, announced today that it has partnered with UK2 Group, an independent hosting provider based in the United Kingdom and United States. UK2 Group (<a href="https://www.uk2group.com" target="_blank">www.uk2group.com</a>) provides global web hosting and internet services to a wide range of customers, from individual technology enthusiasts and small businesses to large enterprises and financial groups.</p>

                <p>UK2 Group provides a variety of services to its diverse base of customers including cloud hosting and VPS, domain name registration and management, reseller hosting, web design, ecommerce solutions, managed and unmanaged dedicated servers, a content delivery network, and more. Serving over a million customers worldwide, UK2 Group's 200 employees strive to offer high performance products and exceptional support.</p>

                <p>UK2 Group will integrate with SiteLock to provide website security products to customers. New online businesses will have the ability to purchase SiteLock scanning software for their website, and current UK2 Group customers will be able to purchase scanning tools through their existing hosting account. SiteLock's scanning technology will perform scans on customers' websites to keep them free of malware and other online threats. Customers whose websites are successfully verified as secure will be given a SiteLock Trust Seal badge to display on their website as visual proof to their visitors that their website is secure.</p>

                <p>"Website security is a crucial component of running an online business in today's world and we don't want our customers to overlook its importance," states UK2 Group Marketing Director, Steve Holford. "We're confident that the addition of SiteLock's website security technology will differentiate us in the market and delight our customers. We believe that providing our customers with SiteLock's security scanning service will complete the protection, confidence & security our business customers require for their online services & websites."</p>

                <p>"At SiteLock, we strive to offer website owners high quality, comprehensive website security and we're thrilled to begin offering it to UK2 Group's hosting customers," says SiteLock Executive Vice President of Business Development, Tom Serani.  "We hope that by making the security scanning easily available to UK2 Group's hosting customers, they will become more aware of the online threats that can affect their data and customers and will take the necessary steps to protect their business. SiteLock's goal is to provide the best products and services in the market, which is a philosophy that UK2 Group abides by as well."</p>

                <p>Learn more about SiteLock at <a href="https://www.sitelock.com">www.sitelock.com</a>. Learn more about UK2 at <a href="https://www.uk2group.com" target="_blank">www.uk2group.com</a>.</p>

                <h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other forms of threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 10 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About UK2 Group</h4>
                <p>UK2 Group is a group of interconnected global and UK web hosting companies that operate independently whilst sharing knowledge, expertise and resources has been a major player in the web hosting industry since 1998. They provide a variety of services including domain names, email hosting, web hosting, cloud hosting, reseller, ecommerce solutions, VPSs and dedicated servers to individuals and businesses. UK2 Group also ownsUK2.net, one of the biggest and fastest growing web hosting companies in the UK. For more information, visit <a href="https://www.uk2group.com" target="_blank">www.uk2group.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>