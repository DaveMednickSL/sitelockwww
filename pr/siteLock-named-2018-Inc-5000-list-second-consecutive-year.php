<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Named to 2018 Inc. 5000 List for Second Consecutive Year</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/siteLock-named-2018-Inc-5000-list-second-consecutive-year" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Named to 2018 Inc. 5000 List for Second Consecutive Year</h1>
            <h3 class="font22">Scottsdale-based website security company SiteLock is excited to announce it has been named to Inc. 5000’s 2018 list of the nation’s fastest-growing private companies in America.</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; August 16, 2018</strong></p>
                <p>Scottsdale-based website security company SiteLock is excited to announce it has been named to Inc. 5000’s 2018 list of the nation’s fastest-growing private companies in America, marking its second appearance on the exclusive list. The list represents a unique look at the most successful companies within the American economy’s most dynamic segment—its independent small businesses. Microsoft, Dell, Domino’s Pizza, Pandora, Timberland, LinkedIn, Yelp, Zillow, and many other well-known names gained their first national exposure as honorees on the Inc. 5000.</p>
                <p>“We are honored to once again be included in the prestigious Inc. 5000 list,” said Neill Feather, CEO of SiteLock. “Over the past 10 years, we’ve grown from just a handful of employees to over 250 globally, protecting more than 12 million websites worldwide. Our team’s dedication to consistently deliver superior technology, innovative solutions and exceptional service to our customers is a testament of our continued growth and success. We are extremely proud of our progress and remain focused on our mission to protect every website on the internet.</p>
                <p>Earning spot No. 1540 on the 2018 list, SiteLock cracked the top third of the ranking and experienced a 306 percent revenue growth in a three-year period. SiteLock also ranked in the top 20 security companies.</p>
                <p>Not only have the companies on the 2018 Inc. 5000 (which are listed online at Inc.com, with the top 500 companies featured in the September issue of Inc., available on newsstands August 15) been competitive within their markets, but the list as a whole shows staggering growth compared with prior years. The 2018 Inc. 5000 honorees achieved an astounding three-year average growth of 538.2 percent, and a median growth rate of 171.8 percent. The Inc. 5000’s aggregate revenue was $206.1 billion in 2017, accounting for 664,095 jobs over the past three year</p>
                <p>Complete results of the Inc. 5000, including company profiles and an interactive database that can be sorted by industry, region, and other criteria, can be found at www.inc.com/inc5000.</p>
                <p>“If your company is on the Inc. 5000, it’s unparalleled recognition of your years of hard work and sacrifice,” says Inc. editor in chief James Ledbetter. “The lines of business may come and go, or come and stay. What doesn’t change is the way entrepreneurs create and accelerate the forces that shape our lives.”</p>
                <p>The annual Inc. 5000 event honoring the companies on the list will be held October 17 to 19, 2018, at the JW Marriott San Antonio Hill Country Resort, in San Antonio, Texas. As always, speakers include some of the greatest innovators and business leaders of our generation.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company has an industry leading threat database and protects over 12 million websites worldwide. For more information, please visit sitelock.com.</p>

                <h4><strong>More about Inc. and the Inc. 5000</strong></h4>

                <h4>Methodology</h4>
                <p>The 2018 Inc. 5000 is ranked according to percentage revenue growth when comparing 2014 and 2018. To qualify, companies must have been founded and generating revenue by March 31, 2014. They had to be U.S.-based, privately held, for profit, and independent—not subsidiaries or divisions of other companies—as of December 31, 2017. (Since then, a number of companies on the list have gone public or been acquired.) The minimum revenue required for 2014 is $100,000; the minimum for 2017 is $2 million. As always, Inc. reserves the right to decline applicants for subjective reasons. Companies on the Inc. 500 are featured in Inc.’s September issue. They represent the top tier of the Inc. 5000, which can be found at <a href="http://www.inc.com/inc5000">http://www.inc.com/inc5000</a>.</p>

                <h4>About Inc. Media</h4>
                <p>Founded in 1979 and acquired in 2005 by Mansueto Ventures, Inc. is the only major brand dedicated exclusively to owners and managers of growing private companies, with the aim to deliver real solutions for today’s innovative company builders. Inc. took home the National Magazine Award for General Excellence in both 2014 and 2012. The total monthly audience reach for the brand has been growing significantly, from 2,000,000 in 2010 to more than 18,000,000 today.  For more information, visit <a href="http://www.inc.com" target="_blank">www.inc.com</a>.</p>
                <p>The Inc. 5000 is a list of the fastest-growing private companies in the nation. Started in 1982, this prestigious list has become the hallmark of entrepreneurial success. The Inc. 5000 Conference & Awards Ceremony is an annual event that celebrates the remarkable achievements of these companies. The event also offers informative workshops, celebrated keynote speakers, and evening functions.</p>
                <p>For more information on Inc. and the Inc. 5000 Conference, visit <a href="http://conference.inc.com/" target="_blank">http://conference.inc.com/</a>.</p>
                <p>
                  <strong>For more information contact:</strong><br>
                  Inc. Media<br>
                  Drew Kerr<br>
                  212-849-8250<br>
                  <a href="mailto:dkerr@mansueto.com">dkerr@mansueto.com</a>
                </p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
