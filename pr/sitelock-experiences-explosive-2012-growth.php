<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Experiences Explosive 2012 Growth</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-experiences-explosive-2012-growth" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Experiences Explosive 2012 Growth</h1>
            <h3 class="font22">Industry-Leading Partnerships and New Products Anchor Year-to-Date Success</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; July 25, 2012</strong></p> 
                <p>(<a href="https://www.sitelock.com">www.sitelock.com</a>), a global provider of website security technology and products for online businesses, has experienced exponential growth during first half of 2012. The company has established more than 100 new relationships with hosting partners, resellers and other providers in the Internet space that serve the small and midsize business market. SiteLock added many of these partners during their presence at HostingCon last week, where they were an event sponsor and exhibitor.</p>   

	           <p>SiteLock organizes partnerships through a SECURE Partner Program in an effort to help website hosting companies and ancillary providers gain a new revenue stream and keep their customers safe by offering low-cost, comprehensive scanning and monitoring solutions. As of July 2012, SiteLock has already secured more than 100 new partners, many of whom join the program through wholesale providers such as OpenSRS and eNom. Additionally, hosting industry giants including HostGator, Lycos, Aruba.it, UK2 Group, and related providers, Attracta and BaseKit, are examples of the company's impressive global expansion in the website security market.</p>   

	           <p>Through the growing number of resellers that SiteLock is acquiring, the company is realizing an increased amount of exposure that is rapidly growing the company's customer base, both organically as well as through channel partnerships. SiteLock now protects more than 700,000 websites, covering 6 million web pages, around the world on a daily basis and is onboarding more than 40,000 new customers each month.</p>    

                <p>To effectively serve global and mobile markets, the SiteLock dashboard is now available in multiple languages including English, French, German, Italian, Spanish, Dutch and Portuguese-Brazilian and also through its new mobile application to keep website owners connected while on the go. SiteLock also just released their new Sitelock SMART<sup>&trade;</sup> (Secure Malware Alert and Removal Tool) technology that includes an enhanced scan that evaluates websites from the inside-out, as well as the outside-in, and can remove malware automatically. In addition, the company recently announced the availability of PCI compliance for businesses that accept credit cards at retail locations and/or through their websites.</p>

	           <p>"We have built a tremendous foundation of industry-leading relationships during the past 18 months, and I am excited about the progress that has been achieved so far this year," says SiteLock President, Neill Feather. "We have a very dedicated team of technology, business development and operational support experts that are committed to serving the security needs of online businesses." </p>

	           <p>"We look forward to building on this momentum and welcoming multiple new partners to the SiteLock family in the coming months," adds Tom Serani, EVP of business development for SiteLock. "Solid relationships are truly the cornerstone of our success and we will continue to create joint opportunities with leading providers in the market."</p>

                <p>For additional information about SiteLock website security technology and services, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

	           <h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security and PCI compliance solutions available on the market. SiteLock helps protect websites against malware, spam, viruses and other forms of online threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 700,000 websites worldwide and scans over 3 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>
                
<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>