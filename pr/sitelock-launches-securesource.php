<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Launches New Partner Resource Center</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-launches-securesource" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Launches New Partner Resource Center – SECURESource</h1>
            <h3 class="font22">Partner Portal Provides Simple, Profitable Delivery of Comprehensive Website Security</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; September 26, 2013</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a>,the global leader of website security solutions for online businesses, announced today the launch of SECURESource, their redesigned and enhanced partner portal. The online resource has been updated with new technical integration tools and product resources, as well as sales and marketing materials to support partner promotion goals and sales targets. With more than $20 million in partner earnings generated to date, SiteLock continuously strives to make it easier for partners to drive additional revenue while delivering superior comprehensive website security to their customers.</p>
                
                <p>A significant hang-up in many partner and reseller programs is that the companies who are ideal to resell other products to their customer base also have a business to run, their own products to maintain, and technical infrastructure to support. The challenge is finding the time and allocating resources to a partner project that could potentially take away from their business initiatives. The true value of a partner is their ability to commit to the reseller’s goals by providing the tools, resources and team effort to accelerate integration and maximize the success of the program.</p>

                <p>Among the enhancements available through SECURESource is the updated API that enables SiteLock partners that use cPanel/WHMCS or Parallels to automate their systems to offer their customers the complete suite of SiteLock website security products with the click of a button. The newly enhanced provisioning modules integrate quickly and seamlessly, making it simpler than ever for partners to configure, sell, and manage SIteLock products. And for hosting partners and other online service providers that use alternate control panels and automation tools, SiteLock provides a custom API that includes the same functionality and simple, flexible service provisioning.</p>

                <p>All of the website security tools offered directly by SiteLock via its multi-lingual dashboard are now available for partners to deliver through the API. New products include TrueShield web application firewall, TrueSpeed website acceleration technology, and PCI Compliance – all of which complement the existing line of patent-pending scanning and malware removal products. So, regardless of the partner platform or configuration, SiteLock, the new modules enable seamless partner integration and options for customers to provision their own accounts and protect their websites with one click.</p>

                <p>The newly renovated SECURESource partner portal delivers a one-stop shop for integration modules and guides, video tutorials, product information, and sales and marketing collateral. SiteLock is dedicated to developing successful partnerships, and to making website security accessible for all online businesses. These new and improved tools make SiteLock website security a truly turnkey add-on for both partners and their customers.</p>

                <p>Prospective partners should visit www.sitelock.com/partner for more information, email: <a href="mailto:partners@sitelock.com">partners@sitelock.com</a>, or call 904-437-4562. Stay up to date on the latest in SiteLock partner news on Twitter @sitelock.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @sitelock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%.  As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate online businesses about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
