<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Joins a Nationwide Global Effort to Support Data Privacy Day by Signing On as a Champion</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-supports-data-privacy-day-as-champion" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Joins a Nationwide Global Effort to Support Data Privacy Day by Signing On as a Champion</h1>
            <h3 class="font22">Hundreds of Organizations Collaborate to Generate Awareness about the Importance of Respecting Privacy, Safeguarding Data and Enabling Trust</h3>
            <hr>
               

                <p><strong>SCOTTSDALE, ARIZONA &mdash; January 05, 2016</strong></p> 
                <p>SiteLock, LLC <a href="https://www.sitelock.com">(www.sitelock.com)</a>, a global leader in website security, announced today that it has committed to being a Champion of Data Privacy Day (DPD) – an international effort held annually on January 28 to create awareness around the importance of privacy and protecting personal information. As a DPD Champion, SiteLock recognizes and supports the principle that organizations, businesses and government all share the responsibility of being conscientious of data privacy by taking part in respecting privacy, safeguarding data and enabling trust.</p>

                <p>Data Privacy Day is part of a greater effort, the #PrivacyAware campaign, which helps consumers understand how they can own their online presence and reminds companies that privacy is good for business. SiteLock joins the growing global effort among organizations, corporations, educational institutions, government entities, municipalities and individuals to raise awareness at home, at work and in their communities. As a DPD Champion, SiteLock is working toward the common goal of improving consumer and business consciousness while encouraging and empowering all users of the web to be #PrivacyAware.</p>

                <p>SiteLock is excited to be a part of Data Privacy Day. The website security company takes pride in helping the community by bringing awareness to data privacy. With security breaches increasing in frequency and size, SiteLock understands how valuable and important privacy is.</p>

                <p>DPD and the #PrivacyAware campaign are spearheaded by the National Cyber Security Alliance (NCSA), a nonprofit, public-private partnership dedicated to promoting a safer, more secure and more trusted Internet. You can follow the campaign on Twitter at @DataPrivacyDay or Facebook at <a href="https://www.facebook.com/DataPrivacyNCSA" target="_blank">https://www.facebook.com/DataPrivacyNCSA</a> and use the official hashtag #PrivacyAware to join the conversation.</p>

                <h4>About SiteLock</h4>
                <p>SiteLock provides comprehensive, cloud-based website security to all businesses. The company offers a suite of products that help businesses defend against malicious activity and harmful requests. Founded in 2008, the company currently protects over seven million websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

                <h4>About Data Privacy Day (DPD)</h4>
                <p>Data Protection Day began in the United States and Canada in January 2008 and commemorates the Jan. 28, 1981, signing of Convention 108, the first legally binding international treaty dealing with privacy and data protection. </p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>