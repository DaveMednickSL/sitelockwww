<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Partners With GMO Cloud For Website Security Solutions</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-partners-with-gmo-cloud" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Partners With GMO Cloud For Website Security Solutions</h1>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; September 28, 2016</strong></p>
                <p><a href="https://www.sitelock.com">SiteLock</a>, the global leader in website security solutions, announced today its partnership with <a href="http://us.gmocloud.com/" target="_blank">GMO Cloud</a>, a full-service provider of hosting and cloud computing solutions.</p>

                <p>Together, SiteLock and GMO Cloud will offer <a href="https://www.sitelock.com/malware-removal">website security solutions</a> to identify, fix, and prevent website vulnerabilities for GMO Cloud customers. The partnership will also provide access to SiteLock's expert service teams for consultation regarding broader security solutions.</p>

                <p>"We are very glad to announce our partnership with SiteLock," said Aoyama Mitsuru, President and CEO of GMO Cloud. "Web security is not just an issue for big enterprises anymore. A substantial amount of SMBs are also demanding stronger web security nowadays. With SiteLock, we will be able to offer strong, cost effective web security that meets the demands of our SMB customers."</p>

                <p>Additionally, SiteLock will continue to provide flexible solutions to ensure websites are running safely through malware detection, testing, automatic malware removal, web apps, firewalls and more.</p>

                <p>"We are proud to partner with GMO Cloud," said Tom Serani, Executive Vice President of Business Development for SiteLock. "We believe these integrations are a perfect fit for customers that need to keep their websites running safely and smoothly. SiteLock has provided significant insights and services to combat security challenges impacting websites worldwide. Through this partnership, we can continue our journey to protect and prevent future attacks."</p>

                <p>SiteLock can detect malware the minute it hits. After identifying malicious content, it automatically neutralizes and removes the threats. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.</p>

                <h4>About SiteLock</h4> 
                <p>SiteLock, the Global Leader in business website security solutions, is the only web security solution to offer complete, cloud-based website protection. Its 360-degree monitoring finds and fixes threats, prevents future attacks, accelerates website performance and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6,000,000 customers worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com.</a></p>

                <h4>About GMO Cloud</h4> 
                <p>GMO Cloud K.K. (TSE: 3788) is a full-service IT infrastructure provider focused on cloud solutions. Established as a hosting company in 1996, the company has managed servers for more than 130,000 businesses and now has 6,000 sales partners throughout Japan. In February of 2011, the company launched GMO Cloud to enhance its focus on cloud-based solutions. Since 2007, the company has also grown its GlobalSign SSL security brand through offices in Belgium, the U.K., the U.S., China and Singapore. For more information, please visit <a href="http://ir.gmocloud.com/english/" target="_blank">http://ir.gmocloud.com/english/</a> .</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>