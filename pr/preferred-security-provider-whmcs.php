<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Partners with WHMCS to Offer Suite of Website Security Solutions</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/preferred-security-provider-whmcs" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="35">SiteLock Partners with WHMCS to Offer Suite of Website Security Solutions</h1>
            <h3 class="font22">SiteLock named preferred website security provider of WHMCS</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; March 12, 2018</strong></p> 
                <p>SiteLock, the global leader in website security solutions, today announced a partnership with WHMCS, the leading web hosting billing automation platform. With this partnership, SiteLock becomes the first website security provider to be integrated into the WHMCS MarketConnect platform, giving web hosting companies access to a suite of industry-leading malware protection and remediation solutions including Find, Fix, Defend, and SiteLock 911.</p>

                <p>As cyber threats continue to rise and make headlines, security has become a top priority for web hosting companies and their customers," said Matt Pugh, CEO of WHMCS. "Partnering with SiteLock enables us to expand our portfolio of MarketConnect cloud service offerings, and gives web hosts access to affordable,industry-leading website security solutions with all the automation and convenience the MarketConnect platform offers."</p>
    
                <p>The average website is attacked 59 times per day, which is up a staggering 168 percent from the previous year. As cybercriminals continue to become more cunning and the threat landscape continues to evolve - website security can no longer be overlooked. Many businesses rely on service providers, such as WHMCS, to provide these website security solutions as part of a full-service offering in order to simplify and streamline the go-to-market process.</p>

                <p>Through this partnership, WHMCS powered web hosting companies will now have access to the SiteLock suite of powerful website security solutions including industry-leading scanners to automatically find and fix malware and vulnerabilities, along with a Web Application Firewall (WAF) that includes a Content Delivery Network (CDN). Products will be available on an individual basis, as well as through packages designed to meet specific customer needs. </p>   

                <p>"We are excited to partner with WHMCS and become their preferred provider of website security solutions," said Tom Serani, Executive Vice President of Business Development for SiteLock. "As the demand for cyber security continues to increase, our combined offerings provide a lot of synergy to web hosts and their customers. In addition to delivering superior and proactive website protection, this partnership will create new revenue streams for WHMCS customers who can now deliver a more comprehensive solution to their end users." </p>
           
                <h4>About SiteLock</h4>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million customers worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

                <h4>About WHMCS</h4>
                <p>WHMCS, the leading web hosting automation platform, automates and simplifies operations for web hosting businesses around the world. Through a single centralized control panel, WHMCS integrates with all the leading names in web hosting, domain registrars and cloud based services to fully automate the provisioning and deployment process. Founded in 2003, we are now the #1 trusted partner in automation used by more than 45,000 companies to help them scale and succeed. For more information, please visit whmcs.com.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>