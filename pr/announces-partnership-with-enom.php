<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Announces Partnership with eNom to Offer New Security Features</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/announces-partnership-with-enom" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
                <h1 class="font35">Announces Partnership with eNom to Offer New Options to Small and Mid-Sized Businesses</h1>
                <h3 class="font22">Website Security Provider and Domain Registrar to Offer New Options to Small and Mid-Sized Businesses</h3>
                <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; June 20, 2012</strong></p> 
                <p>SiteLock LLC, a global leader in website security solutions for online business, is pleased to announce its plans to join forces with eNom, the world's largest ICANN accredited wholesale registrar.</p> 

				<p>eNom, Inc. manages a network of 8,800 resellers globally. Resellers primarily join eNom's reseller program for domain registration services but also have the option of reselling many other online tools services such as hosting and email.</p> 

				<p>The partnership with SiteLock will allow eNom to offer malware removal, blacklist monitoring, website security, and web application scanning to its Reseller channel. The introduction of SiteLock will expand eNom's current security portfolio, which is focused primarily on SSL certificates.</p> 

				<p>Tom Serani, Executive VP of Business Development at SiteLock, sees this as a promising partnership. "eNom and SiteLock both understand that the importance for online security products is greater than ever, and we are thrilled to take part in this venture together," he says.</p> 

				<p>"At eNom, we are always looking to enhance our security offerings," explains Chris Sheridan, VP of Business Development at eNom. "We believe the robustness of SiteLock's technology will lead to strong adoption by our reseller base. This alliance will give us greater opportunity to differentiate the tools we can provide our community."</p> 

                <p>For additional information about SiteLock security solutions for online businesses, visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <p>For additional information about eNom domain and hosting technology and services, visit <a href="https://www.enom.com" target="_blank">www.enom.com</a>.</p>
 
				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other vulnerabilities. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>. </p>

				<h4>About eNom</h4>
                <p>eNom, Inc., is the world's largest ICANN accredited domain name wholesale registrar. As part of the Demand Media family of companies, eNom makes it possible for individuals and organizations to buy and sell Internet domains and services. <a href="https://www.enom.com" target="_blank">www.enom.com</a></p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>
