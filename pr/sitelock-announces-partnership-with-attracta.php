<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces Partnership with Attracta</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-partnership-with-attracta" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces Partnership with Attracta</h1>
            <h3 class="font22">SiteLock Joins the Attracta Community to Offer Website Security Scans for SEO Clients</h3>
            <hr>

                <p><strong>JACKSONVILLE, FLORIDA &mdash; October 10, 2012</strong></p>
                <p>SiteLock LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), a global leader in website security solutions for online businesses, announced today that it has partnered with California-based Attracta, a provider of search engine visibility technology.  Attracta (www.attracta.com) is the world's largest Search Engine Optimization (SEO) tool provider, serving more than 2.5 million websites in 90 countries worldwide.</p> 

                <p>Attracta ensures that clients' website content is detected by major search engines' website crawling processes and brings customers' content to the forefront of search results. Clients can access an advanced dashboard that monitors and reports on indexed pages and their search rankings. Attracta's services are especially helpful for companies whose websites have frequent updates or dynamic content.</p>  

				<p>SiteLock utilizes a similar scanning technique to check customers' small and mid-sized business websites for security vulnerabilities and infiltrations. The patent-pending 360-degree scan performed by SiteLock detects malware, injections, spam and other threats. Customers are given a SiteLock Trust Seal badge to display on their website once they are verified as secure. Likewise, customers are able to access an account dashboard to receive alerts and website scan reports.</p>

				<p>SiteLock is teaming with Attracta to offer a version of their website security system specifically designed for search engine optimization clients to be bundled with Attracta accounts. The scanning package will include blacklist monitoring through a non-intrusive website scan and access to SiteLock's Expert Service support team.</p>  

				<p>This partnership will also enable both companies to deliver the first completely automated provisioning or a website security seal by leveraging patent-pending JavaScript injection technology. With just the click of mouse, Attracta customers will have the ability to display the "SiteLock for Search" Trust Seal, showing their website visitors that their pages are free from blacklists. </p>

				<p>"At Attracta, we focus on increasing traffic to our clients' websites. Once that's accomplished, visitors need to know their experience there is secure and that their data is safe," states "Troy McCasland", VP of Business Development with Attracta. "By teaming with SiteLock and making their cloud-based, lightweight scan available to our clients, we are demonstrating our commitment to not only securing their popular websites, but also creating a positive experience for their customers."</p>

				<p>"We predict that Attracta's customer base will find SiteLock's Internet security to be a very attractive addition to their SEO service.  Website protection is a critical, but often overlooked, responsibility of online business owners. By bundling the security scanning into their Attracta service, it will be easy for website owners to keep their pages running smoothly and safe from cyber-attacks," explains SiteLock Executive Vice President of Business Development, Tom Serani.  "We already know that Attracta clients are taking steps to increase their website traffic and visibility, so once they begin displaying the SiteLock Trust Seal, they will also increase their visitors' trust."</p>

				<p>The SiteLock for Search service and Trust Seal is now available to all Attracta customers. An option to upgrade to SiteLock's Premium package will be made easily available for a small charge and includes SSL verification and more comprehensive website scans that are required for larger, more complex websites.</p>
 
                <p>Learn more about SiteLock at www.sitelock.com. Learn more about Attracta at <a href="https://www.attracta.com" target="_blank">www.attracta.com</a>.</p>

				<h4>About SiteLock</h4>
                <p>Established in 2008, SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @SiteLock) has helped small businesses protect their websites and reputations through website security services and scanning. SiteLock offers the most affordable and complete website security solution available on the market, protecting against malware, spam, viruses and other forms of threats from hackers. SiteLock's Expert Services team can remove malware and harden website platforms to help prevent future attacks. SiteLock currently protects over 500,000 websites worldwide and scans over 2 million web pages daily for threats. Many of these customers are online merchants that rely on their website as their business storefront. Each subscription to the service includes SiteLock's Trust Seal, which is proven to increase sales and conversions by more than 10%. SiteLock is headquartered in Jacksonville, Florida and has offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About Attracta</h4>
                <p>Attracta is a California-based company that provides search engine visibility service in more than 90 countries worldwide.  With the world's most popular Search Engine Optimization (SEO) tools, Attracta helps over 2.5 million websites promote over 3 billion webpages in the world's major search engines.  Each day, Google alone updates over 75 million webpages through Attracta's SML Sitemap service.  Many over the world's largest webhosting and SEO service providers incorporate Attracta's technology into their service offerings. The Attracta management team also founded several highly successful Internet companies including TABNet, MeetChina.com, Miva Merchant and ScanAlert.  For more information, call +1.707.320.2050 or visit <a href="https://www.attracta.com" target="_blank">www.attracta.com</a>.</p>

        <div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>