<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Expands Global Reach with Acmetek</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-expands-global-reach-with-acmetek" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">­SiteLock Expands Global Reach with Acmetek</h1>
            <h3 class="font22">Acmetek now offers full portfolio of SiteLock website security solutions to enterprise customers across the United States and India</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; February 26, 2018</strong></p> 
                <p>
                    SiteLock, the global leader in website security solutions, today announced its partnership with Acmetek, a trusted advisor of website security solutions for enterprises. The partnership provides mutual benefit for both companies to capitalize on growth markets while leveraging in-demand technologies to drive new revenue streams.   
                </p>
                <p>
                    Acmetek will now offer the full portfolio of SiteLock website security solutions to cover the needs of the small to medium-sized business (SMB) to the enterprise-level customer. This will include SiteLock industry-leading scanners to automatically find and fix malware and vulnerabilities, along with a Web Application Firewall (WAF) that includes DDoS mitigation and a Content Delivery Network (CDN). Products will be available on an individual basis, as well as through packages designed to meet specific customer needs.    
                </p>
                <p>
                    "The addition of SiteLock solutions rounds out our security portfolio to both SMB and enterprise-level customers, and further reinforces our reputation as a trusted advisor of security solutions and services provider," said Ramesh Nuti, Founder & CEO, Kevin Naidoo Co-Founder & COO of Acmetek. "Our customers will now have access to powerful, comprehensive, website security solutions that deliver proactive and reliable protection against today's every changing threats and vulnerabilities."
                </p>
                <p>
                    "The Asia-Pacific region is facing unprecedented levels of complex cybercrime and fraud attacks," said Sridhar Swamy, Director APAC, Acmetek. "The addition of SiteLock substantially adds further strength to our SSL Portfolio in minimizing cybercrime in the APAC region."
                </p>
                <p>
                    With websites experiencing an average of 59 attacks per day - which is up a staggering 168 percent from the previous year -  the importance of website security can no longer be overlooked. The partnership allows SiteLock to provide Acmetek with industry-leading website security options to enhance the security profile of its global customer base and help business owners proactively defend against today's evolving threat landscape. 
                </p>
                <p>
                    "We are excited to be a preferred security partner for Acmetek," said Tom Serani, Executive Vice President of Business Development for SiteLock. "The addition of SiteLock solutions to the Acmetek portfolio offers customers even more options and flexibility to ensure the highest level of website security. Our partnership will make it easy for Acmetek customers to take a proactive approach to security."
                </p>
                <p>
                    SiteLock can detect known malware the minute it hits. After identifying malicious content, it automatically neutralizes and removes the threats. SiteLock then provides businesses with complete reports on scans, threats detected and items removed.
                </p>

                <h5>About SiteLock</h5>
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 12 million websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>
           
                <h5>About Acmetek</h5>
                <p>
                    Acmetek is a Trusted Advisor of Security Solutions and Services. Our comprehensive security solutions include Encryption & Authentication (SSL), Multi-factor Authentication, PKI/Digital Signing Certificates, DDOS, WAF and Malware Removal. 
                </p>
                <p>
                    Acmetek started its journey into Website Security Solutions mainly focusing on SSL Certificates as a result of a simple observation: SSL has evolved over the years, but Technology Distributors and Businesses have not adapted. This mismatch led Acmetek to create the vision for the SSL & PKI experience and to develop Channel Enablement Model to support it. With our powerful enablement model, businesses can implement PKI Solutions with ease. With our integrated set of tools and enablement support, partners can now offer PKI and beyond and implement for their clients across the globe. Our sole mission is to make the world more secure, transforming one business after another.
                </p>
                <p>
                    We are located both in North Americas and APAC regions serving clients across the globe. For the difference that Acmetek is making, the company won several awards including a Symantec Trust Services Collaborative Partner of the Year Award in 2013 and has been Strategic Platinum Partner 6 years and recognized by Inc 5000 2 years in a row.
                </p>
                <a href="https://www.acmetek.com">acmetek.com</a>
                <a href="https://www.acmetek.in">acmetek.in</a>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>