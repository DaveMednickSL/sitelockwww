<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock Announces FatCow Partnership</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-announces-fatcow-partnership" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock Announces FatCow Partnership</h1>
            <hr>

                <p><strong>TEMPE, ARIZONA &mdash; August 8, 2009</strong></p> 
                <p>Web Security firm SiteLock announced today its partnership with leading web host FatCow. This partnership gives FatCow customers access to SiteLock’s innovative line of security products and services, which will help these site owners solidify the security and credibility of their online properties. The partnership provides FatCow yet another value-added product to add to the array of services its customers can take advantage of.</p>

                <p>SiteLock General Manager Josh Lesavoy commented:<br />
                "We're thrilled to have created this partnership with FatCow to help provide the small business owners we are both passionate about serving an opportunity to have access to our security solution."</p>

                <h4>About SiteLock</h4>
                <p>SiteLock provides web security services that ensure a safe and productive Internet environment for online businesses. SiteLock solutions provide a comprehensive suite of security products with packages tailored to meet the specific needs of small business. Unlike competitive offerings, SiteLock's Business Verification and Security services are combined to provide the most complete and affordable business security solution available.</p>

                <p>SiteLock's offices are located in Tempe, Arizona. For more information, visit the website at <a href="https://www.sitelock.com">www.siteLock.com</a> or call 877-257-9263.</p>

                <h4>About FatCow</h4>
                <p>FatCow provides shared web hosting solutions for individuals and small to medium sized businesses looking for an affordable, reliable web hosting option. FatCow pioneered the "one plan one price" philosophy when it began offering its signature all-in-one website hosting solution, The FatCow, back in 1998.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>