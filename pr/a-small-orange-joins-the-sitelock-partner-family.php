<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>A Small Orange Joins the SiteLock Partner Family</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/a-small-orange-joins-the-sitelock-partner-family" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">

                <h1 class="font35">A Small Orange Joins the SiteLock Partner Family</h1>
                <h3 class="font22">Homegrown Hosting Promotes Complete Security to Customers</h3>
                <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; December 10, 2013</strong></p> 
                <p><a href="https://www.sitelock.com">SiteLock</a>, a global leader in website security solutions for online businesses, announced today that A Small Orange (ASO) has joined the SiteLock partner program to meet the increasingly complex security needs of its customer base.</p>

                <p>According to a recent article on Forbes, 30,000 websites are hacked each day. Blogs and other small business websites are prime targets, often without the business owner knowing they have anything to fear. SiteLock identifies 5,000-10,000 sites daily that have malware or vulnerabilities, weaknesses that hackers can use to get malicious code into the site. And the value of having quick, complete website analysis for these site owners is often the difference between keeping or losing their business, which makes it a perfect fit to partner with A Small Orange.</p>

                <p>A Small Orange emphasizes customer service, ease of use, and competitive prices, providing a complete range of hosting services from shared hosting and virtual private servers to dedicated servers and hybrid hosting. A Small Orange offers plans that can be used for blogs to e-commerce websites, A Small Orange creates a unique, homegrown experience for each business website they support.</p>

                <p>“At A Small Orange, many of our customers are just like us &mdash; homegrown,” states Douglas Hanna, CEO of A Small Orange. “Because we are so focused on the customer experience, we strive to offer the most comprehensive solutions available, and SiteLock leads the market with a complete website security package for our customers.”</p>

                <p>“As we find new ways to protect online businesses, we continue to look for partners that share our passion and focus,” adds Tom Serani, Executive Vice President of Business Development for SiteLock. “We are very excited to welcome A Small Orange to the SiteLock community, as they take the same approach to serving the online community.”</p>

                <p>SiteLock website security services will be integrated into A Small Orange’s online sign-up process for new customers as well as other points in the customer experience. A Small Orange will make SiteLock’s services available through cPanel to easily provision the products for their customers.</p>

                <p>For additional information regarding the SiteLock website security suite, visit <a href="https://www.sitelock.com">www.sitelock.com</a>. For additional information about A Small Orange, visit <a href="https://www.asmallorange.com" target="_blank">www.asmallorange.com</a>.</p>  

                <h4>About SiteLock</h4>
                <p>SiteLock (<a href="https://www.sitelock.com">www.sitelock.com</a> / @sitelock) is a global website security technology and services leader, with more than 1,000,000 websites protected. SiteLock finds, fixes and helps prevent malware and other threats from affecting websites and their visitors. The SiteLock 360-degree website security services include malware and vulnerability scanning, automatic malware removal, advanced website protection through a web application firewall, website acceleration powered by a global CDN as well as PCI Compliance and expert services that are available 24/7. Each subscription to the service includes the SiteLock Trust Seal, which is proven to increase sales and conversions by more than 10%.  As a member of a number of cybercrime awareness and prevention associations, including the Anti-phishing Working Group (APWG), the Online Trust Alliance (OTA) and StopBadware.org, SiteLock continually strives to educate the small business market about the risks to their websites and help prevent them. SiteLock was founded in 2008 and is headquartered in Jacksonville, Florida with offices in Scottsdale, Arizona and Boston, Massachusetts. For more information, call +1.877.257.9263 or visit <a href="https://www.sitelock.com">www.sitelock.com</a>.</p>

                <h4>About A Small Orange</h4>
                <p>Established in 2003, A Small Orange delivers high quality web hosting services and outstanding customer support with a focus on people: our customers, our employees, and our community. Our remote workforce spread across the world works 24/7 to perfect web hosting while maintaining our culture of “homegrown hosting.” Tens of thousands of customers benefit from affordably priced plans designed to provide the resources they need, secure state-of-the-art infrastructure engineered to ensure 99.9% uptime, and round-the-clock access to a team of passionate customer service and technical support professionals. To learn more about our homegrown hosting services, visit <a href="http://www.asmallorange.com" target="_blank">www.asmallorange.com</a>, and follow us on Twitter, @asmallorange.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>