<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>New SiteLock Survey Reveals 60 Percent Of Consumers Abandon eCommerce Retailers After Cybercrime Occurrence</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-survey-reveals-consumers-abandon-retailers" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">New SiteLock Survey Reveals 60 Percent Of Consumers Abandon eCommerce Retailers After Cybercrime Occurrence</h1>
            <h3 class="font22">1 in 5 Consumers Will Avoid Online Holiday Shopping; Millenials Are the Most Concerned Consumers</h3>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; November 17, 2016</strong></p>  
                <p>According to a recent survey<sup>1</sup> for SiteLock, the global leader in website security solutions, two thirds of those surveyed will no longer shop at the site where their information was stolen, indicating a huge potential loss in the event of a breach.</p>

                <p>The survey underscored the importance of website security for online retailers big and small as they prepare for the critical holiday shopping season. In fact, websites experience 22 attacks per day on average, and a recent <a href="http://www.infosecurity-magazine.com/news/ponemon-more-than-half-of-smbs/" target="_blank">Ponemon study</a> uncovered that 50 percent of small businesses experienced a breach in the last year. Even more alarming, eCommerce sites are 1.5 times more likely to be compromised than a website that does not sell anything online.</p>

                <p>It's no wonder that nearly 20 percent of Americans do not plan to shop online this holiday season. When asked the main reason why they will not shop online, concern about their information being stolen was one of the top responses. Of those who have ever shopped online and worry about shopping online during the holidays, Millennials surprisingly outweighed every other age group, with nearly 40% indicating that they were more worried at this time of year.</p>

                <p>"Over 760,000 websites are breached each year and it is estimated that 60 percent of small businesses fold within six months of a breach," said Neill Feather, president of SiteLock. "Consumers are seeking assurances from the brands where they shop and expect them to do everything in their power to secure the websites themselves and, in turn, protect customer data. During the profitable holiday shopping season, it is imperative for online retailers to demonstrate that they are doing so."</p>

                <p>Of those asked what the most important thing an online store can do to make them feel more confident and indicated that they want to know which security company protects the sites they shop on, 34% are Baby Boomers, 28% are of Gen Xers and 39% are Millennials. More than half indicated that a secure payment process makes them feel more confident, while 17 percent indicated a recognized security logo. The risk of a data breach is always potentially crippling, but even more so in the coming weeks when many online retailers do the majority of their business for the year.</p>

                <p>SiteLock scans over six million websites and web applications daily, providing companies with insights into not only what the risks are, in real time, but how businesses and consumers can protect themselves. To protect your website, and your business, our experts suggest the following:</p>

                <ul>
                    <li><span><strong>Clean house.</strong> Keep all the systems you use for business malware and infection free by regularly scanning and updating software and apps.</span></li>
                    <li><span><strong>Don't store data you don't need.</strong> Identity thieves and those seeking customer data of all kinds cannot steal what you don't have. Eliminate any private customer data that is not essential to your business.</span></li>
                    <li><span><strong>Reassure shoppers.</strong> When asked what the most important thing an online store can do to make them feel more confident, Baby Boomers, Gen Xers and Millennials alike said they want to know which security company protects the sites they shop on.</span></li>
                    <li><span><strong>Instill confidence.</strong> A secure payment process and a recognized security logo also rated as top confidence boosters so be sure your eCommerce site is on a secure network. Look for HTTPS or SHTTP and the padlock symbol in your URL. If you use a security company or tool, be sure to prominently feature its logo on your site.</span></li>
                </ul>
                
                <p>To learn more, visit SiteLock.com. To join the conversation, follow @SiteLock on Twitter and use #SecureShopper.</p>

                <h4>About SiteLock</h4> 
                <p>SiteLock, the global leader in website security solutions, is the only provider to offer complete, cloud-based website protection. Its 360-degree monitoring detects and fixes threats, prevents future attacks, accelerates website performance, and meets PCI compliance standards for businesses of all sizes. Founded in 2008, the company protects over 6 million websites worldwide. For more information, please visit <a href="https://www.sitelock.com">sitelock.com</a>.</p>

                <h4><sup>1</sup>About the Survey</h4>
                <p>The survey of a demographically representative sample of 1,036 US adults (ages 18 and older) was conducted using ORC International's Online Caravan<sup>&reg;</sup> survey. Survey responses were collected during the period September 22-25, 2016.</p>

                <h4>About ORC International</h4> 
                <p>ORC International is a leader in the art of business intelligence. Their teams are passionate about discovering what engages people around the world. By combining quality data, smart synthesis and best in class digital platforms, ORC delivers insight that powers the growth and drives the future of their clients' businesses. To learn more about ORC International, visit their website, <a href="https://www.orcinternational.com" target="_blank">www.orcinternational.com</a>.</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>