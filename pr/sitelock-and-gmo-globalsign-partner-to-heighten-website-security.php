<!DOCTYPE html5>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>SiteLock and GMO GlobalSign Partner to Heighten Website Security</title>
  <link rel="canonical" href="https://www.sitelock.com/pr/sitelock-and-gmo-globalsign-partner-to-heighten-website-security" />
  <link rel="icon" type="image/ico" href="../favi.ico">
  <!-- Bootstrap -->
  <link href="/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/fringe.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DMG67');</script>
<!-- End Google Tag Manager -->
<style>
#mainBody {
    max-width: 800px;
}
</style>
</head>

<body>
<!-- HEADER -->
<div  id="mainBody"><div id="content">
<br>
<a href="/"><img class="logoNavFringe" src="/img/logos/SiteLock_red.svg" alt="sitelock logo"></a>
<hr>
</div></div>

<!-- HEADER -->
<div class="linkred" id="mainBody"><div id="content">
            <h1 class="font35">SiteLock and GMO GlobalSign Partner to Heighten Website Security</h1>
            <hr>

                <p><strong>SCOTTSDALE, ARIZONA &mdash; February 2, 2015</strong></p> 
                <p>SiteLock, LLC (<a href="https://www.sitelock.com">www.sitelock.com</a>), the global leader in website security and <a href="/pci-compliance">PCI compliance</a>, announced a partnership with GlobalSign (www.globalsign.com), the security division of GMO Internet Group (TSE:9449). GlobalSign, a leading provider of identity services including SSL certificates for web security, will now bundle SiteLock’s website security products with solutions for customers who purchase certain SSL certificates in selected markets, including Latin America.</p>

                <p>GlobalSign, founded in 1996, is one of the Internet’s original trust service providers — also known as Certificate Authorities — that provide SSL certificates with the strongest 2048-bit encryption to ensure websites are protected. SSL (Secure Sockets Layer) is a security protocol used to encrypt, or secure, data between two online machines, for example a PC user and a website server. It is most commonly recognized by the padlock symbol in an Internet browser or the browser bar turning green to indicate that the site has been verified and provides for a secure, encrypted transaction – a must in ecommerce and other applications that require data protection.</p>

                <p>“SiteLock’s comprehensive security solutions are a natural fit for GlobalSign’s customer base,” said Doug Beattie, vice president of product management at GlobalSign. “SiteLock provides both a “Find” and “Fix” solution via their <a href="/website-scanning">SMART Technology</a> (Secure Malware Alert & Removal Tool) that far surpasses typical scan only solutions. It’s exactly the same “above and beyond” focus we bring to our business, and we look forward to helping website owners build strong online trust with our SSL and SiteLock’s SMART Technology.”</p> 
                
                <p>“We are thrilled to add GlobalSign to the rapidly growing family of SiteLock partners,” said Tom Serani, executive vice president of business development for SiteLock. “The Internet-based certification and authentication services GlobalSign provides for its clients combined with SiteLock’s website security and acceleration services will give GlobalSign a clear competitive advantage in markets where the products are bundled, by providing a turnkey security solution of website security and SSL.”</p>
                
                <h4>ABOUT GlobalSign</h4>
                <p>GlobalSign is a provider of identity services for the internet of everything, mediating trust to enable safe commerce, communications, content delivery and community interactions for billions of online transactions occurring around the world at every moment. GlobalSign’s core digital certificate solutions allow its thousands of authenticated customers to conduct SSL secured transactions, data transfer, distribution of tamper-proof code, and protection of online identities for secure email and access control. The company has offices in the US, Europe and throughout Asia. For the latest news on GlobalSign, visit <a href="https://www.globalsign.com" target="_blank">www.globalsign.com</a> or follow GlobalSign on Twitter (@globalsign).</p>

<div class="whiteSpace50"></div>
</div></div>

<!-- FOOTER -->
<div class="footer">
<div id="content" style="padding-top: 10px;">

<hr class="footerHR">

<p class="text-center footCopy">Copyright &copy; SiteLock <span id="COPYRIGHT"></span> | <a href="https://sitelock.com/privacy-policy">Privacy Policy</a> | <a href="https://sitelock.com/terms">Terms of Use</a></p>
<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

<hr class="footerHR">

</div>
</div>

<!-- JS -->
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap.bundle.min.js"></script>
<script src="../js/custom.js?v=1.0.1"></script>
</body>
</html>