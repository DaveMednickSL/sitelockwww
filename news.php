<?php
//DEFINITIONS
$title = "News | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace25"></div>
<h1 class="sourceBlack">SiteLock News & Events</h1>
<h3>Where website security is simple.</h3>
<a class="btn btn-red" href="#news">Read About SiteLock in the News</a>
<div class="whiteSpace50"></div>
</div></div>

<div class="whiteSpace50"></div>
<div id="mainBody"><div id="content"><h1 class="sourceBlack text-center">Featured In</h1></div></div>
<?php include 'includes/news-bar-1.php';?>

<div class="whiteSpace50"></div>
<div id="mainGrey"><div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>

<div class="whitebk" div="mainBody"><div id="content" style="max-width: 90%;">
<div class="whiteSpace25"></div>
<h1 class="font45 text-center">SiteLock is making<br><span class="sourceBlack">headlines</span></h1>
<span id="news"></span>
<br>
<p class="text-center"><a class='btn btn-ghost-grey' href="articles">Read All SiteLock News</a></p>
<br>
</div></div>

<div class="whiteSpace50"></div>

<div class="bkdark colorWhite" div="mainBody"><div id="content" style="max-width: 90%;">
<div class="whiteSpace25"></div>
<h1 class="font45 text-center">Hot off the press<br><span class="sourceBlack">SiteLock press releases</span></h1>
<span id="prbox"></span>
<br>
<p class="text-center"><a class='btn btn-ghost-white' href="press-releases">Read All Press Releases</a></p>
<br>
</div></div>

<div class="whiteSpace50"></div>

<div class="row">
<div class="col-md-6"></div>
<div class="col-md-6"></div>
</div>

</div></div>
</div>

<div class="text-center linkgrey" id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<h2 class="font45"><span class="sourceBlack">Media</span> inquiries</h2>
<h3 class="font25">Want to connect with SiteLock?<br>Email <a href="mailto:pr@sitelock.com">pr@sitelock.com</a> and we’ll get right back to you.</h3>
<div class="whiteSpace50"></div>
</div></div>

<!--<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace50"></div>
<h2 class="font45 text-center">Catch up on <span class="sourceBlack">cybersecurity news</span></h2>
<div class="row">
<div class="col-md-4">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}
</div></div>
</div>

<div class="col-md-4">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}
</div></div>
</div>

<div class="col-md-4">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}
</div></div>
</div>

</div>
<br>
<p class="text-center"><a class="btn btn-red" href="blog">Check out our Blog</a></p>

<div class="whiteSpace50"></div>
</div></div>-->

<div id="mainGrey"><div class="linkgrey" id="mainBody"><div id="content">
<div class="whiteSpace50"></div>

<div class="row">
<div class="col-md-6 my-auto"><h2 class="font45">See what’s<br><span class="sourceBlack">Trending Now</span></h2>
</div>

<div class="col-md-6">
<a class="twitter-timeline" data-height="500" data-dnt="true" data-theme="light" data-link-color="#38ACD2" href="https://twitter.com/SiteLock?ref_src=twsrc%5Etfw">Tweets by SiteLock</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
</div>
</div>

<div class="whiteSpace50"></div>
</div></div></div>

<!--<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace50"></div>
<h2 class="font45 text-center">SiteLock <span class="sourceBlack">in the wild</span></h2>
<h3 class="font25 text-center">SiteLock is always attending and sponsoring hosting conferences, trade shows,<br>and events around the globe. Check out where we’ll be next.</h3>
<div class="row">
<div class="col-md-4">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}
</div></div>
</div>

<div class="col-md-4">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}
</div></div>
</div>

<div class="col-md-4">
<div id="darkBorderBoxAlt"><div id="darkBorderBoxContent">
{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}<br>{{DYNAMIC CONTENT}}
</div></div>
</div>

</div>
<br>
<p class="text-center"><a class="btn btn-red" href="blog">Check out our Blog</a></p>
<div class="whiteSpace50"></div>
</div>-->

<div id="mainBlue"><div id="mainBody"><div class="text-center colorWhite" id="content">
<div class="whiteSpace50"></div>
<h2 class="font45 sourceBlack">Connect with SiteLock</h2>
<div class="whiteSpace25"></div>
<div class="row text-center linkWhite">
<div class="col-sm-3"><a href="https://www.facebook.com/SiteLock/"><i class="fab fa-facebook-square fa-6x"></i></a></div>
<div class="col-sm-3"><a href="https://twitter.com/SiteLock"><i class="fab fa-twitter-square fa-6x"></i></a></div>
<div class="col-sm-3"><a href="https://www.linkedin.com/company/sitelock/"><i class="fab fa-linkedin fa-6x"></i></a></div>
<div class="col-sm-3"><a href="https://www.instagram.com/sitelock/"><i class="fab fa-instagram fa-6x"></i></a></div>
</div>
<div class="whiteSpace50"></div>
</div></div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<script>
fetch('includes/data/press_releases.json')
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }

      // Examine the text in the response
      response.json().then(function(prdata) {
        console.log(prdata.slice(-5));
        var prbox = "";
        for(index in prdata.slice(-5)) {
          for(pr in prdata[index]) {
            prbox += "<div class='linkwhite' id='pr'>"
            + "<span class='font14 capitalize'>" + prdata[index][pr]['month'] + " " + prdata[index][pr]['day'] + ", " + prdata[index][pr]['year'] + "</span>"
            + "<h3 class='sourceBlack font18'>" + prdata[index][pr]['title'] + "</h3>"
            + "<a href='" + prdata[index][pr]['source'] + "' style='text-decoration: underline !important;' target='_blank'>Read more</a>"
            + "</div></div>";
          }
        }
        document.getElementById("prbox").innerHTML = prbox;
      });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });

fetch('includes/data/news.json')
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }

      // Examine the text in the response
      response.json().then(function(data) {
        console.log(data.slice(-5));
        var news = "";
        for(index in data.slice(-5)) {
          for(post in data[index]) {
            news += "<div class='linkgrey' id='news'><div class='row'>"
            + "<div class='col-md-2 my-auto'><img class='newsImg' src='img/news/" + data[index][post]['image'] + "' alt='Article Source'></div><div class='col-md-10'>"
            + "<span class='capitalize'>" + data[index][post]['month'] + "</span> " + data[index][post]['day'] + ", " + data[index][post]['year']
            + "<h3 class='font18'>" + data[index][post]['title'] + "</h3>"
            + "<a class'font14' href='" + data[index][post]['source'] + "' target='_blank'>Keep reading</a>"
            + "</div></div></div>";
          }
        }
        document.getElementById("news").innerHTML = news;
      });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });
</script>

</body>
</html>