<?php
//DEFINITIONS
$title = "Personal Sites | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div id="mainBody">
<div class="whiteSpace25"></div>
<div class="row">
<div class="col-lg-6 my-auto solutionHead">
  <h1>Secure Your <span class="sourceBlack">Personal Site</span></h1>
  <h3>Real-time cyber protection for your visitors and content.</h3>
  <a class="btn btn-red" href="#onpagepricing"><strong>Pick Your Perfect Plan</strong></a>
</div>
<div class="col-lg-6">
  <img class="headerimgmain" src="/img/personal-site/personalHeader.png" alt="Website Security">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<?php include('includes/rating-bar.php');?>

<div id="mainBody">

  <div class="whiteSpace50"></div>

  <div id="mainSplitRight">
    <div class="row splitSpace">
     <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2><span class="sourceBlack">Have peace of mind</span><br>knowing your website is secure</h2><p style="width: 90%;">Let’s talk about your website. The truth is, all websites, including yours, are inherently at risk of compromise, not just big businesses you read about in the news. In fact, the more features your site has, the more at risk it becomes. The good news is, there’s a perfect security solution for your website and needs, and we’re confident you’ll love the benefits of having a secure site.</p><a class="btn btn-red font-weight-bold" href="#onpagepricing">Start Protecting Your Website</a></div></div>
     <div class="col-lg-5 splitPadding iconFirst"><img class="splitImg" src="/img/personal-site/personalSite.svg" alt="Scanning Calendar"></div>
    </div>
  </div>

  <div class="whiteSpace100"></div>

   <div class="row">
    <div class="col-md-6"><img class="img90" src="/img/personal-site/personalMedusa.svg" alt="Scanning Threat"></div>
    <div class="col-md-6 my-auto"><div id="content"><h2><span class="sourceBlack">Know your risk</h2><p class="font18">Take a look at just some of the features that could be contributing to your likelihood of compromise.</p><a class="btn btn-red font-weight-bold" href="#onpagepricing">Lower Your Risk of Attack</a></div></div>
   </div>

  <div class="whiteSpace100"></div>

  <div id="mainSplitLeft">
    <div class="row splitSpace">
     <div class="col-lg-6 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2><span class="sourceRegular">Good days or bad days,</span> <span class="sourceBlack">we’re here for you</span></h2><p style="width: 85%;">A website attack can be stressful and overwhelming. Don’t worry—we’ll work together to get your site back up and running fast. Let us walk you through the SiteLock experience.</p></div></div>
     <div class="col-lg-6 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/259753291" style="position:absolute;top:0;left:0;width:100%;height:100%; marginLl" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
    </div>
  </div>

  <div class="whiteSpace100"></div>

</div>

<?php include('includes/cms-bar.php');?>

<?php
$benefitsTitle = '<h2 class="text-center font55">Website security <span class="sourceBlack">benefits you’ll love</span></h2>'; 
$icon1= 'fa-star'; $title1 = 'Become a hero'; $content1 = 'Be the hero your website needs and protect your visitors, content, and design from getting into the hands of someone else. Cybercriminals—you’ve met your match.';
$icon2 = 'fa-thumbs-up'; $title2 = 'Make a good impression'; $content2 = 'Your site is your prized possession. It could be a bit embarrassing if it’s vandalized by cybercriminals. Make a good first impression and protect your design with website security.';
$icon3 = 'fa-share-alt'; $title3 = 'Expand your readership'; $content3 = 'Ensure visitors can find your site. If search engines find malware on it, it will be blacklisted and potentially removed from results. Protect your SEO efforts by staying malware-free.';
$icon4 = 'fa-badge-check'; $title4 = 'Establish credibility'; $content4 = 'Let your visitors know they can trust you by showcasing the SiteLock Trust Seal on your site. This signifies your site is safe. Really, it’s that easy.';
$icon5 = 'fa-heart'; $title5 = 'Focus on your passion'; $content5 = 'Don’t let a cyberattack interfere with what you do best—following your passion. Automated website security keeps your site up and running all day, every day. You’ll see.';
$icon6 = 'fa-headset'; $title6 = 'Get help 24/7/365'; $content6 = 'Keep your security simple and let us help you out. Consider us your tech friends you can call, email, or live chat with 24/7/365.';
$benefitsBTN = 'Let’s See Some Pricing';

include('includes/benefits-bar.php');
?>

<div id="onpagepricing"></div>
<div id="mainBody">
<div class="whiteSpace100"></div>

<h2 class="sourceLight text-center font55">Don’t wait, protect your website now.<br><span class="sourceBlack">Buy your website security plan today!</span></h2>

<?php include('includes/price-bar-1.php');?>

<?php include('includes/includes-bar.php');?>
  <div class="whiteSpace100"></div>
</div>

<?php include('includes/solutions-bar.php');?>

<div id="mainGrey">
<div class="whiteSpace25"></div>
<h2 class="sourceBlack text-center font45">As Seen In</h2>
<?php include('includes/news-bar-1.php');?>
<div class="whiteSpace25"></div>
</div>

<div id="mainBody">
<div class="whiteSpace50"></div>
<h2 class="sourceBlack text-center font45">Get to know the products that protect your site</h2>
<div class="row">
<div class="col-md-3 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/scanningWebsite.svg" alt="Scanning Website"><h4 class="sourceBlack">Website<br>Scanning</h4><p>Scan your site or blog daily for malicious or suspicious activity. When threats are found, you are notified immediately.</p><br><a class="btn btn-ghost-grey" href="website-scanning">Learn More</a></div><br></div>
</div>

<div class="col-md-3 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/scanningCloud.svg" alt="Scanning Cloud"><h4 class="sourceBlack">Malware<br>Removal</h4><p>Find and automatically remove website malware before any damage is caused to your site or visitors.</p><br><a class="btn btn-ghost-grey" href="malware-removal">Learn More</a></div><br></div>
</div>

<div class="col-md-3 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/scanningThreat.svg" alt="Scanning Threat"><h4 class="sourceBlack">Vulnerability<br>Patching</h4><p>Automatically patch vulnerabilities in your CMS website before cybercriminals exploit them and gain access to your site.</p><br><a class="btn btn-ghost-grey" href="vulnerability-patching">Learn More</a></div><br></div>
</div>

<div class="col-md-3 text-center">
<div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight170" src="/img/scanningWAF.svg" alt="Scanning WAF"><h4 class="sourceBlack">Web Application<br>Firewall</h4><p>Prevent hackers from entering while increasing your site speed by as much as 50 percent.</p><br><br><a class="btn btn-ghost-grey" href="web-application-firewall">Learn More</a></div><br></div>
</div>
</div>
<div class="whiteSpace100"></div>
</div></div>

<?php include('includes/trusted-bar.php');?>

<div id="mainBody">

  <div class="whiteSpace100"></div>

  <div id="mainSplitLeft">
   <div class="row splitSpace">
    <div class="col-lg-5 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2><span class="sourceBlack">Stop a cyberattack in its tracks</h2>Learn how likely your site is to be compromised, so you can make an educated decision about your security.<br><a class="btn btn-blue" href="contact">Get Your Free Risk Assessment</a></div></div>
    <div class="col-lg-7 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/273777919" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
   </div>
  </div>

  <div class="whiteSpace100"></div>

  <div id="mainSplitRight">
    <div class="row splitSpace">
     <div class="col-md-5 splitPadding"><img class="splitImgAlt" src="/img/personal-site/billKasal.png" alt="Bill Kasal"></div>
     <div class="col-md-7 splitPadding"><div id="content" style="max-width: 75% !important; margin: 0 auto 0 auto !important;"><h2><span class="sourceBlack">Meet this security superhero:</span><br>Bill Kasal</h2>Bill is the author of seven self-published books & relies heavily on his blog, billkasal.com, to promote his stories. After a cyber attack, Bill connected with SiteLock and the malware was cleaned from billkasal.com within hours.<p><br><i>“This was the first time I’ve ever experienced a cyberattack, but with SiteLock protecting my site, I know it will be the last.”</i></p><a class="btn btn-ghost-blue font-weight-bold" href="https://www.sitelock.com/blog/2017/05/sitelock-reviews-bill-kasal/">Read More</a></div></div>
    </div>
  </div>

  <div class="whiteSpace100"></div>

</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>
