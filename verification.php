<?php
//DEFINITIONS
$title = "Website Verification";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
$pagephone = "877.563.2876";
$linkphone = "tel:8775632876";

$ct_title = "Contact SiteLock 24/7";
$ct_phone = "877.563.2876";
$ct_btn = 'Send Message';
include 'includes/forms/contact.html';
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>


<div id="menu_basic"><div id="menu_inner">
<div class="whiteSpace25"></div>
<div id="mainBody">
<div class="row">
<div class="col-md-6"><a href="/"><img class="logoNav" src="img/logos/SiteLock_red.svg" alt="sitelock logo"></a></div>
<div class="col-md-6"><h3 class="sourceBlack float-right"><a href="<?php echo $linkphone;?>"><?php echo $pagephone;?></a></h3></div>
</div>
</div>
<div class="whiteSpace25"></div>
</div></div>

<div id="mainHeader" style="margin-top: 0 !important;"><div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-md-6">
<h1 class="sourceBlack">Want to display the SiteLock Trust Seal on your website?</h1>
<h3>It’s easy! Submit your information and we’ll be in touch shortly.</h3>
<a class="btn btn-blue" href="#ContactForm" data-toggle="modal" data-target="#ContactForm">Get Started</a>
</div>
<div class="col-md-6">
<img class="img70" src="/img/verification/verificationBadge.png" alt="Website Verification Badge">
</div>
</div>
</div>
<div class="whiteSpace50"></div>
</div>

<div id="mainBody">
<div class="whiteSpace50"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-md-5 splitPadding"><img class="splitImg" src="/img/verification/verificationSeal.svg" alt="Verification Seal"></div>
  <div class="col-md-7 whitebk splitPadding"><div id="content"><h2><span class="sourceRegular">What is the SiteLock</span> <span class="sourceBlack">Trust Seal?</span></h2>The SiteLock Trust Seal instills confidence and lets visitors know your site is safe and credible. Today, 79 percent of online shoppers expect to see a trust badge. Not only that, but trust badges are proven to increase ROI and conversion rates! Ready to start showcasing one on your site?<br><a class="btn btn-red" href="#ContactForm" data-toggle="modal" data-target="#ContactForm">Start displaying the badge on your site today!</a></div></div>
 </div>
</div>

<div class="whiteSpace100"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-md-7 whitebk splitPadding"><div id="content"><h2 class="sourceBlack">Who is SiteLock?</span></h2>SiteLock is the global leader in website security. The company was founded in 2008 with a mission to protect every website on the internet. We currently protect over 12 million websites of all sizes around the world and are just getting started!</strong><br><a class="btn btn-red" href="#ContactForm" data-toggle="modal" data-target="#ContactForm">Gain Control of Your Site Now</a></div></div>
   <div class="col-md-5 splitPadding iconFirst"><img class="splitImg" src="/img/verification/verificationCloud.svg" alt="Verification Cloud"></div>
  </div>
</div>

<div class="whiteSpace50"></div>
</div>

<div class="footer">
<div id="content" style="padding-top: 10px;">


<hr class="footerHR">


<div class="row text-center">
<div class="col-md-3"><h4><a href="privacy-policy">SiteLock Privacy Policy</a></h4></div>
<div class="col-md-3"><h4><a href="terms">Terms of Service</a></h4></div>
<div class="col-md-3"><h4>&copy; SiteLock 2008-<span id="COPYRIGHT"></span></h4></div>
<div class="col-md-3"><h4><a href="<?php echo $linkphone;?>"><?php echo $pagephone;?></a></h4></div>
</div>

<hr class="footerHR">

<p class="text-center footSubtext">Use of this Site is subject to express Terms and Conditions. By using this site, you signify that you agree to be bound by our terms and conditions and privacy policy.</p>

</div>
</div>

<?php include 'includes/assets/OMEGA.php';?>
</body>
</html>