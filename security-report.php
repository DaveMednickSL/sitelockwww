<?php
//DEFINITIONS
$title = "Website Security Insider | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-6">
<h1>SiteLock<br><span class="sourceBlack">Website Security Insider</span></h1>
<h3>Insights into Today’s Most Challenging Cybersecurity Threats</h3>
<a href="#HighBarrierForm" class="btn btn-red" data-toggle="modal" data-target="#HighBarrierForm">Email Me the Report</a>
</div>
<div class="col-lg-6">
<img class="headerimg" src="/img/qsrCover.png" alt="Website Security Report">
</div>
</div>
</div>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainGrey"><div id="mainBody"><div id="content">
<div class="whiteSpace50"></div>
<div class="row font18">
<div class="col-md-6"><div class="divheight340" id="darkBorderBox"><div id="content90">
  <p>
  <br>
  The SiteLock Website Security Insider includes analysis and trends based on proprietary data from over 6 million websites. The report delivers exclusive insights into the increased risk of compromise website owners face, including:
  <ul>
    <li>Malware Trends</li>
    <li>Search Engine Blacklist Warnings</li>
    <li>Popular Vulnerabilities and Website Attacks</li>
    <li>Content Management Systems Vulnerabilities</li>
    <li>Website Risk Score</li>
  </ul>
  </p>
  </div></div></div>

  <div class="col-md-6"><div class="divheight340" id="darkBorderBox"><div id="content90">
  <p><br><strong>Download the report and learn how to effectively prepare and respond to today’s ever-evolving cyber threats.</strong></p>
  <p>
  Explore past reports:
  <ul class="linkgrey">
    <li><a href="download/SiteLock Website Security Insider Q1 2018.pdf">Q1 2018</a></li>
    <li><a href="download/SiteLock-Website-Security-Insider-Q4-2017.pdf">Q4 2017</a></li>
    <li><a href="download/SiteLock-Website-Security-Insider-Q3-2017.pdf">Q3 2017</a></li>
    <li><a href="download/SiteLock-Website-Security-Insider-Q2-2017.pdf">Q2 2017</a></li>
  </ul>
  </p>
  </div></div></div>
  </div>
<div class="whiteSpace100"></div>
</div></div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>
<?php
$hb_title = "Get the Latest Security Report";
$hb_phone = "(877) 798-5144";
$hb_btn = 'Start Today';
?>

<!-- Contact Modal -->
<div class="modal fade" id="HighBarrierForm" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content modalForm modalFix">
      <div class="modal-header">
        <h5 class="modal-title sourceRegular" id="exampleModalLabel"><?php echo $hb_title; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="fal fa-times-circle" style="user-select: auto;"></i>
        </button>

      </div>
      <div class="modal-body">
        <p class="text-center redLink">Call us anytime at <a href="tel:<?php echo $hb_phone_link; ?>"><?php echo $hb_phone; ?></a> or fill out the form below:</p>
      <form method="post" action="http://a29565.actonservice.com/acton/eform/29565/aa6595f7-77b0-4c32-b50c-e08a9e855ca4/d-ext-0001">
        <span class="form-inline form-group"><i id="validateName" class="far fa-circle grey fa-lg" title="First Name should be formatted as firstname"></i>&nbsp;<input id="HBinputName" style="width: 90%;" type="text" class="form-control" placeholder="First Name" name="First Name" required><label class="form-control-placeholder" for="HBinputName">First Name</label></span>
        <span class="form-inline form-group"><i id="validateLastName" class="far fa-circle grey fa-lg" title="Last Name should be formatted as lastname"></i>&nbsp;<input id="HBinputlastName" style="width: 90%;" type="text" class="form-control" placeholder="Last Name" name="Last Name" required><label class="form-control-placeholder" for="HBinputlastName">Last Name</label></span>
        

        <span class="form-inline form-group"><i id="validateEmail" class="far fa-circle grey fa-lg" title="Email should be formatted as you@domain.com"></i>&nbsp;<input id="HBinputEmail" style="width: 90%;" type="email" class="form-control" placeholder="Email" name="E-mail" required><label class="form-control-placeholder" for="HBinputEmail" >Email</label></span>

      <div class="custom-control custom-checkbox" style="font-size: 14px;">
        <input type="checkbox" class="custom-control-input" id="customCheck1">
        <label class="custom-control-label" for="customCheck1">Email me the latest website security news</label>
      </div>
      <br>
      <p class="text-center"><button type="submit" class="btn btn-blue"><?php echo $hb_btn; ?></button></center>
      </form>
      </div>
      <div class="modal-footer">
        <p class="form-notice">By submitting this form you confirm that you have read and accepted our <a href="terms" target="_blank">Terms & Conditions</a> and <a href="privacy-policy" target="_blank">Privacy Policy</a></p>
      </div>
    </div>
  </div>
</div>

</body>
</html>