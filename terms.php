<?php
//DEFINITIONS
$title = "Terms and Conditions";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="sourceRegular"><span class="sourceBlack">SiteLock</span> Terms and Conditions</h1>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainTerms">

<div class="whiteSpace50"></div>

<h3 class="sourceRegular">ACCEPTABLE USE POLICY ("AUP")</h3>
<p>
General Information. As a provider of Internet/World Wide Web-related services, SITELOCK, LLC, an Arizona limited liability company (the "Company") offers its customers, the means to acquire and disseminate a wealth of public, private, commercial, and non-commercial information.
<br><br><a class="btn btn-ghost-blue" href="download/legal/SiteLock_AUP_2016.pdf" target="_blank">SiteLock AUP</a>
</p>
<div class="whiteSpace50"></div>

<h3 class="sourceRegular">PRIVACY POLICY</h3>
<p>
Your privacy is very important to SITELOCK, LLC, an Arizona limited liability company (the "Company"). We want to make your experience using our services and on the Internet as enjoyable and rewarding as possible, and we want you to use the Internet's vast array of information, tools, and opportunities with complete confidence.
<br><br><a class="btn btn-ghost-blue" href="privacy-policy" target="_blank">SiteLock Privacy Policy</a>
</p>
<div class="whiteSpace50"></div>

<h3 class="sourceRegular">CUSTOMER AGREEMENT</h3>
<p>
The SiteLock Responsible Disclosure Policy details the process by which SiteLock publicly discloses vulnerabilities found during research and malware cleaning processes. The Policy reflects SiteLock's dedication to a secure web for all site owners and developers, and stresses privacy and the protection of sensitive data.
<br><br><a class="btn btn-ghost-blue" href="download/legal/SITELOCK_CUSTOMER_AGREEMENT_2019.pdf" target="_blank">SiteLock Customer Agreement</a>
</p>
<div class="whiteSpace50"></div>

<h3 class="sourceRegular">RESPONSIBLE DISCLOSURE POLICY</h3>
<p>
The SiteLock Responsible Disclosure Policy details the process by which SiteLock publicly discloses vulnerabilities found during research and malware cleaning processes. The Policy reflects SiteLock's dedication to a secure web for all site owners and developers, and stresses privacy and the protection of sensitive data.
<br><br><a class="btn btn-ghost-blue" href="download/legal/SiteLock_Responsible_Disclosure_Policy_2016.pdf" target="_blank">SiteLock Responsible Disclosure Policy</a>
</p>
<div class="whiteSpace50"></div>

<h3 class="sourceRegular">SITELOCK TRADEMARKS</h3>
<p>
When using these trademarks in publications that will be distributed only within the United States, include the appropriate TM, SM, or ® symbol. For publications distributed outside the United States, use an appropriate trademark attribution notice, for example: SecureSite® is a trademark of SiteLock, LLC, registered in the U.S.
<br><br><a class="btn btn-ghost-blue" href="download/legal/SiteLock_Trademarks_Notice.pdf" target="_blank">SiteLock Trademarks Notice</a>
</p>
<div class="whiteSpace50"></div>

</div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>
