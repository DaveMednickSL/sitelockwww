<?php
//DEFINITIONS
$title = "Privacy Policy";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace50"></div>
<h1 class="sourceRegular"><span class="sourceBlack">SiteLock</span> Privacy Policy</h1>
<div class="whiteSpace50"></div>
</div></div>

<div id="mainTerms">

<div class="whiteSpace50"></div>
<p>
<strong>Effective Date:</strong> May 25, 2018
<hr>
<a class="btn btn-ghost-blue" href="download/legal/SiteLockPrivacyPolicy_May2018.pdf" target="_blank">Download PDF Copy</a>
</p>
</div>

<div class="linkred" id="mainTerms">
<div class="whiteSpace50"></div>

<h2 class="sourceRegular">PRIVACY POLICY </h2>
<p>
Your privacy is very important to Sitelock, LLC and its subsidiaries (Henceforward the "Company", "We", Us"). We want to make your experience using our services on the Internet as enjoyable and rewarding as possible, and we want you to use the Internet's vast array of information, tools, and opportunities with complete confidence. We collect and use personal data only as it might be needed for us to deliver to you our security services.
</p>
<div class="whiteSpace25"></div>

<p>
We have created this Privacy Policy to demonstrate our firm commitment to privacy and security. This Privacy Policy applies to the information that SiteLock, LLC and its subsidiary, Patchman B.V., collectively collects through sitelock.com, patchman.co. and other websites, online features, and applications owned or controlled by SiteLock. This Privacy Policy describes how the Company collects information from all end users of the Company's services (the "Services"). It also describes your choices regarding use, access and correction of your personal information.
</p>
<div class="whiteSpace25"></div>

<p>
Our Privacy Policy is intended to describe to you how and what data we collect, how and why we use your personal data. It also describes options we provide for you to access, update or otherwise take control of your personal data that we process.
</p>
<div class="whiteSpace25"></div>

<h3>Contact information</h3>
<p>
If at any time you have questions about our practices or any or your rights described below you may reach out to us at privacy@sitelock.com.
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>Why does SiteLock collect personal data?</strong></h4>
<p>
The Company collects information, of which some can be considered personal data, in different ways from Visitors and Customers who access the various parts of our Website, Services and the network of Web sites accessible through our Services. We use this information primarily to provide a customized experience as you use our Services and, generally, do not share this information with third parties. However, we may disclose personal information collected if we have received your permission beforehand or in very special circumstances, such as when we believe that such disclosure is required by law or other special cases described below.
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>What information does SiteLock collect?</strong></h4>
<h3>Account Information</h3>
<p>
Customers are asked to provide certain personal information when they sign up for our Services including name, address, telephone number, billing information (such as a credit card number), domain address and the type of personal computer being used to access the Services. Visitors inquiring about the Services may be asked to submit business information, such as company name, phone number, and domain address. The personal information collected from Customers during the registration process is used to manage each Customer's account (such as for billing purposes). This information is not shared with third parties. The Company may also generate non-identifying and aggregate profiles from personal information Customers provide during registration (such as the total number, but not the names, of Customers). As explained in more detail below, we may use this aggregated and non- identifying information to sell advertisements that appear on the Services.
</p>
<div class="whiteSpace25"></div>

<h3>Job applicant information</h3>
<p>
Job applicant data is collected for our online job application process. We collect your name, address, email address, data of birth and phone number. This information might be shared with third parties to further the job application process.
</p>
<div class="whiteSpace25"></div>

<h3>Cookies and Tracking Technologies</h3>
<p>
Sitelock, LLC and our partners use cookies or similar technologies to analyze trends, administer the website, track users' movements around the website, and gather demographic information about our user base as a whole. Users can control the use of cookies at the individual browser level. Cookies are text files containing small amounts of information, which your computer, tablet or mobile device downloads when you visit a website. They enable the site’s or service provider’s systems to recognize your browser and capture and remember certain information. We have the following types:

<ul>
<li>Required- these cookies are required to enable core site functionality</li>
<li>Functional- these cookies allow is to analyze site usage, so we can measure and improve performance</li>
<li>Advertising cookies- these cookies are used by advertising companies (Google) to serve ads that are relevant to your interests</li>
</ul>
</p>

<p>
You can control how cookies are placed on your devices either from within your browser. Since browser is a little different, look at your browser’s Help Menu to learn the correct way to modify your cookies.
</p>
<div class="whiteSpace25"></div>

<h3>Statistics</h3>
<p>
As is true of most web sites, we gather certain information automatically and store it in log files. This information includes internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and clickstream data. We use this information, which does not identify individual users, to analyze trends, to administer the site, to track users' movements around the site and to gather demographic information about our user base as a whole. We do not link this automatically collected data to personally identifiable information.
</p>

<h3>Responses to Email Inquiries</h3>
<p>
When Visitors or Customers send email inquiries to the Company, the return email address is used to answer the email inquiry we receive. The Company does not use the return email address for any other purpose and does not share the return email address with any third party.
</p>
<div class="whiteSpace25"></div>

<h3>Testimonials</h3>
<p>
We display personal testimonials of satisfied customers on our site in addition to other endorsements. With your consent we may post your testimonial along with your name. If you wish to update or delete your testimonial, you can contact us at support@sitelock.com.
</p>

<p>
Our Web site also offers publicly accessible bios or community forums. You should be aware that any information you provide in these areas may be read, collected, and used by others who access them. You can request the removal of your personal information from our blog or community forum, by contacting us at support@sitelock.com. In some cases, we may not be able to remove your personal information, in which case we will let you know if we are unable to do so and why.
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>How SiteLock utilizes the Information.</strong></h4>
<p>
We strongly believe in both minimizing the data we collect and limiting its use and purpose to only that (1) for which we have been given permission, (2) as necessary to deliver the services you purchase or (3) as we might be required or permitted for legal compliance or other lawful purposes.
</p>
<div class="whiteSpace25"></div>


<h3>Delivering services to you</h3>
<p>
We collect personal information and website data in order to deliver industry-leading website security services to small-to-medium size businesses worldwide.
</p>
<div class="whiteSpace25"></div>

<h3>Transfer of personal data</h3>
<p>
SiteLock is a U.S. based company. For users outside the U.S., please be aware that any information you provide to us may be transferred to and processed in the United States. Our Patchman operation processes and stores the data in the United Kingdom. By using our websites and providing us with any information, you consent to this transfer, processing and storage of your information in the U.S., UK, and other operating sites SiteLock may establish in the future.
</p>
<div class="whiteSpace25"></div>

<h3>Compliance with legal, regulatory and law enforcement requests</h3>
<p>
We cooperate with the government and law enforcement officials and private parties that we, in our sole discretion, believe necessary or appropriate to respond to claims and legal process (such as subpoena requests), to protect our property and rights or the property and rights of a third party, to protect the safety of the public or any person, or to prevent or stop activity we consider to be illegal or unethical. To the extent we are legally permitted to do so, we will take reasonable steps to notify you in the event that we are required to provide your personal information to third parties as part of legal process.
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>How and When we disclose information to Third Parties</strong></h4>
<h3>Behavioral Advertising</h3>
<p>
We partner with a third party to either display advertising on our Web site or to manage our advertising on other sites. Our third-party partner may use technologies such as cookies to gather information about your activities on this site and other sites in order to provide you advertising based upon your browsing activities and interests. If you wish to not have this information used for the purpose of serving you interest-based ads, you may opt-out by <a href="http://preferences-mgr.truste.com/" target="_blank">clicking here</a> (or if located in the European Union <a href="http://www.youronlinechoices.eu/" target="_blank">click here</a>) Please note this does not opt you out of being served ads. You will continue to receive generic ads.
</p>
<div class="whiteSpace25"></div>

<h3>Voluntary Customer Surveys</h3>
<p>
We may periodically conduct both business and individual customer surveys. We encourage our customers to participate in these surveys because they provide us with important information that helps us to improve the types of services we offer and how we provide them to you. Your personal information and responses will remain strictly confidential, even if the survey is conducted by a third party; that company is prohibited from using our users' personal information for any other purpose. Participation in our customer surveys is voluntary. We will not share the personal information you provide through a contest or survey with other third parties unless we give you prior notice and choice. We take the information we receive from individuals responding to our Customer Surveys and combine (or aggregate) it with the responses of other Company customers to create broader, generic responses to the survey questions (such as gender, age, residence, hobbies, education, employment, industry sector, or other demographic information). We then use the aggregated information to improve the quality of our services to you, and to develop new services and products. This aggregated, non-personal information may be shared with third parties.
</p>
<div class="whiteSpace25"></div>

<h3>Special Cases</h3>
<p>
It is the Company's policy not to use or share the personal information about Visitors or Customers in ways unrelated to the ones described above without also providing you an opportunity to opt out or otherwise prohibit such unrelated uses. However, the Company may disclose personal information about Visitors or Customers, or information regarding your use of the Services or Web sites accessible through our Services, for any reason if, in our sole discretion, we believe that it is reasonable to do so, including: to satisfy laws, such as the Electronic Communications Privacy Act, regulations, or governmental or legal requests for such information; to disclose information that is necessary to identify, contact, or bring legal action against someone who may be violating our Acceptable Use Policy or other user policies; to operate the Services properly; or to protect the Company and our Customers. If the Company is involved in a merger, acquisition, or sale of all or a portion of its assets, you will be notified via email and/or a prominent notice on our Web site of any change in ownership or uses of your personal information, as well as any choices you may have regarding your personal information.
</p>
<div class="whiteSpace25"></div>

<h3>The Company's Partners and Sponsors</h3>
<p>
Some of the Company products and services are offered to Visitors and Customers in conjunction with a non-affiliated partner. To provide Visitors and Customers some of these products and services, the partner may need to collect and maintain personal information. In these instances, we will provide such data collected or transferred to our Partners and Sponsors (e.g., domain registration). Additionally, many of the Company Customers have co- branded pages that are co-sponsored by non-affiliated partners. The Company will share non- identifying and aggregate information (except as described above), but not personal information, with such partners in order to administer the co-branded products or services offered. We may provide your personal information to companies that provide services to help us with our business activities limited to security, fraud detection and / or compliance services. These companies are authorized to use your personal information only as necessary to provide these services to us.
</p>
<div class="whiteSpace25"></div>

<h3>EU-US Privacy Shield</h3>
<p>
Sitelock, LLC participates in and has certified its compliance with the EU-U.S. Privacy Shield Framework and the Swiss-US Privacy Shield Framework. We are committed to subjecting all personal data received from European Union (EU) member countries and Switzerland, respectively, in reliance on the Privacy Shield Framework, to the Framework's applicable Principles. To learn more about the Privacy Shield Framework, visit the U.S. Department of Commerce's Privacy Shield List https://www.privacyshield.gov.
</p>

<p>
Sitelock, LLC is responsible for the processing of personal data it receives, under the Privacy Shield Framework, and subsequently transfers to a third party acting as an agent on its behalf. We comply with the Privacy Shield Principles for all onward transfers of personal data from the EU and Switzerland, including the onward transfer liability provisions.
</p>

<p>
With respect to personal data received or transferred pursuant to the Privacy Shield Framework, we are subject to the regulatory enforcement powers of the U.S. Federal Trade Commission. In certain situations, we may be required to disclose personal data in response to lawful requests by public authorities, including to meet national security or law enforcement requirements.
</p>

<p>
If you have an unresolved privacy or data use concern that we have not addressed satisfactorily, please contact our U.S.-based third-party dispute resolution provider (free of charge) at https://feedback-form.truste.com/watchdog/request
</p>

<p>
Under certain conditions, more fully described on the Privacy Shield website, https://www.privacyshield.gov/article?id=How-to-Submit-a-Complaint, you may invoke binding arbitration when other dispute resolution procedures have been exhausted.
</p>
<div class="whiteSpace25"></div>

<h3>Links to 3rd Party Sites.</h3>
<p>
Our Site includes links to other Web sites whose privacy practices may differ from those of the Company. If you submit personal information to any of those sites, your information is governed by their privacy policies. We encourage you to carefully read the privacy policy of any Web site you visit.
</p>

<p>
Our Web site also includes Social Media Features, such as the Facebook button and Widgets, such as the Share this button or interactive mini-programs that run on our site. These Features may collect your IP address, which page you are visiting on our site, and may set a cookie to enable the Feature to function properly. Social Media Features and Widgets are either hosted by a third party or hosted directly on our Site. Your interactions with these Features are governed by the privacy policy of the company providing it.
</p>
<div class="whiteSpace25"></div>

<h3>Frames</h3>
<p>
Some of our pages utilize framing techniques to serve content from our partners while preserving the look and feel of our site. Please be aware that you are providing your personal information to these third parties and not to www.sitelock.com.
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>How data subjects can access, update or delete their data.</strong></h4>
<p>
Upon request we will provide you with information about whether we hold, or process on behalf of a third party, any of your personal information. If your personal information changes, or if you no longer desire our service, you may correct, update, or amend it by making the change on your account settings page, request deletion by emailing our Customer Support at <a href="mailto:support@sitelock.com">support@sitelock.com</a> or by contacting us by telephone at the <strong>contact information listed below</strong>. We will respond to your request to access within 30 days.
</p>

<p>
Administrators of company accounts have the ability to add new users by inputting their name and email address. If a user of the Services desires to have their information modified or deleted, they should contact the administrator they deal with directly.
</p>

<p>
We will retain your information for as long as your account is active or as needed to provide you services. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.
</p>

<p>
If you are unable for any reason to access your account settings or if you do not have an account with us, you may also contact us by one of the methods describe in the “Contact Us” section below.
</p>

<p>
If you wish to subscribe to our newsletter(s), we will use your name and email address to send the newsletter to you. Out of respect for your privacy, you may choose to stop receiving our newsletter or marketing emails by following the unsubscribe instructions included in these emails, accessing the email preferences in your account settings page or you can contact us at support@sitelock.com
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>Applying for a job on our site</strong></h4>
<p>
If you apply for a job through the Site, we, or a human resources company assisting us, may ask you to provide self-identifying information (such as veteran status, gender, and ethnicity) in conjunction with laws and regulations enforced by the Equal Employment Opportunity Commission ("EEOC"), the Office of Federal Contract Compliance Programs ("OFCCP"), and similar state and local regulatory agencies. Providing self-identifying information is voluntary, but if you do provide us with that information, we may submit it to the EEOC, the OFCCP, and similar state and local regulatory agencies for business-related purposes, including responding to information requests, fulfilling regulatory reporting requirements, and defending against employment related complaints.
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>How we secure, store and retain your data.</strong></h4>
<p>
The security of your personal information is important to us. When you enter sensitive information (such as a credit card number) on our order forms, we encrypt the transmission of that information using secure socket layer technology (SSL). We follow generally accepted standards to protect the personal information submitted to us, both during transmission and once we receive it. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, we cannot guarantee its absolute security. Services and Websites, we sponsor have security measures in place to protect the loss, misuse, and alteration of the information under our control. If you have any questions about security on our Web site, you can contact us at support@sitelock.com
</p>

<p>
We retain personal data only for as long as necessary to provide services you have requested and thereafter for a variety of legitimate legal or business purposes. These might include retention periods:

<ul>
<li>Mandated by law, contract or similar obligation applicable to our business operations;</li>
<li>For preserving, resolving, defending or enforcing our legal/contractual rights; or</li>
<li>Need to maintain adequate and accurate business and financial records.</li>
</ul>
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>Age restrictions</strong></h4>
<p>
Our services are available for purchase only for those over the age of 18. Our services are not targeted to, intended to be consumed by or designed to entice individuals under the age of 18. If you know of our have reason to believe anyone under the age of 18 has provide us with any personal data, please contact us.
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>Changes to this policy</strong></h4>
<p>
We reserve the right to modify this Privacy Policy at any time. If we decide to change our Privacy Policy, we will post those changes to this Primacy Policy and any other places we deem appropriate, so that you are aware of the information we collect, how we use it, and under what circumstances, if any, we disclose it. If we make any material changes we will notify you by email (sent to the e-mail address specified in your account) or by means of a notice on this Site prior to the change becoming effective. We encourage you to periodically review this page for the latest information on our privacy practices.
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>Data Protection Authority</strong></h4>
<p>
If you are a resident of the European Economic Area (EEA) and believe we maintain your personal data subject to the General Data Protection Regulation (GDPR), you may direct questions or complaints to our lead supervisory authority, Dutch Data Protection Authority, as
</p>

<p>
<strong>noted below</strong><br>
https://autoriteitpersoonsgegevens.nl/en/contact-dutch-dpa/contact-us
</p>

<p>
<strong>Dutch Data Protection Authority</strong><br>
Autoriteit Persoonsgegevens
Postbus 93374
2509 AJ DEN HAAG
</p>

<p>
<strong>Telephone</strong><br>
Telephone number: (+31) - (0)70 - 888 85 00<br>
Fax: (+31) - (0)70 - 888 85 01
</p>
<div class="whiteSpace25"></div>

<h4 class="text-center"><strong>Contact Us</strong></h4>
<p>
If you have any questions, concerns or complaints about our Privacy Policy, our practices or our Services, you may contact our office at privacy@sitelock.com. As an alternative, you may contact us by either of the following means:

<ul>
<li>By mail: Attn: GDPR Owner 8701 East Hartford Drive Suite 200, Scottsdale, AZ 85255 USA</li>
<li>By phone: U.S. (877)257-9263 or International: (415) 390-2500</li>
</ul>
</p>

<p>
We will respond to all requests, inquiries or concerns within 30 days.
</p>

<div id="7d494ce9-2e39-4a4f-a1ee-be6081ee3bd1">
<script type="text/javascript" src="//privacy-policy.truste.com/privacy-seal/SiteLock,-LLC/asc?rid=7d494ce9-2e39-4a4f-a1ee-be6081ee3bd1"></script><a href="//privacy.truste.com/privacy-seal/SiteLock,-LLC/validation?rid=3fc6f9d5-9959-423e-a8a7-b516ce9bdef4" title="TRUSTe European Safe Harbor certification" target="_blank"><img style="border: none" src="//privacy-policy.truste.com/privacy-seal/SiteLock,-LLC/seal?rid=3fc6f9d5-9959-423e-a8a7-b516ce9bdef4" alt="TRUSTe European Safe Harbor certification"></a>
</div>
<div class="whiteSpace50"></div>


</div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>
