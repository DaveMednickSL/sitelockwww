<?php
//DEFINITIONS
$title = "Careers | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderWhite"><div class="careersHeader text-center" id="mainBody"><div id="content">
<div class="careersHeaderBox">
<h1 class="blue sourceBlack">Start Your<br>SiteLock Career</h1>
<a class="btn btn-red" href="#openpositions">Explore Open Jobs</a>
<div class="whiteSpace25"></div>
</div>
</div></div></div>

<div id="mainBody"><div id="content">
<div class="whiteSpace25"></div>
<p class="font18">
Award-winning culture? Yep. Career advancement? You betcha. Competitive compensation? Oh yeah. At SiteLock, we’re focused on the “Three Cs”—culture, career advancement, and competitive compensation. This is because we genuinely care about our employees, their futures, and their happiness. In fact, we’ve been recognized as a top company to work for in Arizona by numerous organizations. 
<br><br>
Although we’ve been around 10 years, truthfully—we’re just getting started. We’re always hiring highly motivated, talented, and energetic individuals to come aboard. Join us as we continue towards explosive growth, expanded partnerships, accelerated innovation, and creating a better and safer internet for all.
</p>

<p class="text-center"><a class="btn btn-red" href="#openpositions">Join The Team</a></p>

<div class="whiteSpace50"></div>

<div id="mainBody">
<div class="row">
<div class="col-md-6 my-auto text-center"><div id="mainRed"><div id="content"><h1 class="sourceBlack">Employees Love<br>SiteLock</h1></div></div></div>
<div class="col-md-6"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/313041808" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
</div>
</div>

<div class="whiteSpace50"></div>
</div></div>

<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace25"></div>
<div id="content">
<div class="row">
<div class="col text-center my-auto">
<img class="careerSiteImgAward" src="img/awards/awardpage/ace2017.png" alt="ACE 2017"><br>
<div class="whiteSpace25"></div>
</div>

<div class="col text-center my-auto">
<img class="careerSiteImgAward" src="img/awards/awardpage/coolest2018.png" alt="Coolest Culture 2018"><br>
<div class="whiteSpace25"></div>
</div>

<div class="col text-center my-auto">
<img class="careerSiteImgAward" src="img/awards/awardpage/mostAdmired2017.png" alt="Most Admired 2017"><br>
<div class="whiteSpace25"></div>
</div>

<div class="col text-center my-auto">
<img class="careerSiteImgAward" src="img/awards/awardpage/azcentralTop2018.png" alt="AZ Central 2018"><br>
<div class="whiteSpace25"></div>
</div>
</div>
</div></div>
</div>


<?php
$benefitsTitle = '<h1 class="text-center font55">Let’s talk  <span class="sourceBlack">benefits and perks</span></h1>'; 
$icon1= 'fa-dollar-sign'; $title1 = ''; $content1 = 'Competitive salary, 401k match, and a focus on career growth and professional development';
$icon2 = 'fa-clipboard-prescription'; $title2 = ''; $content2 = 'Medical, dental, vision, and life insurance, and maternity leave';
$icon3 = 'fa-thumbs-up'; $title3 = ''; $content3 = 'Casual dress code, upbeat environment, off-site family-friendly events';
$icon4 = 'fa-utensil-fork'; $title4 = ''; $content4 = 'Catered meals for special events, in-suite marketplace';
$icon5 = 'fa-plane'; $title5 = ''; $content5 = 'Paid time off and paid holidays';
$icon6 = 'fa-heartbeat'; $title6 = ''; $content6 = 'On-site gym, cafeteria, game room, covered parking, and more';

echo '';
?>

<div id="mainBody">
<div class="whiteSpace100"></div>
<?php  echo $benefitsTitle;?>

<div class="row font18">
<div class="col-lg-4 text-center">
<div id="benefitCareer"><div id="benefitBoxContent">
<div id="benefitCircleBlueAlt"><i class="fal <?php echo $icon1; ?> fa-3x"></i></div>
<h3 class="sourceBlack font25"><?php echo $title1; ?></h3>
<p><?php echo $content1; ?></p>
</div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitCareer"><div id="benefitBoxContent">
  <div id="benefitCircleRedAlt"><i class="fal <?php echo $icon2; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title2; ?></h3>
  <p><?php echo $content2; ?></p>
  </div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitCareer"><div id="benefitBoxContent">
  <div id="benefitCircleBlueAlt"><i class="fal <?php echo $icon3; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title3; ?></h3>
  <p><?php echo $content3; ?></p>
  </div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitCareer"><div id="benefitBoxContent">
  <div id="benefitCircleRedAlt"><i class="fal <?php echo $icon4; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title4; ?></h3>
  <p><?php echo $content4; ?></p>
  </div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitCareer"><div id="benefitBoxContent">
  <div id="benefitCircleBlueAlt"><i class="fal <?php echo $icon5; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title5; ?></h3>
  <p><?php echo $content5; ?></p>
  </div></div>
</div>

<div class="col-lg-4 text-center">
  <div id="benefitCareer"><div class="" id="benefitBoxContent">
  <div id="benefitCircleRedAlt"><i class="fal <?php echo $icon6; ?> fa-3x"></i></div>
  <h3 class="sourceBlack font25"><?php echo $title6; ?></h3>
  <p><?php echo $content6; ?></p>
  </div></div>
</div>

</div>
</div>

<!--<div id="mainBody"><div id="content">
<h1 class="text-center font55"><span class="sourceBlack">Describing SiteLock</span> in one word</h1>

<div class="whiteSpace25"></div>

<div class="row">
<div class="col-md-5 my-auto text-center"><div id="mainRed"><div id="content"><h1 class="sourceBlack">Dynamic</h1></div></div></div>
<div class="col-md-7"><iframe width="100%" height="315" src="https://www.youtube.com/embed/lgcaqc4rGmA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
</div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-md-7"><iframe width="100%" height="315" src="https://www.youtube.com/embed/lgcaqc4rGmA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
    <div class="col-md-5 my-auto text-center"><div id="mainBlue"><div id="content"><h1 class="sourceBlack">Diverse</h1></div></div></div>
</div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-md-5 my-auto text-center"><div id="mainRed"><div id="content"><h1 class="sourceBlack">Empowering</h1></div></div></div>
<div class="col-md-7"><iframe width="100%" height="315" src="https://www.youtube.com/embed/lgcaqc4rGmA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
</div>

<div id="openpositions" class="whiteSpace50"></div>
</div></div>-->

<div id="openpositions" class="whiteSpace50"></div>
<div id="mainGrey"><div id="mainTerms"><div class="font18" id="content">
<div class="whiteSpace100"></div>
<h2 class="text-center font55 sourceBlack blue">Open Positions</h2>
<p class="sourceBlack font25">ACCOUNTING</p>
<div class="row">
<div class="col-md-8 mobCenter">Accounting Coordinator<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Marketing Coordinator.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36787&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>
<div class="row">
<div class="col-md-8 mobCenter">Staff Accountant<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Staff Accountant Job Posting.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=37145&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>

<p class="sourceBlack font25">OPERATIONS</p>
<div class="row">
<div class="col-md-8 mobCenter">CRM Administrator<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/SugarCRM Administrator.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=37240&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>
<div class="row">
<div class="col-md-8 mobCenter">Customer Service Agent<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Customer Service Agent.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36783&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>
<div class="row">
<div class="col-md-8 mobCenter">Technical Support Agent<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Technical Support Agent.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36710&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>
<div class="row">
<div class="col-md-8 mobCenter">Tier 3 Support Team Lead<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/SECCON Team Lead.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36451&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>

<p class="sourceBlack font25">ENGINEERING</p>
<div class="row">
<div class="col-md-8 mobCenter">Senior PERL Developer<br>Jacksonville, FL</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Senior PERL Developer Job Description.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36546&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>

<p class="sourceBlack font25">SALES</p>
<div class="row">
<div class="col-md-8 mobCenter">Field Channel Development Executive<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Field Channel Development Executive.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36346&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>
<div class="row">
<div class="col-md-8 mobCenter"> Field Channel Sales Executive<br>Kyiv, Ukraine</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Field Channel Development Executive.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36343&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>
<div class="row">
<div class="col-md-8 mobCenter">Business Development Representative<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Business Development Representitive.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36516&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>
<div class="row">
<div class="col-md-8 mobCenter">Inside Sales Representative<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Inside Sales Representative.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36354&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>
<div class="row">
<div class="col-md-8 mobCenter">Inside Sales Consultant<br>Scottsdale, AZ</div>
<div class="col-md-2 text-center"><a class="btn btn-ghost-blue btn-sm" href="download/careers/Inside Sales Representative.pdf" target="_blank">LEARN MORE</a></div>
<div class="col-md-2 text-center"><a class="btn btn-blue btn-sm" href="https://www.paycomonline.net/v4/ats/web.php/jobs/ViewJobDetails?job=36351&clientkey=E6E85CA92339B0EF86074860F63DA3B0" target="_blank">APPLY NOW</a></div>
</div>
<br>
</div></div></div>


<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace100"></div>
<h2 class="text-center font55 sourceBlack blue">Employee Reviews</h2>
<div class="whiteSpace25"></div>
<div id="content">
<div class="row">
<div class="col-md-6 text-center">
<img class="careerSiteImg" src="img/careers/glassdoor.png" alt="Glassdoor Logo"><br>
<a class="btn btn-ghost-grey" href="https://www.glassdoor.com/Overview/Working-at-SiteLock-EI_IE658094.11,19.htm" target="_blank">See SiteLock on Glassdoor</a>
<div class="whiteSpace25"></div>
<hr class="footMobileShow">
</div>

<div class="col-md-6 text-center">
<img class="careerSiteImg" src="img/careers/indeed.png" alt="Indeed Logo"><br>
<a class="btn btn-ghost-grey" href="https://www.indeed.com/cmp/Sitelock/reviews" target="_blank">See SiteLock on Indeed</a>
<div class="whiteSpace25"></div>
</div>
</div>
</div></div>
<div class="whiteSpace100"></div>
</div>

<div class="whiteSpace50"></div>

<div id="mainBody"><div id="content">

<div class="theWayHeader text-center sourceBlack">
<br>
<h2 class="font55">The SiteLock Way</h2>
<br>
</div>

<div class="theWayBox wayBK1">
<div class="whiteSpace25"></div>
<div id="content">
<div class="row">
<div class="col-md-3"><img class="img70 wayIMG" src="img/careers/theWay1.svg" alt="The Way"></div>
<div class="col-md-9">
<h3 class="font45">Put customers first</h3>
<p class="font25">Everything we do is for our customers. We take every opportunity to go above and beyond, because protecting their websites is our number one priority.</p>
</div>
</div>
</div>
<div class="whiteSpace25"></div>
</div>

<div class="theWayBox wayBK2">
<div class="whiteSpace25"></div>
<div id="content">
<div class="row">
<div class="col-md-3"><img class="img70 wayIMG" src="img/careers/theWay2.svg" alt="The Way"></div>
<div class="col-md-9">
<h3 class="font45">Lead and embrace change</h3>
<p class="font25">We shape the future of web security and know that it takes a strong desire to win, diverse ideas, and swif action to be the best.</p>
</div>
</div>
</div>
<div class="whiteSpace25"></div>
</div>

<div class="theWayBox wayBK3">
<div class="whiteSpace25"></div>
<div id="content">
<div class="row">
<div class="col-md-3"><img class="img70 wayIMG" src="img/careers/theWay3.svg" alt="The Way"></div>
<div class="col-md-9">
<h3 class="font45">Love it</h3>
<p class="font25">We love what we do and genuinely care about the well-being of each other, our customers, and the communities where we live and work.</p>
</div>
</div>
</div>
<div class="whiteSpace25"></div>
</div>

<div class="theWayBox wayBK4">
<div class="whiteSpace25"></div>
<div id="content">
<div class="row">
<div class="col-md-3"><img class="img70 wayIMG" src="img/careers/theWay4.svg" alt="The Way"></div>
<div class="col-md-9">
<h3 class="font45">Win Together</h3>
<p class="font25">We hire exceptional people who know that working as a team always delivers the best results.</p>
</div>
</div>
</div>
<div class="whiteSpace25"></div>
</div>

<div class="theWayFooter wayBK5">
<div class="whiteSpace25"></div>
<div id="content">
<div class="row">
<div class="col-md-3"><img class="img70 wayIMG" src="img/careers/theWay5.svg" alt="The Way"></div>
<div class="col-md-9">
<h3 class="font45">Take responsibility</h3>
<p class="font25">We know that trust, respect, and open communication are foundational to our interactions with each other and our customers. We exercise good judgment and take accountability for our actions and the impact of our decisions.</p>
</div>
</div>
</div>
<div class="whiteSpace25"></div>
</div>

</div></div>

<div class="whiteSpace50"></div>

<div id="mainDarkerGrey"><div id="mainBody"><div class="text-center colorWhite" id="content">
<div class="whiteSpace50"></div>
<h2 class="font45 sourceBlack">Connect with SiteLock</h2>
<div class="whiteSpace25"></div>
<div class="row text-center linkWhite">
<div class="col"><a href="https://www.facebook.com/SiteLock/"><i class="fab fa-facebook-square fa-5x"></i></a></div>
<div class="col"><a href="https://twitter.com/SiteLock"><i class="fab fa-twitter-square fa-5x"></i></a></div>
<div class="col"><a href="https://www.linkedin.com/company/sitelock/"><i class="fab fa-linkedin fa-5x"></i></a></div>
<div class="col"><a href="https://www.instagram.com/sitelock/"><i class="fab fa-instagram fa-5x"></i></a></div>
</div>
<div class="whiteSpace50"></div>
</div></div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>