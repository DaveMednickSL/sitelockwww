<?php
//DEFINITIONS
$title = "WordPress | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div id="mainHeaderBlue"><div id="mainBody">
<div class="whiteSpace50 tabshow"></div>
<div class="row">
<div class="col-lg-6 my-auto solutionHead">
<h1>WordPress Security <span class="sourceBlack">Designed for You</span></h1>
<a class="btn btn-red" href="#onpagepricing">Protect Your Website Today</a>
</div>
<div class="col-lg-6">
<img class="headerimgmain" src="/img/wordpress/wordpress-header.png" alt="WordPress Security">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<?php include 'includes/rating-bar.php';?>

<div id="mainBody">

<div class="whiteSpace50"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2><span class="sourceRegular">WordPress security</span> <span class="sourceBlack">made easy</span></h2>Welcome to your dedicated place for WordPress security. Protecting your website has never been easier with solutions that automatically find and fix threats for you. Let us help you choose your perfect plan, so you can focus on what’s most important—your business, your passion, your word.<br><a class="btn btn-red font-weight-bold" href="#onpagepricing">Pick A WordPress Security Plan</a></div></div>
   <div class="col-lg-5 footMobileHide"><img class="splitImg" src="/img/wordpress/wpDatabase.svg" alt="WordPress Database"></div>
  </div>
</div>

<div class="whiteSpace100"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-md-5 splitPadding"><img class="splitImg" src="/img/wordpress/wpPlugins.svg" alt="Scanning Alert"></div>
  <div class="col-md-7 whitebk splitPadding"><div id="content"><h2><span class="sourceRegular">Reduce your </span> <span class="sourceBlack">WordPress risk</span></h2>We know why you love WordPress, because we love it too. This popular website builder allows you to create, customize, and manage your website without any developer experience. However, with customization and popularity comes risk. In fact, it’s the factors that make your website unique, like your plugins, that put your site at an increased risk. Don’t worry, we have products designed specifically for WordPress—so you don’t have to sacrifice your great user experience, or budget, for security.<br><a class="btn btn-red font-weight-bold" href="#onpagepricing">Reduce Your Risk</a></div></div>
 </div>
</div>
<div class="whiteSpace50"></div>

</div>

<?php 
$benefitsTitle = '<h1 class="text-center font55">Website security <span class="sourceBlack">benefits</span></h1>'; 
$icon1= 'fa-umbrella'; $title1 = 'Worry-free'; $content1 = 'Don’t worry about missing core security updates. Choose a website scanner that automatically patches vulnerabilities, so you can run updates on your own time.';
$icon2 = 'fa-rocket'; $title2 = 'Fast protection'; $content2 = 'Find, fix, and prevent cyberthreats as soon as they hit. Your automated security plan will block or fix threats before they can cause damage.';
$icon3 = 'fa-crown'; $title3 = 'Protect your content'; $content3 = 'Don’t let anyone vandalize your website. Your website scanner prevents website defacements, and your web application firewall keeps spam off your content.';
$icon4 = 'fa-chart-line'; $title4 = 'Increase visitor trust'; $content4 = 'Show your visitors they can trust you with a security badge on your site. Today, 79 percent of online shoppers expect to see a trust badge. Your website scanner comes with a SiteLock Trust Seal that tells your visitors your site is safe.';
$icon5 = 'fa-heart'; $title5 = 'Protect SEO rankings'; $content5 = 'Keep your SEO rankings strong with the help of website security. If search engines find malware on your site, it will be blacklisted. Your website scanner will find and automatically remove website malware before search engines blacklist your site.';
$icon6 = 'fa-magic'; $title6 = 'Easy and accessible'; $content6 = 'Stay updated about the security of your site with automated alerts and by visiting your SiteLock Dashboard. And, with the free SiteLock WordPress security plugin, you can manage your security without ever having to leave WordPress.';
$benefitsBTN = 'Protect Your WordPress Today';

include('includes/benefits-bar.php');
?>

<div id="onpagepricing"></div>
<div id="mainBody">
<div class="whiteSpace100"></div>
<h1 class="sourceLight text-center font55">Don’t wait, protect your website now.<br><span class="sourceBlack">Buy your website security plan today!</span></h1>
<?php include('includes/price-bar-1.php');?>

<?php include('includes/includes-bar.php');?>

</div>

<?php include('includes/solutions-bar.php');?>

<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace100"></div>
<h2 class="sourceLight font45 text-center"><span class="sourceBlack">Secure your word</span><br>WordPress security products just for you</h2>

<div class="row">
<div class="col">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningCloud.svg" alt="Scanning Cloud">
  <h5 class="sourceBlack text-center">SMART PLUS&trade;</h5>
  <p class="text-center">Automated malware removal</p>
  <ul>
  <li>Daily scanning</li>
  <li>Automatic malware removal</li>
  <li>Remove malware in WordPress databases</li>
  <li>Automatically patch CMS vulnerabilities</li>
  <li>Receive immediate threat alerts</li>
  </ul>
  <p class="text-center"><a class="btn btn-ghost-grey" href="malware-removal">Learn More</a></p></div></div>
</div>

<div class="col">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningThreat.svg" alt="Scanning Threat">
  <h5 class="sourceBlack text-center">SiteLock INFINITY&trade;</h5>
  <p class="text-center">Automated vulnerability patching</p>
  
  <ul>
  <li>Automatically remove malware</li>
  <li>Remove malware in WordPress databases</li>
  <li>Automatically patch CMS vulnerabilities</li>
  <li>24/7 access to SiteLock engineers</li>
  </ul>
  <p class="text-center"><a class="btn btn-ghost-grey" href="vulnerability-patching">Learn More</a></p></div></div>
</div>

<div class="col">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningWAF.svg" alt="Scanning WAF">
  <h5 class="sourceBlack text-center">SiteLock TrueShield&trade;</h5>
  <p class="text-center">Web application firewall</p>
  <ul>
  <li>Block bad bots</li>
  <li>Block comment spam</li>
  <li>Block direct hack attempts</li>
  <li>Increase website speed</li>
  <li>Receive traffic statistics<br>and reports</li>
  <li>Supports SSL enabled sites</li>
  </ul>
  <br><p class="text-center"><a class="btn btn-ghost-grey" href="web-application-firewall">Learn More</a></p></div></div>
</div>

<div class="col">
  <div id="darkBorderBox"><div id="darkBorderBoxContent"><img class="imgHeight130" src="/img/scanningWebsite.svg" alt="Scanning Website">
  <h5 class="sourceBlack text-center">PCI Compliance</h5>
  <p class="text-center">Ecommerce protection</p>
  <ul>
  <li>Protects payment card data</li>
  <li>Simple self-assessment questionnaire</li>
  <li>Quarterly vulnerability<br>scans included</li>
  <li>Custom fix-it plans</li>
  </ul>
  <br><br><p class="text-center"><a class="btn btn-ghost-grey" href="website-scanning">Learn More</a></div></p></div>
</div>
</div>

</div>
<div class="whiteSpace50"></div>
</div>

<?php include('includes/kidsfund-bar.php');?>

<div id="mainGrey"><div id="mainBody">
<div class="whiteSpace25"></div>
<div class="row">
<div class="col-md-6"><img class="img70" src="img/wordpress/wpEvents.svg" alt="WordPress Events"></div>
<div class="col-md-6"><h2 class="sourceRegular font45">Find out how<br>to get involved in the<br><span class="sourceBlack">WordPress community</span></h2><p>SiteLock is proud to be a part of the WordPress community. Visit our community hub to see our travel schedule, our past talks and presentations, and how you can get involved too!</p><a class="btn btn-ghost-blue" href="community">Check Out Our Community Hub</a></div>
</div>
<div class="whiteSpace25"></div>
</div></div>

<div id="mainBody">
<div class="whiteSpace50"></div>

  <div id="mainSplitLeft">
   <div class="row splitSpace">
    <div class="col-lg-6 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2><span class="sourceBlack">Get your complimentary website risk assessment</h2>The factors that make your website unique and engaging could also be putting it at risk of compromise. That’s why SiteLock created a proprietary Risk Assessment to offer education on how likely your website is to be compromised.<br><a class="btn btn-blue font-weight-bold" href="contact">Get My Free Risk Assessment</a></div></div>
    <div class="col-lg-6 splitPadding iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/273777919" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
   </div>
  </div>

  <div class="whiteSpace100"></div>

  <div id="mainSplitRight">
    <div class="row splitSpace">
     <div class="col-md-5 whitebk splitPadding"><img class="splitImgAlt" src="/img/wordpress/amandNaor.png" alt="Bill Kasal"></div>
     <div class="col-md-7 splitPadding"><div id="content" style="max-width: 70% !important; margin: 0 auto 0 auto !important;"><h2 class="sourceBlack">Meet this security superhero: </h2>Amanda of amandanaorphotography.com is known for her documentary-style photos of children, families, and “the beauty of the everyday.” Her website ran smoothly for years until it was attacked by cybercriminals—twice in a row. As a result, her site appeared distorted and her photos failed to load. Amanda called SiteLock and saved her website within a few hours.<p><p><i>"To see my site so deformed was heartbreaking. I never thought this could happen to me. Ignorance is bliss up until the point where you see all of your hard work in the hands of someone else. If you have a website, you need to have website security because anyone is susceptible!"</i></p><a class="btn btn-ghost-blue font-weight-bold" href="https://www.sitelock.com/blog/2018/05/case-study-amanda-naor-photography/">Read More</a></div></div>
    </div>
  </div>

<div class="whiteSpace50"></div>
</div>

<div id="mainBlue"><div class="text-center" id="mainBody">
<div class="whiteSpace25"></div>
<h2 class="font55">
<span class="sourceLight">Secure your Business</span><br>
<span class="sourceRegular">Secure your Passion</span><br>
<span class="sourceBlack">Secure your word</span>
</h2>
<a class="btn btn-red" href="contact">Contact SiteLock Today</a>
<div class="whiteSpace25"></div>
</div></div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>

</body>
</html>
