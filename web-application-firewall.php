<?php
//DEFINITIONS
$title = "Web Application Firewall | SiteLock";
$description = "SiteLock provides comprehensive, cloud-based website security solutions for businesses of all sizes.";
$keywords = "website security, website protection, web security, website scanner, malware scan, vulnerability scanning, CDN, WAF, DDoS Protection, SiteLock";
?>
<!DOCTYPE html5>
<html lang="en">
<?php include 'includes/assets/session.php';?>
<body>
<?php include 'includes/assets/ALPHA.php';?>
<?php include 'includes/page_ends/menu.php';?>

<div class="headerBottomGrey" id="mainHeaderBlue"><div id="mainBody">
<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-6">
<h1 class="headTxtDrop">Your Powerful and Secure <span class="sourceBlack">Web Application Firewall</span></h1>
<h3>Be the Hero Your Website Needs</h3>
<a class="btn btn-red" href="#onpagepricing">Prevent Website Attacks Now</a>
</div>
<div class="col-lg-6">
<img class="headerimg headImgDrop" src="/img/LaptopSiteLockTrueshield.png" alt="Laptop SiteLock Trueshield">
</div>
</div>
</div>
<div class="whiteSpace50 tabshow"></div>
</div>

<div id="mainBody">

<div class="whiteSpace50"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2><span class="sourceRegular">What is a</span> <span class="sourceBlack">web application firewall (WAF)?</span></h2>A web application firewall (WAF) protects your website from cyberthreats and harmful traffic, like cybercriminals and bad bots. It’s like having a force field around your site—it only lets good visitors in and keeps malicious ones out. Beyond protecting your website, your web application firewall comes with a content delivery network (CDN) proven to accelerate your website speed by as much as 50 percent.<ul><li>Websites are attacked approximately 50 times per day, per website.</li></ul><a class="btn btn-red" href="#onpagepricing">Start Blocking Cyberattacks</a></div></div>
   <div class="col-lg-5 footMobileHide"><img class="splitImg" src="/img/waf/wafCloud.svg" alt="WAF Cloud"></div>
  </div>
</div>

<div class="whiteSpace100"></div>

<div id="mainSplitLeft">
 <div class="row splitSpace">
  <div class="col-lg-5 splitPadding"><div id="content" style="width: 90%; margin-left: auto; margin-right: auto;"><h2>Block <span class="sourceBlack">cyberthreats</span></h2>Learn how a WAF can protect your website and increase your website speed.</div></div>
  <div class="col-lg-7 iconFirst"> <div style="padding: 56.25% 0 0 0 !important; position: relative !important;"><iframe src="https://player.vimeo.com/video/189012727" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div> </div>
 </div>
</div>
<div class="whiteSpace50"></div>
</div>

<?php include 'includes/trusted-bar.php';?>

<div id="mainTerms">
<div class="whiteSpace100"></div>
<h2 class="text-center font55">SiteLock WAF products available to you</h2>
<h3 class="text-center font25">Start protecting your website today. Choose the best WAF for your<br>website, business, and budget.</h3>

<div class="whiteSpace50"></div>
<div class="row">
<div class="col-lg-7 my-auto"><div id="content90">
<p>
<h2 id="malware-scan">TrueShield <span class="sourceBlack">Basic</span></h2>
<strong>TrueShield Basic firewall protects your site from malicious bot traffic while reducing load time by up to 50 percent for static website content.</strong>
<ul>
<li>Blocks bad bots</li>
<li>Includes traffic statistics and reports</li>
<li>Includes a basic content delivery network to increase website speed</li>
<li>Static content caching</li>
<li>Does <strong>NOT</strong> support SSL enabled sites</li>
</ul>
</p>
</div></div>
<div class="col-lg-5 iconFirst"><img class="splitImg" src="img/waf/wafSheildBot.svg"></div>
</div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-md-5"><img class="splitImg" src="img/waf/wafSheildSSL.svg"></div>
<div class="col-md-7 my-auto">
<h2 id="spam-scan">TrueShield <span class="sourceBlack">Professional</span></h2>
<p>
<strong>TrueShield Professional brings dynamic caching and threat protection to your website, stopping attacks before they begin.</strong>
  <ul>
  <li>Blocks bad bots</li>
  <li>Includes traffic statistics and reports</li>
  <li>Includes a professional content delivery network to increase website speed</li>
  <li>Advanced caching for dynamic and database driven websites</li>
  <li>Supports SSL enabled sites</li>
  </ul>
  </p>
</div>
</div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-lg-7 my-auto"><div id="content90">
<h2 id="network-scan">TrueShield <span class="sourceBlack">Premium</span></h2>
<p>
<strong>TrueShield Premium offers advanced threat protection while increasing site speed.</strong>
  <ul>
  <li>Blocks bad bots</li>
  <li>Includes traffic statistics and reports</li>
  <li>Includes a premium content delivery network to increase website speed</li>
  <li>Advanced static and dynamic content caching</li>
  <li>Sophisticated threat protection from the OWASP Top 10 cyberthreats</li>
  <li>Supports SSL enabled sites</li>
  </ul>
</p>
</div></div>
<div class="col-lg-5 iconFirst"><img class="splitImg" src="img/waf/wafSheildBug.svg"></div>
</div>

<div class="whiteSpace100"></div>

<div class="row">
<div class="col-md-5 my-auto"><img class="splitImg" src="img/waf/wafSheildEarth.svg"></div>
<div class="col-md-7 linkred">
<h2 id="app-scan">TrueShield <span class="sourceBlack">Enterprise</span></h2>
<p>
<strong>Ideal protection for businesses of all levels with SSL support, customizable traffic blocking, and sophisticated threat protection.</strong>
  <ul>
  <li>Blocks bad bots</li>
  <li>Includes traffic statistics and reports and manual investigation into events detected by the WAF</li>
  <li>Includes enterprise-level content delivery network to increase website speed</li>
  <li>Advanced, worldwide caching of dynamic and static content</li>
  <li>Sophisticated threat protection from the OWASP Top 10 cyberthreats</li>
  <li>Supports SSL enabled sites</li>
  </ul>
  </p>
</div>
</div>

<div id="onpagepricing"></div>
</div>
<div id="mainBody">
<div class="whiteSpace100"></div>
<h1 class="sourceLight text-center font55">DDoS <br><span class="sourceBlack">Attack Protection</span></h1>
<?php include('includes/price-bar-1.php');?>

<?php include('includes/includes-bar.php');?>
</div>

<?php include('includes/solutions-bar.php');?>

<?php 
$benefitsTitle = '<h1 class="text-center font55">Web application firewall <span class="sourceBlack">benefits</span></h1>'; 
$icon1= 'fa-umbrella'; $title1 = 'Peace of mind'; $content1 = 'Be confident knowing your website and visitors are safe from cybercriminals at all times. Your WAF blocks sophisticated attacks with 99.99 percent accuracy.';
$icon2 = 'fa-gem'; $title2 = 'Brand integrity'; $content2 = 'Uphold your brand reputation by protecting your website from cyberattacks. Over 24 percent of website owners experience damage to their reputation as a result of a security incident.';
$icon3 = 'fa-chart-line'; $title3 = 'SEO success'; $content3 = 'Protect your SEO efforts with a WAF to prevent malware infections. If search engines find malware on your site, it could be blacklisted and temporarily removed from search results.';
$icon4 = 'fa-rocket'; $title4 = 'Increase website speed'; $content4 = 'Give your visitors the best experience possible. Websites using the SiteLock CDN are up to 50 percent faster than sites that don\'t.';
$icon5 = 'fa-user-graduate'; $title5 = 'Stay informed'; $content5 = 'You’ll always have visibility to your website security with weekly emails highlighting your WAF results. You can also view your results anytime from your SiteLock Dashboard.';
$icon6 = 'fa-magic'; $title6 = 'Quick and easy set-up'; $content6 = 'Protect your website within minutes of calling SiteLock. It only takes five minutes to set up your WAF. Plus, the SiteLock team is available 24/7/365 to get you started.';
$benefitsBTN = 'Choose Your Web Application Firewall';

include('includes/benefits-bar.php');
?>
<?php include('includes/cms-bar.php');?>

<div id="mainBody">

<div class="whiteSpace100"></div>

<div id="mainSplitRight">
  <div class="row splitSpace">
   <div class="col-lg-7 whitebk splitPadding"><div id="content"><h2><span class="sourceRegular">What is a </span> <span class="sourceBlack">bad bot?</span></h2>
   Bots run automated tasks on websites across the internet. In fact, <strong>bots represent over 50 percent of all website traffic</strong>. There are good bots and bad bots— good bots visit your site to perform tasks like search engine crawling, while bad bots are designed to cause harm, such as comment spam and targeted DDoS attacks. A WAF blocks bad bots while allowing legitimate traffic to pass through.
   <ul>
   <li>The average website receives 2,400 bot visits each week.</li>
   <li class="red">Bad bots represent over 35 percent of all bot traffic.</li>
   </ul>
   </div></div>
   <div class="col-lg-5 splitPadding my-auto iconFirst"><img class="splitImg" src="/img/siteMalware.svg" alt="Scanning Calendar"></div>
  </div>
</div>

<div class="whiteSpace100"></div>

</div>

<?php include 'includes/qsr-bar.php';?>

<div id="mainBody">
<div class="whiteSpace50"></div>
<?php include('includes/price-bar-1.php');?>
</div>

<?php include 'includes/page_ends/footer.php';?>
<?php include 'includes/assets/OMEGA.php';?>


</body>
</html>